<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
CJSCore::Init(array("jquery"));
$_SESSION["rear_id"] = "";

function cutString($string, $maxlen) {

    $len = (strlen($string) > $maxlen)
        ? strripos(substr($string, 0, $maxlen), ' ')
        : $maxlen
    ;
//    echo $len;
    $cutStr = substr($string, 0, $len);
    return (strlen($string) > $maxlen)
        ? $cutStr . '...'
        : $cutStr . '...'
        ;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html prefix="og: http://ogp.me/ns#">
<head>
    <meta name="google-site-verification" content="bzVYlnFNC_9fAiG4CxyY_P0RBSQSECBddw75IDytfvM" />
    <meta name="google-site-verification" content="gIZK15akVSYiZUwEzso3GoZOj6NNYnK8cL23yHrnptI" />
    <meta name="yandex-verification" content="448de8a91311de3d" />
    <meta name="yandex-verification" content="cc9f826367ba2d7d" />
    <meta name="verify-admitad" content="ec29c5d1b2" />
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:700&subset=cyrillic,cyrillic-ext,latin' rel='stylesheet' type='text/css'>
    <link href="<?=SITE_TEMPLATE_PATH?>/js/liScrollToTop.css" rel="stylesheet" type="text/css"/>
    <link href="<?=SITE_TEMPLATE_PATH?>/js/jcarousel.responsive.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/slick/slick-theme.css"/>
    <meta http-equiv="Content-Type" content="text/html; charset=<?=LANG_CHARSET;?>" />
    <?$APPLICATION->ShowCSS();?>
    <?$APPLICATION->ShowHeadStrings()?>
    <?$APPLICATION->ShowHeadScripts()?>
    <?$APPLICATION->ShowMeta("description")?>
    <?=showCanonicalLink()?>
    <title><?$APPLICATION->ShowTitle()?></title>
    <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/main.js"></script>
    <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/jquery.liFixar.js"></script>
    <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/jquery.liScrollToTop.js"></script>
    <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/jquery.jcarousel.min.js"></script>
    <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/jcarousel.responsive.js"></script>
    <script type="text/javascript" src="/js/jquery-1.11.0.min.js" test="1"></script>
    <script type="text/javascript" src="/js/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/slick/slick.min.js"></script>
    <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/jquery.hc-sticky.min.js"></script>
    <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/jquery.scrolldepth.min.js"></script>
    <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/riveted.min.js"></script>

    <script async src="https://cdn.onthe.io/io.js/p1LUI2jVIMk1"></script>
    <script type="text/javascript" async src="https://relap.io/api/v6/head.js?token=qp7n1S-rEu8SYWRh"></script>
    <script>
        function check_plagin()
        {
            confirm('Вы пользуетесь браузером Chrome, для того чтобы подписаться на RSS рассылку у Вас должен быть установлен специальный плагин. Например RSS Subscription Extension. Если подобный плагин у Вас установлен, просто нажмите "ОК", в противном случае, кликнете по "Отменить" - Вы перейдете на страницу установки плагина.');
            /*if(confirm('Вы пользуетесь браузером Chrome, для того чтобы подписаться на RSS рассылку у Вас должен быть установлен специальный плагин. Например RSS Subscription Extension. Если подобный плагин у Вас установлен, просто нажмите "ОК", в противном случае, кликнете по "Отменить" - Вы перейдете на страницу установки плагина.'))
            {
                return true;
            }
            else
            {
                window.open('https://chrome.google.com/webstore/detail/rss-subscription-extensio/nlbjncdgjeocebhnmkbbbdekmmmcbfjd/related');
                return false;
            }*/
            return false;
        }

        function add_favorite(a)
        {
            title=document.title;
            url=document.location;
            try {
                // Internet Explorer
                window.external.AddFavorite(url, title);
            }
            catch (e)
            {
                try {
                    // Mozilla
                    window.sidebar.addPanel(title, url, "");
                }
                catch (e)
                {
                    // Opera
                    if (typeof(opera)=="object")
                    {
                        a.rel="sidebar";
                        a.title=title;
                        a.url=url;
                        return true;
                    }
                    else
                    {
                        // Unknown
                        alert('Нажмите Ctrl-D чтобы добавить страницу в закладки');
                    }
                }
            }
            return false;
        }

    </script>
    <script type="text/javascript">
        jQuery(document).ready(function(){
            /* функция кроссбраузерного определения отступа от верха документа к текущей позиции скроллера прокрутки */
            function getScrollTop() {
                var scrOfY = 0;
                if( typeof( window.pageYOffset ) == "number" ) {
                    //Netscape compliant
                    scrOfY = window.pageYOffset;
                } else if( document.body
                    && ( document.body.scrollLeft
                        || document.body.scrollTop ) ) {
                    //DOM compliant
                    scrOfY = document.body.scrollTop;
                } else if( document.documentElement
                    && ( document.documentElement.scrollLeft
                        || document.documentElement.scrollTop ) ) {
                    //IE6 Strict
                    scrOfY = document.documentElement.scrollTop;
                }
                return scrOfY;
            }
            /* пересчитываем отступ при прокрутке экрана */
            jQuery(window).scroll(function() {
                fixPaneRefresh();
            });

            function fixPaneRefresh(){
                if (jQuery("#fixedBox").length) {
                    var top  = getScrollTop();
                    if (top < 2450) jQuery("#fixedBox").css("top",2450-top+"px"); /* 170 - это наш отступ */
                    else jQuery("#fixedBox").css("top","0px");
                }
            }

        });
    </script>
    <script type="text/javascript">
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-58726870-1', 'auto');
        ga('send', 'pageview');

    </script>
    <script>
        jQuery(function() {
            jQuery.scrollDepth();
        });
    </script>
    <script>riveted.init();</script>
    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
            n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
            document,'script','//connect.facebook.net/en_US/fbevents.js');

        fbq('init', '1708759799356872');
        fbq('track', "PageView");
        fbq('track', 'Search');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=1708759799356872&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Facebook Pixel Code -->

    <?$APPLICATION->ShowProperty("ogtitle",'<meta property="og:title" content="Розничная торговля: новости, бизнес, магазины, шоппинг - New Retail" />');?>
    <?$APPLICATION->ShowProperty("ogdescription",'<meta property="og:description" content="Новости и аналитические обзоры на New Retail: главные новости, бизнес, маркетинг, магазины, технологии, персоны, лайфстайл, шоппинг, мода, стиль. Ежедневные комментарии экспертов, интервью, репортажи." />');?>
    <?$APPLICATION->ShowProperty("ogurl",'<meta property="og:url" content="https://new-retail.ru'.$_SERVER['REQUEST_URI'].'" />');?>
    <?$APPLICATION->ShowProperty("ogimg",'<meta property="og:image" content="https://new-retail.ru/logo_new_share.png"/>');?>

    <script src="https://yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
    <script src="https://yastatic.net/share2/share.js" async="async"></script>

</head>
<body>
<!-- Rating@Mail.ru counter -->
<script type="text/javascript">
    var _tmr = _tmr || [];
    _tmr.push({id: "2697244", type: "pageView", start: (new Date()).getTime()});
    (function (d, w, id) {
        if (d.getElementById(id)) return;
        var ts = d.createElement("script"); ts.type = "text/javascript"; ts.async = true; ts.id = id;
        ts.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//top-fwz1.mail.ru/js/code.js";
        var f = function () {var s = d.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ts, s);};
        if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); }
    })(document, window, "topmailru-code");
</script><noscript><div style="position:absolute;left:-10000px;">
        <img src="//top-fwz1.mail.ru/counter?id=2697244;js=na" style="border:0;" height="1" width="1" alt="Рейтинг@Mail.ru" />
    </div></noscript>
<!-- //Rating@Mail.ru counter -->
<!--facebook-->
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/ru_RU/all.js#xfbml=1";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<!--facebook-->
<?if($USER->IsAdmin()) $APPLICATION->ShowPanel();?>
<script>
    $(function () {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 20)
                $('a#move_up').fadeIn(200);
            else
                $('a#move_up').fadeOut(200);
        });
        $('a#move_up').click(function () {
            $('body,html').animate({
                scrollTop: 0
            }, 520);
            return false;
        });
    });
</script>

<!-- bg-banner -->
<?$arrFilter = Array(
    "=SECTION_ID" => 224,
    "ACTIVE_DATE" => "Y"
);?>
<?$APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "banner",
    array(
        "IBLOCK_TYPE" => "news",
        "IBLOCK_ID" => "12",
        "NEWS_COUNT" => "10000",
        "SORT_BY1" => "RAND",
        "SORT_ORDER1" => "DESC",
        "SORT_BY2" => "ACTIVE_FROM",
        "SORT_ORDER2" => "DESC",
        "FILTER_NAME" => "arrFilter",
        "FIELD_CODE" => array(
            0 => "",
            1 => "",
        ),
        "PROPERTY_CODE" => array(
            0 => "URL",
            1 => "PAGE_BANNER",
            2 => "THROUGH_BANNER",
        ),
        "CHECK_DATES" => "N",
        "DETAIL_URL" => "",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "36000000",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "N",
        "PREVIEW_TRUNCATE_LEN" => "",
        "ACTIVE_DATE_FORMAT" => "d.m",
        "SET_TITLE" => "N",
        "SET_STATUS_404" => "N",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "ADD_SECTIONS_CHAIN" => "N",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "INCLUDE_SUBSECTIONS" => "Y",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "PAGER_TITLE" => "пїЅпїЅпїЅпїЅпїЅпїЅпїЅ",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => "",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "SET_BROWSER_TITLE" => "Y",
        "SET_META_KEYWORDS" => "Y",
        "SET_META_DESCRIPTION" => "Y"
    ),
    false
);?>
<div id="topcontainer">
    <!-- top-banner -->
    <?$arrFilter = Array(
        "=SECTION_ID" => 143,
        "ACTIVE_DATE" => "Y"
    );?>
    <?$APPLICATION->IncludeComponent(
        "bitrix:news.list",
        "banner",
        array(
            "IBLOCK_TYPE" => "news",
            "IBLOCK_ID" => "12",
            "NEWS_COUNT" => "1",
            "SORT_BY1" => "RAND",
            "SORT_ORDER1" => "DESC",
            "SORT_BY2" => "ACTIVE_FROM",
            "SORT_ORDER2" => "DESC",
            "FILTER_NAME" => "arrFilter",
            "FIELD_CODE" => array(
                0 => "",
                1 => "",
            ),
            "PROPERTY_CODE" => array(
                0 => "URL",
                1 => "",
            ),
            "CHECK_DATES" => "N",
            "DETAIL_URL" => "",
            "AJAX_MODE" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "AJAX_OPTION_HISTORY" => "N",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "36000000",
            "CACHE_FILTER" => "N",
            "CACHE_GROUPS" => "N",
            "PREVIEW_TRUNCATE_LEN" => "",
            "ACTIVE_DATE_FORMAT" => "d.m",
            "SET_TITLE" => "N",
            "SET_STATUS_404" => "N",
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            "ADD_SECTIONS_CHAIN" => "N",
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
            "PARENT_SECTION" => "",
            "PARENT_SECTION_CODE" => "",
            "INCLUDE_SUBSECTIONS" => "Y",
            "DISPLAY_TOP_PAGER" => "N",
            "DISPLAY_BOTTOM_PAGER" => "N",
            "PAGER_TITLE" => "",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_TEMPLATE" => "",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N",
            "AJAX_OPTION_ADDITIONAL" => "",
            "SET_BROWSER_TITLE" => "Y",
            "SET_META_KEYWORDS" => "Y",
            "SET_META_DESCRIPTION" => "Y"
        ),
        false
    );?>
</div>
<div id="wrapper">
    <div id="top">
        <div class="top-filling">
            <!--noindex-->
            <div class="icons">
                <a href="https://twitter.com/NewRetail_ru" class="tw" target="_blank" rel="nofollow"></a>
                <a href="http://vk.com/newretail" class="vk" target="_blank" rel="nofollow"></a>
                <a href="https://www.facebook.com/pages/New-Retail/362233667239742" class="fb" target="_blank" rel="nofollow"></a>
                <a href="http://www.linkedin.com/groups/New-Retailru-6573881?trk=my_groups-b-grp-v" class="in" target="_blank" rel="nofollow"></a>

                <a href="/rss/index_all.php" <?if(strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome')){?>onclick="return check_plagin();"<?}?> class="rss"></a>
            </div>
            <!--/noindex-->
            <?/*?>
            <div class="subscribe">
                <a style="cursor: pointer;" class="e-mail">пїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅ пїЅпїЅ пїЅпїЅпїЅпїЅпїЅпїЅпїЅ
	                <div id="UlTest">
	                	<li></li>
	                	<li></li>
	                </div>
                </a>
				<script>
					$(".subscribe .e-mail").click(function(){
						$(".ModalWindow.subscr .field").show(1);
						$(".ModalWindow.subscr .answer").remove();
						$(".ModalWindow.subscr").fadeIn();
						$("body").prepend('<div id="Black"></div>');
					})
				</script>
            </div>
         <?*/?>
            <div class="bookmark">
                <a onclick="add_favorite();">Добавьте в закладки</a>
            </div><!--bookmark-->
            <?/*?>
            <div class="subscribe">
                <a style="cursor: pointer;" class="e-mail">Подпишитесь на новости
	                <div id="UlTest">
	                	<li></li>
	                	<li></li>
	                </div>
                </a>
				<script>
					$(".subscribe .e-mail").click(function(){
						$(".ModalWindow.subscr .field").show(1);
						$(".ModalWindow.subscr .answer").remove();
						$(".ModalWindow.subscr").fadeIn();
						$("body").prepend('<div id="Black"></div>');
					})
				</script>
            </div>
         <?*/?>
            <div class="bookmark">
                <a onclick="add_favorite();">Добавьте в закладки</a>
            </div><!--bookmark-->

            <!--<script type="text/javascript">
                $(function () {
                    $(window).scroll(function () {
                        if ($(this).scrollTop() > 10)   $('#logo_fixed').fadeIn(200);
                        else				$('#logo_fixed').fadeOut(200);
                    });
                });
            </script>
            <a href='/'><div id=logo_fixed></div></a>-->

            <div class="rightblock">
                <!--<div class="censore" style="border: none;">
                    <div>16+</div>
                </div>-->

                <div class="menu">

                    <?/*<a href="/arhiv">пїЅпїЅпїЅпїЅпїЅ</a>*/?>
                    <!--<a style="cursor:pointer;" class="last"><?
                    global $USER;
                    if ($USER->GetFullName()) {
                        echo $USER->GetFullName();
                    } elseif ($USER->GetLogin()) {
                        echo $USER->GetLogin();
                    } else {
                        echo 'пїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅ';
                    }
                    ?></a>
					<script>
					$(".menu .last").click(function(){
						$(".ModalWindow.auth").fadeIn();
						$("body").prepend('<div id="Black"></div>');
					})
				</script>-->
                    <div class="search">
                        <!--<p>пїЅпїЅпїЅпїЅпїЅ:</p>-->
                        <?$APPLICATION->IncludeComponent(
                            "nr:search.form",
                            "",
                            Array(
                                "USE_SUGGEST" => "Y",
                                "PAGE" => "#SITE_DIR#search/index.php"
                            )
                        );?>
                    </div>
                    <p>18+</p>
                </div>
            </div><!--rightblock-->
            <!--<div class="letters">
                <div class="s" onclick="textSizeSmall();">
                    A
                </div>
                <div class="m" onclick="textSizeNormal();">
                    A
                </div>
                <div class="l" onclick="textSizeBig();">
                    A
                    <span class="ss">+</span>
                </div>
            </div>-->
        </div>
    </div>
    <?// new block //?>
    <div class="logo-block">
        <div style="float: left; width: 750px; margin-top: 0px;">
            <a href="/" class="logo"><img src="<?=SITE_TEMPLATE_PATH?>/img/logo.png" height="80px" width="137px"></a>
            <!--<a href="/" class="logo"><img src="<?=SITE_TEMPLATE_PATH?>/img/logo.png" height="60px" width="60px"></a>-->
            <!--<a href="/" class="logo-text">NEW RETAIL</a>-->

            <div class="Oil" style="margin-left: 60px;margin-top:35px;">
                <a href="/o_reklame/">Реклама на<br> New Retail</h4></a>
            </div><!--oil-->
            <style>

                .logo-block {
                    padding: 0;
                }

                #top {display: none;}

                #wrapper {margin-top: 0px;}
                .EditorChoise {width: 340px;}
                .EditorChoise .Photo {
                    /*margin:0 20px 0 0;*/
                    margin: 40px 20px 0 0;
                    line-height: 70px;
                    height: 70px;
                }
                .EditorChoise .Photo img {
                    width: 100%;
                    /*margin-top:40px;*/
                    vertical-align: middle;
                }
                .EditorChoise .Text a {font-size: 14px;margin-top:7px;}
            </style>

            <div class="EditorChoise" style="float:left;">

                <?php
                $res = CIBlock::GetList(
                    Array(),
                    Array(
                        "CODE"=>'SPEC_PROJECT'
                    ), true
                );
                if($ar_res = $res->Fetch())
                {
                    global $news_auth;
                    $news_auth = array("PROPERTY_IN_HEADER_VALUE"=>"Y");
                    $APPLICATION->IncludeComponent(
                        "bitrix:news.list",
                        "nr-home_author",
                        Array(
                            "IBLOCK_TYPE"	=>	"news",
                            "IBLOCK_ID"	=>	$ar_res['ID'],
                            "NEWS_COUNT"	=>	100,
                            "SORT_BY1"	=>	"ACTIVE_FROM",
                            "SORT_ORDER1"	=>	"DESC",
                            "SORT_BY2"	=>	"SORT",
                            "SORT_ORDER2"	=>	"ASC",
                            "FIELD_CODE"	=>	array(
                                0 => "",
                                1 => "",
                            ),
                            "PROPERTY_CODE"	=> array(
                                0 => "LINK",
                                1 => "IMAGE_HEADER",
                            ),
                            "DETAIL_URL"	=>	"",
                            "SECTION_URL"	=>	"",
                            "IBLOCK_URL"	=>	"",
                            "DISPLAY_PANEL"	=>	"",
                            "SET_TITLE"	=>	"Y",
                            "SET_STATUS_404" => "Y",
                            "INCLUDE_IBLOCK_INTO_CHAIN"	=>	"N",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "36000000",
                            "CACHE_FILTER" => "N",
                            "CACHE_GROUPS" => "N",
                            "DISPLAY_TOP_PAGER" => "N",
                            "DISPLAY_BOTTOM_PAGER" => "Y",
                            "PAGER_TITLE" => "пїЅпїЅпїЅпїЅпїЅпїЅпїЅ",
                            "PAGER_TEMPLATE" => "",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "Y",
                            "DISPLAY_DATE" => "Y",
                            "DISPLAY_PICTURE" => "Y",
                            "DISPLAY_NAME"	=>	"Y",
                            "DISPLAY_PREVIEW_TEXT"	=>	"Y",
                            "PREVIEW_TRUNCATE_LEN"	=>	"",
                            "ACTIVE_DATE_FORMAT"	=>	"d.m.Y",
                            "USE_PERMISSIONS"	=>	"N",
                            "GROUP_PERMISSIONS"	=>	"",
                            "FILTER_NAME"	=>	"news_auth",
                            "HIDE_LINK_WHEN_NO_DETAIL"	=>	"N",
                            "CHECK_DATES"	=>	"Y",
                        ),
                        false
                    );
                }
                ?>
            </div><!--EditorChoise-->

        </div>

        <div style="float: right;margin-right: -24px;"><?$APPLICATION->IncludeComponent("local:subscribe.panel", "retail_newsletter", array());?> </div>

    </div>
    <?/* old block ?>
    <div class="logo-block">

        <a href="/" class="logo">
            <img src="<?=SITE_TEMPLATE_PATH?>/img/logo.png" height="60px" width="60px">
        </a>
        <a href="/" class="logo-text">NEW RETAIL</a>

        <div class="Course">
			<?global $DB;
			$ress = $DB->Query("SELECT * FROM `CURRENCY` where `DATE_RATE` <= '".date("Y-m-d")."' ORDER BY ID DESC LIMIT 2", false, $err_mess.__LINE__);
			while($arcur = $ress->Fetch())
			{
				$CUR[$arcur["CUR"]] = round($arcur["RATE"], 2);
			}
			if(count($CUR))
			{?>

				<span><b>$</b> <?=$CUR["USD"]?> пїЅпїЅпїЅ.</span>
				<span><b>пїЅ</b> <?=$CUR["EUR"]?> пїЅпїЅпїЅ.</span>
			<?}?>
        </div><!--Course-->

        <div class="Oil">
	        <a href="http://new-retail.ru/o_reklame/">пїЅпїЅпїЅпїЅпїЅпїЅпїЅ пїЅпїЅ пїЅпїЅпїЅпїЅпїЅ пїЅпїЅпїЅпїЅпїЅ!</h4></a>
        </div><!--oil-->

        <div class="EditorChoise">
			<?global $news_auth;
			$news_auth = array("PROPERTY_REDACTOR_CHOOSE_Y_VALUE"=>"Y", "SECTION_ID"=>145);?>
			<?$APPLICATION->IncludeComponent(
				"bitrix:news.list",
				"nr-home_author",
				Array(
					"IBLOCK_TYPE"	=>	"news",
					"IBLOCK_ID"	=>	2,
					"NEWS_COUNT"	=>	1,
					"SORT_BY1"	=>	"ACTIVE_FROM",
					"SORT_ORDER1"	=>	"DESC",
					"SORT_BY2"	=>	"SORT",
					"SORT_ORDER2"	=>	"ASC",
					"FIELD_CODE"	=>	array(
						0 => "",
						1 => "",
					),
					"PROPERTY_CODE"	=> array(
						0 => "REDACTOR_CHOOSE",
						1 => "",
					),
					"DETAIL_URL"	=>	"",
					"SECTION_URL"	=>	"",
					"IBLOCK_URL"	=>	"",
					"DISPLAY_PANEL"	=>	"",
					"SET_TITLE"	=>	"Y",
					"SET_STATUS_404" => "Y",
					"INCLUDE_IBLOCK_INTO_CHAIN"	=>	"N",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "36000000",
					"CACHE_FILTER" => "N",
					"CACHE_GROUPS" => "N",
					"DISPLAY_TOP_PAGER" => "N",
					"DISPLAY_BOTTOM_PAGER" => "Y",
					"PAGER_TITLE" => "пїЅпїЅпїЅпїЅпїЅпїЅпїЅ",
					"PAGER_TEMPLATE" => "",
					"PAGER_DESC_NUMBERING" => "N",
					"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
					"PAGER_SHOW_ALL" => "Y",
					"DISPLAY_DATE" => "Y",
					"DISPLAY_PICTURE" => "Y",
					"DISPLAY_NAME"	=>	"Y",
					"DISPLAY_PREVIEW_TEXT"	=>	"Y",
					"PREVIEW_TRUNCATE_LEN"	=>	"",
					"ACTIVE_DATE_FORMAT"	=>	"d.m.Y",
					"USE_PERMISSIONS"	=>	"N",
					"GROUP_PERMISSIONS"	=>	"",
					"FILTER_NAME"	=>	"news_auth",
					"HIDE_LINK_WHEN_NO_DETAIL"	=>	"N",
					"CHECK_DATES"	=>	"Y",
				),
				false
			);?>
        </div><!--EditorChoise-->

    </div>
<?*/?>
    <div id="menu">
        <?$APPLICATION->IncludeComponent(
            "bitrix:menu",
            "horizontal_newretail2",
            array(
                "ROOT_MENU_TYPE" => "submenu",
                "MAX_LEVEL" => "3",
                "CHILD_MENU_TYPE" => "top",
                "USE_EXT" => "Y",
                "DELAY" => "N",
                "ALLOW_MULTI_SELECT" => "N",
                "MENU_CACHE_TYPE" => "A",
                "MENU_CACHE_TIME" => "360000",
                "MENU_CACHE_USE_GROUPS" => "Y",
                "MENU_CACHE_GET_VARS" => array(
                ),
                "MENU_THEME" => "site"
            ),
            false
        );?>
    </div>

    <?/*
    <div class="wrapper-main-menu">
    <div class="main-menu">
        <?$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"horizontal_multilevel",
	array(
		"ROOT_MENU_TYPE" => "top",
		"MAX_LEVEL" => "4",
		"CHILD_MENU_TYPE" => "submenu",
		"USE_EXT" => "N",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "N",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array(
		)
	),
	false
);?>
    </div>
    </div>
	*/?>

    <!--    <div style="display: none;">-->
    <!--        --><?//$APPLICATION->IncludeComponent(
    //        "nr:news",
    //        ".default-advanced",
    //        Array(
    //            "DISPLAY_DATE" => "Y",
    //            "DISPLAY_PICTURE" => "Y",
    //            "DISPLAY_PREVIEW_TEXT" => "Y",
    //            "USE_SHARE" => "N",
    //            "SEF_MODE" => "Y",
    //            "AJAX_MODE" => "N",
    //            "IBLOCK_TYPE" => "news",
    //            "IBLOCK_ID" => "2",
    //            "NEWS_COUNT" => "",
    //            "USE_SEARCH" => "N",
    //            "USE_RSS" => "Y",
    //            "USE_RATING" => "N",
    //            "USE_CATEGORIES" => "Y",
    //            "USE_REVIEW" => "Y",
    //            "USE_FILTER" => "N",
    //            "SORT_BY1" => "ACTIVE_FROM",
    //            "SORT_ORDER1" => "DESC",
    //            "SORT_BY2" => "SORT",
    //            "SORT_ORDER2" => "ASC",
    //            "CHECK_DATES" => "Y",
    //            "PREVIEW_TRUNCATE_LEN" => "",
    //            "LIST_ACTIVE_DATE_FORMAT" => "d.m.Y",
    //            "LIST_FIELD_CODE" => array(),
    //            "LIST_PROPERTY_CODE" => array(
    //                0 => "",
    //                1 => "NEW_ARTICLE",
    //            ),
    //            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
    //            "DISPLAY_NAME" => "N",
    //            "META_KEYWORDS" => "-",
    //            "META_DESCRIPTION" => "-",
    //            "BROWSER_TITLE" => "-",
    //            "DETAIL_ACTIVE_DATE_FORMAT" => "H:i d/m/Y",
    //            "DETAIL_FIELD_CODE" => array(),
    //            "DETAIL_PROPERTY_CODE" => array(),
    //            "DETAIL_DISPLAY_TOP_PAGER" => "N",
    //            "DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
    //            "DETAIL_PAGER_TITLE" => "пїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅ",
    //            "DETAIL_PAGER_TEMPLATE" => "",
    //            "DETAIL_PAGER_SHOW_ALL" => "Y",
    //            "SET_TITLE" => "Y",
    //            "SET_STATUS_404" => "N",
    //            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
    //            "ADD_SECTIONS_CHAIN" => "Y",
    //            "USE_PERMISSIONS" => "N",
    //            "CACHE_TYPE" => "A",
    //            "CACHE_TIME" => "36000000",
    //            "CACHE_FILTER" => "N",
    //            "CACHE_GROUPS" => "Y",
    //            "DISPLAY_TOP_PAGER" => "N",
    //            "DISPLAY_BOTTOM_PAGER" => "Y",
    //            "PAGER_TITLE" => "пїЅпїЅпїЅпїЅпїЅпїЅпїЅ",
    //            "PAGER_SHOW_ALWAYS" => "N",
    //            "PAGER_TEMPLATE" => "",
    //            "PAGER_DESC_NUMBERING" => "N",
    //            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
    //            "PAGER_SHOW_ALL" => "Y",
    //            "NUM_NEWS" => "",
    //            "NUM_DAYS" => "30",
    //            "YANDEX" => "N",
    //            "CATEGORY_IBLOCK" => array(),
    //            "CATEGORY_CODE" => "THEME",
    //            "CATEGORY_ITEMS_COUNT" => "",
    //            "MESSAGES_PER_PAGE" => "",
    //            "USE_CAPTCHA" => "Y",
    //            "REVIEW_AJAX_POST" => "N",
    //            "PATH_TO_SMILE" => "/bitrix/images/forum/smile/",
    //            "FORUM_ID" => "5",
    //            "URL_TEMPLATES_READ" => "",
    //            "SHOW_LINK_TO_FORUM" => "N",
    //            "POST_FIRST_MESSAGE" => "N",
    //            "SEF_FOLDER" => "/#SECTION_CODE#/",
    //            "AJAX_OPTION_JUMP" => "N",
    //            "AJAX_OPTION_STYLE" => "Y",
    //            "AJAX_OPTION_HISTORY" => "N",
    //            "SEF_URL_TEMPLATES" => Array(
    //                "news" => "",
    //                "detail" => "#ELEMENT_CODE#/",
    //                "rss" => "rss/",
    //                "rss_section" => "#SECTION_ID#/rss/"
    //            ),
    //            "VARIABLE_ALIASES" => Array(
    //                "news" => Array(),
    //                "detail" => Array(),
    //                "rss" => Array(),
    //                "rss_section" => Array(),
    //            )
    //        )
    //    );?>
    <!--    </div>-->
    <?php
    $isPremier = strpos($_SERVER['REQUEST_URI'],'/premier') !== false ? true : false;
    ?>
    <div data-id="test" class="content-wrapper<?=$isPremier ? ' premiere-block--content-wrapper' : '';?>">
        <?$GLOBALS['news1'] = array(
            /*"SECTION_CODE"=>"novosti", */
            "PROPERTY_ON_MAIN_VALUE"=>"Yes",
        );?>
        <?$APPLICATION->IncludeComponent(
            "bitrix:news.list",
            "nr-home-slider",
            array(
                "IBLOCK_TYPE" => "news",
                "IBLOCK_ID" => "2",
                "NEWS_COUNT" => "5",
                "SORT_BY1" => "PROPERTY_SORT_EX",
                "SORT_ORDER1" => "DESC",
                "SORT_BY2" => "ACTIVE_FROM",
                "SORT_ORDER2" => "DESC",
                "FIELD_CODE" => array(
                    0 => "",
                    1 => "",
                ),
                "PROPERTY_CODE" => array(
                    0 => "MORE_NEWS",
                    1 => "IMG_HOME",
                    2 => "",
                ),
                "DETAIL_URL" => "",
                "SECTION_URL" => "",
                "IBLOCK_URL" => "",
                "DISPLAY_PANEL" => "",
                "SET_TITLE" => "Y",
                "SET_STATUS_404" => "Y",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "36000000",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "N",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "PAGER_TITLE" => "пїЅпїЅпїЅпїЅпїЅпїЅпїЅ",
                "PAGER_TEMPLATE" => "",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "Y",
                "DISPLAY_DATE" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "PREVIEW_TRUNCATE_LEN" => "",
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "USE_PERMISSIONS" => "N",
                "GROUP_PERMISSIONS" => "",
                "FILTER_NAME" => "news1",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "CHECK_DATES" => "Y",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "SET_BROWSER_TITLE" => "Y",
                "SET_META_KEYWORDS" => "Y",
                "SET_META_DESCRIPTION" => "Y",
                "ADD_SECTIONS_CHAIN" => "Y",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "INCLUDE_SUBSECTIONS" => "Y",
                "PAGER_SHOW_ALWAYS" => "Y",
                "AJAX_OPTION_ADDITIONAL" => ""
            ),
            false
        );?>
        <?/*$APPLICATION->IncludeComponent("nr:news", ".default", array(
        "IBLOCK_TYPE" => "news",
        "IBLOCK_ID" => "2",
        "NEWS_COUNT" => "14",
        "USE_SEARCH" => "N",
        "USE_RSS" => "Y",
        "NUM_NEWS" => "20",
        "NUM_DAYS" => "30",
        "YANDEX" => "N",
        "USE_RATING" => "N",
        "USE_CATEGORIES" => "Y",
        "CATEGORY_IBLOCK" => array(
            0 => "2",
        ),
        "CATEGORY_CODE" => "THEME",
        "CATEGORY_ITEMS_COUNT" => "5",
        "CATEGORY_THEME_2" => "photo",
        "USE_REVIEW" => "Y",
        "MESSAGES_PER_PAGE" => "10",
        "USE_CAPTCHA" => "Y",
        "REVIEW_AJAX_POST" => "Y",
        "PATH_TO_SMILE" => "/bitrix/images/forum/smile/",
        "FORUM_ID" => "1",
        "URL_TEMPLATES_READ" => "",
        "SHOW_LINK_TO_FORUM" => "Y",
        "POST_FIRST_MESSAGE" => "N",
        "USE_FILTER" => "N",
        "SORT_BY1" => "ACTIVE_FROM",
        "SORT_ORDER1" => "DESC",
        "SORT_BY2" => "SORT",
        "SORT_ORDER2" => "ASC",
        "CHECK_DATES" => "Y",
        "SEF_MODE" => "Y",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "36000000",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "Y",
        "SET_TITLE" => "Y",
        "SET_STATUS_404" => "N",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "ADD_SECTIONS_CHAIN" => "Y",
        "USE_PERMISSIONS" => "N",
        "PREVIEW_TRUNCATE_LEN" => "",
        "LIST_ACTIVE_DATE_FORMAT" => "d.m.Y",
        "LIST_FIELD_CODE" => array(
            0 => "",
            1 => "",
        ),
        "LIST_PROPERTY_CODE" => array(
            0 => "",
            1 => "IMG_HOME",
            2 => "MORE_NEWS",
        ),
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "DISPLAY_NAME" => "N",
        "META_KEYWORDS" => "-",
        "META_DESCRIPTION" => "-",
        "BROWSER_TITLE" => "-",
        "DETAIL_ACTIVE_DATE_FORMAT" => "H:i d/m/Y",
        "DETAIL_FIELD_CODE" => array(
            0 => "",
            1 => "",
        ),
        "DETAIL_PROPERTY_CODE" => array(
            0 => "THEME",
            1 => "",
            2 => "",
        ),
        "DETAIL_DISPLAY_TOP_PAGER" => "N",
        "DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
        "DETAIL_PAGER_TITLE" => "пїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅ",
        "DETAIL_PAGER_TEMPLATE" => "",
        "DETAIL_PAGER_SHOW_ALL" => "Y",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "Y",
        "PAGER_TITLE" => "пїЅпїЅпїЅпїЅпїЅпїЅпїЅ",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => "",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "Y",
        "DISPLAY_DATE" => "Y",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "USE_SHARE" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "SEF_FOLDER" => "/novosti/",
        "SEF_URL_TEMPLATES" => Array(
            "detail" => "#ELEMENT_CODE#/",
            "rss" => "rss/",
            "rss_section" => "#SECTION_ID#/rss/"
        )
    ),
    false
);*/?>
        <div class="left-column">
            <table cellpadding="0" cellspacing="0" border="0">
                <tr valign="top">
                    <td id="cell_1_1"></td>
                    <td id="cell_2_1"></td>
                </tr>
                <tr valign="top">
                    <td id="cell_1_2"></td>
                    <td id="cell_2_2"></td>
                </tr>
                <tr valign="top">
                    <td id="cell_1_3"></td>
                    <td id="cell_2_3"></td>
                </tr>
                <tr valign="top">
                    <td id="cell_1_4"></td>
                    <td id="cell_2_4"></td>
                </tr>
                <tr valign="top">
                    <td id="cell_1_5"></td>
                    <td id="cell_2_5"></td>
                </tr>
                <tr valign="top">
                    <td id="cell_1_6"></td>
                    <td id="cell_2_6"></td>
                </tr>
                <tr valign="top">
                    <td id="cell_1_7"></td>
                    <td id="cell_2_7"></td>
                </tr>
                <tr valign="top">
                    <td id="cell_3_1"></td>
                    <td id="cell_4_1"></td>
                </tr>
                <tr valign="top">
                    <td id="cell_3_2"></td>
                    <td id="cell_4_2"></td>
                </tr>
                <tr valign="top">
                    <td id="cell_3_3"></td>
                    <td id="cell_4_3"></td>
                </tr>
                <tr valign="top">
                    <td id="cell_3_4"></td>
                    <td id="cell_4_4"></td>
                </tr>
                <tr valign="top">
                    <td id="cell_3_5"></td>
                    <td id="cell_4_5"></td>
                </tr>
                <tr valign="top">
                    <td id="cell_3_6"></td>
                    <td id="cell_4_6"></td>
                </tr>
                <tr valign="top">
                    <td id="cell_3_7"></td>
                    <td id="cell_4_7"></td>
                </tr>
                <tr valign="top">
                    <td id="cell_5_1"></td>
                    <td id="cell_6_1"></td>
                </tr>
                <tr valign="top">
                    <td id="cell_5_2"></td>
                    <td id="cell_6_2"></td>
                </tr>
                <tr valign="top">
                    <td id="cell_5_3"></td>
                    <td id="cell_6_3"></td>
                </tr>
                <tr valign="top">
                    <td id="cell_5_4"></td>
                    <td id="cell_6_4"></td>
                </tr>
                <tr valign="top">
                    <td id="cell_5_5"></td>
                    <td id="cell_6_5"></td>
                </tr>
                <tr valign="top">
                    <td id="cell_5_6"></td>
                    <td id="cell_6_6"></td>
                </tr>
                <tr valign="top">
                    <td id="cell_5_7"></td>
                    <td id="cell_6_7"></td>
                </tr>
            </table>
