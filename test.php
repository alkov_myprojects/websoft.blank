<?
require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");


class DocBlock {

    public $docblock,
        $description = null,
        $all_params = array();

    public function getDescription() {
        return $this->description;
    }

    /**
     * Parses a docblock;
     */
    function __construct($docblock) {
        if( !is_string($docblock) ) {
            throw new Exception("DocBlock expects first parameter to be a string");
        }

        $this->docblock = $docblock;
        $this->parse_block();
    }

    /**
     * An alias to __call();
     * allows a better DSL
     *
     * @param string $param_name
     * @return mixed
     */
    public function __get($param_name) {
        return $this->$param_name();
    }

    /**
     * Checks if the param exists
     *
     * @param string $param_name
     * @return mixed
     */
    public function __call($param_name, $values = null) {
        if( $param_name == "description" ) {
            return $this->description;
        }else if( isset($this->all_params[$param_name]) ) {
            $params = $this->all_params[$param_name];

            if( count($params) == 1 ) {
                return $params[0];
            }else {
                return $params;
            }
        }

        return null;
    }

    /**
     * Parse each line in the docblock
     * and store the params in `$this->all_params`
     * and the rest in `$this->description`
     */
    private function parse_block() {
        // split at each line
        foreach(preg_split("/(\r?\n)/", $this->docblock) as $line){

            // if starts with an asterisk
            if( preg_match('/^(?=\s+?\*[^\/])(.+)/', $line, $matches) ) {

                $info = $matches[1];

                // remove wrapping whitespace
                $info = trim($info);

                // remove leading asterisk
                $info = preg_replace('/^(\*\s+?)/', '', $info);

                // if it doesn't start with an "@" symbol
                // then add to the description
                if( $info[0] !== "@" ) {
                    $this->description .= "\n$info";
                    continue;
                }else {
                    
                    // get the name of the param
                    preg_match('/@(\w+)/', $info, $matches);
                    $param_name = $matches[1];

                    // remove the param from the string
                    $value = str_replace("@$param_name ", '', $info);

                    echo '<b>$value</b>';
                    echo '<pre>';
                    print_r($value);
                    echo '</pre>';

                    $value_arr = explode(" ", $value);
                    echo '<b>$value_arr</b>';
                    echo '<pre>';
                    print_r($value_arr);
                    echo '</pre>';
                    echo '<hr>';

                    $value = array(
                        'name' => '',
                        'type' => '',
                        'description' => ''
                    );

                    //$type_return = false;

                    //region Определить имя аргумента и тип
                    // @param [Type] [name] [<description>]
                    // or
                    // @param [name] [Type] [<description>]
                    // or
                    // @param [name] [<description>]
                    if(strstr('$',$value_arr[0])) { // первое слово имя переменной
                        $value['name'] = $value_arr[0];
                    } else { // второе слово имя переменной
                        $value['type'] = $value_arr[0];
                        $value['name'] = $value_arr[1];
                    }

                    //endregion
                    
                    
                    echo '<hr>';
                    
                    

                    // if the param hasn't been added yet, create a key for it
                    if( !isset($this->all_params[$param_name]) ) {
                        $this->all_params[$param_name] = array();
                    }

                    // push the param value into place
                    $this->all_params[$param_name][] = $value;

                    continue;
                }
            }
        }
    }
}

class Person {

    /**
     * This is the description of my
     * doc comment
     *
     * @param string $name
     * @param integer $age
     *
     * @author Baylor Rae
     */
    public static function greet($name, $age) {
        echo "Hello $name, you are $age";
    }
}


//\Websoft\Blank\Classes\CHLBlock::UF_Field_Add(94,array());

include $_SERVER['DOCUMENT_ROOT'].'/local/modules/websoft.blank/lib/classes/chlblock.php';
//$r = new ReflectionMethod('Person', 'greet');
//$r = new ReflectionMethod('\Websoft\Blank\Classes\CHLBlock', 'UF_Field_GetList');
//$r = new ReflectionMethod('\Websoft\Blank\Classes\CHLBlock', 'UF_Field_GetAllByTableID');
$r = new ReflectionMethod('\Websoft\Blank\Classes\CHLBlock', 'test');
//$r = new ReflectionMethod('\Websoft\Blank\Classes\CHLBlock');

$block = $r->getDocComment();

$doc_block = new DocBlock($block);

echo '<b>$doc_block->getDescription()</b>';
echo '<pre>';
print_r($doc_block->getDescription());
print_r($doc_block->all_params);
echo '</pre>';
echo '<hr>';
echo '<hr>';


die();

echo '<b>$block</b>';
echo '<pre>';
print_r($block);
echo '</pre>';
echo '<hr>';


echo '<b>$doc_block</b>';
echo '<pre>';
print_r($doc_block);
echo '</pre>';
echo '<hr>';

/*echo 'Author: ' . $doc_block->author;
echo '<hr>';
echo 'Params:';
echo '<b>$doc_block->params</b>';
echo '<pre>';
print_r($doc_block->params);
echo '</pre>';
echo '<hr>';*/


die();


$str ='
/**    
 * @param   integer  $int  An integer
 * @return  boolean
 */
';
//$str = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/local/modules/websoft.blank/lib/classes/calendar.php');
$str = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/local/modules/websoft.blank/lib/classes/chlblock.php');


$l = explode('*', $str);
$res = array();
foreach($l as $el) {
    if (preg_match("/@(\w+) (.*)$/", $el, $m)) {
        $res[$m[1]][] = $m[2];
    }
}
echo '<b>$res</b>';
echo '<pre>';
print_r($res);
echo '</pre>';
echo '<hr>';

die();
if (preg_match_all('/@(\w+)\s+(.*)\r?\n/m', $str, $matches)){
    $result = array_combine($matches[1], $matches[2]);
}

echo '<b>$result</b>';
echo '<pre>';
print_r($result);
echo '</pre>';
echo '<hr>';

die();

include ($_SERVER['DOCUMENT_ROOT'].'/local/modules/websoft.blank/lib_add/ReflectionDocBlock_master/src/DocBlock.php');

$DocBlockFactory = new DocBlock();
echo '<b>$DocBlockFactory</b>';
echo '<pre>';
print_r($DocBlockFactory);
echo '</pre>';
echo '<hr>';

die();
//local/modules/websoft.blank/lib_add/ReflectionDocBlock_master/src/DocBlockFactory.php
use phpDocumentor\Reflection\DocBlockFactory;
$docComment = <<<DOCCOMMENT
/**
 * This is an example of a summary.
 *
 * This is a Description. A Summary and Description are separated by either
 * two subsequent newlines (thus a whiteline in between as can be seen in this
 * example), or when the Summary ends with a dot (`.`) and some form of
 * whitespace.
 */
DOCCOMMENT;
$factory  = DocBlockFactory::createInstance();
$docblock = $factory->create($docComment);
// Should contain the first line of the DocBlock
$summary = $docblock->getSummary();
// Contains an object of type Description; you can either cast it to string or use
// the render method to get a string representation of the Description.
//
// In subsequent examples we will be fiddling a bit more with the Description.
$description = $docblock->getDescription();

die();
/**
 * This is an Example class
 */
class Example
{

    /**
     * @param string $a - тест 1
     * @param integer $b - тест 2
     */
    public function fn($a,$b)
    {
        // void
    }
}

$reflector = new ReflectionClass('Example');

// to get the Class DocBlock
$methods = $reflector->getMethods();
echo '<b>$methods</b>';
echo '<pre>';
print_r($methods);
echo '</pre>';

foreach ($methods as $method) {
    echo '<b>$method</b>';
    echo '<pre>';
    print_r($method);
    echo '</pre>';

    $declaringClass = $method->getDeclaringClass();
    echo '<b>$declaringClass</b>';
    echo '<pre>';
    print_r($declaringClass);
    echo '</pre>';

    $getClosure = $method->getClosure();
    echo '<b>$getClosure</b>';
    echo '<pre>';
    print_r($getClosure);
    echo '</pre>';

    $getModifiers = $method->getModifiers ();
    echo '<b>$getModifiers</b>';
    echo '<pre>';
    print_r($getModifiers);
    echo '</pre>';

    /*$getPrototype = $method->getPrototype();
    echo '<b>$getPrototype</b>';
    echo '<pre>';
    print_r($getPrototype);
    echo '</pre>';*/



    echo '<hr>';
}

echo '<hr>';

/*
// to get the Method DocBlock
$Method = $reflector->getMethod('fn'); //->getDocComment();
echo '<b>$Method</b>';
echo '<pre>';
print_r($Method);
echo '</pre>';
echo '<hr>';


$props = $reflector->getProperties();
echo '<b>$props</b>';
echo '<pre>';
print_r($props);
echo '</pre>';
echo '<hr>';*/

die();
/**
 * @param integer $user_id_from - содатель поста
 * @param string $title - заголовок
 * @param string $text - текс сообщения (тут BB код юзаем)
 * @param array $user_ids_to - массив описывающий получаетелей поста в живой ленте (поле "кому")
 */
function CreatePostInBuzz( $user_id_from, $title, $text, $user_ids_to = array('UA','G2') ) {

    CModule::IncludeModule("blog");
    CModule::IncludeModule("socialnetwork");

    // Отпрвить сообщение в живую ленту
    $arBlog = CBlog::GetByOwnerID($user_id_from);

    $arFields= array(
        "TITLE" => $title,
        //"DETAIL_TEXT_TYPE" => 'html', //"Описание",
        "DETAIL_TEXT" => $text, //"Описание",
        "DATE_PUBLISH" => date('d.m.Y H:i:s'),
        "PUBLISH_STATUS" => "P",
        "CATEGORY_ID" => "",
        "PATH" => "/company/personal/user/1/blog/#post_id#/",
        "URL" => "admin-blog-s1",
        "PERMS_POST" => Array(),
        "PERMS_COMMENT" => Array (),
        "SOCNET_RIGHTS" => $user_ids_to,/*Array(
                //"UA", // Для всех пользователей
                //"G2", // Для группы пользователей с ID = 2
                //"U2", // Для конкретного пользователя с ID = 2
            ),*/
        "=DATE_CREATE" => "now()",
        "AUTHOR_ID" => $user_id_from, //$USER->GetID(),
        "BLOG_ID" => $arBlog['ID'],
    );

    $newID= CBlogPost::Add($arFields);

    $arFields["ID"] = $newID;
    $arParamsNotify = Array(
        "bSoNet" => true,
        "UserID" => $user_id_from, //$USER->GetID(),
        "user_id" => $user_id_from, //$USER->GetID(),
    );

    CBlogPost::Notify($arFields, array(), $arParamsNotify);
}

CreatePostInBuzz(1,'title','text',array('U328'));

die();

if(\CModule::IncludeModule("calendar")) {

    // 0) Результирующий массив с ID-шниками созданных событий
    //$result = array();

    // 1) Входящие данные
    $owner_id = 1; // ID создателя
    $users = array(); // Список пользователей
    $dateBegin = '31.01.2019 12:00:00'; // Дата начала
    $dateEnd = '31.01.2019 14:00:00'; // Дата окончания
    $name = 'Наименование мероприятия 111'; // Наименование мероприятия
    $description = 'Описание мероприятия'; // Описание мероприятия

    // 2) Конвертирование времени (считаем что входящие дата начала/окочание берётся из настроек пользовате и преобразуем
    // их во время сервера)
    //$dateBegin = self::convertDateToTimeServer($dateBegin);
    //$dateEnd = self::convertDateToTimeServer($dateEnd);

    // 3) Добавить родительское событие
    $objDateTime = new \Bitrix\Main\Type\DateTime();
    $tz = $objDateTime->getTimeZone()->getName();
    $arEventFields = array(
        'CAL_TYPE' => 'user',
        'OWNER_ID' => $owner_id,
        'DT_FROM' => $dateBegin,
        //'TIMESTAMP_X' => $dateBegin,
        'DT_TO' => $dateEnd,
        'TZ_FROM' => $tz,
        'TZ_TO' => $tz,
        'NAME' => $name,
        'DESCRIPTION' => \CCalendar::ParseHTMLToBB($description),
        'IS_MEETING' => true,
        'MEETING_HOST' => $owner_id,
        'MEETING' => array( 'HOST_NAME' => \CCalendar::GetUserName($owner_id) ),
        'ATTENDEES' => $users,
        //'LOCATION' => 'ECMR_'.$meeting_id, // Воркает
        /*'LOCATION' => array(
            'OLD' => '',
            'NEW' => 'ECMR_'.$meeting_id.'_'.$bargaining_id,
        )*/

        /*'RRULE' => array(
            'FREQ' => "DAILY", // NONE - Не повторять
                               // DAILY - Каждый день
                               // MONTHLY - Каждый месяц
                               // WEEKLY - Каждую неделю
                               // YEARLY - Каждый год
            'INTERVAL' => '2'  // Интервал
            'COUNT' => 2,      // Кол-во повторений
            'UNTIL' => ''      // Дата остановки (только дата)
        )*/
        // $_REQUEST['rrule_endson'] // never||count||until

        //region Напоминать
        /*'REMIND' => Array(
            Array( 'type' => 'min', 'count' => '0'),    // В момент события
            Array( 'type' => 'min', 'count' => '10'),   // За 10 минут
            Array( 'type' => 'min', 'count' => '15'),   // За 15 минут
            Array( 'type' => 'min', 'count' => '30'),   // За 30 минут
            Array( 'type' => 'min', 'count' => '60'),   // За час
            Array( 'type' => 'min', 'count' => '120'),  // За 2 часа
            Array( 'type' => 'min', 'count' => '120'),  // За 2 часа
            Array( 'type' => 'min', 'count' => '1440'), // За сутки
            Array( 'type' => 'min', 'count' => '2880'), // За сутки
        ),*/
        //endregion
    );

    $EventID = \CCalendar::SaveEvent(array(
        'arFields' => $arEventFields,
        'userId' => $owner_id,
        'autoDetectSection' => true,
        'autoCreateSection' => true
    ));
}


die();

preg_match('/^[0-9]{1}$/', '2', $matches, PREG_OFFSET_CAPTURE);

echo '<b>$matches</b>';
echo '<pre>';
print_r($matches);
echo '</pre>';
echo '<hr>';

die();

$user = new \Websoft\Blank\Classes\User(1);
echo $user->GetFieldByCode('NAME').'<hr>';
echo $user->GetFullName().'<hr>';

/*$user = \Websoft\Blank\Classes\User::GetByID(1);
echo '<b>$user</b>';
echo '<pre>';
print_r($user);
echo '</pre>';
echo '<hr>';*/

die();

\Websoft\Blank\Classes\CHLBlock::UF_Field_Add(94,array());
$errors = $APPLICATION->GetException()->GetMessages();

echo '<b>$error</b>';
echo '<pre>';
print_r($error);
echo '</pre>';
echo '<hr>';

die();


echo '<b>$_REQUEST</b>';
echo '<pre>';
print_r($_REQUEST);
echo '</pre>';
echo '<hr>';

\Bitrix\Highloadblock\HighloadBlockTable::update(73,array(
    'NAME' => 'Twelvee',//$NAME, // должно начинаться с заглавной буквы и состоять только из латинских букв и цифр
    'TABLE_NAME' => 'twelvee'//$TABLE_NAME,// должно состоять только из строчных латинских букв, цифр и знака подчеркивания
));

die();
$module_id = 'websoft.blank';
if(\CModule::includeModule($module_id)) {
    \Websoft\Blank\Classes\CHLBlock::UpdateTable(70, 'Twelve', 'twelve','Test 1');
}
/*\Bitrix\Highloadblock\HighloadBlockTable::update(69,array(
    'NAME' => 'Twelveeee', // должно начинаться с заглавной буквы и состоять только из латинских букв и цифр
    'TABLE_NAME' => 'twelveeee',// должно состоять только из строчных латинских букв, цифр и знака подчеркивания
));*/
//\Bitrix\Highloadblock\HighloadBlockLangTable::update($ID,array( 'LID'=>'ru', 'NAME'=>$NAME_RU ));

die();


$module_id = 'websoft.blank';
$Module = \CModule::CreateModuleObject($module_id);
$AllOptions = $Module->GetAllOptions();

echo '<b>$AllOptions</b>';
echo '<pre>';
print_r($AllOptions);
echo '</pre>';
echo '<hr>';

die();
//region Обновление пользовательского поля
$module_id = 'websoft.blank';
$Module = \CModule::CreateModuleObject($module_id);
$CHLBlock = $Module->GetNameSpace() . 'Classes\CHLBlock';
$Handler = $Module->GetNameSpace() . 'Handler';
$listEntityStructure = $Handler::GetStructureAllEntityHL();
$ListTables = $CHLBlock::GetListTables();

$Table = $ListTables[0];

echo '<b>$Table</b>';
echo '<pre>';
print_r($Table);
echo '</pre>';
echo '<hr>';

$UF_FIELDS_DB = $CHLBlock::UF_FieldsByTableID($Table['ID']);
$UF_FIELD = $UF_FIELDS_DB[0];

echo '<b>$UF_FIELD</b>';
echo '<pre>';
print_r($UF_FIELD);
echo '</pre>';
echo '<hr>';

$FIELD_ID = $UF_FIELD['ID'];
unset($UF_FIELD['ID']);


$UF_FIELD_UPDATE = array(
    // 'FIELD_NAME' => 'UF_FIELD_111', // Нельзя обновить... жаль =(

    //region
    'XML_ID' => '...',      // Тут наверное не имеет смысла править... поскольку это системаное поле.
                            // Т.е. задаётся только один раз при создании!
    'SORT' => '300',        // Сортировка
    'MANDATORY' => 'N',     // Обязательное => Y|N
    'SHOW_FILTER' => 'N',   // Показывать в фильтре списка:
                            //    N - не показывать
                            //    I - точное совпадение
                            //    E - поиск по маске
                            //    S - поиск по подстроке
    'IS_SEARCHABLE' => 'N', // Значения поля участвуют в поиске => Y|N
    'EDIT_FORM_LABEL' => array(
        'ru'    => '111111111111',
    )
    //endregion

    //region То что не удалось отредактировать стандартным способом
    //'SHOW_IN_LIST' => 'Y',  // Не показывать в списке => Y|N
    //'EDIT_IN_LIST' => 'Y',  // Не разрешать редактирование пользователем => Y|N
    // Дополнительные настройки поля (зависят от типа)
    // 'EDIT_FORM_LABEL'   => array(
    //     'ru'    => 'Моё поле',
    //     'en'    => 'My field',
    // )
    //endregion

);




$resUpdate = $CHLBlock::UF_Field_Update(152, $UF_FIELD_UPDATE);
//$resUpdate = $CHLBlock::UF_Field_Update($FIELD_ID, $UF_FIELD_UPDATE);

/*echo '<b>$UF_FIELDS_DB</b>';
echo '<pre>';
print_r($UF_FIELDS_DB);
echo '</pre>';
echo '<hr>';*/



//endregion

die();

$PATH = $_SERVER['DOCUMENT_ROOT'] . '/test_array.php';
//echo '$PATH: '.$PATH.'<br>';

$data = array(
    'hello world'
);
/*
echo '<b>$arr</b>';
echo '<pre>';
///print_r($arr);
//var_dump($arr);
var_export($arr);
echo '</pre>';
echo '<hr>';*/

//CFileMan::SaveMenu($PATH.'/test_array.php',$arr);
$module = 'websoft.blank';
if (CModule::includeModule($module)) {
    //\Websoft\Blank\Handler::SaveArrayToFile($PATH,$arr);
    $test = \Websoft\Blank\Handler::GetArrayToFile($PATH);

    echo '<b>$test</b>';
    echo '<pre>';
    print_r($test);
    echo '</pre>';
    echo '<hr>';

    echo '<b>$data</b>';
    echo '<pre>';
    print_r($data);
    echo '</pre>';
    echo '<hr>';
}

die();
$filename = 'array.txt';

$bookshelf = array(
    'test' => 1,
    'test_2' => array(
        '2_1' => 1,
        '2_2' => 2,
    ),
);

// Запись.
$data = serialize($bookshelf);      // PHP формат сохраняемого значения.
//$data = json_encode($bookshelf);  // JSON формат сохраняемого значения.
file_put_contents($filename, $data);

// Чтение.
$data = file_get_contents($filename);
//$bookshelf = json_decode($data, TRUE); // Если нет TRUE то получает объект, а не массив.
$bookshelf = unserialize($data);

echo '<b>$bookshelf</b>';
echo '<pre>';
print_r($bookshelf);
echo '</pre>';
echo '<hr>';