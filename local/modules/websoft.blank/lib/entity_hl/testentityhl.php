<?php

namespace Websoft\Blank\Entity_HL;
use \Websoft\Blank\Classes\CHLBlock as CHLBlock;
/**
 * Тестовая сущность 1 (на основе HL таблицы)
 * Class TestEntity1Table
 * @package Websoft\Blank\E
 */
class TestEntityHLTable extends CHLBlock {

    /*return array(
        'NAME' => 'CheckedObj',// Название сущности
        'TABLE_NAME' => 'checked_obj', // Название таблицы в БД
        'NAME_LANG_RU' => 'Привет мир, я первая сущность на HLBlock', // Языковое название (Только русский вариант)
    );*/

    public static function getNameTable() { return 'первая сущность на HLBlock'; }
    public static function getMapProps() {
        return array(
            array(),
            array(),
            array(),
        );
    }

    /*public static function getMap(){
        // TODO: Implement getMap() method.
        return array(
            'NAME' => 'CheckedObj',// Название сущности
            'TABLE_NAME' => 'checked_obj', // Название таблицы в БД
            'NAME_LANG_RU' => 'Привет мир, я первая сущность на HLBlock', // Языковое название (Только русский вариант)
            'UF_PROPS' => array(
                array(),
            )
        );
    }*/

    //region События

        //region Add
        // OnBeforeAdd - до добавление элемента
        // OnAdd - перед добавлением в бд, после валидации полей. Модификация данных не доступна
        // OnAfterAdd -  после добавления элемента HL блока. Можно добавить дополнительную логику, которая будет
        //               запускаться после добавления нового элемента данного HL-блока.
        // endregion

        //region Update
        // OnBeforeUpdate - до обновления элемента highload(HL) блока и валидации полей.
        //                  Можно модифицировать данные, добавить собственную валидацию, вернуть ошибку,
        //                  если валидация не пройдена.
        // OnUpdate - перед обновлением в бд, после валидации полей. Модификация данных не доступна.
        // OnAfterUpdate - после обновления элемента HL блока. Можно добавить дополнительную логику, которая будет
        //                 запускаться после обновления элемента данного HL-блока.
        //endregion

        //region Delete
        // OnBeforeDelete - до удаления элемента highload(HL) блока. Можно отменить удаление.
        // OnDelete - перед удалением из бд. Отмена удаления не доступна.
        // OnAfterDelete - после удаления элемента HL блока. Можно добавить дополнительную логику,
        //                 которая будет запускаться после удаления элемента данного HL-блока.
        //endregion

    //endregion

    //region Grid
    // Фильтр
    // Столбцы
    //endregion
}
