<?php

namespace #NAME_SPACE#Entity_HL;
use \#NAME_SPACE#Classes\CHLBlock as CHLBlock;

/**
 * Тестовая сущность 1 (на основе HL таблицы)
 * Class TestEntity1Table
 * @package Websoft\Blank\E
 */
class TestEntityHLTable extends CHLBlock {

    /*return array(
        'NAME' => 'CheckedObj',// Название сущности
        'TABLE_NAME' => 'checked_obj', // Название таблицы в БД
        'NAME_LANG_RU' => 'Привет мир, я первая сущность на HLBlock', // Языковое название (Только русский вариант)
    );*/

    public static function getNameTable() { return 'первая сущность на HLBlock'; }
    public static function getMapProps() {
        return array(
            array(),
            array(),
            array(),
        );
    }

    /*public static function getMap(){
        // TODO: Implement getMap() method.
        return array(
            'NAME' => 'CheckedObj',// Название сущности
            'TABLE_NAME' => 'checked_obj', // Название таблицы в БД
            'NAME_LANG_RU' => 'Привет мир, я первая сущность на HLBlock', // Языковое название (Только русский вариант)
            'UF_PROPS' => array(
                array(),
            )
        );
    }*/
}
