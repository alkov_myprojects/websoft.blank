<?php

/**
 * Создание элементов формы для административной панели
 * Class CustomAdminPanel
 */
namespace Websoft\Blank\Admin;
class CustomAdminTags {
    private $widthL = '40';
    private $widthR = '60';
    private $nameForm;
    private $title;
    private $module_id;
    private $aTabs = array();
    private $listCodeOption = array();
    private $change = false;
    private $messRequared = false;
    public function GetChange(){ return $this->change; }

    /**
     * CustomAdminTags constructor.
     * @param $title - Заголовок формы
     * @param $nameForm - Символьный код формы
     * @param $module_id - Идентификатор модуля
     * @param $method - Метод который отправляет форма
     * @param $bitrixSessid - Идентификатора сессии
     * @param $right - Наличия прав на запись для модуля
     * @param $requetsMethod - Текущий метода вызова страницы
     * @param $save - нажатия кнопок "Сохранить"
     * @param $aply - нажатия кнопок "Применить"
     * @param bool $widthL
     * @param bool $widthR
     */
    public function __construct(
        $title,$nameForm,$module_id,
        $method, $bitrixSessid, $right, $requetsMethod, $save, $apply,
        $widthL = false,$widthR = false
    ) {
        if ( $bitrixSessid && $right == "W" && $requetsMethod == $method && ($save != "" || $apply != "") )
        { $this->change = true; }
        $this->module_id = $module_id;
        $this->nameForm = $nameForm;
        $this->title = $title;
        if($widthL) { $this->widthL = $widthL; }
        if($widthR) { $this->widthR = $widthR; }
    }

    /** ________________ Публичные методы ________________ */

    public function NameForm() { return $this->nameForm; }
    public function SetTitle() { global $APPLICATION; $APPLICATION->SetTitle($this->title); }
    public function Messages($message) {
        if ($_REQUEST["mess"] == "ok") { \CAdminMessage::ShowMessage(array("MESSAGE" => 'Сохранено', "TYPE" => "OK")); }
        if ($message) { echo $message->Show(); }
    }
    public function Note($mess,$required=false) {
        echo BeginNote();
        if($required){?><span class="required">*</span> <?}
        echo $mess;
        echo EndNote();
    }

    /**
     * Генерация вёрстки загаловка + вставка в него текста
     * @param $text - Текст заголовка
     * @return string - возвращает сгенерированный HTML
     */
    public function Title($text) { return '<tr class="heading"><td colspan="2">'.$text.'</td></tr>'; }

    /**
     * Описание опции модуля в виде Input + js календаря bx24
     * @param string $title - Название поля (устанавливатеся в левом контейнере)
     * @param string $name - Уникальный код текущего тега в внутри форме
     * @param bool $required - заполнение сгенерированного тега обязательно, по умолчанию false
     * @param int $size - Ширина поля ввода
     * @param bool $widthL - Ширина левого контейнера (по умолчанию 40%)
     * @param bool $widthR - Ширина правого контейнера (по умолчанию 60%)
     * @return string - возвращает сгенерированный HTML
     */
    public function SetCalendarDate($tab,$title,$name,$required = false,$size = 20,$widthL = false,$widthR = false) {
        $this->SetOption($name);
        $value = $this->GetOption($name);
        $this->aTabs[$tab][] = $this->CalendarDate($title,$name,$value,$required,$size,$widthL,$widthR);
    }

    /**
     * Описание опции модуля в виде textarea
     * @param string $title - Название поля (устанавливатеся в левом контейнере)
     * @param string $name - Уникальный код текущего тега в внутри форме
     * @param boolean $required - заполнение сгенерированного тега обязательно, по умолчанию false
     * @param string $rows - Высота рабочей области в строках текста.
     * @param string $cols - Ширина рабочей области в символах.
     * @param bool $widthL - Ширина левого контейнера (по умолчанию 40%)
     * @param bool $widthR - Ширина правого контейнера (по умолчанию 60%)
     * @return string - возвращает сгенерированный HTML
     */
    public function SetTextArea($tab,$title,$name,$required = false,$rows="5",$cols="45",$widthL = false,$widthR = false) {
        $this->SetOption($name);
        $value = $this->GetOption($name);
        $this->aTabs[$tab][] = $this->TextArea($title,$name,$value,$required,$rows,$cols,$widthL,$widthR);
    }

    /**
     * Генерация тега checkbox
     * @param string $title - Название поля (устанавливатеся в левом контейнере)
     * @param string $name - Уникальный код текущего тега в внутри форме
     * @param boolean $required - заполнение сгенерированного тега обязательно, по умолчанию false
     * @param bool $widthL - Ширина левого контейнера (по умолчанию 40%)
     * @param bool $widthR - Ширина правого контейнера (по умолчанию 60%)
     * @return string - возвращает сгенерированный HTML
     */
    public function SetCheckBox($tab,$title,$name,$mirror = false,$disabled = false,$required = false,$widthL = false,$widthR = false) {
        $this->SetOption($name);
        $value = $this->GetOption($name);
        $this->aTabs[$tab][] = $this->CheckBox($title,$name,$value,$mirror,$disabled,$required,$widthL,$widthR);
    }

    /**
     * Генерация тега input
     * @param string $title - Название поля (устанавливатеся в левом контейнере)
     * @param string $name - Уникальный код текущего тега в внутри форме
     * @param string $value - Значение input`а
     * @param boolean $required - заполнение сгенерированного тега обязательно, по умолчанию false
     * @param int $size - Ширина текстового поля.
     * @param bool $widthL - Ширина левого контейнера (по умолчанию 40%)
     * @param bool $widthR - Ширина правого контейнера (по умолчанию 60%)
     * @return string - возвращает сгенерированный HTML
     */
    public function SetInput($tab,$title,$name,$required = false,$size = 30,$widthL = false,$widthR = false) {
        $this->SetOption($name);
        $value = $this->GetOption($name);
        $this->aTabs[$tab][] = $this->Input($title,$name,$value,$required,$size,$widthL,$widthR);
    }

    /**
     * Генарация тега select - выпадающй список
     * @param string $title - Название поля (устанавливатеся в левом контейнере)
     * @param string $name - Уникальный код текущего тега в внутри форме
     * @param array $arrValues - Массив набора элементов вида "value" => "название опции"
     * @param bool $valueSelected - ключ по которому будет выбрана опция, сравнивается с value. Если нет, то выбирается первый
     * @param boolean $required - заполнение сгенерированного тега обязательно, по умолчанию false
     * @param bool $widthL - Ширина левого контейнера (по умолчанию 40%)
     * @param bool $widthR - Ширина правого контейнера (по умолчанию 60%)
     * @return string - возвращает сгенерированный HTML
     */
    public function SetSelect($tab,$title,$name,$arrValues,$required = false,$widthL = false,$widthR = false) {
        $this->SetOption($name);
        $value = $this->GetOption($name);
        $this->aTabs[$tab][] = $this->Select($title,$name,$arrValues,$value,$required,$widthL,$widthR);
    }

    /**
     * Генарация тега select с атрибутом multiple - множественны выпадающй список
     * @param string $title - Название поля (устанавливатеся в левом контейнере)
     * @param string $name - Уникальный код текущего тега в внутри форме
     * @param array $arrValues - Массив набора элементов вида "value" => "название опции"
     * @param boolean $required - заполнение сгенерированного тега обязательно, по умолчанию false
     * @param bool $size - Количество отображаемых строк списка.
     * @param bool $widthL - Ширина левого контейнера (по умолчанию 40%)
     * @param bool $widthR - Ширина правого контейнера (по умолчанию 60%)
     * @return string - возвращает сгенерированный HTML
     */
    public function SetSelectMultiple($tab,$title,$name,$arrValues,$required = false,$size = false,$widthL = false,$widthR = false) {
        $this->SetOption($name,true);
        $value = $this->GetOption($name,true);
        $this->aTabs[$tab][] = $this->SelectMultiple($title,$name,$arrValues,$value,$required,$size,$widthL,$widthR);
    }

    /**
     * Генерация тега множественный checkbox
     * @param string $title - Название поля (устанавливатеся в левом контейнере)
     * @param string $name - Уникальный код текущего тега в внутри форме
     * @param array $arrValues - Массив набора элементов вида "value" => "название опции"
     * @param boolean $required - заполнение сгенерированного тега обязательно, по умолчанию false
     * @param bool $widthL - Ширина левого контейнера (по умолчанию 40%)
     * @param bool $widthR - Ширина правого контейнера (по умолчанию 60%)
     * @return string - возвращает сгенерированный HTML
     * @return string
     */
    public function SetCheckBoxMultiple($tab,$title,$name,$arrValues,$required = false,$widthL = false,$widthR = false) {
        $this->SetOption($name,true);
        $value = $this->GetOption($name,true);
        $this->aTabs[$tab][] = $this->CheckBoxMultiple($title,$name,$arrValues,$value,$required,$widthL,$widthR);
    }

    /**
     * Генерация тега input
     * @param string $title - Название поля (устанавливатеся в левом контейнере)
     * @param string $name - Уникальный код текущего тега в внутри форме
     * @param boolean $required - заполнение сгенерированного тега обязательно, по умолчанию false
     * @param bool $widthL - Ширина левого контейнера (по умолчанию 40%)
     * @param bool $widthR - Ширина правого контейнера (по умолчанию 60%)
     * @return string - возвращает сгенерированный HTML
     */
    // TODO: Один radio btn это очень плохо, нужно как минимум два!!!
    public function SetRadioButtom($tab,$title,$name,$required = false,$widthL = false,$widthR = false) {
        $this->SetOption($name);
        $value = $this->GetOption($name);
        $this->aTabs[$tab][] = $this->RadioButtom($title,$name,$value,$required,$widthL,$widthR);
    }

    public function SetButtom($tab,$value,$idBtn,$title,$onclick='',$widthL = false,$widthR = false) {
        $this->aTabs[$tab][] = $this->Buttom($title,$value,$idBtn,$onclick,$widthL,$widthR);
    }

    /**
     * Печать опций модуля в форме
     * @param $tabControl
     */
    public function printOptions($tabControl) {
        foreach ($this->aTabs as $codeTab => $options) {
            $tabControl->BeginNextTab();
            foreach ($options as $option) { echo $option; }
        }
    }

    /**
     * Получить ассоциативный массив "код свойства" => "значение свойства"
     * @return array
     */
    public function GetListValOption() { return $this->listCodeOption; }

    public function issetМandatoryOption() { return $this->messRequared; }

    /** ________________ Внутренние методы ________________ */
    private function SetOption($code,$multiple = false) {
        if($this->change) {
            if($multiple) { $value = $_REQUEST[$code]; $value = json_encode($value); }
            else { $value = $_REQUEST[$code] ? $_REQUEST[$code] : ''; }
            \COption::SetOptionString($this->module_id, $code,$value);
        }
    }
    private function GetOption($code,$multiple = false) {
        $value = \COption::GetOptionString($this->module_id,$code);
        if($multiple) { $value = json_decode($value); }
        $this->listCodeOption[$code] = $value;
        return $value;
    }
    private function CalendarDate($title,$name,$first_date_str,$required = false,$size = 20,$widthL = false,$widthR = false) {
        $contents = CalendarDate($name, $first_date_str, $this->nameForm, $size);
        return $this->generateTr($title,$contents,$widthL,$widthR,$required);
    }
    private function TextArea($title,$name,$text = '',$required = false,$rows="5",$cols="45",$widthL = false,$widthR = false) {
        ob_start();
        ?><textarea class="typearea" name="<?=$name;?>"
                    cols="<?=$cols;?>" rows="<?=$rows;?>"
                    wrap="VIRTUAL"><?=$text;?></textarea><?
        $contents = ob_get_contents();
        ob_end_clean();
        return $this->generateTr($title,$contents,$widthL,$widthR,$required);
    }
    private function Input($title,$name,$value = '',$required = false,$size = 30,$widthL = false,$widthR = false) {
        ob_start();
        ?><input type="text" name="<?=$name;?>" value="<?=$value;?>"
                 size="<?=$size?>" ><?
        $contents = ob_get_contents();
        ob_end_clean();
        return $this->generateTr($title,$contents,$widthL,$widthR,$required);
    }
    private function Select($title,$name,$arrValues,$valueSelected = false,$required = false,$widthL = false,$widthR = false) {
        ob_start();
        ?><select name="<?=$name;?>" class="typeselect">
        <? $firs = true; ?>
        <? foreach ($arrValues as $key => $val): ?>
            <option
                <?if(!$valueSelected && $firs){?>selected<?};?>
                <?if($valueSelected && $valueSelected == $key){?>selected<?};?>
                value="<?=$key;?>"><?=$val;?></option>
            <? $firs=false; ?>
        <? endforeach; ?>
        </select><?
        $contents = ob_get_contents();
        ob_end_clean();
        return $this->generateTr($title,$contents,$widthL,$widthR,$required);
    }
    private function CheckBox($title,$name,$checked,$mirror,$disabled,$required,$widthL,$widthR) {
        ob_start();
        ?><input type="checkbox" name="<?=$name;?>" <?if ($checked) echo " checked" ?> <?if ($disabled) echo " disabled" ?>><?
        $contents = ob_get_contents();
        ob_end_clean();
        return $this->generateTr($title,$contents,$widthL,$widthR,$required,$mirror);
    }
    private function SelectMultiple($title,$name,$arrValues,$arrSelected = array(),$required = false,$size = false,$widthL = false,$widthR = false) {
        ob_start();
        ?><select name="<?=$name;?>[]" class="typeselect" multiple <?if($size){?>size="<?=$size;?>"<?}?>>
        <? foreach ($arrValues as $key => $val): ?>
            <option
                <?if(is_array($arrSelected) && in_array($key,$arrSelected)){?>selected<?};?>
                value="<?=$key;?>"><?=$val;?></option>
        <? endforeach; ?>
        </select><?
        $contents = ob_get_contents();
        ob_end_clean();
        return $this->generateTr($title,$contents,$widthL,$widthR,$required);
    }
    private function CheckBoxMultiple($title,$name,$arrValues,$arrSelected = array(),$required = false,$widthL = false,$widthR = false) {
        ob_start();
        ?>
        <table cellspacing=1 cellpadding=0 border=0 class="internal">
            <tr class="heading"><?
                foreach ($arrValues as $strVal => $strDoW): ?>
                    <td><?= $strDoW ?></td>
                <?endforeach; ?>
            </tr>
            <tr>
                <? foreach ($arrValues as $strVal => $strDoW): ?>
                    <td><input type="checkbox" name="<?=$name?>[]" value="<?=$strVal?>"<?
                        if (!empty($arrSelected) && array_search($strVal, $arrSelected) !== false) echo " checked" ?>></td>
                <?endforeach; ?>
            </tr>
        </table>
        <?
        $contents = ob_get_contents();
        ob_end_clean();
        return $this->generateTr($title,$contents,$widthL,$widthR,$required);
    }
    private function RadioButtom($title,$name,$value,$required = false,$widthL = false,$widthR = false) {

        ob_start();
        ?><input type="radio" id="<?=$name;?>_ID" name="<?=$name;?>" value="Y" <?if($value):?>checked<?endif;?>><?
        $contentL = ob_get_contents();
        ob_end_clean();

        ob_start();
        ?><label for="<?=$name;?>_ID"><?=$title?></label><?
        $contentR = ob_get_contents();
        ob_end_clean();

        return $this->generateTr($contentL,$contentR,$widthL,$widthR,$required);
    }
    private function Buttom($title,$value,$idBtn,$onclick,$widthL,$widthR) {
        ob_start();
        ?><input
            type="button" onclick="<?=$onclick?>" class="adm-detail-content-btns-wrap"
            id="<?=$idBtn;?>" title="<?=$title?>" value="<?=$value?>"
        ><?
        $contentR = ob_get_contents();
        ob_end_clean();

        ob_start();
        ?><label><?=$title?></label><?
        $contentL = ob_get_contents();
        ob_end_clean();
        return $this->generateTr($contentL,$contentR,$widthL,$widthR);
    }

    /**
     * Генерация скрытыго тега input
     * @param string $name - Уникальный код текущего тега в внутри форме
     * @param string $value - Значение input`а
     * @return string - возвращает сгенерированный HTML
     */
    // TODO: Подумать как его использовать, может сохранять значение сесси в форме???
    private function InputHidden($name,$value = '') {
        return '<input type="hidden" name="'.$name.'" value="'.$value.'">';
    }

    /**
     * Внутренний метод для генерации левого и правого контейнера и вставки HTML-контена в них
     * @param $contentL - HTML который будет вставлен в левый контейнер
     * @param $contentR - HTML который будет вставлен в левый контейнер
     * @param bool $widthL - Ширина левого контейнера (по умолчанию 40%)
     * @param bool $widthR - Ширина правого контейнера (по умолчанию 60%)
     * @return string - возвращает сгенерированный HTML
     */
    private function generateTr($contentL,$contentR,$widthL = false,$widthR = false,$required = false,$mirror = false) {
        if($required) { $this->messRequared = true; }
        if($mirror) {
            ob_start(); ?>
            <tr>
                <td width="<?if($widthL) { echo $widthL; } else echo $this->widthL;?>%">
                    <?if($required): ?> <span class="required">*</span><? endif; ?><?=$contentR;?>
                </td>
                <td width="<?if($widthR) { echo $widthR; } else echo $this->widthR;?>%"><?=$contentL?></td>
            </tr>
        <? } else {
        ob_start(); ?>
        <tr>
            <td width="<?if($widthL) { echo $widthL; } else echo $this->widthL;?>%">
                <?=$contentL?>
                <?if($required): ?> <span class="required">*</span><? endif; ?>
            </td>
            <td width="<?if($widthR) { echo $widthR; } else echo $this->widthR;?>%"><?=$contentR;?></td>
        </tr>
        <? }
        $contents = ob_get_contents(); ob_end_clean();
        return $contents;
    }
}