<?php

/**
 * Created by PhpStorm
 * User: Sergey Pokoev
 * www.pokoev.ru
 * @ Академия 1С-Битрикс - 2015
 * @ academy.1c-bitrix.ru
 *
 * файл event.php
 */

namespace Websoft\Blank;
use Websoft\Blank\Classes\CHBlockBase;
//use Websoft\Blank\EntityHL\TestEntityHLTable as TestEntityHL;

class Handler {

    private static $module_id; // ID Модуля
    private static $asset; // Для регистрации JS + Css + Lang

    /** Инициализация */
    public static function init() {

        //\websoft_blank::test();

        $NameTable = \Websoft\Blank\Entity_HL\TestEntityHLTable::getNameTable();
        //echo '$NameTable: <b>'.$NameTable.'</b><br>';

        //TODO: urn текущей страницы
        global $APPLICATION;
        $page = $APPLICATION->GetCurPageParam("", array());

        //TODO: Системные данные
        $dir = dirname(__FILE__);
        for ($i = 1; $i < ($level = 2); $i++) { $dir = dirname($dir); }
        self::$module_id = basename($dir);
        self::$asset = \Bitrix\Main\Page\Asset::getInstance();

        //TODO: Получить пространства имён и классы сущностей
        $db = \Bitrix\Main\Application::getConnection();
        $listEntity = array();
        if(is_dir($dir = dirname(__FILE__).'/entity/')) {
            $TreeFiles = self::TreeDirAndFiles($dir);
            foreach ($TreeFiles as $file) { $listEntity[] = self::get_namespace($file); }
        }

        //TODO: Процесс синхронизации(файлов и сущностей) на время разработки
        $update = false;

        /** Синхронизация файлов */
        $SynchronizeFilesModule = \COption::GetOptionString(self::$module_id, "SynchronizeFilesModule");
        if($SynchronizeFilesModule) {
        //if(false) {
            $directiories_files = \COption::GetOptionString(self::$module_id, "directiories_files");
            $directiories_files = json_decode($directiories_files, true);
            foreach ($directiories_files as $pathDirModuleAbs => $pathiDirPulic) {
                // Получить Хэш-сумму файлов модуля
                $TreeDirAndFiles_Module = self::TreeDirAndFiles($pathDirModuleAbs);
                $ArrHash_Module = array();
                foreach ($TreeDirAndFiles_Module as $path) {
                    $ArrHash_Module[] = md5_file($path);
                }
                $hashSumModule = md5(implode('', $ArrHash_Module));

                // Получить Хэш-сумму файлов в публичной части
                $ArrHashSumPublic = array();
                foreach ($TreeDirAndFiles_Module as $path) {
                    $path = str_replace($pathDirModuleAbs, $pathiDirPulic, $path);
                    $path = $_SERVER['DOCUMENT_ROOT'] . $path;
                    $ArrHashSumPublic[] = md5_file($path);
                }
                $hashSumPublic = md5(implode('', $ArrHashSumPublic));

                // Если не равны, то копировать папку из модуля в публичку:
                if ($hashSumPublic != $hashSumModule) {
                    CopyDirFiles($pathDirModuleAbs, $_SERVER['DOCUMENT_ROOT'] . $pathiDirPulic, true, true);
                    $update = true;
                }
            }
        }

        /** Синхронизация сущностей */
        $SynchronizeEntity = \COption::GetOptionString(self::$module_id, "SynchronizeEntity");
        if($SynchronizeEntity) {
        //if(false) {
            foreach ($listEntity as $entityName) {
                if(class_exists($entityName)) {
                    $entity = $entityName::getEntity();
                    if (!$db->isTableExists($entityName::getTableName())) { $entity->createDbTable(); $update = true; }
                    else { // Переобновление сущностей
                        $TableFieldsCode = array(); // Поля текущей таблицы
                        $TableFields = $db->getTableFields($entityName::getTableName());
                        foreach ($TableFields as $code => $field) { $TableFieldsCode[] = $code; }
                        $EntityFieldsCode = array(); // Поля сущности
                        $EntityFields = $entityName::getMap();
                        foreach ($EntityFields as $EnFields) {
                            if($EnFields->getDataType() != '\Bitrix\Main\User') {
                                $EntityFieldsCode[] = $EnFields->getName();
                            }
                        }
                        $addFields = array_diff($EntityFieldsCode,$TableFieldsCode); // Каких полей не хватает?
                        $removeFields = array_diff($TableFieldsCode,$EntityFieldsCode); // Лишние поля?
                        if(!empty($addFields) || !empty($removeFields)) {
                            $connection = \Bitrix\Main\Application::getConnection();
                            $connection->dropTable($entityName::getTableName());
                            $update = true;
                        }
                    }

                    $oUserTypeEntity = new \CUserTypeEntity();
                    $mapUF = $entityName::getMapUF();
                    if(!empty($mapUF)) {
                        foreach ($mapUF as $uf_field) {
                            if(!empty($uf_field)) {
                                $rsData = \CUserTypeEntity::GetList( array(), array(
                                    'ENTITY_ID' => $uf_field['ENTITY_ID'], 'FIELD_NAME' => $uf_field['FIELD_NAME']
                                ));
                                if(!($arRes = $rsData->Fetch())) { // Свойства не найдено, над добавить
                                    $iUserFieldId = $oUserTypeEntity->Add($uf_field); // int
                                    if(!$iUserFieldId) {
                                        /*echo 'Что то пошло не так =(';
                                        $errors[] = $APPLICATION->getException()->getString();
                                        echo '<pre>';
                                        print_r($errors);
                                        echo '</pre>';*/
                                    }
                                } else { // Обновление
                                    /*echo '<b>$uf_field</b>';
                                    echo '<pre>';
                                    print_r($uf_field);
                                    echo '</pre>';
                                    echo '<hr>';*/
                                }
                            }
                        }
                    }
                }
            }
        }

        //TODO: Установка DB чёт не работает в модуле =), придумал хак!
        foreach ($listEntity as $entityName) {
            if(class_exists($entityName)) {
                $entity = $entityName::getEntity();
                if (!$db->isTableExists($entity->getDBTableName())) { $entity->createDbTable(); $update = true; }
            }
        }

        /** Синхронизация сущностей HL */
        /*$Module = \CModule::CreateModuleObject(self::$module_id);
        $CHLBlock = $Module->GetNameSpace().'Classes\CHLBlock';
        $listEntityStructure = self::GetStructureAllEntityHL();
        $ListTables = $CHLBlock::GetListTables();
        foreach ($listEntityStructure as $entityHL_structure) {
            //...Определить наличие таблицы
            $DB_HL = $entityHL_structure['DB'];
            $ListTables = $CHLBlock::GetListTables(array('TABLE_NAME'=>$DB_HL['TABLE_NAME']));
            if(empty($ListTables)) {
                $CHLBlock::AddTable($DB_HL['NAME'],$DB_HL['TABLE_NAME'],$DB_HL['NAME_RU']);
                $update = true;
            } else {

            }
        }*/
        
        //...Получить все сущности HL
//        $Module = \CModule::CreateModuleObject(self::$module_id);
//        $CHLBlock = $Module->GetNameSpace().'Classes\CHLBlock';
//        $listEntityStructure = self::GetStructureAllEntityHL();
//        $ListTables = $CHLBlock::GetListTables();
//
//        foreach ($listEntityStructure as $entityHL_structure) {
//
//            /*echo '<b>$entityHL_structure</b>';
//            echo '<pre>';
//            print_r($entityHL_structure);
//            echo '</pre>';
//            echo '<hr>';*/
//
//            //...Определить наличие таблицы
//            $DB_HL = $entityHL_structure['DB'];
//            $ListTables = $CHLBlock::GetListTables(array('TABLE_NAME'=>$DB_HL['TABLE_NAME']));
//            if(empty($ListTables)) {
//                $CHLBlock::AddTable($DB_HL['NAME'],$DB_HL['TABLE_NAME'],$DB_HL['NAME_RU']);
//                $update = true;
//            } else {
//                if(isset($DB_HL['UF_FIELDS'])) {
//
//                    //region $UF_FIELDS - поля из файла
//                    $UF_FIELDS = $DB_HL['UF_FIELDS'];
//                    $UF_FIELDS_NEW = array();
//                    foreach ($UF_FIELDS as $UF_FIELD) {
//                        $UF_FIELD['FIELD_NAME'] = 'UF_'.$UF_FIELD['FIELD_NAME'];
//                        $UF_FIELDS_NEW[] = $UF_FIELD;
//                    }
//                    $UF_FIELDS = $UF_FIELDS_NEW;
//                    //endregion
//
//                    //region $UF_FIELDS_DB - поля из базы
//                    $table_id = $CHLBlock::GetID_ByTableName($DB_HL['TABLE_NAME']);
//                    $UF_FIELDS_DB = $CHLBlock::UF_Field_GetAllByTableID($table_id);
//                    //endregion
//
//                    //region Определить какие поля нужно DEL
//                    foreach ($UF_FIELDS_DB as $uf_field_db) { // Тех полей которых нет
//                        $del = true;
//                        foreach ($UF_FIELDS as $uf_field) {
//                            if($uf_field_db['FIELD_NAME'] == $uf_field['FIELD_NAME']) {
//                                $del = false;
//                            }
//                        }
//                        if($del) { $CHLBlock::UF_Field_Delete($uf_field_db['ID']); }
//                    }
//                    //endregion
//
//                    //region Определить какие поля нужно ADD
//                    foreach ($UF_FIELDS as $uf_field) {
//                        $add = true;
//                        foreach ($UF_FIELDS_DB as $uf_field_db) {
//                            if($uf_field_db['FIELD_NAME'] == $uf_field['FIELD_NAME']) {
//                                $add = false;
//                            }
//                        }
//                        if($add) {
//                            $name_ru = $uf_field['NAME'];
//                            unset($uf_field['NAME']);
//                            $uf_field['EDIT_FORM_LABEL']['ru'] = $name_ru;
//                            $uf_field['LIST_COLUMN_LABEL']['ru'] = $name_ru;
//                            $uf_field['LIST_FILTER_LABEL']['ru'] = $name_ru;
//                            $CHLBlock::UF_Field_Add($table_id,$uf_field);
//                        }
//                    }
//                    //endregion
//
//                    //region Определить какие поля нужно UPDATE
//                    //... самая сложная часть, если поле изменило тип или множественность то удаляется иначе обновляется
//                    //endregion
//                }
//            }
//        }

        //TODO: Если произошли какие то изменения(update/add файлы или сущности), то редирект на текущую страницу
        if($update) { LocalRedirect($page); }

        //TODO: Подключение JS библиотек
        self::initCJSCore();

        //region Подключение к пунктам меню

        /*global $BX_MENU_CUSTOM;
        $BX_MENU_CUSTOM->AddItem('left', array(
            'TEXT' => 'Назначенные курсы',
            'LINK' => '/services/learning/listsnapcourse.php',
            'SELECTED' => strstr($page,'/services/learning/listsnapcourse.php') ? true : false,
            'PERMISSION' => 'X',
            'ADDITIONAL_LINKS' => array(),
            'ITEM_TYPE' => 'P',
            'ITEM_INDEX' => 6,
            'PARAMS' => array(),
            'DEPTH_LEVEL' => 1,
            'IS_PARENT' => false

        ));*/

        //Левое меню пользователя
        //        $newLink = array(
        //            'TEXT' => 'Резервирование переговорных',
        //            'LINK' => '/booking/',
        //            'ID' => 'WEBSOFT_BOOKING',
        //            'NEW_PAGE' => 'N'
        //        );
        //        $selfItems = \CUserOptions::GetOption("intranet", "left_menu_self_items_".SITE_ID);
        //
        //        $add = true;
        //        foreach ($selfItems as $item) { if($item['ID'] == $newLink['ID']) { $add = false; } }
        //
        //        if($add) {
        //            $selfItems[] = $newLink;
        //            \CUserOptions::SetOption("intranet", "left_menu_self_items_".SITE_ID, $selfItems);
        //        }

        //endregion
    }
    public static function initCJSCore() {

        global $APPLICATION;
        $page = $APPLICATION->GetCurPage();

        // Инициализация класса модальных окон везде
        self::AddCJSCore('MyPopupBX',array('core_viewer','disk'),true,false,true);

        // Инициализация класса для работы с Ajax
        $path_to_ajax_files = '/bitrix/ajax/'.self::$module_id.'/';
        $JS = '<script>BX.ready(function(){ BX.MyAjax = new BX.MyAjax("'.$path_to_ajax_files.'"); });</script>';
        self::AddCJSCore('MyAjax',array(),true,false,false,$JS);

        global $APPLICATION;
        $page = $APPLICATION->GetCurPage();
        if($page === '/bitrix/admin/websoft.blank_settings.php') {
            // Подключение стилей модальных окон если в настройках модуля
            $APPLICATION->SetAdditionalCSS("/bitrix/js/main/core/css/core_viewer.min.css");

            // Подключение классов JS для работы с модулем
            $JS = '<script>BX.ready(function(){ BX.FilesModule = new BX.FilesModule(); });</script>';
            self::AddCJSCore('FilesModule',array(),true,true,true,$JS);
        }

        // Пример подключения JS + Css + Lang через CJSCore
        //$JS = '<script>BX.ready(function(){ BX.TestJS = new BX.TestJS("Иван","Альков"); BX.TestJS.init(); });</script>';
        //self::AddCJSCore('FilesModule',array(/*'disk',*//*'ajax',*/),true,true,true,$JS);
    }

    /**
     * Добавление пунктов меню в менюшки CRM`ки
     * @param $items - ссылка на менюшки
     */
    public static function addItemsToCrmControlPanelBuild(&$items) {

        //region Левое меню пользователя
        $newLink = array(
            'TEXT' => 'Резервирование переговорных',
            'LINK' => '/booking/',
            'ID' => 'WEBSOFT_BOOKING',
            'NEW_PAGE' => 'N'
        );
        $selfItems = \CUserOptions::GetOption("intranet", "left_menu_self_items_".SITE_ID);

        $add = true;
        foreach ($selfItems as $item) { if($item['ID'] == $newLink['ID']) { $add = false; } }

        if($add) {
            $selfItems[] = $newLink;
            \CUserOptions::SetOption("intranet", "left_menu_self_items_".SITE_ID, $selfItems);
        }
        //endregion

        //region Менюха CRM
        /*echo '<pre>';
        print_r($selfItems);
        echo '</pre>';*/
        //CUserOptions::SetOption("intranet", "left_menu_self_items_".SITE_ID, $selfItems);
        /*$items[16]['NAME'] = 'Test';
        $items[16]['TITLE'] = 'Test 2';*/
        /*$items[] = array(
            'ID' => 'BOOKING',
            'MENU_ID' => 'menu_websoft_booking',
            'NAME' => 'Резервирование перегоорных',
            'TITLE' => 'Резервирование перегоорных',
            'URL' => '/booking/',
            'ICON' => 'booking'
        );*/
        //endregion
    }

    /**
     * Системаный метод, для создания своего расширения - JS + Css + Lang и ещё подключение расширений в Bitrix
     * такие как jquery, popup, date, bootsrap и другие, в скором времени будет подробное описание каждого расширения =)
     *
     * @param string $name - наименование файлов и ID вашего расширения (пример: $name.js,$name.php,$name.css)
     * @param array $rel - список расширений которые нужно подключить, например ('popup','jquery'), по умолчанию подключаются
     *                     'core','jquery','date','window' + ваши =)
     * @param bool $js - флаг, регистрировать js файла?
     * @param bool $css - флаг, регистрировать css файла?
     * @param bool $lang - флаг, регистрировать lang файла?
     * @param string $JS - JS код, который будет вызван после подключения
     */
    private static function AddCJSCore($name='',$rel = array(),$js = false,$css = false,$lang = false,$JS = '') {
        if($js) { $add['js'] = '/bitrix/js/'.self::$module_id.'/'.$name.'.js'; }
        if($css) { $add['css'] = '/bitrix/css/'.self::$module_id.'/'.$name.'.css'; }
        if($lang) { $add['lang'] = '/bitrix/js/'.self::$module_id.'/lang/'.LANGUAGE_ID.'/'.$name.'.php'; }
        $add['rel'] = array('core','jquery','date','window','popup'); // Базовые расширения, которые всегда
                                                       // пригодяться + можно свою подключить =)
                                                       // Ещё есть: disk, ajax, jquery, popup, ls, timer, tootip,
                                                       //          webrtc, json, input, fx, ws_progectssettings_main,
                                                       //          im_common, voximplant, mobile_voximplant, file_dialog
                                                       //          uploader, dd и наверно ещё много чего =)
                                                       //TODO:: Надо будет потом расписать про каждый по пдробней!!!
        // "skip_core" => false | true, -> При подключении расширения не требуется подключение core.js.
        if(!empty($rel)) {
            $add['rel'] = array_merge($add['rel'],$rel);
            $add['rel'] = array_unique($add['rel']);
        }
        \CJSCore::RegisterExt($name,$add);
        \CJSCore::Init($name);
        if($JS) { self::$asset->addString($JS); }
    }
    private static function hashDirectory($directory) {
        if (! is_dir($directory)) { return false; }
        $files = array();
        $dir = dir($directory);
        while (false !== ($file = $dir->read())) {
            if ($file != '.' and $file != '..') {
                $checkDir = $directory . $file;
                if (is_dir($checkDir)) { $files[] = self::hashDirectory($checkDir . '/'); }
                else { $files[] = md5_file($checkDir); }
            }
        }
        $dir->close();
        return md5(implode('', $files));
    }
    private static function TreeDirAndFiles($directory) {
        if (! is_dir($directory)) { return false; }
        $files = array();
        $dir = dir($directory);
        while (false !== ($file = $dir->read())) {
            if ($file != '.' and $file != '..') {
                $checkDir = $directory . $file;
                if (is_dir($checkDir)) { $files = array_merge($files,self::TreeDirAndFiles($checkDir . '/')); }
                else { $files[] = $directory . $file; }
            }
        }
        $dir->close();
        return $files;
    }
    private static function get_namespace($path) {
        $content = file_get_contents($path);
        $tokens = token_get_all($content);
        $namespace = '';
        for ($index = 0; isset($tokens[$index]); $index++) {
            if (!isset($tokens[$index][0])) {
                continue;
            }
            if (T_NAMESPACE === $tokens[$index][0]) {
                $index += 2; // Skip namespace keyword and whitespace
                while (isset($tokens[$index]) && is_array($tokens[$index])) {
                    $namespace .= $tokens[$index++][1];
                }
            }
            if (T_CLASS === $tokens[$index][0]) {
                $index += 2; // Skip class keyword and whitespace
                $fqcns[] = $namespace.'\\'.$tokens[$index][1];
                $namespace .= '\\'.$tokens[$index++][1];
            }
        }
        return '\\'.$namespace;
    }

    /** Публичные методы */

    //region User Bitrix -> Продублированы в классе \Blank\Classes\User
    public static function fullNameUser($USER_ID) {
        $rsUser = \CUser::GetByID($USER_ID);
        $arUser = $rsUser->Fetch();
        return \CUser::FormatName(\CSite::GetNameFormat(false), $arUser, true, false);
    }
    public static function getFieldUser($USER_ID,$CODE) {
        $rsUser = \CUser::GetByID($USER_ID);
        $arUser = $rsUser->Fetch();
        return $arUser[$CODE];
    }
    public static function getPathUser($USER_ID) {
        return \CComponentEngine::MakePathFromTemplate(
            \Bitrix\Main\Config\Option::get('intranet', 'path_user', '', SITE_ID), array('USER_ID' => $USER_ID)
        );
    }
    //endregion

    public static function covertDateToServer($date,$format = 'd.m.Y H:i:s') {
        $strtotime = strtotime($date);
        $time_string = ConvertTimeStamp($strtotime-\CTimeZone::GetOffset(), "FULL");
        return $time_string;
    }

    public static function dd($name,$mass) {
        global $USER;
        if($USER->GetID()==1) {
            echo '<b>'.$name.'</b>';
            echo '<pre>';
            print_r($mass);
            echo '</pre>';
            echo '<hr>';
        }
    }

    public static function SaveArrayToFile($abs_path, $data) {
        global $APPLICATION;
        $strArray = "\$data = ".var_export($data,true);
        $APPLICATION->SaveFileContent($abs_path, "<"."?\n".$strArray."\n?".">");
    }
    public static function GetArrayToFile($abs_path) {
        $data = Array();
        $io = \CBXVirtualIo::GetInstance();
        if ($io->FileExists($abs_path)) { include($io->GetPhysicalName($abs_path)); }
        return $data;
    }
    public static function GetStructureAllEntityHL() {
        $Module = \CModule::CreateModuleObject(self::$module_id);
        $path_entity_hl = $Module->dir.'/lib/entity_hl/structures/';
        $listEntityStructure = array();
        $TreeDirAndFiles = self::TreeDirAndFiles($path_entity_hl);
        foreach ($TreeDirAndFiles as $treeDirAndFile) {
            //if(strstr($treeDirAndFile,'_structure.php')) {
                $listEntityStructure[] = self::GetArrayToFile($treeDirAndFile);
            //}
        }
        return $listEntityStructure;
    }
    public static function GetAllPathFilesEntityHL() {
        $Module = \CModule::CreateModuleObject(self::$module_id);
        $path_entity_hl = $Module->dir.'/lib/entity_hl/';
        $listEntityFiles = array();
        $TreeDirAndFiles = self::TreeDirAndFiles($path_entity_hl);
        foreach ($TreeDirAndFiles as $treeDirAndFile) {
            $listEntityFiles[] = $treeDirAndFile;
        }
        return $listEntityFiles;
    }

    public static function isJson($string) {
        return ((is_string($string) &&
            (is_object(json_decode($string)) ||
                is_array(json_decode($string))))) ? true : false;
    }

    /**
     * По значение $number возвращает $one | $two | $five (можно использовать для генерации склонений)
     * Например:
     * <code>
     *    Handler::Decline(1,'минута','минуты','минут'); // вернёт 1 минута
     *    Handler::Decline(2,'минута','минуты','минут'); // вернёт 2 минуты
     *    Handler::Decline(5,'минута','минуты','минут'); // вернёт 5 минут
     * </code>
     * @param $number - рассматриваемое значение
     * @param string $one -
     * @param string $two -
     * @param string $five -
     * @return string - результат
     */
    public static function Decline($number, $one, $two, $five) {
        if (($number - $number % 10) % 100 != 10) {
            if ($number % 10 == 1) {
                $result = $one;
            } elseif ($number % 10 >= 2 && $number % 10 <= 4) {
                $result = $two;
            } else {
                $result = $five;
            }
        } else {
            $result = $five;
        }
        return $result;
    }



}