<?php

namespace Websoft\Blank\AddToMenu;

use Bitrix\Main\Localization\Loc;

class CrmMenu{
    public static function addItems(&$items){

        //TODO: Системные данные
        $dir = dirname(__FILE__);
        for ($i = 1; $i < ($level = 3); $i++) { $dir = dirname($dir); }
        $module_id = basename($dir);
        $defOpt = \Bitrix\Main\Config\Option::getDefaults($module_id);
        $class_up = $defOpt['class_up'].'_MENU_ITEM_';
        $items[] = array(
            'ID' => 'STORES',
            'MENU_ID' => 'menu_crm_stores',
            'NAME' => Loc::getMessage($class_up.'ENTITY'),
            'TITLE' => Loc::getMessage($class_up.'ENTITY'),
            'URL' => '/crm/stores/'
        );
    }
}