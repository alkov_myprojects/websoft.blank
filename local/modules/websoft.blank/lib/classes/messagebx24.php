<?php
namespace Websoft\Blank\Classes;
class MessageBX24 {
    public static function SendMessage($ID_USER_TO,$TITLE,$TEXT,$form = '') {
        if(!\CModule::IncludeModule("im")) return false;
        $arFields = array(
            //"MESSAGE_TYPE" => "S",
            "MESSAGE_TYPE" => "S", # P - private chat, G - group chat, S - notification
            "TO_USER_ID" => $ID_USER_TO,
            "FROM_USER_ID" => $form ? $form : 479,
            "NOTIFY_MESSAGE" => 'html',
            "MESSAGE" => $TEXT,
            "AUTHOR_ID" => $form ? $form : 479,
            "EMAIL_TEMPLATE" => "some",
            "NOTIFY_TYPE" => 4,
            "NOTIFY_MODULE" => "main",
            "NOTIFY_EVENT" => "IM_GROUP_INVITE",
            "NOTIFY_TITLE" => $TITLE, //"title to send email",
        );
        \CIMMessenger::Add($arFields);
    }
    public static function SendMessageToLiveLenta($user_id, $title,$text,$user_id_from = '') {

        CModule::IncludeModule("blog");
        CModule::IncludeModule("socialnetwork");

        // Отпрвить сообщение в живую ленту
        $arBlog = CBlog::GetByOwnerID($user_id);

        $arFields= array(
            "TITLE" => $title,
            //"DETAIL_TEXT_TYPE" => 'html', //"Описание",
            "DETAIL_TEXT" => $text, //"Описание",
            "DATE_PUBLISH" => date('d.m.Y H:i:s'),
            "PUBLISH_STATUS" => "P",
            "CATEGORY_ID" => "",
            "PATH" => "/company/personal/user/1/blog/#post_id#/",
            "URL" => "admin-blog-s1",
            "PERMS_POST" => Array(),
            "PERMS_COMMENT" => Array (),
            "SOCNET_RIGHTS" => Array(
                "UA", // Для всех пользователей
                "G2", // Для группы пользователей с ID = 2
                //"U2", // Для конкретного пользователя с ID = 2
            ),
            "=DATE_CREATE" => "now()",
            "AUTHOR_ID" => $user_id, //$USER->GetID(),
            "BLOG_ID" => $arBlog['ID'],
        );

        $newID= CBlogPost::Add($arFields);

        $arFields["ID"] = $newID;
        $arParamsNotify = Array(
            "bSoNet" => true,
            "UserID" => $user_id, //$USER->GetID(),
            "user_id" => $user_id, //$USER->GetID(),
        );

        CBlogPost::Notify($arFields, array(), $arParamsNotify);
    }
    public static function SendMesIn_LL_to_User($ID_FROM,$ID_TO,$title,$text) {
        CModule::IncludeModule("blog");
        CModule::IncludeModule("socialnetwork");

        // Отпрвить сообщение в живую ленту
        $arBlog = CBlog::GetByOwnerID($ID_FROM);

        $arFields= array(
            "TITLE" => $title,
            "DETAIL_TEXT_TYPE" => 'html', //"Описание",
            "ALLOW_HTML" => 'Y', //"Описание",
            "DETAIL_TEXT" => $text, //"Описание",
            "DATE_PUBLISH" => date('d.m.Y H:i:s'),
            "PUBLISH_STATUS" => "P",
            "CATEGORY_ID" => "",
            "PATH" => "/company/personal/user/1/blog/#post_id#/",
            "URL" => "admin-blog-s1",
            "PERMS_POST" => Array(),
            "PERMS_COMMENT" => Array (),
            "SOCNET_RIGHTS" => Array(
                "U".$ID_TO
                //"UA", // Для всех пользователей
                //"G2", // Для группы пользователей с ID = 2
                //"U2", // Для конкретного пользователя с ID = 2
            ),
            "=DATE_CREATE" => "now()",
            "AUTHOR_ID" => $ID_FROM, //$USER->GetID(),
            "BLOG_ID" => $arBlog['ID'],
        );

        $newID= CBlogPost::Add($arFields);

        $arFields["ID"] = $newID;
        $arParamsNotify = Array(
            "bSoNet" => true,
            "UserID" => $ID_FROM, //$USER->GetID(),
            "user_id" => $ID_FROM, //$USER->GetID(),
            "allowHTML" => 'Y', //$USER->GetID(),
        );

        CBlogPost::Notify($arFields, array(), $arParamsNotify);
    }
}