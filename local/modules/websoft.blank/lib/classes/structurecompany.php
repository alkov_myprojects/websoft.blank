<?php
namespace Websoft\Blank\Classes;
class StructureCompany {

    //region публичные методы

    /**
     * Получить список пользователей из департаментов( разделов инфоблока )
     * @param array $department_ids - массив ID-шников департаментов( ID разделов )
     * @param array $select - ассоциативный массив с ключами которые нужно извлекать
     * @return array - Массив пользователей с запрошенными полями из $select
     */
    public static function GetDepartmentEmployees($department_ids = array(),$select = array( 'ID' )) {
        $result = array();
        if(!empty($department_ids)) {
            $result = UserTable::getList(array(
                'filter' => array( 'UF_DEPARTMENT' => $department_ids ),
                'select' => $select
            ))->fetchAll();
        }
        return $result;
    }

    /**
     * Получить ID руководителя отдела по ID сотрудника (пользователь Bitrix)
     * @param $USER_ID - ID пользователя Bitrix
     * @return int
     */
    public static function GetDepartmentManagerByEmployeeID($USER_ID) {
        $Department = self::GetDepartmentByUserID($USER_ID);
        return \CUser::GetByID($Department['UF_HEAD'])->Fetch();
    }

    /**
     * Получить отдел пользователя (пользователь является сотрудником отдела или его руководителем)
     * @param $user_id - ID пользователя Bitrix
     * @param $arSelect - Массив для выборки информации о разделе
     * @return array || false - ID отдела (раздел инфоблока со структурой компании)
     */
    public static function GetDepartmentByUserID($user_id,$arSelect = array('ID','UF_HEAD')) {
        $arUser = \CUser::GetByID($user_id)->Fetch();
        return self::GetDepartamentByID($arUser['UF_DEPARTMENT'][0],$arSelect);
    }

    //endregion

    //region приватные медоты
    
    /**
     * Полуить ID инфоблока где лежит структура компании
     * @return int || bool - ID инфоблока || false
     */
    private static function GetDepartamentIblockID() {
        $res = \CIBlock::GetList( Array(), Array('TYPE'=>'structure','CODE'=>'departments' ), true );
        if($ar_res = $res->Fetch()) { return $ar_res['ID']; }
    }

    /**
     * Получить отдел
     * @param $DepartamentID - ID раздела инфоблока
     * @param array $arSelect - те поля которые нужно извлечь
     * @return array || false
     */
    private static function GetDepartamentByID($DepartamentID,$arSelect = array('ID','UF_HEAD')) {
        $IBLOCK_DEPARTAMENT = self::GetDepartamentIblockID();
        $arFilter = array( 'IBLOCK_ID' => $IBLOCK_DEPARTAMENT, 'ID' => $DepartamentID );
        return \CIBlockSection::GetList(array(),$arFilter,false,$arSelect)->Fetch();
    }

    //endregion
}