<?php

namespace Websoft\Blank\Classes;

/**
 * Class Calendar - тестовое описание для парсера
 * @package Websoft\Blank\Classes
 */
abstract class Calendar {

    //region Тип события
    private static $Types = array(
        0 => 'Простое',
        1 => 'Переодичное N раз',
        2 => 'Переодичное до конкретной даты',
    );
    public static  function GetTypes() { return self::$Types; }
    //endregion

    /**
     * @param $ID
     * @return mixed
     */
    public function GetEventByID($ID) {

        $date_str = $_REQUEST['DATE'];
        $event = \CCalendarEvent::GetById($ID,false);
        $TIME_START = date("H:i:s",\CCalendar::Timestamp($event['DATE_FROM']));
        $TIME_END = date("H:i:s",\CCalendar::Timestamp($event['DATE_TO']));
        $result = array(
            'Type' => '',        // self::GetTypes(); - один из индексов возвращаемого массива
            'When' => '',        // время начала события
            'Periodicity' => '', // описание повторения
        );

        //region Определить какое это событие:
            $rrule = \CCalendarEvent::ParseRRULE($event['RRULE']);

            //region Простое
            if(empty($rrule)) {
                $result['Type'] = 0;
                $result_L = self::GenerateCustomDescriptionEvent($event,$event['DATE_FROM'],$event['DATE_TO']);
                foreach ($result_L as $key => $val) { $result[$key] = $val; }
            }
            //endregion

            //region Переодичное -> N раз
            elseif(isset($rrule['COUNT'])) {
                $result['Type'] = 1;
                $result_L = self::GenerateCustomDescriptionEvent(
                    $event,$date_str.' '.$TIME_START,$date_str.' '.$TIME_END
                );
                foreach ($result_L as $key => $val) { $result[$key] = $val; }
            }
            //endregion

            //region Переодичное -> До конкретной даты
            else {
                $result['Type'] = 2;
                $result_L = self::GenerateCustomDescriptionEvent(
                    $event,$date_str.' '.$TIME_START,$date_str.' '.$TIME_END
                );
                foreach ($result_L as $key => $val) { $result[$key] = $val; }
            }
            //endregion

        //endregion

        $event['CUSTOM_DATA'] = $result;
        return $event;
    }
    private static function GenerateCustomDescriptionEvent($event,$DATE_FROM,$DATE_TO) {
        $result = array();
        $fromTs = \CCalendar::Timestamp($DATE_FROM);
        $toTs = \CCalendar::Timestamp($DATE_TO);
        $skipTime = $event['DT_SKIP_TIME'] == "Y";
        if ($skipTime) { $toTs += \CCalendar::DAY_LENGTH; }
        if (!$skipTime) { $fromTs -= $event['~USER_OFFSET_FROM']; $toTs -= $event['~USER_OFFSET_TO']; }
        $fromTs = date("d.m.Y H:i:s", $fromTs);
        $toTs = date("d.m.Y H:i:s", $toTs);
        $result['When'] = \CCalendar::GetFromToHtml($fromTs, $toTs, $event['DT_SKIP_TIME'] == 'Y', $event['DT_LENGTH']);
        $Periodicity = \CCalendarEvent::GetRRULEDescription($event, true);
        $Periodicity = str_replace('<br>',' ',$Periodicity);
        $result['Periodicity'] = $Periodicity;
        return $result;
    }
    public static function changeDateEvent($id,$dateStart,$dateEnd) {
        $errors = array();
        if(\CModule::IncludeModule("calendar")) {
            //... Определить тип события (Обычное/Переодичное)
            //$calendarEvent = \CCalendarEvent::GetById($id, false);
            //if($calendarEvent['RRULE'] == '') {
            self::updateEvent($id,array(
                'DATE_FROM' => $dateStart,
                'DATE_TO' => $dateEnd,
            ));
            /*} else {
                echo 'Переодичное событие для него пока нет алгоритма...';
            }*/
        }
        return $errors;
    }


    public static function addEvent($name,$description,$owner_id,$users,$dateBegin,$dateEnd,$rrule,$remind) {
        if(\CModule::IncludeModule("calendar")) {

            // 0) Результирующий массив с ID-шниками созданных событий
            //$result = array();

            // 1) Входящие данные
            //$negId = 1; // ID переговорной (наш модуль)
            //$owner_id = 1; // ID создателя
            //$users = array(1, 254, 11); // Список пользователей
            //$dateBegin = '25.09.2018 12:00:00'; // Дата начала
            //$dateEnd = '25.09.2018 14:00:00'; // Дата окончания
            //$name = 'Наименование мероприятия'; // Наименование мероприятия
            //$description = 'Описание мероприятия'; // Описание мероприятия

            // 2) Конвертирование времени (считаем что входящие дата начала/окочание берётся из настроек пользовате и преобразуем
            // их во время сервера)
            //$dateBegin = self::convertDateToTimeServer($dateBegin);
            //$dateEnd = self::convertDateToTimeServer($dateEnd);

            // 3) Добавить родительское событие
            $objDateTime = new \Bitrix\Main\Type\DateTime();
            $tz = $objDateTime->getTimeZone()->getName();

            $arEventFields = array(
                'CAL_TYPE' => 'user',
                'OWNER_ID' => $owner_id,
                'DT_FROM' => $dateBegin,
                //'TIMESTAMP_X' => $dateBegin,
                'DT_TO' => $dateEnd,
                'TZ_FROM' => $tz,
                'TZ_TO' => $tz,
                'NAME' => $name,
                'DESCRIPTION' => \CCalendar::ParseHTMLToBB($description),
                'IS_MEETING' => true,
                'MEETING_HOST' => $owner_id,
                'MEETING' => array( 'HOST_NAME' => \CCalendar::GetUserName($owner_id) ),
                'ATTENDEES' => $users,
                //'LOCATION' => 'ECMR_'.$meeting_id, // Воркает
                /*'LOCATION' => array(
                    'OLD' => '',
                    'NEW' => 'ECMR_'.$meeting_id.'_'.$bargaining_id,
                )*/

                /*'RRULE' => array(
                    'FREQ' => "DAILY", // NONE - Не повторять
                                       // DAILY - Каждый день
                                       // MONTHLY - Каждый месяц
                                       // WEEKLY - Каждую неделю
                                       // YEARLY - Каждый год
                    'INTERVAL' => '2'  // Интервал
                    'COUNT' => 2,      // Кол-во повторений
                    'UNTIL' => ''      // Дата остановки (только дата)
                )*/
                // $_REQUEST['rrule_endson'] // never||count||until

                //region Напоминать
                /*'REMIND' => Array(
                    Array( 'type' => 'min', 'count' => '0'),    // В момент события
                    Array( 'type' => 'min', 'count' => '10'),   // За 10 минут
                    Array( 'type' => 'min', 'count' => '15'),   // За 15 минут
                    Array( 'type' => 'min', 'count' => '30'),   // За 30 минут
                    Array( 'type' => 'min', 'count' => '60'),   // За час
                    Array( 'type' => 'min', 'count' => '120'),  // За 2 часа
                    Array( 'type' => 'min', 'count' => '120'),  // За 2 часа
                    Array( 'type' => 'min', 'count' => '1440'), // За сутки
                    Array( 'type' => 'min', 'count' => '2880'), // За сутки
                ),*/
                //endregion
            );

            if($rrule) { $arEventFields['RRULE'] = $rrule; }
            if($remind) { $arEventFields['REMIND'] = $remind; }

            $EventID = \CCalendar::SaveEvent(array(
                'arFields'=>$arEventFields,
                'userId'=>$owner_id,
                'autoDetectSection'=>true,
                'autoCreateSection'=>true
            ));



            return $EventID;
        }
    }


    public static function removeEvent($id) {
        if (\CModule::IncludeModule("calendar")) {

            $event = \Websoft\Booking\Classes\CustomEventsCalendar::GetEventByID($id);
            $LOCATION = explode('_',$event['LOCATION']);
            if(!\CIBlockElement::Delete($LOCATION[2])) {
                echo 'Ошибка удаления места проведения в событии календаря';
            }
            \CCalendar::DeleteEvent($id, false);
        }
    }
    public static function updateEvent($ID,$data,$recursionEditMode = '') { //
        if(\CModule::IncludeModule("calendar")) {
            $arFields = $data;
            $arFields['ID'] = $ID;
            //$arFields['NAME'] = $ID; // string
            $newId = \CCalendar::SaveEvent(array(
                'arFields' => $arFields,
                //'UF' => $arUFFields,
                //'silentErrorMode' => false,
                'recursionEditMode' => $recursionEditMode, //$_REQUEST['rec_edit_mode'], // 'this' - Только это || 'next' || -
                //'currentEventDateFrom' => CCalendar::Date(CCalendar::Timestamp(self::$request['current_date_from']), false)
            ));
        }
    }
}