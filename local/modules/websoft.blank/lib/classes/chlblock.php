<?php
namespace Websoft\Blank\Classes;

//region IncludeModule
\CModule::IncludeModule('iblock');
\CModule::IncludeModule('highloadblock');
//endregion

use \Bitrix\Main\Entity;
use \Bitrix\Main\SystemException;

/**
 * Class CHLBlock
 * @package Websoft\Blank\Classes
 */
abstract class CHLBlock {

    /**
     * Тестовое описание <b><i>список пользовательских полей</b> по фильтру <b>$filter</b> с сортировкой <b>$order</b>.
     *
     * @param $a string переменная БЕЗ типа данные
     * @param string $b переменная C типа данные
     * @param string $g - переменная C типа данные
     * @param mixed $g2 - переменная C типа данные
     * @param mixed[] $g3 - переменная C типа данные
     * @param $c
     * @param string $d
     * @param $e string
     * @return string Результат
     */
    static function test($a,$b,$g,$g2,$g3,$c,$d,$e) {
        return '';
    }

    //region Table

    static function GetID_ByTableName($tableName) {
        if($tableName) {
            $rsData = \Bitrix\Highloadblock\HighloadBlockTable::getList(array(
                'filter'=>array('TABLE_NAME'=>$tableName), 'select'=>array('ID')
            ));
            if( $item = $rsData->fetch() ){ return $item['ID']; }
        }
    }
    static function GetListTables($filter = array()){
        $result = array();
        $rsData = \Bitrix\Highloadblock\HighloadBlockTable::getList(array(
            'filter' => $filter, // массив фильтров в части WHERE запроса в виде: "(condition)field"=>"value".
            //'select' => array(), // массив фильтров в части SELECT запроса, алиасы возможны в виде: "alias"=>"field".
            //'group' => array(), // массив полей в части GROUP BY запроса.
            //'order' => array(), // массив полей в части ORDER BY запроса в виде: "field"=>"asc|desc".
            //'limit' => 1,  // целое число, указывающее максимальное число столбцов в выборке (Подобно LIMIT n в MySql)
            //'offset' => 1, // целое число, указывающее номер первого столбца в результате. (Подобно LIMIT n, 100 в MySql)
            //'runtime' => array(), // массив полей сущности, создающихся динамически.
        ));
        while ( $hldata = $rsData->fetch() ){
            $hlentity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hldata);
            $result[] = $hldata;
        }
        return $result;
    }

    static function AddTable($NAME,$TABLE_NAME,$NAME_RU = '') {

        $result = '';
        try {

            $resultAdd = \Bitrix\Highloadblock\HighloadBlockTable::add(array(
                'NAME' => $NAME, // должно начинаться с заглавной буквы и состоять только из латинских букв и цифр
                'TABLE_NAME' => $TABLE_NAME,// должно состоять только из строчных латинских букв, цифр и знака подчеркивания
            ));

            if (!$resultAdd->isSuccess()) {
                $result = $resultAdd->getErrorMessages();
            } else {
                $result = $resultAdd->getId();
                \Bitrix\Highloadblock\HighloadBlockLangTable::add(array( 'ID'=>$result, 'LID'=>'ru', 'NAME'=>$NAME_RU ));
            }

            return $result;

            // ...
            throw new SystemException("Error");
        }
        catch (SystemException $exception) {
            echo $exception->getMessage();
        }

        return $result;
    }
    static function UpdateTable($ID,$NAME,$TABLE_NAME,$NAME_RU = '') {
        \Bitrix\Highloadblock\HighloadBlockTable::update( $ID, array(
            'NAME' => $NAME, // должно начинаться с заглавной буквы и состоять только из латинских букв и цифр
            'TABLE_NAME' => $TABLE_NAME // должно состоять только из строчных латинских букв, цифр и знака подчеркивания
        ));
        \Bitrix\Highloadblock\HighloadBlockLangTable::update($ID,array( 'LID'=>'ru', 'NAME'=>$NAME_RU ));
    }
    static function DelTableByID($ID) { \Bitrix\Highloadblock\HighloadBlockTable::delete($ID); }

    //endregion

    //region UF_*

    /**
     * Тестовое описание
     * @param array $table_id - 123
     * @return array - реезультат
     */
    static function UF_Field_GetAllByTableID( $table_id ) {
        return self::UF_Field_GetList(
            array( 'ENTITY_ID' => 'HLBLOCK_'.$table_id )
        );
    }

    /** Возвращает <b><i>список пользовательских полей</b> по фильтру <b>$filter</b> с сортировкой <b>$order</b>.
     *
     * @param array $filter - <p>Массив вида array("фильтруемое поле" => "значение" [, ...]). Может принимать значения:</p>
     *                          <ul>
     *                              <li><b>ID</b> - ID пользовательского поля;</li>
     *                              <li>
     *                                  <b>ENTITY_ID</b> - название объекта, которому принадлежит пользовательское поле.
     *                                  Напр: "ENTITY_ID" => "IBLOCK_".$iblock_id."_SECTION";
     *                              </li>
     *                              <li><b>FIELD_NAME</b> - Название поля;</li>
     *                              <li><b>SORT</b> - Значение сортировки;</li>
     *                              <li><b>USER_TYPE_ID</b></li>
     *                              <li><b>XML_ID</b></li>
     *                              <li><b>MULTIPLE</b> - Множественность свойства;</li>
     *                              <li><b>MANDATORY</b></li>
     *                              <li><b>SHOW_FILTER</b></li>
     *                              <li><b>SHOW_IN_LIST</b></li>
     *                              <li><b>EDIT_IN_LIST</b></li>
     *                              <li><b>IS_SEARCHABLE</b></li>
     *                              <li><b>LANG</b> - ID языка</li>
     *                          </ul>
     *
     *                          <i>
     *                          Внимание! При не указание в фильтре ключа LANG со значением необходимого
     *                          языка ('LANG' => 'ru'), поля "Подпись в форме редактирования", "Заголовок в списке"
     *                          и т.д. не будут участвовать в выборке. ([EDIT_FORM_LABEL], [LIST_COLUMN_LABEL],
     *                          [LIST_FILTER_LABEL], [ERROR_MESSAGE], [HELP_MESSAGE])
     *                          Необязательное. По умолчанию записи не фильтруются.
     *                          </i>
     *                          <br>
     *
     * @param array $order - Массив полей для сортировки, содержащий пары поле сортировки => направление сортировки.
     *                       Поля сортировки:
     *                          <ul>
     *                              <li><b>ID </b> - ID пользовательского поля</li>
     *                              <li>
     *                                  <b>ENTITY_ID </b> - Код объекта, которому принадлежит пользовательское поле.
     *                              </li>
     *                              <li><b>FIELD_NAME </b> - Символьный код</li>
     *                              <li><b>USER_TYPE_ID</b> - Тип данных (можно задать только для нового поля)</li>
     *                              <li><b>XML_ID</b> - Уникальный идентификатор</li>
     *                              <li>
     *                                  <b>SORT</b> - направление сорткровки ASC - по возрастанию, DESC - по убыванию.
     *                              </li>
     *                          </ul>
     *
     * @return array - результат
     */
    static function UF_Field_GetList( $filter = array(), $order = array() ) {
        $result = array();
        $rsData = \CUserTypeEntity::GetList( $order, $filter );
        while($arRes = $rsData->Fetch()) { $result[] = $arRes; }
        return $result;
    }
    static function UF_Field_Add( $ID_HL, $Fields ) {
        $oUserTypeEntity = new \CUserTypeEntity();
        $aUserFields    = array(
            'ENTITY_ID'         => 'HLBLOCK_'.$ID_HL,
            /*
            'FIELD_NAME'        => 'UF_MYFIELD',
            'USER_TYPE_ID'      => 'string',
            'MULTIPLE'          => 'N',
            'MANDATORY'         => 'N',
            'SHOW_FILTER'       => 'I',
            'EDIT_FORM_LABEL'   => array(
                'ru'    => 'Моё поле',
                'en'    => 'My field',
            )
            */
        );
        $aUserFields = array_merge($aUserFields,$Fields);
        $oUserTypeEntity->Add($aUserFields);

        global $APPLICATION;
        $errors = $APPLICATION->GetException();
        if($errors) {
            return $errors->GetMessages();
        }

        return $errors;
    }
    static function UF_Field_Delete( $ID ) {
        $oUserTypeEntity = new \CUserTypeEntity();
        $oUserTypeEntity->Delete( $ID );
    }
    static function UF_Field_Update( $ID, $Fields ) {
        $oUserTypeEntity = new \CUserTypeEntity();
        $res = $oUserTypeEntity->Update( $ID, $Fields );

        echo '<b>$res</b>';
        echo '<pre>';
        print_r($res);
        echo '</pre>';
        echo '<hr>';
        
        global $APPLICATION;
        $error = $APPLICATION->GetException();

        echo '<b>$error</b>';
        echo '<pre>';
        print_r($error);
        echo '</pre>';
        echo '<hr>';
    }
    //endregion
    
    //region Element
    static function AddElement($arrElement,$table_name) {
        // Подключаемся к таблице по её имени
        $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getList(
            array( "filter" => array('TABLE_NAME' => $table_name) )
        )->fetch();

        $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock); // Получим объект - сущность
        //Создадим объект DataClass
        $DataClass = $entity->getDataClass();
        $result=$DataClass::add($arrElement);
        return $result->getId();
    }
    static function GetListElements($tableName,$filter = array(),$order = array(),$select = array('*')){
        $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getList(
            array( "filter" => array('TABLE_NAME' => $tableName) )
        )->fetch();
        $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock); // Получим объект - сущность
        $query = new \Bitrix\Main\Entity\Query($entity); //Создадим объект - запрос
        $query->setSelect($select); //Зададим параметры запроса, любой параметр можно опустить
        //Дополнительные запросы из доков, вдруг пригодятся:
        // if ($limit) {
        //     $limit = array(
        //      'nPageSize' => $limit,
        //   );
        //
        //   $query->setLimit($limit['nPageSize']);
        // }
        if($filter) {
            $query->setFilter($filter);
        }
        if($order) {
            $query->setOrder($order);
        }

        $result = $query->exec(); //Выполним запрос
        $result = new \CDBResult($result); //Получаем результат по привычной схеме
        $arRes = array();
        while ($row = $result->Fetch()) {
            $arRes[] = $row; //self::print_masAndMessage('<br><br>Элемент таблицы<br><br>',$row);
        }

        return $arRes;
    }
    static function GetElementByID($tableName,$ID) {
        $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getList(
            array( "filter" => array('TABLE_NAME' => $tableName) )
        )->fetch();
        $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock); // Получим объект - сущность
        $query = new \Bitrix\Main\Entity\Query($entity); //Создадим объект - запрос
        $query->setSelect(array('*')); //Зададим параметры запроса, любой параметр можно опустить
        //Дополнительные запросы из доков, вдруг пригодятся:
        // if ($limit) {
        //     $limit = array(
        //      'nPageSize' => $limit,
        //   );
        //
        //   $query->setLimit($limit['nPageSize']);
        // }
        $query->setFilter(array('ID' => $ID));
        $result = $query->exec(); //Выполним запрос
        $result = new \CDBResult($result); //Получаем результат по привычной схеме
        if ($row = $result->Fetch()) {
            return $row; //self::print_masAndMessage('<br><br>Элемент таблицы<br><br>',$row);
        }
        return false;
    }
    static function DelElement($ID,$table_name)
    {
        // Подключаемся к таблице по её имени
        $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getList(
            array( "filter" => array('TABLE_NAME' => $table_name) )
        )->fetch();

        $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock); // Получим объект - сущность
        //Создадим объект DataClass
        $DataClass = $entity->getDataClass();
        $DataClass::Delete($ID);
    }
    static function SaveElement($id,$arrElement,$table_name) {
        // Подключаемся к таблице по её имени
        $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getList(
            array( "filter" => array('TABLE_NAME' => $table_name) )
        )->fetch();

        $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock); // Получим объект - сущность
        //Создадим объект DataClass
        $DataClass = $entity->getDataClass();
        $DataClass::update($id, $arrElement);
    }
    //endregion

    //region Element OLD
    static function ___GetListElements($tableName,$filter = array(),$order = array(),$select = array('*')){
        $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getList(
            array( "filter" => array('TABLE_NAME' => $tableName) )
        )->fetch();
        $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock); // Получим объект - сущность
        $query = new \Bitrix\Main\Entity\Query($entity); //Создадим объект - запрос
        $query->setSelect($select); //Зададим параметры запроса, любой параметр можно опустить
        //Дополнительные запросы из доков, вдруг пригодятся:
        // if ($limit) {
        //     $limit = array(
        //      'nPageSize' => $limit,
        //   );
        //
        //   $query->setLimit($limit['nPageSize']);
        // }
        if($filter) {
            $query->setFilter($filter);
        }
        if($order) {
            $query->setOrder($order);
        }

        $result = $query->exec(); //Выполним запрос
        $result = new CDBResult($result); //Получаем результат по привычной схеме
        $arRes = array();
        while ($row = $result->Fetch()) {
            $arRes[] = $row; //self::print_masAndMessage('<br><br>Элемент таблицы<br><br>',$row);
        }

        return $arRes;
    }
    static function ___SaveElement($id,$arrElement,$table_name) {
        // Подключаемся к таблице по её имени
        $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getList(
            array( "filter" => array('TABLE_NAME' => $table_name) )
        )->fetch();

        $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock); // Получим объект - сущность
        //Создадим объект DataClass
        $DataClass = $entity->getDataClass();
        $DataClass::update($id, $arrElement);
    }
    static function ___AddElement($arrElement,$table_name) {
        // Подключаемся к таблице по её имени
        $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getList(
            array( "filter" => array('TABLE_NAME' => $table_name) )
        )->fetch();

        $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock); // Получим объект - сущность
        //Создадим объект DataClass
        $DataClass = $entity->getDataClass();
        $result=$DataClass::add($arrElement);
        return $result->getId();
    }
    static function ___DelElement($ID,$table_name)
    {
        // Подключаемся к таблице по её имени
        $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getList(
            array( "filter" => array('TABLE_NAME' => $table_name) )
        )->fetch();

        $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock); // Получим объект - сущность
        //Создадим объект DataClass
        $DataClass = $entity->getDataClass();
        $DataClass::Delete($ID);
    }
    //endregion

}