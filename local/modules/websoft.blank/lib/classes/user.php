<?php

namespace Websoft\Blank\Classes;
class User {


    //region Class instance
    private $ID;
    private $fields;
    public function __construct($ID) {
        $this->ID = $ID;
    }
    public function GetID() {
        return $this->ID;
    }
    public function GetAllFields() {
        if(!$this->fields) { $this->fields = self::GetByID($this->ID); }
        return $this->fields;
    }
    public function GetFieldByCode($CODE) {
        return $this->GetAllFields()[$CODE];
    }
    public function GetFullName() {
        return \CUser::FormatName(\CSite::GetNameFormat(false), $this->GetAllFields(),true,false);
    }
    //endregion

    //region Static
    public static function GetByID($ID) {
        return \CUser::GetByID($ID)->Fetch();
    }
    public static function getFieldUser($USER_ID,$CODE) {
        $rsUser = \CUser::GetByID($USER_ID);
        $arUser = $rsUser->Fetch();
        return $arUser[$CODE];
    }
    public static function getPathUser($USER_ID) {
        return \CComponentEngine::MakePathFromTemplate(
            \Bitrix\Main\Config\Option::get('intranet', 'path_user', '', SITE_ID), array('USER_ID' => $USER_ID)
        );
    }
    //endregion

    //region События
    public static function OnBeforeUserUpdate(&$arFields) {

        $debug = array();

        //region debug
        if(!empty($debug)) {
            \Bitrix\Main\Diag\Debug::writeToFile($debug, "", "User_OnBeforeUserUpdate.txt");
        }
        //endregion

    }
    //endregion

}