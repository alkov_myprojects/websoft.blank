<?php

namespace Websoft\Blank\Classes;

use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Entity\DatetimeField;
use Bitrix\Main\Entity\FloatField;
use Bitrix\Main\Entity\ScalarField;
use Bitrix\Main\Entity\EnumField;
use Bitrix\Main\Entity\BooleanField;   // TODO:: Булеан
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Security\Random;

/**
 * Class AEntity
 * @package Websoft\Blank\Entity базовый класс для сущностей помогает запихнуть их в комплексный компонент (
 *                               грид + просмотр + редактирование)
 */
abstract class EntityBase extends DataManager {

    //region Пользовательские свойства для сущности UF

    /**
     * Описание пользотельских полей сущности
     * @return array
     */
    abstract static function getMapUF();

    protected static function UF_Integer(
        $code ='',$multiple = false,$required = false,
        $xml_id = '',$sort = 100,$defaultValue = '',$size = 20,$minLenght = 0,$maxLenght = 0
    ) {
        $settings = array(
            'DEFAULT_VALUE' => $defaultValue, /* Значение по умолчанию */
            'SIZE'          => $size,         /* Размер поля ввода для отображения */
            'MIN_LENGTH'    => $minLenght,    /* Минимальная длина строки (0 - не проверять) */
            'MAX_LENGTH'    => $maxLenght,    /* Максимальная длина строки (0 - не проверять) */
        );
        $keyLoc = $code;
        return self::UF_Field( self::getUfId(),$code,'integer',$xml_id,$multiple,$required,$keyLoc,$settings,$sort);
    }

    /**
     * Генерация массива UF_ типа "Строка"
     * @param $keyLoc        - Ключ по которому будут извлекаться языковые названия имени свойства
     * @param bool $multiple - Является поле множественным или нет
     * @param bool $required - Обязательное или нет свойство
     * @param string $code   - Код поля
     * @param string $xml_id - XML_ID пользовательского свойства. Используется при выгрузке в качестве названия поля
     * @param int $sort      - Сортировка
     * @param string $defaultValue - Значение по умолчанию
     * @param int $size - Размер поля ввода для отображения
     * @param int $rows - Количество строчек поля ввода
     * @param int $minLenghth - Минимальная длина строки (0 - не проверять)
     * @param int $maxLenghth - Максимальная длина строки (0 - не проверять)
     * @param string $regexp - Регулярное выражение для проверки
     * @return array
     */
    protected static function UF_String(
        $code ='',$multiple = false,$required = false,
        $xml_id = '',$sort = 100,$defaultValue = '',$size = 20,$rows = 1,$minLenghth = 0,$maxLenghth = 0,$regexp = ''
    ) {
        $settings = array(
            'DEFAULT_VALUE' => $defaultValue, /* Значение по умолчанию */
            'SIZE'          => $size,        /* Размер поля ввода для отображения */
            'ROWS'          => $rows,        /* Количество строчек поля ввода */
            'MIN_LENGTH'    => $minLenghth,  /* Минимальная длина строки (0 - не проверять) */
            'MAX_LENGTH'    => $maxLenghth,  /* Максимальная длина строки (0 - не проверять) */
            'REGEXP'        => $regexp,      /* Регулярное выражение для проверки */
        );
        $keyLoc = $code;
        return self::UF_Field( self::getUfId(),$code,'string',$xml_id,$multiple,$required,$keyLoc,$settings,$sort);
    }

    /**
     * Генерация массива UF_ типа "Файл"
     * @param bool $multiple - Является поле множественным или нет
     * @param bool $required - Обязательное или нет свойство
     * @param string $code - Код поля
     * @param string $xml_id - XML_ID пользовательского свойства. Используется при выгрузке в качестве названия поля
     * @param int $sort - Сортировка
     * @param integer $size - Размер поля ввода для отображения
     * @param integer $width -  Максимальные ширина д/отображения в списке
     * @param integer $height - Максимальные высота д/отображения в списке
     * @param integer т$maxSizeShow - Максимально допустимый размер для показа в списке (0 - не ограничивать)
     * @param integer т$maxSizeFile - Максимально допустимый размер файла для загрузки (0 - не проверять)
     * @param array $exception - Расширения
     * @return array
     */
    protected static function UF_File(
        $code,$multiple = false,$required = false, $xml_id = '',$sort = 100,$size = 20,$width = 200,
        $height = 200,$maxSizeShow = 0,$maxSizeFile = 0, $exception = array()
    ) {
        $settings = array(
            'SIZE' => $size, 'LIST_WIDTH' => $width, 'LIST_HEIGHT' => $height,
            'MAX_SHOW_SIZE' => $maxSizeShow, 'MAX_ALLOWED_SIZE' => $maxSizeFile, 'EXTENSIONS' => $exception,
        );
        $keyLoc = $code;
        return self::UF_Field( self::getUfId(),$code,'file',$xml_id,$multiple,$required,$keyLoc,$settings,$sort);
    }

    /**
     * Добавление пользовательского свойства (Общая реализация)
     * @param string $entity_id - Идентификатор сущности, к которой будет привязано свойство.
     *                            Для секция формат следующий - IBLOCK_{IBLOCK_ID}_SECTION
     * @param $code             - Код поля. Всегда должно начинаться с UF_
     * @param $type             - Указываем, что тип нового пользовательского свойства строка
     * @param string $xml_id    - XML_ID пользовательского свойства. Используется при выгрузке в качестве названия поля
     * @param integer $sort     - Сортировка
     * @param boolean $multiple - Является поле множественным или нет
     * @param boolean $required - Обязательное или нет свойство
     * @param string $keyLoc     - Ключ по которому будут извлекаться языковые названия имени свойства
     * @param array $settings   - Дополнительные настройки поля (зависят от типа).
     * @return array            - Массив описывающий свойство
     */
    private static function UF_Field($entity_id,$code,$type,$xml_id,$multiple,$required,$keyLoc,$settings,$sort = 100) {
        $cl = new \ReflectionClass(get_called_class());
        $pathLangFile = $cl->getFileName();
        $arLang = array('ru'=>'','en'=>'');
        foreach ($arLang as $key => $value_lang_file) {
            \Bitrix\Main\Localization\Loc::loadLanguageFile($pathLangFile,$key);
            $arLang[$key] = Loc::getMessage($keyLoc,null,$key);
        }

        /**
            ещё не реализованные типы:
                enumeration - Список
                double - Число
                integer - Целое число
                boolean - Да/Нет
                video - Видео
                datetime - Дата/Время
                iblock_section - Привязка к разделам инф. блоков
                iblock_element - Привязка к элементам инф. блоков
                string_formatted - Шаблон
                crm - Привязка к элементам CRM
                crm_status - Привязка к справочникам CRM
         */
        if(!$code) { $code = Random::getStringByAlphabet(15,Random::ALPHABET_NUM); }
        if(!$xml_id) { $xml_id = Random::getString(50); }
        $xml_id .= '_auto_generate_uf';
        //$oUserTypeEntity = new CUserTypeEntity();
        $aUserFields    = array(
            'ENTITY_ID' => $entity_id, 'FIELD_NAME' => $code, 'USER_TYPE_ID' => $type, 'XML_ID' => $xml_id,
            'SORT' => $sort,
            'HELP_MESSAGE'      => array('ru' => '', 'en' => ''), /* Помощь */
            //region Возоможно это не нужно, но оставим, вдруг внезапно пригодиться =)
            'SHOW_FILTER' => 'N', /* Показывать в фильтре списка. Возможные значения:
                               * не показывать = N, точное совпадение = I, поиск по маске = E, поиск по подстроке = S */
            'SHOW_IN_LIST' => '', /* Не показывать в списке. Если передать какое-либо значение, то будет считаться,
                               * что флаг выставлен.*/
            'EDIT_IN_LIST' => '', /* Пустая строка разрешает редактирование. Если передать какое-либо значение,
                               * то будет считаться, что флаг выставлен. */
            'IS_SEARCHABLE'     => 'N', /* Значения поля участвуют в поиске */
            //endregion
        );
        if($multiple) { $aUserFields['MULTIPLE'] = 'Y'; } else { $aUserFields['MULTIPLE'] = 'N'; }
        if($required) { $aUserFields['MANDATORY'] = 'Y'; } else { $aUserFields['MANDATORY'] = 'N'; }
        if(!empty($arLang)) {
            $arCodeLabels = array(
                'EDIT_FORM',   //Подпись в форме редактирования
                'LIST_COLUMN', //Заголовок в списке
                'LIST_FILTER'  //Подпись фильтра в списке
            );
            foreach ($arCodeLabels as $codeLabel) {
                foreach ($arLang as $lang => $value_str) { $aUserFields["{$codeLabel}_LABEL"][$lang] = $value_str; }
            }
            foreach ($arLang as $lang => $value) { // Сообщение об ошибке (не обязательное)
                $sub_str = 'Ошибка заполнения свойства';
                if($lang == 'en') { $sub_str = 'Property Fill Error'; }
                $aUserFields['ERROR_MESSAGE'][$lang] = "{$sub_str} {$value}";
            }
        }

        // Дополнительные настройки поля (зависят от типа). В нашем случае для типа string
        $aUserFields['SETTINGS'] = $settings;
        /*
        echo '<b>$aUserFields</b>';
        echo '<pre>';
        print_r($aUserFields);
        echo '</pre>';
        echo '<hr>';
        */

        //$iUserFieldId = $oUserTypeEntity->Add( $aUserFields ); // int
        /*if($iUserFieldId) {
            echo 'Пользовательское свойство добавлено =)';
        } else {
            echo 'Ошибка добавление пользовательскоего свойства =(';
        }*/

        //return $iUserFieldId; // int
        return $aUserFields;
    }
    //endregion

    //region Грид
    /**
     * Столбцы грида
     * @return mixed
     */
    abstract static function getHeaderGrid();

    /**
     * Фильтр грида
     * @return mixed
     */
    abstract static function filterFieldsGrid();

    /**
     * Уникальное отображение данных в ячейке
     * @param $row - элемент, который добавляем в строку (в нём данные из БД)
     * @return array возвращет ассоциятивный массив в виде [ключ1:значение1, ключ2:значение2, ..., ключN:значениеN],
     * где ключ идентификатор поля, а значение это результирующий html, который должен отобразится в ячейке грида.
     */
    abstract static function uniqueDisplayColumn($row);

    /**
     * Действия которые доступны пользователю из коробки, список:
     * <ul>
     *  <li><b>delete<b> - Удаление</li>
     *  <li><b>edit<b> - Редактирование</li>
     *  <li><b>add<b> - Добавление</li>
     *  <li><b>export_excel<b> - Экспорт в Excel</li>
     *  <li><b>export_csv<b> - Экспорт в CSV</li>
     *  <li><b>import<b> - Импорт</li>
     *  <li><b>migration<b> - Миграция</li>
     * </ul>
     * Для генерации массива использовать self::generationAtions();
     * @return array - массив действий которыу будут доступны пользователю
     */
    abstract static function getActions();

    /**
     * Возвращает гриду метки, по которым он опледеляет какие контролы стандартных действий включать.
     * Внутренний метод для генерации массива.
     * @param bool $delite
     * @param bool $edit
     * @param bool $add
     * @param bool $export_excel
     * @param bool $export_csv
     * @param bool $import
     * @param bool $migration
     * @return array
     */
    protected static function generationAtions(
        $delete=true, $edit=true, $add=true, $export_excel=true, $export_csv=true, $import=true, $migration=true, $edit_row=true
    ) {
        $result = array();
        if($delete) { $result[]='delete'; }
        if($edit) { $result[]='edit'; }
        if($edit_row) { $result[]='edit_row'; }
        if($add) { $result[]='add'; }
        if($export_excel) { $result[]='export_excel'; }
        if($export_csv) { $result[]='export_csv'; }
        if($import) { $result[]='import'; }
        if($migration) { $result[]='migration'; }
        return $result;
    }

    /**
     * Добавление кастомных действий (выпадающий список "Выберете действие", появляется если есть хотя бы одно
     * действие)
     * @param $gridManagerId - Идентификатор грида (нужен для вызова функций JS.
     * @param $applyButton - кнопка применить, генерируется в гриде и её можно использовать в ваших кастомных
     * действиях.
     * @return array
     */
    abstract static function addCustomAction($gridManagerId,$applyButton);

    /**
     * Обработчик кастомных событий
     * @param $action - идентификатор кастомного действия
     * @param $allRows - кликнули checkBox "Для всех"
     * @param $request - запрос
     * @return void
     */
    abstract static function processGridActions($action,$allRows,$request);

    abstract static function validateFormEdit($element, $error);

    //region Обязательные методы - добавление элемента сущности
    /**
     * Добавление чего?
     * @return string
     */
    abstract static function getNameOneElement();
    abstract static function getFieldsToEdit();
    //endregion

    //endregion

    //region Публичные методы
    protected static function fieldCustom($customhtml,$default = true) {
        return self::field($default,'custom',true,'','',0,'','',$customhtml);
    }
    /**
     * Генерация параметров столбца для грида
     * @param bool $default - Если пользователь ещё не пользовался гридом, и это поле помечено этим флаго, то оно будет
     *                        отображено таблице, доступно:
     *      <ul>
     *          <li><b>0</b> или <b>false</b></li>
     *          <li><b>1</b> или <b>true</b></li>
     *      </ul>
     * @param bool $type - Тип поля, по умолчанию text, доступно:
     *      <ul>
     *          <li><b>i</b> = integer</li>
     *          <li><b>n</b> = number</li>
     *          <li><b>t</b> = text</li>
     *          <li><b>ta</b> = textarea</li>
     *          <li><b>d</b> = date</li>
     *          <li><b>c</b> = checkbox</li>
     *          <li><b>l</b> = list</li>
     *          <li><b>g</b> = group_list</li>
     *          <li><b>q</b> = quick</li>
     *          <li><b>u</b> = intranet_user_search</li>
     *          <li><b>custom</b> = custom</li>
     *          <li><b>''</b> или <b>0</b> = default (это input(ставим false), чтобы установить его.)</li>
     *      </ul>
     * @param bool $editable - когда мы выделяем элементы и нажимаем "редактировать", то столбцы, помеченные этим флагом
     *                         перейдут в режим редактирования, доступные значения:
     *      <ul>
     *          <li><b>0</b> или <b>false</b></li>
     *          <li><b>1</b> или <b>true</b></li>
     *      </ul>
     * @param string $first_order - Если пользователь зашёл на страницу, то изначально сортировка будет по этому столбцу
     *                            , доступные значения:
     *      <ul>
     *          <li><b>d</b> = desc</li>
     *          <li><b>a</b> = asc</li>
     *          <li><b>''</b>, <b>0</b> или <b>false</b> => без пометки первой сортировки1</li>
     *      </ul>
     * @param string $sort - Уникальная сортировка, например ASSIGNED_BY -> ASSIGNED_BY_ID
     * @param integer $width - ширина столбца грида
     * @param string $class - где то нашёл, что есть такая штука, но не понял её функционала =)
     * @param boolean $hidden - хз... что это такое, но видимо надо было =)
     * @param string $customhtml - Вот это полезная штука... если нет в стандартном функционале отображения, то можно своё забубенить =)
     * @param string $items - если $type = list_custom, то добавляются эти элементы
     * @return array
     */
    protected static function field(
        $default = true, $type = false, $editable = false, $first_order = '', $sort = '', $width = 0, $class = '',
        $hidden=false, $customhtml='',$items = array()
    ) {
        $result = array( 'default' => $default );
        if($width > 0) { $result['width'] = $width; }
        if(!$default) { $result['default'] = false; }
        if($type) {
            switch ($type) {
                case 'list_custom':
                    $type = 'list_custom';
                    $result['items'] = $items;
                    break;
                case 'i':
                    $type = 'int';
                    break;
                case 'n':
                    $type = 'number';
                    break;
                case 't':
                    $type = 'text';
                    break;
                case 'ta':
                    $type = 'textarea';
                    break;
                case 'd':
                    $type = 'date';
                    break;
                case 'c':
                    $type = 'checkbox';
                    break;
                case 'l':
                    $type = 'list';
                    break;
                case 'g':
                    $type = 'group_list';
                    break;
                case 'q':
                    $type = 'quick';
                    break;
                //case 'intranet_user_search':
                case 'u':
                    $type = 'intranet_user_search';
                    $result['componentParams'] = array(
                        'NAME' => 'crmstores_edit_responsible',
                        'INPUT_NAME' => 'ASSIGNED_BY_ID',
                        'SEARCH_INPUT_NAME' => 'ASSIGNED_BY_NAME',
                        'NAME_TEMPLATE' => \CSite::GetNameFormat()
                    );
                    break;
                case 'f':
                    $type = 'file';
                    /*$type = 'custom';
                    $result['value'] = \CFile::InputFile($code, 20);*/
                    break;
                case 'custom':
                    $result['value'] = $customhtml;
                    break;
            }
            $result['type'] = $type;
        }
        if($editable) { $result['editable'] = true; }
        if($first_order) {

            switch ($first_order) {
                case 'd':
                    $first_order = 'desc';
                    break;
                case 'a':
                    $first_order = 'asc';
                    break;
            }
            $result['first_order'] = $first_order;

        }
        if($sort) { $result['sort'] = $sort; }
        if($class) { $result['class'] = $class; }
        if($hidden) { $result['hidden'] = $hidden; }
        return $result;
    }

    //endregion

    //region НЕ ТРОГАЕМ ЭТИ МЕТОДЫ!!!!!!! ОНИ АВТОМАТИЧЕСКИЕ Забываем что они есть =)
    public static function sortTableFieldsGrid() {
        $TableFieldsCode = array(); // Поля текущей таблицы
        $db = \Bitrix\Main\Application::getConnection();
        $TableFields = $db->getTableFields(self::getTableName());
        foreach ($TableFields as $code => $field) { $TableFieldsCode[] = $code; }
        return $TableFieldsCode;
    }
    public static function filterTableFieldsGrid() {
        $TableFieldsCode = array(); // Поля текущей таблицы
        $db = \Bitrix\Main\Application::getConnection();
        $called_class = get_called_class();
        $TableFields = $db->getTableFields($called_class::getTableName());
        foreach ($TableFields as $code => $field) {
            if($field instanceof DatetimeField || $field instanceof FloatField) {
                $TableFieldsCode[] = '>='.$code; $TableFieldsCode[] = '<='.$code;
            }
            else { $TableFieldsCode[] = $code; }
        }
        return $TableFieldsCode;
    }
    public static function CheckingCorrectValueFieldsBeforeInputToDB($fields) {
        $result = array();
        $MapType = self::GetColumnNameAndType();
        foreach ($fields as $code => $value) {
            if(isset($MapType[$code])) {
                if($MapType[$code] instanceof DatetimeField){
                    if(!$value) { $value = date('m/d/Y h:i:s', time()); }
                    //region Преобразовать время пользователя к локальному времени сервера
                    $dateUser = strtotime($value);
                    $time_string = ConvertTimeStamp($dateUser-\CTimeZone::GetOffset(), "FULL");
                    $new_value = new \Bitrix\Main\Type\DateTime($time_string);
                    //endregion
                    $result[$code] = $new_value;
                } else if($MapType[$code] instanceof BooleanField){
                    $result[$code] = ($value == "Y") ? true : false;
                }
            } else { $result[$code] = $value; }
        }
        return $result;
    }
    public static function ConverDateTimeToServer($value) {
        //region Преобразовать время пользователя к локальному времени сервера
        $dateUser = strtotime($value);
        $time_string = ConvertTimeStamp($dateUser-\CTimeZone::GetOffset(), "FULL");
        return new \Bitrix\Main\Type\DateTime($time_string);
        //endregion
    }
    public static function GetColumnNameAndType() {
        $result = array();
        $called_class = get_called_class();
        $Map = $called_class::getMap();
        foreach ($Map as $field) {
            if($field instanceof ScalarField) { $result[$field->getColumnName()] = $field; }
        }
        return $result;
    }
    public static function GetColumnNameAndType_UF() {
        $result = array();
        $called_class = get_called_class();
        $MapUF = $called_class::getMapUF();
        foreach ($MapUF as $item) { $result[$item['FIELD_NAME']] = $item['USER_TYPE_ID']; }
        return $result;
    }
    public static function GetListEnum($code) {
        $result = array();
        $called_class = get_called_class();
        $Map = $called_class::getMap();
        foreach ($Map as $field) {
            if($field instanceof ScalarField) {
                if($field->getColumnName() === $code) {
                    $result = $field->getValues();
                    $result_new = array();
                    foreach ($result as $key => $value) { $result_new[$value] = $key; }
                    $result = $result_new;
                }
            }
        }
        return $result;
    }
    public static function GetAllEnums() {
        $result = array();
        $called_class = get_called_class();
        $Map = $called_class::getMap();
        foreach ($Map as $field) {
            if($field instanceof EnumField) {
                $values = $field->getValues();
                $values_new = array();
                foreach ($values as $key => $value) { $values_new[$value] = $key; }
                $result[$field->getColumnName()] = $values_new;
            }
        }
        return $result;
    }
    public static function GetAllBooleans() {
        $result = array();
        $called_class = get_called_class();
        $Map = $called_class::getMap();
        foreach ($Map as $field) {
            if($field instanceof BooleanField) { $result[] = $field->getColumnName(); }
        }
        return $result;
    }
    public static function getTableName() {
        return mb_strtolower(str_replace("\\","_",get_called_class()));
    }
    public static function getUfId(){
        // Можно добавлять пользовательские поля через админку
        /*
            Websoft\Blank\Entity\StoreTable::update(1,array('UF_MONEY' => 3333));
            $res = Websoft\Blank\Entity\StoreTable::getList(array('select' => array('ID','UF_*')))->fetchAll();
            echo '<pre>';
            print_r($res);
            echo '</pre>';
         */
        $UfID = str_replace('Table','',get_called_class());
        $UfID = mb_strtoupper($UfID);
        $UfID = explode('\\',$UfID);
        return $UfID[count($UfID)-1];
    }
    protected static function getMessage() {
        $cl = new \ReflectionClass(get_called_class());
        $pathLangFile = $cl->getFileName();
        \Bitrix\Main\Localization\Loc::loadLanguageFile($pathLangFile);
        return Loc::getMessage('NameOneElementToAdd');
    }
    /*public static function getStringName() { // Перестал нормально работать =( файлы локализации начали затираться..
        return Loc::getMessage('NAME_ENTITY');
    }*/
    abstract static function getStringName();
    protected static function genHeader($generate) {
        $result = array();
        foreach ($generate as $code => $item) {
            $add = array( 'id' => $code,  'name' => Loc::getMessage($code),  'sort' => $code );
            if(!empty($item)) { $add = array_merge($add,$item); }
            $result[] = $add;
        }
        return $result;
    }
    protected static function genFilter($generate) {
        $result = array();
        foreach ($generate as $code => $item) {
            $add = array( 'id' => $code,  'name' => Loc::getMessage($code) );
            if(isset($item['selector'])) { $add['name'] = Loc::getMessage($item['selector']['DATA']['ID']); }
            if(!empty($item)) { $add = array_merge($add,$item); }
            if(!empty($item) && $item['type'] === 'list') { $add['items'] = self::GetListEnum($code); }
            if(!empty($item) && $item['type'] === 'list_custom'){
                $add['type'] = 'list';
                $add['items'] = $item['items'];
                $add['params'] = array(
                    'multiple' => 'Y'
                );
            }
            $result[] = $add;
        }
        return $result;
    }
    //endregion

    //region Тут пока не понятно что =)
    public static function addInMenu(&$items) {
        /*$items[] = array(
            'ID' => self::$NAME_ENTITY,
            'MENU_ID' => 'menu_crm_'.strtolower(self::$NAME_ENTITY),
            'NAME' => Loc::getMessage('CRMSTORES_MENU_ITEM_STORES'),
            'TITLE' => Loc::getMessage('CRMSTORES_MENU_ITEM_STORES'),
            'URL' => '/crm/stores/'
        );*/
    }

    /**
     * Облегчённый getList
     * @param bool $fetchAll - все элементы или один?
     * @param array $filter - фильтр
     * @param array $select - выборка, по умолчанию, только ID
     * @param array $order - массив полей в части ORDER BY запроса в виде: "field"=>"asc|desc"
     * @return mixed
     */
    public static function getListPure($fetchAll=true,$filter=array(),$select=array('ID'),$order = array()) {
        if($fetchAll) { return self::getList(array('select'=>$select,'filter'=>$filter,'order'=>$order))->fetchAll(); }
        else { return self::getList(array('select'=>$select,'filter'=>$filter))->fetch(); }
    }
    //endregion

    public static function getList(array $parameters = array(),$grid = false) {

        $child = get_called_class();
        $db = \Bitrix\Main\Application::getConnection();
        if(!$db->isTableExists($child::getTableName())) { $entity = $child::getEntity(); $entity->createDbTable(); }
        if(isset($parameters['select'])){
            foreach ($parameters['select'] as $index => $elSelected) {
                if($elSelected == 'UF_*') {
                    unset($parameters['select'][$index]);
                    $called_class = get_called_class();
                    $ColumnNameAndType_UF = $called_class::GetColumnNameAndType_UF();
                    foreach ($ColumnNameAndType_UF as $code => $type) {
                        $parameters['select'][] = $code;
                    }
                }
            }
        }
        return parent::getList($parameters);
    }

    //region Проверка полноценности данных (предупреждения)
    public static function ListWarning() { return array(); }
    //endregion

    public static function uniqueEditForm($fields) {}


    public static function generateRowHistory($user_id,$description = '',$before = '...',$after ='...',$entity_id,$entity_class) {
        return array(
            'ENTITY_ID' => $entity_id, // ID Сущности
            //'ENTITY_CLASS' => $entity_class, // Карточка бронирования или дочерняя карточка бронирования?
            'DATE_TIME' => new \Bitrix\Main\Type\DateTime(), // Дата изменения
            'CHANGE_BY_ID' => $user_id,
            'DESCRIPTION' => $description,
            'BEFORE' => $before ? $before : '',
            'AFTER' => $after ? $after : ''
        );
    }
    protected static function AddRowsHistory($rows) { foreach ($rows as $row) { \Websoft\Booking\Entity\StoriesCardTable::add($row); } }

}