<?
AddEventHandler('main','OnBuildGlobalMenu',function (&$arGlobalMenu, &$arModuleMenu){
    $dir = dirname(__FILE__);
    for ($i = 1; $i < ($level = 2); $i++) { $dir = dirname($dir); }
    $moduleID = basename($dir);
    if($GLOBALS['APPLICATION']->GetGroupRight($moduleID) >= 'R'){
        $defOpt = \Bitrix\Main\Config\Option::getDefaults($moduleID);
        $class_up = $defOpt['class_up'].'_';
        $class = $defOpt['class'];
        ob_start(); ?>
        <style>
            #global_menu_<?=$class;?>_main_icon .adm-main-menu-item-icon {
                background: url(/bitrix/images/<?=$moduleID;?>/main_icon_menu.png) no-repeat;
                background-size: 50%; background-position: 50% -16%;
            }
            #global_menu_<?=$class;?>_main_icon:hover .adm-main-menu-item-icon,
            #global_menu_<?=$class;?>_main_icon.adm-main-menu-item-active .adm-main-menu-item-icon {
                background-position: 50% 104%;
            }
        </style>
        <? $css = ob_get_clean();
        $GLOBALS['APPLICATION']->AddHeadString($css);
        $arMenu = array(
            'menu_id' => $class.'_main_icon', 'text' => GetMessage($class_up.'GLOBAL_MENU_TEXT'),
            'title' => GetMessage($class_up.'GLOBAL_MENU_TEXT'), 'sort' => 100000,
            'items_id' => 'global_menu_'.$class.'_items', 'items' => array(
                array( // Пункт подменю меню
                    'text'=>GetMessage($class_up.'SETTINGS_TITLE'),'title'=>GetMessage($class_up.'SETTINGS_TITLE'),
                    'sort'=>100, 'url'=>'/bitrix/admin/'.$moduleID.'_settings.php','items_id'=>$class.'_settings',
                ),
                array( // Пункт подменю меню
                    'text'=>GetMessage($class_up.'ENTITY_HL_TITLE'),'title'=>GetMessage($class_up.'ENTITY_HL_TITLE'),
                    'sort'=>200, 'url'=>'/bitrix/admin/'.$moduleID.'_entity_hl.php','items_id'=>$class.'_entity_hl',
                ),
            ),
        );
        $arGlobalMenu[] = $arMenu;
    }
});
?>