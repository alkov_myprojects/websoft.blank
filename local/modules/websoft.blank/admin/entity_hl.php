<?
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_before.php');
if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_after.php');
}

use \Bitrix\Main\Config\Option;
use Bitrix\Main\Localization\Loc;

use Websoft\Blank\Admin\CustomAdminTags; /** TODO:: Обязательно поменять namespace, если не соответсвует */

Loc::loadMessages(__FILE__);

//region $module_id
$MODULE_ID = "";
$dir = dirname(__FILE__);
for ($i = 1; $i < ($level = 2); $i++) { $dir = dirname($dir); }
include $dir.'/module_id.php';
$module_id = $MODULE_ID;
//endregion

if ($module_id && CModule::IncludeModule($module_id)) {
    if ($_SERVER['REQUEST_METHOD']==='GET'){ $GLOBALS['APPLICATION']->SetTitle('Сущности HL'); }

    //region UserTypes
    $arUserTypes = $USER_FIELD_MANAGER->GetUserType();
    $arUserTypes_L = array(
        array('value' => "", 'name' => '---------- Выберите тип поля ----------')
    );
    foreach ($arUserTypes as $arUserType) {
        $arUserTypes_L[] = array(
            'value' => $arUserType['USER_TYPE_ID'],
            'name' => $arUserType['DESCRIPTION'],
        );
    }
    //endregion

    $Module = CModule::CreateModuleObject($module_id);
    $Handler = $Module->GetNameSpace().'Handler';
    $listEntityStructure = $Handler::GetStructureAllEntityHL();
    $SelectEntityHL = $Module->GetOption('SelectEntityHL');
    ?>

    <style>
        .adm-workarea { height: 100%; }
        .adm-workarea select {
            background: #fff url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 4 5'%3E%3Cpath fill='%23343a40' d='M2 0L0 2h4zm0 5L0 3h4z'/%3E%3C/svg%3E") no-repeat right .75rem center !important;
            background-size: 8px 10px !important;
        }
        .custom-select {

           /*all: unset;*/

           width: 100%;

           background-size: 8px 10px;

           -webkit-appearance: none;
           -moz-appearance: none;

           background: none;
           border-radius: .25rem;

           color: #495057;
           -webkit-box-shadow: none;
           box-shadow: none;
           font-size: unset;
           height: calc(2.25rem + 2px) !important;
           margin: unset;
           vertical-align: middle !important;
           padding: .375rem 1.75rem .375rem .75rem !important;
           -webkit-font-smoothing: none;
        }
        .custom-select.is-invalid, .form-control.is-invalid, .was-validated .custom-select:invalid,
        .was-validated .form-control:invalid {
            border-color: #dc3545 !important;
        }
        .custom-select.is-valid, .form-control.is-valid, .was-validated .custom-select:valid,
        .was-validated .form-control:valid {
            border-color: #28a745 !important;
        }
        .flag_my:hover i{
            color: white;
        }
    </style>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
          crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/solid.css"
          integrity="sha384-aj0h5DVQ8jfwc8DA7JiM+Dysv7z+qYrFYZR+Qd/TwnmpDI6UaB3GJRRTdY8jYGS4" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/fontawesome.css"
          integrity="sha384-WK8BzK0mpgOdhCxq86nInFqSWLzR5UAsNg0MGX9aDaIIrFWQ38dGdhwnNCAoXFxL" crossorigin="anonymous">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <style>
        .adm-header-right {
            height: 45px;
        }
        .adm-header-btn-site, .adm-header-btn-admin {
            padding-bottom: 27px;
        }
        .adm-header-notif-block {
            padding-bottom: 21px;
        }

    </style>

    <div id="app">
        <div class="card">
            <div class="card-body">
                <!--<transition name="fade" mode="out-in">-->
                <transition name="slide-fade" mode="out-in">

                    <actions-entity v-if="action === 'actions'" key="actions"
                                    v-bind:types="types"
                                    v-bind:list_entity="listEntity"
                                    v-bind:ind="ind"
                                    @add-entity="action = 'entity'; edit = false;"
                                    @edit-entity="editEntity"
                                    @delete-entity-by-ind="deleteEntity"
                                    @change-ind="changeInd"
                    ></actions-entity>

                    <one-entity v-if="action === 'entity'" key="entity"
                                v-bind:types="types"
                                v-bind:edit="edit"
                                @back-to-actions-entity="action = 'actions'"
                                @add-entity="addEntity"
                                @update-entity="updateEntity"
                    ></one-entity>

                </transition>
            </div>
        </div>
    </div>
    
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>
    <script>

        Vue.directive('scroll', {
            inserted: function (el, binding) {
                let f = function (evt) {
                    if (binding.value(evt, el)) {
                        window.removeEventListener('scroll', f)
                    }
                };
                window.addEventListener('scroll', f)
            }
        });

        //region actions-entity
        Vue.component('actions-entity', {
            props: [ 'list_entity','ind','types' ],
            data: function () {
                return {
                    message: { text:'',type:'primary' }, // сообщение
                    openStructureFile: false
                }
            },
            computed: {
                openStructureFileClassForBtn(){
                    if(this.openStructureFile) { return 'fa-rotate-180'; }
                    else { return ''; }
                },
                getNameSelectEntity(){
                    if(this.list_entity.legend > 0) { return this.list_entity[ind].DB.NAME_RU; }
                    return '';
                },
            },
            methods: {
                CheckActive(val_1,val_2) { return val_1 == val_2 ? 'active' : ''; },
                ChangeActive(ind) {
                    this.$emit('change-ind',ind);
                },
                deleteEntity() {
                    let ind = this.ind;
                    if(ind >= 0) {
                        $('#questionDeleteEntityHL').modal('show');
                        $('.modal').on('shown.bs.modal', function() {
                            $(this).find('[autofocus]').focus();
                        });
                    }
                },
                deleteEntityAjax() {

                    $('#questionDeleteEntityHL').modal('hide');
                    
                    //region Программное удаление
                    BX.MyAjax.Get('EntityHL/DeleteEntityHL',{'data':this.list_entity[this.ind]},'html',function (data) {
                        console.log('');
                        console.log(data);
                    });
                    //endregion

                    //region Визуалное удалние
                    if(this.ind > 0) {
                        this.$emit( 'change-ind', this.ind - 1 );
                    }
                    this.$emit( 'delete-entity-by-ind', this.ind);
                    //endregion

                },
                isset(val) {
                    if (typeof val == "undefined") { return false; }
                    else { return true; }
                },
                getNameType(code) {
                    for (var i=0;i < this.types.length;i++) {
                        if(this.types[i].value == code) {
                            return this.types[i].name;
                        }
                    }
                },
                getNameBool(str) {
                    if(str == 'Y') { return 'Да'; }
                    return 'Нет';
                },
                openFields() {
                    return this.isset(this.list_entity[this.ind].DB.UF_FIELDS) &&
                        this.list_entity[this.ind].DB.UF_FIELDS.length > 0;
                }
            },
            template: `
                <div>

                    <div class="modal fade" id="questionDeleteEntityHL" tabindex="-1" role="dialog"
                        aria-labelledby="exampleModalCenterTitle" aria-hidden="true">

                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLongTitle">Подтверждение удаления</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    Вы действительно хотите удалить "<b>{{ getNameSelectEntity }}</b>"?<br>
                                    Сущность будет утеряна полностью вместе с данными
                                    <u>без взможности восставноления</u>!
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary"
                                            @click="deleteEntityAjax"
                                            :autofocus="'autofocus'"
                                            >Да, я уверен</button>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Нет...</button>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-4">
                                <div class="list-group" id="list-tab" role="tablist" style="margin-bottom: 15px;">
                                    <a  v-for="(entity, index) in list_entity"
                                        href="javascript:void(0);"
                                        class="list-group-item list-group-item-action"
                                        :class="CheckActive(ind,index)"
                                        @click="ChangeActive(index); return false;">{{ entity.DB.NAME_RU }}</a>
                                </div>
                                <button type="button" class="btn btn-outline-success" @click="$emit('add-entity')">
                                    <i class="fas fa-plus mr-1"></i> Добавить сущность HL
                                </button>
                            </div>
                            <div class="col-8" v-if="list_entity.length > 0">
                                <div class="tab-content" id="nav-tabContent">
                                    <div class="tab-pane fade show active" >
                                        <div class="errors" v-show="message.text">
                                            <div class="alert" :class="'alert-' + message.USER_TYPE_ID" role="alert">{{ message.text }}</div>
                                            <hr>
                                        </div>
                                        <h3>Файлы:</h3>
                                            <i>Класс</i>: <b>{{ list_entity[ind].FILES.CLASS }}</b><br>
                                            <i>Структура</i>: <b>{{ list_entity[ind].FILES.STRUCTURE }}</b>
                                            <button type="button" class="btn btn-outline-success ml-1"
                                                    @click="openStructureFile=!openStructureFile"
                                                    :class="openStructureFileClassForBtn"
                                                    style="padding: .01rem .3rem; line-height: 1.2;">
                                                <i class="fas fa-angle-down"></i>
                                            </button>
                                            <transition name="fade">
                                                <pre v-if="openStructureFile"> {{ list_entity[ind] }} </pre>
                                            </transition>
                                        <hr>
                                        <h3>База данных:</h3>
                                            <i>Название </i>: <b>{{ list_entity[ind].DB.NAME_RU }}</b><br>
                                            <i>Сущность </i>: <b>{{ list_entity[ind].DB.NAME }}</b><br>
                                            <i>Таблица </i>: <b>{{ list_entity[ind].DB.TABLE_NAME }}</b><br>
                                            <div v-if="openFields()">
                                                <i>Поля</i>:
                                                <table class="table">
                                                  <thead>
                                                    <tr>
                                                      <th scope="col">Код</th>
                                                      <th scope="col">Имя</th>
                                                      <th scope="col">Тип</th>
                                                      <th scope="col">Множ.</th>
                                                      <th scope="col">Обяз.</th>
                                                      <th scope="col">Доп.</th>
                                                    </tr>
                                                  </thead>
                                                  <tbody>
                                                    <tr v-for="UF_FIELD in list_entity[ind].DB.UF_FIELDS">
                                                      <th scope="row">UF_{{ UF_FIELD.FIELD_NAME }}</th>
                                                      <td>{{ UF_FIELD.NAME }}</td>
                                                      <td>{{ getNameType(UF_FIELD.USER_TYPE_ID) }}</td>
                                                      <td>{{ getNameBool(UF_FIELD.MULTIPLE) }}</td>
                                                      <td>{{ getNameBool(UF_FIELD.MANDATORY) }}</td>
                                                      <td>...</td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                            </div>

                                        <hr>

                                        <button type="button" class="btn btn-outline-info mr-2"
                                            @click="$emit('edit-entity',list_entity[ind])"
                                            >
                                            <i class="fas fa-pencil-alt mr-1"></i> Редактировать
                                        </button>

                                        <button type="button" class="btn btn-outline-danger" @click="deleteEntity(ind)">
                                            <i class="fas fa-trash-alt mr-1"></i> Удалить
                                        </button>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            `
        });
        //endregion

        //region one-entity
        let one_entity = Vue.component('one-entity', {
            props: ['edit','types'], // простая форма записи
            data: function () {
                return {
                    firstLoad:true,
                    State: 'createEntityHL',

                    //region NAME_RU
                    nameEntity: '',
                    nameEntityStatus: '',
                    //endregion

                    //region NAME
                    nameEntity_2: '',
                    nameEntityStatus_2: '',
                    //endregion

                    //region TABLE_NAME
                    nameEntity_3: '',
                    nameEntityStatus_3: '',
                    //endregion

                    //region UF_FIELDS
                    fields: [],
                    fieldsStatus: [],
                    //endregion

                    additionalSettings: false, // Доп.настройки полей


                    elFixed: false
                }
            },
            created: function () { // Вызывается после монтирования экземпляра, $el ещё не доступно
                this.setEntityToEdit();
                window.addEventListener('resize', this.handleScroll);
            },
            mounted: function ( ) { // аналогично created, но $el доступен
                this.jq_plugs();
            },
            updated: function () { // аналогично created, но $el доступен
                this.jq_plugs();
            },
            ready: function () {
                window.addEventListener('resize', this.handleScroll)
            },
            beforeDestroy: function () {
                window.removeEventListener('resize', this.handleScroll)
            },
            computed: {
                showFields() { return this.fields.length > 0; },
                classBtnAdditionalSettings() {
                    let classBtn = 'fa-door-closed';
                    if(this.additionalSettings) { classBtn = 'fa-door-open'; }

                    //region Логика по открытию/Закрытию Доп.настроек
                    if(!this.firstLoad) {
                        if(this.additionalSettings) {
                            console.log('Открыть доп.настройки пользовательских полей');
                        } else {
                            console.log('Закрыть доп.настройки пользовательских полей');
                        }
                    } else { this.firstLoad = false; }

                    //endregion
                    return classBtn;
                },
                addOrEdit() { return this.edit; },
            },
            methods:{

                jq_plugs() {
                    $( "#sortable" ).sortable({
                        axis:"y", cursor:"grabbing", opacity:0.8, scroll:true, scrollSensitivity:200, scrollSpeed:15,
                        /*change: function( event, ui ) {

                            /!*console.log('event');
                            console.log(event);

                            console.log('ui');
                            console.log(ui);*!/

                        },
                        sort: function( event, ui ) {

                            console.log('event');
                            console.log(event);

                            console.log('ui');
                            console.log(ui);

                        },*/
                        stop: function(event, ui) {
                            console.log(ui);
                            //console.log( ui.item.index() );
                            //alert("New position: " + ui.item.index());
                        }
                    }).disableSelection();
                },

                //region Системные методы
                generateXmlID(len) {
                    let text = "", possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
                    for(let i=0; i < len; i++){ text += possible.charAt(Math.floor(Math.random() * possible.length)); }
                    return text;
                },
                yandex_translate_ru_en(text,func_call) {
                    if(text) {
                        let url = "https://translate.yandex.net/api/v1.5/tr.json/translate",
                            keyAPI = "trnsl.1.1.20181229T092301Z.36e52fb990b6a776.bb0daa83f1687e43280fbd1362b4518963759080";
                        let xhr = new XMLHttpRequest(),
                            textAPI = text, //this.nameEntity, //document.querySelector('#source').value,
                            langAPI = 'ru-en', //document.querySelector('#lang').value;
                            data = "key="+keyAPI+"&text="+textAPI+"&lang="+langAPI;
                        xhr.open("POST",url,true);
                        xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                        xhr.send(data);
                        xhr.onreadystatechange = function() {
                            if(this.responseText) {
                                let json = JSON.parse(this.responseText); if(func_call) {
                                    translate_text = json.text[0];
                                    if(!translate_text) { translate_text = ''; }
                                    func_call(translate_text);
                                }
                            }
                        };
                    } else { if(func_call) { func_call(''); } }
                },
                isset(val) {
                    if (typeof val == "undefined") { return false; }
                    return true;
                },
                //endregion

                //region FILES
                changeNameEntity() {
                    one_entity = this;
                    this.yandex_translate_ru_en(this.nameEntity,function (text) {

                        text = text.replace(/[^a-zA-Z0-9 ]/g, ""); // убираем все спец символы

                        //region генерация NAME

                        //region каждая буква заглавная
                        let a = text; //transliterate(text); //this.nameEntity); // транслитирование
                        let b = '';
                        for (let i = 0; i < a.length; i++) { b += (a[i - 1] === ' ') ? a[i].toUpperCase() : a[i]; }
                        //endregion

                        //region Сделать первую букву заглавной
                        b = b.charAt(0).toUpperCase() + b.substr(1);
                        //endregion

                        //region Убираем все пробелы
                        b = b.replace(/\s/g, '');
                        //endregion

                        one_entity.nameEntity_2 = b;

                        //endregion

                        //region генерация TABLE_NAME
                        let c = text //transliterate(text); // транслитирование
                        c = c.toLowerCase(); // все строчные буквы
                        c = c.replace(/\s/g, '_'); // заменить пробелы на _
                        one_entity.nameEntity_3 = c;
                        //endregion

                        one_entity.validateNames();
                    });
                },
                //endregion

                //region DB
                changeOneFieldType(i) {
                    let type = 'is-valid';
                    if(!this.fields[i].USER_TYPE_ID) { type = 'is-invalid'; }
                    this.$set(this.fieldsStatus, i, {USER_TYPE_ID:type,name:this.fieldsStatus[i].name});
                },
                changeOneFieldName(i) {
                    app = this;
                    this.yandex_translate_ru_en(this.fields[i].NAME,function (text) {

                        //region конвертация наименование пользовательского поля на English
                        let a = text;
                        a = a.replace(/[^a-zA-Z0-9 ]/g, ""); // убираем все спец символы
                        a = a.toUpperCase(); // все прописные буквы
                        a = a.replace(/\s/g, '_'); // заменить пробелы на _
                        a = a.substring(0,17); // сократить до 17-ти символов (ограничение Bitrix)
                        //endregion

                        app.fields[i].FIELD_NAME = a;
                        let name = 'is-valid',FIELD_NAME = 'is-valid';
                        if(!app.fields[i].NAME) { name = 'is-invalid'; }
                        if(!app.fields[i].FIELD_NAME) { FIELD_NAME = 'is-invalid'; }
                        app.$set(
                            app.fieldsStatus, i,
                            { USER_TYPE_ID:app.fieldsStatus[i].USER_TYPE_ID, NAME:name,FIELD_NAME:FIELD_NAME }
                        );
                        app.validateFields();

                    });
                },
                onAddField() {
                    let model = {
                        USER_TYPE_ID:'',MULTIPLE:'N',MANDATORY:'N',NAME:'',FIELD_NAME:'',XML_ID:this.generateXmlID(100),
                        SORT: 100 + (this.fields.length * 100)
                    };
                    Vue.set(this.fields, this.fields.length, model);
                    Vue.set(this.fieldsStatus, this.fieldsStatus.length, model);

                    var app = this;
                    setTimeout(function () {
                        app.handleScroll();
                    },0);
                },
                onReset() {
                    this.nameEntity = '';
                    this.nameEntity_2 = '';
                    this.nameEntity_3 = '';
                    this.nameEntityStatus = '';
                    this.nameEntityStatus_2 = '';
                    this.nameEntityStatus_3 = '';
                    this.fields = [];
                    this.fieldsStatus = [];
                    
                    var app = this;
                    setTimeout(function () {
                        app.handleScroll();
                    },0);
                },
                onUpSort(index) {
                    if(index > 0) {
                        let field_before = this.fields[index - 1];
                        let field_this = this.fields[index];
                        this.$set(this.fields, index - 1, field_this);
                        this.$set(this.fields, index, field_before);

                        let field_before_status = this.fieldsStatus[index - 1];
                        let field_this_status = this.fieldsStatus[index];
                        this.$set(this.fieldsStatus, index - 1, field_this_status);
                        this.$set(this.fieldsStatus, index, field_before_status);
                    }
                },
                onDownSort(index) {
                    if(index < this.fields.length - 1) {
                        let field_after = this.fields[index + 1];
                        let field_this = this.fields[index];
                        this.$set(this.fields, index + 1, field_this);
                        this.$set(this.fields, index, field_after);

                        let field_after_status = this.fieldsStatus[index + 1];
                        let field_this_status = this.fieldsStatus[index];
                        this.$set(this.fieldsStatus, index + 1, field_this_status);
                        this.$set(this.fieldsStatus, index, field_after_status);
                    }
                },
                onDelete(index) {

                    this.$delete(this.fields, index);
                    this.$delete(this.fieldsStatus, index);

                    var app = this;
                    setTimeout(function () {
                        app.handleScroll();
                    },0);
                },
                //endregion

                //region Валидация
                validateNames() {
                    let good = true;

                    if(!this.nameEntity) { good = false; this.nameEntityStatus = 'is-invalid'; }
                    else { this.nameEntityStatus = 'is-valid'; }

                    if(!this.nameEntity_2) { good = false; this.nameEntityStatus_2 = 'is-invalid'; }
                    else { this.nameEntityStatus_2 = 'is-valid'; }

                    if(!this.nameEntity_3) { good = false; this.nameEntityStatus_3 = 'is-invalid'; }
                    else { this.nameEntityStatus_3 = 'is-valid'; }
                    return good;
                },
                validateFields() {
                    let good = true;
                    for (let i = 0; i < this.fieldsStatus.length; i++) {
                        let type = 'is-valid', name = 'is-valid', FIELD_NAME = 'is-valid';
                        if(!this.fields[i].USER_TYPE_ID) { type = 'is-invalid'; good = false; }
                        if(!this.fields[i].NAME) { name = 'is-invalid'; good = false; }
                        if(!this.fields[i].FIELD_NAME) {
                            FIELD_NAME = 'is-invalid';
                            good = false;
                        } else {
                            //region Проверить на дубликат
                            let fineDublicate = false;
                            for (let g = 0; g < this.fields.length; g++) {
                                if( g !== i && this.fields[g].FIELD_NAME === this.fields[i].FIELD_NAME) {
                                    fineDublicate = true;
                                }
                            }
                            if(fineDublicate) { FIELD_NAME = 'is-invalid'; good = false; }
                            //endregion
                        }
                        let modelStatus = {USER_TYPE_ID:type,NAME:name,FIELD_NAME:FIELD_NAME};
                        this.$set(this.fieldsStatus, i, modelStatus);
                    }
                    return good;
                },
                validate() { return this.validateFields() & this.validateNames(); },
                //endregion

                onGenerate() {
                    let app = this;
                    let good = this.validate();
                    if(good) {
                        let entity = {
                            'FILES': { // Описание файлов
                                'CLASS': this.nameEntity_2.toLowerCase() + '.php',
                                'STRUCTURE': this.nameEntity_2.toLowerCase() + '_structure.php',
                            },
                            'DB': { // Описание БД
                                'NAME_RU': this.nameEntity,
                                'NAME': this.nameEntity_2,
                                'TABLE_NAME': this.nameEntity_3,
                                'UF_FIELDS': this.fields
                            },
                        };
                        if(app.addOrEdit) { app.$emit('update-entity', entity); }
                        else { app.$emit('add-entity', entity); }
                        app.$emit('back-to-actions-entity');
                    }
                },
                setEntityToEdit() {
                    if(this.edit) {
                        this.nameEntity = this.edit.DB.NAME_RU;
                        this.nameEntity_2 = this.edit.DB.NAME;
                        this.nameEntity_3 = this.edit.DB.TABLE_NAME;
                        this.fields = [];
                        this.fieldsStatus = [];
                        if(this.isset(this.edit.DB.UF_FIELDS)) {
                            for (let i = 0; i < this.edit.DB.UF_FIELDS.length; i++) {
                                let model = this.edit.DB.UF_FIELDS[i];
                                Vue.set(this.fields, i, model);
                                Vue.set(this.fieldsStatus, i, model);
                            }
                        }
                        this.validate();
                        var app = this;
                        setTimeout(function () {
                            app.handleScroll();
                        },0);
                    }
                },
                handleScroll: function (evt, el) {

                    //region fixed el
                    el = $('#box_controlls');
                    if(typeof($(el).offset()) != 'undefined') {
                        if ($(document).scrollTop() + $(window).height() > ($(el).offset().top + $(el).height()) &&
                            $(document).scrollTop() - ($(el).offset().top + $(el).height()) < $(el).height()
                        ) { /*элемент на экране*/ }
                        else { //элемент не на экране
                            $(el).css('background','white').css('position','fixed').css('bottom','0')
                                .css('padding','10px 20px').css('border','1px solid rgba(0,0,0,.125)')
                                .css('border-bottom','none').css('margin-left','-21px').find('hr').css('display','none');
                            this.elFixed = true;
                        }
                    }

                    //endregion

                    //region unFixed el
                    if(this.elFixed) {
                        var el_2 = $('#app');
                        if ( $(document).scrollTop() + $(window).height() > ($(el_2).offset().top + $(el_2).height()) &&
                             $(document).scrollTop() - ($(el_2).offset().top + $(el_2).height()) < $(el_2).height()
                        ) { //элемент на экране
                            $(el).attr('style', '').find('hr').css('display','block');
                            this.elFixed = false;
                        }
                    }
                    //endregion

                }
            },
            template: `
                <div>
                    <div v-if="State === 'createEntityHL'" key="createEntityHL" class="row">
                        <div class="col-sm">

                            <h5 class="">Наименование сущности > ClassName > TableName:</h5>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm">
                                        <input type="text" class="form-control" :class="nameEntityStatus"
                                               style="padding: 18px"
                                               placeholder="NAME_RU: Тестовая таблица HL сущности" v-model="nameEntity"
                                               @input="changeNameEntity" :autofocus="'autofocus'">
                                        <i class="fas fa-angle-right"
                                           style="position:absolute;top:4px;right:-7px;font-size:30px;color:#6c757d;"></i>
                                    </div>
                                    <div class="col-sm">
                                        <input type="text" class="form-control" :class="nameEntityStatus_2"
                                               style="padding: 18px"
                                               placeholder="CLASS_NAME: TestEntityHL" v-model="nameEntity_2">
                                        <i class="fas fa-angle-right"
                                           style="position:absolute;top:4px;right:-7px;font-size:30px;color:#6c757d;"></i>
                                    </div>
                                    <div class="col-sm">
                                        <input type="text" class="form-control" :class="nameEntityStatus_3"
                                               style="padding: 18px"
                                               placeholder="TABLE_NAME: test_entity_hl" v-model="nameEntity_3">
                                    </div>
                                </div>
                            </div>

                            <h5 v-if="fields.length > 0">Пользовательские поля:</h5>
                            <div class="form-group" v-show="showFields">
                                <ul id="sortable" style="padding: 0;">
                                    <li style="list-style-type:none; height:50px;" v-for="(field,index) in fields">
                                        <div class="form-row align-items-center">

                                            <div class="col-auto">
                                                <i class="fas fa-arrows-alt-v handle" style="
                                                                                padding: 9px;
                                                                                border-radius: 5px;
                                                                                border: 1px solid #a3a5a5;
                                                                                color: #2c2c2c;
                                                                                font-size: 18px;
                                                                            "></i>
                                            </div>

                                            <div class="col">
                                                <select class="custom-select" v-bind:class="fieldsStatus[index].USER_TYPE_ID"
                                                        v-model="field.USER_TYPE_ID" @change="changeOneFieldType(index)">
                                                    <option v-for="type in types" :value="type.value">{{ type.name }}</option>
                                                </select>
                                            </div>

                                            <div class="col">
                                                <input type="text" class="form-control" style="padding: 18px"
                                                       v-bind:class="fieldsStatus[index].name" placeholder="Наименование"
                                                       v-model="field.NAME" @input="changeOneFieldName(index)">
                                            </div>

                                            <div class="col">
                                                <input type="text" maxlength="17" class="form-control" style="padding: 18px"
                                                       v-bind:class="fieldsStatus[index].FIELD_NAME"
                                                       v-model="field.FIELD_NAME" placeholder="Символьный код">
                                            </div>

                                            <div class="col-auto">
                                                <select class="custom-select" v-model="field.MULTIPLE">
                                                    <option value="N" selected>Не множественное</option>
                                                    <option value="Y">Множественное</option>
                                                </select>
                                            </div>

                                            <div class="col-auto">
                                                <select class="custom-select" v-model="field.MANDATORY">
                                                    <option value="N" selected>Не обязателное</option>
                                                    <option value="Y">Обязателное</option>
                                                </select>
                                            </div>

                                            <div class="col-auto">
                                                <button v-if="false" type="button" class="btn btn-outline-secondary" @click="onUpSort(index)">
                                                    <i class="fas fa-chevron-down fa-rotate-180"></i>
                                                </button>
                                                <button v-if="false" type="button" class="btn btn-outline-secondary" @click="onDownSort(index)">
                                                    <i class="fas fa-chevron-down"></i>
                                                </button>
                                                <button type="button" class="btn btn-outline-danger flag_my" @click="onDelete(index)"
                                                        style="color:#dc3545">
                                                    <i class="fas fa-trash-alt "></i>
                                                </button>
                                            </div>

                                        </div>
                                    </li>
                                </ul>
                            </div>

                        </div>
                    </div>

                    <div style="height: 55px; min-width: 100px;">

                        <div v-scroll="handleScroll" id="box_controlls">

                            <hr>

                            <button @click="onAddField();" class="btn btn-success mr-2">
                                <i class="fas fa-plus mr-1"></i>
                                Добавить поле
                            </button>

                            <button class="btn btn-info mr-2"
                                    v-show="fields.length"
                                    @click="additionalSettings = !additionalSettings">
                                <i class="fas mr-1" :class="classBtnAdditionalSettings"></i>
                                Доп. настройки полей
                            </button>

                            <button type="submit" class="btn btn-outline-success mr-2" @click="onGenerate">
                                <i class="fas fa-layer-group mr-1"></i>
                                <span v-if="edit">Сохранить</span><span v-if="!edit">Генерировать</span> сущность
                            </button>

                            <button type="button" class="btn btn-outline-danger mr-2" @click="onReset">
                                <i class="fas fa-step-backward"></i> Очистить всё
                            </button>

                            <button v-if="edit" type="button" class="btn btn-outline-primary mr-2" @click="setEntityToEdit">
                                <i class="fas fa-undo"></i> Восстановить исходник
                            </button>

                            <button type="button" class="btn btn-outline-info" @click="$emit('back-to-actions-entity')">
                                <i class="fas fa-long-arrow-alt-left mr-1"></i> Назад
                            </button>

                        </div>
                    </div>

                </div>
            `
        });
        //endregion

        //region Main App Vue
        var app = new Vue({ // родительское приложение
            el: '#app',
            data: {
                edit:false,
                types: <?=json_encode($arUserTypes_L)?>,
                ind: 0,
                action: 'actions',
                listEntity: <?=json_encode($listEntityStructure)?>,
                selectEntityHL: <?=json_encode($SelectEntityHL)?>,
            },
            methods: {
                changeInd(new_ind) {
                    this.ind = new_ind;
                    BX.MyAjax.Get('COption/SetOption',{'data':{CODE:'SelectEntityHL',VALUE:this.listEntity[this.ind]}});
                },
                addEntity(entity) {
                    app = this;
                    BX.MyAjax.Get('EntityHL/CreateEntityHL',{'data':entity});
                    this.ind = this.listEntity.length;
                    this.$set( this.listEntity, this.ind, entity );
                    this.changeInd(this.ind);
                },
                updateEntity(entity) {
                    BX.MyAjax.Get('EntityHL/UpdateEntityHL',{'data':{NewEntity:entity,OldEntity:this.edit}});
                    this.$set( this.listEntity, this.ind, entity );
                },
                editEntity(entity) { this.edit = entity; this.action = 'entity'; },
                deleteEntity(index) {
                    this.$delete(this.listEntity,index);
                },
                sortedListEntity(arr) {
                    function compare(a, b) {
                        if (a.DB.NAME_RU < b.DB.NAME_RU)
                            return -1;
                        if (a.DB.NAME_RU > b.DB.NAME_RU)
                            return 1;
                        return 0;
                    }
                    return arr.sort(compare);
                },
                isset(val) {
                    if (typeof val == "undefined") { return false; }
                    return true;
                },
            },
            created: function () {
                this.listEntity = this.sortedListEntity(this.listEntity);
                if(this.selectEntityHL) {
                    for (var i=0; i < this.listEntity.length; i++) {
                        if(this.listEntity[i].FILES.CLASS === this.selectEntityHL.FILES.CLASS) {
                            this.ind = i;
                            break;
                        }
                    }
                }
            }
        });
        //endregion

    </script>
    <style>

        .fade-enter-active, .fade-leave-active {
            transition: opacity .2s;
        }
        .fade-enter, .fade-leave-to /* .fade-leave-active до версии 2.1.8 */ {
           opacity: 0;
        }

        /* Анимации появления и исчезновения могут иметь */
        /* различные продолжительности и динамику.       */

        /* preloader */
        .bounce-enter-active {
            animation: bounce-in .5s;
        }
        .bounce-leave-active {
            animation: bounce-in .5s reverse;
        }
        @keyframes bounce-in {
            0% {
                transform: scale(0);
                opacity: 0;
            }
            50% {
                transform: scale(1.5);
                opacity: 0.5;
            }
            100% {
                transform: scale(1);
                opacity: 1;
            }
        }

        .flip-enter-active {
            transition: all .2s cubic-bezier(0.55, 0.085, 0.68, 0.53); //ease-in-quad
        }

        .flip-leave-active {
            transition: all .25s cubic-bezier(0.25, 0.46, 0.45, 0.94); //ease-out-quad
        }

        .flip-enter, .flip-leave-to {
            transform: scaleY(0) translateZ(0);
            opacity: 0;
        }


        .list-complete-item {
            transition: all 1s;
            display: inline-block;
            margin-right: 10px;
        }
        .list-complete-enter, .list-complete-leave-to
            /* .list-complete-leave-active до версии 2.1.8 */ {
            opacity: 0;
            transform: translateY(30px);
        }
        .list-complete-leave-active {
            position: absolute;
        }


       /* .loader{
            width: 150px;
            height: 150px;
            margin: 40px auto;
            transform: rotate(-45deg);
            font-size: 0;
            line-height: 0;
            animation: rotate-loader 5s infinite;
            padding: 25px;
            border: 1px solid #cf303d;
        }
        .loader .loader-inner{
            position: relative;
            display: inline-block;
            width: 50%;
            height: 50%;
        }
        .loader .loading{
            position: absolute;
            background: #cf303d;
        }
        .loader .one{
            width: 100%;
            bottom: 0;
            height: 0;
            animation: loading-one 1s infinite;
        }
        .loader .two{
            width: 0;
            height: 100%;
            left: 0;
            animation: loading-two 1s infinite;
            animation-delay: 0.25s;
        }
        .loader .three{
            width: 0;
            height: 100%;
            right: 0;
            animation: loading-two 1s infinite;
            animation-delay: 0.75s;
        }
        .loader .four{
            width: 100%;
            top: 0;
            height: 0;
            animation: loading-one 1s infinite;
            animation-delay: 0.5s;
        }
        @keyframes loading-one {
            0% {
                height: 0;
                opacity: 1;
            }
            12.5% {
                height: 100%;
                opacity: 1;
            }
            50% {
                opacity: 1;
            }
            100% {
                height: 100%;
                opacity: 0;
            }
        }
        @keyframes loading-two {
            0% {
                width: 0;
                opacity: 1;
            }
            12.5% {
                width: 100%;
                opacity: 1;
            }
            50% {
                opacity: 1;
            }
            100% {
                width: 100%;
                opacity: 0;
            }
        }
        @keyframes rotate-loader {
            0% {
                transform: rotate(-45deg);
            }
            20% {
                transform: rotate(-45deg);
            }
            25% {
                transform: rotate(-135deg);
            }
            45% {
                transform: rotate(-135deg);
            }
            50% {
                transform: rotate(-225deg);
            }
            70% {
                transform: rotate(-225deg);
            }
            75% {
                transform: rotate(-315deg);
            }
            95% {
                transform: rotate(-315deg);
            }
            100% {
                transform: rotate(-405deg);
            }
        }*/


        .animationload {
            background-color: #fff;
            height: 100%;
            left: 0;
            position: fixed;
            top: 0;
            width: 100%;
            z-index: 10000;
        }
        .osahanloading {
            animation: 1.5s linear 0s normal none infinite running osahanloading;
            background: #fed37f none repeat scroll 0 0;
            border-radius: 50px;
            height: 50px;
            left: 50%;
            margin-left: -25px;
            margin-top: -25px;
            position: absolute;
            top: 50%;
            width: 50px;
        }
        .osahanloading::after {
            animation: 1.5s linear 0s normal none infinite running osahanloading_after;
            border-color: #85d6de transparent;
            border-radius: 80px;
            border-style: solid;
            border-width: 10px;
            content: "";
            height: 80px;
            left: -15px;
            position: absolute;
            top: -15px;
            width: 80px;
        }
        @keyframes osahanloading {
            0% {
                transform: rotate(0deg);
            }
            50% {
                background: #85d6de none repeat scroll 0 0;
                transform: rotate(180deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }
        .slide-fade-enter-active {
            transition: all .3s ease;
        }
        .slide-fade-leave-active {
            transition: all .3s cubic-bezier(1.0, 0.5, 0.8, 1.0);
        }
        .slide-fade-enter, .slide-fade-leave-to /* .slide-fade-leave-active до версии 2.1.8 */ {
            transform: translateX(10px);
            opacity: 0;
        }
    </style>
    <?
}
if ( $_SERVER['REQUEST_METHOD'] === 'GET' ) {
    require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/epilog_admin.php');
}
?>