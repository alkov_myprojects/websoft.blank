<?
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_before.php');
if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_after.php');
}

use \Bitrix\Main\Config\Option;
use Bitrix\Main\Localization\Loc;

use Websoft\Blank\Admin\CustomAdminTags; /** TODO:: Обязательно поменять namespace, если не соответсвует */

Loc::loadMessages(__FILE__);

$MODULE_ID = "";
$dir = dirname(__FILE__);
for ($i = 1; $i < ($level = 2); $i++) { $dir = dirname($dir); }
include $dir.'/module_id.php';
$module_id = $MODULE_ID;
if ($module_id && CModule::IncludeModule($module_id)) {
    if ($_SERVER['REQUEST_METHOD']==='GET'){$GLOBALS['APPLICATION']->SetTitle('Сущности HL');}

    $arUserTypes = $USER_FIELD_MANAGER->GetUserType();
    $arUserTypes_L = array(
        array('value' => "", 'name' => '---------- Выберите тип поля ----------')
    );
    foreach ($arUserTypes as $arUserType) {
        $arUserTypes_L[] = array(
                'value' => $arUserType['USER_TYPE_ID'],
                'name' => $arUserType['DESCRIPTION'],
        );
    }
    ?>

    <style>
        .adm-workarea select {
            background: #fff url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 4 5'%3E%3Cpath fill='%23343a40' d='M2 0L0 2h4zm0 5L0 3h4z'/%3E%3C/svg%3E") no-repeat right .75rem center !important;
            background-size: 8px 10px !important;
        }
        .custom-select {

           /*all: unset;*/

           width: 100%;

           background-size: 8px 10px;

           -webkit-appearance: none;
           -moz-appearance: none;

           background: none;
           border-radius: .25rem;

           color: #495057;
           -webkit-box-shadow: none;
           box-shadow: none;
           font-size: unset;
           height: calc(2.25rem + 2px) !important;
           margin: unset;
           vertical-align: middle !important;
           padding: .375rem 1.75rem .375rem .75rem !important;
           -webkit-font-smoothing: none;
        }

        .custom-select.is-invalid, .form-control.is-invalid, .was-validated .custom-select:invalid,
        .was-validated .form-control:invalid {
            border-color: #dc3545 !important;
        }
        .custom-select.is-valid, .form-control.is-valid, .was-validated .custom-select:valid,
        .was-validated .form-control:valid {
            border-color: #28a745 !important;
        }
        .flag_my:hover i{
            color: white;
        }
    </style>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
          crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/solid.css"
          integrity="sha384-aj0h5DVQ8jfwc8DA7JiM+Dysv7z+qYrFYZR+Qd/TwnmpDI6UaB3GJRRTdY8jYGS4" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/fontawesome.css"
          integrity="sha384-WK8BzK0mpgOdhCxq86nInFqSWLzR5UAsNg0MGX9aDaIIrFWQ38dGdhwnNCAoXFxL" crossorigin="anonymous">

    <style>
        .adm-header-right {
            height: 45px;
        }
        .adm-header-btn-site, .adm-header-btn-admin {
            padding-bottom: 27px;
        }
        .adm-header-notif-block {
            padding-bottom: 21px;
        }

    </style>

    <div class="container-fluid">
        <div class="row " style="margin-left: 0 !important;">
            <h2>Создание сущности HL</h2>
        </div>
        <hr>
    </div>

    <div id="app" >
        <div class="container-fluid">
            <div v-if="State === 'createEntityHL'" key="createEntityHL" class="row">
                <div class="col-sm">
                    <h5 class="">Наименование сущности > ClassName > TableName:</h5>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm">
                                <input type="text" class="form-control" :class="nameEntityStatus"
                                       style="padding: 18px"
                                       placeholder="RU: Тестовая таблица HL сущности" v-model="nameEntity"
                                       @input="changeNameEntity"
                                >
                                <i class="fas fa-angle-right"
                                   style="
                                        position: absolute;
                                        top: 4px;
                                        right: -7px;
                                        font-size: 30px;
                                        color: #6c757d;
                                    "
                                ></i>
                            </div>
                            <div class="col-sm">
                                <input type="text" class="form-control" :class="nameEntityStatus_2"
                                       style="padding: 18px"
                                       placeholder="CLASS_NAME: TestEntityHL" v-model="nameEntity_2">

                                <i class="fas fa-angle-right"
                                   style="
                                        position: absolute;
                                        top: 4px;
                                        right: -7px;
                                        font-size: 30px;
                                        color: #6c757d;
                                    "
                                ></i>
                            </div>
                            <div class="col-sm">
                                <input type="text" class="form-control" :class="nameEntityStatus_3"
                                       style="padding: 18px"
                                       placeholder="TABLE_NAME: test_entity_hl" v-model="nameEntity_3">
                            </div>
                        </div>
                    </div>

                    <h5>Пользовательские поля:</h5>
                    <div class="form-group" v-show="showFields">
                        <div class="form-row align-items-center" v-for="(field,index) in fields"
                             style="margin-bottom: 15px;">
                            <div class="col-auto">
                                <select class="custom-select"
                                        v-bind:class="fieldsStatus[index].type"
                                        v-model="field.type"
                                        @change="changeOneFieldType(index)"

                                >
                                    <option v-for="type in types" :value="type.value">{{ type.name }}</option>
                                </select>
                            </div>

                            <!-- MULTIPLE -->
                            <div class="col-auto">
                                <select class="custom-select">
                                    <option value="N" selected>Не множественное</option>
                                    <option value="Y">Множественное</option>
                                </select>
                            </div>
                            <!-- END MULTIPLE -->

                            <!-- MANDATORY -->
                            <div class="col-auto">
                                <select class="custom-select">
                                    <option value="N" selected>Не обязателное</option>
                                    <option value="Y">Обязателное</option>
                                </select>
                            </div>
                            <!-- END MANDATORY -->

                            <!-- NAME_FIELD_RU -->
                            <div class="col">
                                <input type="text" class="form-control" style="padding: 18px"
                                       v-bind:class="fieldsStatus[index].name"
                                       placeholder="Наименование"
                                       v-model="field.name"
                                       @input="changeOneFieldName(index)"
                                >
                            </div>
                            <!-- END NAME_FIELD_RU -->

                            <!-- CODE_FIELD_RU -->
                            <div class="col">
                                <input type="text" maxlength="17" class="form-control" style="padding: 18px"
                                       v-bind:class="fieldsStatus[index].code"
                                       v-model="field.code"
                                       placeholder="Символьный код"
                                >
                            </div>
                            <!-- END CODE_FIELD_RU -->

                            <div class="col-auto">
                                <button type="button" class="btn btn-outline-secondary" @click="onUpSort(index)">
                                    <i class="fas fa-chevron-down fa-rotate-180"></i>
                                </button>
                                <button type="button" class="btn btn-outline-secondary" @click="onDownSort(index)">
                                    <i class="fas fa-chevron-down"></i>
                                </button>
                                <button type="button" class="btn btn-outline-danger flag_my" @click="onDelete(index)"
                                        style="color: #dc3545">
                                    <i class="fas fa-trash-alt "></i>
                                </button>
                            </div>
                            <br>
                        </div>
                    </div>
                    <button @click="onAddField()" class="btn btn-success">+ Добавить поле</button>
                    <hr>
                    <button type="submit" class="btn btn-info" @click="onGenerate">Генерировать сущность</button>
                    <button type="button" class="btn btn-secondary" @click="onReset">Cброс</button>
                </div>
            </div>
            <!-- Preloader -->
            <transition name="bounce">
                <div v-show="prelodaer" class="row" style="
                                                        position: fixed !important;
                                                        top: 0 !important;
                                                        left: 0 !important;
                                                        width: 105% !important;
                                                        height: 100% !important;
                                                        background: rgba(255,255,255,0.34);
                                                        padding-top: 0%;
                                                        z-index: 99999;
                                                    ">
                    <div class="osahanloading"></div>
                </div>
            </transition>
            <!-- end Prelodaer -->

        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script>
        var app = new Vue({
            el: '#app',
            data: {

                prelodaer: false,
                State: 'createEntityHL',

                //region animation
                //State: 'createEntityHL',
                //createEntityHL: true,
                //test: false,
                //endregion

                types: types(),

                //region RU
                nameEntity: '',
                nameEntityStatus: '',
                //endregion

                //region NAME
                nameEntity_2: '',
                nameEntityStatus_2: '',
                //endregion

                //region TABLE_NAME
                nameEntity_3: '',
                nameEntityStatus_3: '',
                //endregion

                fields: [],
                fieldsStatus: []

            },
            computed: {
                showFields() { return (this.fields.length > 0) ? true : false; },
            },
            methods: {
                changeOneFieldType(i) {
                    let type = 'is-valid';
                    if(!this.fields[i].type) { type = 'is-invalid'; }
                    this.$set(this.fieldsStatus, i, {type:type,name:this.fieldsStatus[i].name});
                },
                changeOneFieldName(i) {
                    this.yandex_translate_ru_en(this.fields[i].name,function (text) {

                        //region конвертация наименование пользовательского поля на English
                        let a = text;
                        a = a.replace(/[^a-zA-Z0-9 ]/g, ""); // убираем все спец символы
                        a = a.toUpperCase(); // все прописные буквы
                        a = a.replace(/\s/g, '_'); // заменить пробелы на _
                        a = a.substring(0,17); // сократить до 17-ти символов (ограничение Bitrix)
                        //endregion

                        app.fields[i].code = a;
                        let name = 'is-valid',code = 'is-valid';
                        if(!app.fields[i].name) { name = 'is-invalid'; }
                        if(!app.fields[i].code) { code = 'is-invalid'; }
                        app.$set(app.fieldsStatus, i, {type:app.fieldsStatus[i].type,name:name,code:code});

                    });
                },
                changeNameEntity() {
                    this.yandex_translate_ru_en(this.nameEntity,function (text) {

                        text = text.replace(/[^a-zA-Z0-9 ]/g, ""); // убираем все спец символы

                        //region генерация NAME

                            //region каждая буква заглавная
                            let a = text; //transliterate(text); //this.nameEntity); // транслитирование
                            let b = '';
                            for (let i = 0; i < a.length; i++) { b += (a[i - 1] == ' ') ? a[i].toUpperCase() : a[i]; }
                            //endregion

                            //region Сделать первую букву заглавной
                            b = b.charAt(0).toUpperCase() + b.substr(1);
                            //endregion

                            //region Убираем все пробелы
                            b = b.replace(/\s/g, '');
                            //endregion

                            app.nameEntity_2 = b;

                        //endregion

                        //region генерация TABLE_NAME
                            let c = text //transliterate(text); // транслитирование
                            c = c.toLowerCase(); // все строчные буквы
                            c = c.replace(/\s/g, '_'); // заменить пробелы на _
                            app.nameEntity_3 = c;
                        //endregion

                        app.validateNames();
                    });
                },
                onAddField() {
                    let model = {type:'',multiply: false,mandatory: false,name:'',code:''};
                    Vue.set(this.fields, this.fields.length, model);
                    Vue.set(this.fieldsStatus, this.fieldsStatus.length, model);
                },
                onReset() { this.nameEntity = ''; this.fields = [];  this.fieldsStatus = []; },
                onUpSort(index) {
                    if(index > 0) {
                        let field_before = this.fields[index - 1];
                        let field_this = this.fields[index];
                        this.$set(this.fields, index - 1, field_this);
                        this.$set(this.fields, index, field_before);

                        let field_before_status = this.fieldsStatus[index - 1];
                        let field_this_status = this.fieldsStatus[index];
                        this.$set(this.fieldsStatus, index - 1, field_this_status);
                        this.$set(this.fieldsStatus, index, field_before_status);
                    }
                },
                onDownSort(index) {
                    if(index < this.fields.length - 1) {
                        let field_after = this.fields[index + 1];
                        let field_this = this.fields[index];
                        this.$set(this.fields, index + 1, field_this);
                        this.$set(this.fields, index, field_after);

                        let field_after_status = this.fieldsStatus[index + 1];
                        let field_this_status = this.fieldsStatus[index];
                        this.$set(this.fieldsStatus, index + 1, field_this_status);
                        this.$set(this.fieldsStatus, index, field_after_status);
                    }
                },
                onDelete(index) { this.$delete(this.fields, index); this.$delete(this.fieldsStatus, index); },
                validateNames() {
                    let good = true;

                    if(!this.nameEntity) { good = false; this.nameEntityStatus = 'is-invalid'; }
                    else { this.nameEntityStatus = 'is-valid'; }

                    if(!this.nameEntity_2) { good = false; this.nameEntityStatus_2 = 'is-invalid'; }
                    else { this.nameEntityStatus_2 = 'is-valid'; }

                    if(!this.nameEntity_3) { good = false; this.nameEntityStatus_3 = 'is-invalid'; }
                    else { this.nameEntityStatus_3 = 'is-valid'; }
                    return good;
                },
                validateFields() {
                    let good = true;
                    for (let i = 0; i < this.fieldsStatus.length; i++) {
                        let type = 'is-valid', name = 'is-valid', code = 'is-valid';
                        if(!this.fields[i].type) { type = 'is-invalid'; good = false; }
                        if(!this.fields[i].name) { name = 'is-invalid'; good = false; }
                        if(!this.fields[i].code) { code = 'is-invalid'; good = false; }
                        this.$set(this.fieldsStatus, i, {type:type,name:name,code:code});
                    }
                    return good;
                },
                validate() {
                    let good = (this.validateFields() & this.validateNames()) ? true : false;
                    return good;
                },
                onGenerate() {
                    let good = this.validate();
                    if(good) {
                        //this.createEntityHL = false;
                        //this.test = true;

                        this.prelodaer = true;
                            /*setTimeout(function () {
                                app.prelodaer = false;
                            },3000);*/

                        //alert('good');
                        console.log('this.nameEntity: ' + this.nameEntity);
                        console.log('this.fields');
                        console.log(this.fields);

                        let data = [];
                        BX.MyAjax.Get('CreateEntityHL',{
                                'date': {
                                    'FILES': { // Описание файлов
                                        'CLASS': this.nameEntity_2.toLowerCase() + '.php',
                                        'STRUCTURE': this.nameEntity_2.toLowerCase() + '_structure.php',
                                    },
                                    'DB': { // Описание базы данные
                                        'NAME_RU': this.nameEntity,
                                        'NAME': this.nameEntity_2,
                                        'TABLE_NAME': this.nameEntity_3,
                                        'UF_FIELDS': {

                                        }
                                    },
                                }
                            },'html', function (data, textStatus, jqXHR) {
                                app.prelodaer = false;
                                console.log(123);
                                console.log('data');
                                console.log(data);
                            }
                        );
                    }
                },
                yandex_translate_ru_en(text,func_call) {
                    if(text) {
                        let url = "https://translate.yandex.net/api/v1.5/tr.json/translate",
                            keyAPI = "trnsl.1.1.20181229T092301Z.36e52fb990b6a776.bb0daa83f1687e43280fbd1362b4518963759080";
                        let xhr = new XMLHttpRequest(),
                            textAPI = text, //this.nameEntity, //document.querySelector('#source').value,
                            langAPI = 'ru-en', //document.querySelector('#lang').value;
                            data = "key="+keyAPI+"&text="+textAPI+"&lang="+langAPI;
                        xhr.open("POST",url,true);
                        xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                        xhr.send(data);
                        xhr.onreadystatechange = function() {
                            if(this.responseText) {
                                let json = JSON.parse(this.responseText); if(func_call) {
                                    translate_text = json.text[0];
                                    if(!translate_text) { translate_text = ''; }
                                    func_call(translate_text);
                                }
                            }
                        };
                    } else { if(func_call) { func_call(''); } }
                }
            }
        });

        //region приватные данные
        function types() { return <?=json_encode($arUserTypes_L,true)?>; }

        //Если с английского на русский, то передаём вторым параметром true.
        transliterate = (
        function() {
            var
            rus = "щ   ш  ч  ц  ю  я  ё  ж  ъ  ы  э  а б в г д е з и й к л м н о п р с т у ф х ь".split(/ +/g),
            eng = "shh sh ch cz yu ya yo zh `` y' e` a b v g d e z i j k l m n o p r s t u f x `".split(/ +/g)
            ;
            return function(text, engToRus) {
                var x;
                for(x = 0; x < rus.length; x++) {
                    text = text.split(engToRus ? eng[x] : rus[x]).join(engToRus ? rus[x] : eng[x]);
                    text = text.split(engToRus ? eng[x].toUpperCase() : rus[x].toUpperCase()).join(engToRus ? rus[x].toUpperCase() : eng[x].toUpperCase());
                }
                return text.replace(/[^a-zA-Z 0-9]/g, "");
            }
        }
        )();

        //endregion

    </script>
    <style>
        .fade-enter-active, .fade-leave-active {
            transition: opacity .2s;
        }
        .fade-enter, .fade-leave-to /* .fade-leave-active до версии 2.1.8 */ {
           opacity: 0;
        }

        /* preloader */
        .bounce-enter-active {
            animation: bounce-in .5s;
        }
        .bounce-leave-active {
            animation: bounce-in .5s reverse;
        }
        @keyframes bounce-in {
            0% {
                transform: scale(0);
                opacity: 0;
            }
            50% {
                transform: scale(1.5);
                opacity: 0.5;
            }
            100% {
                transform: scale(1);
                opacity: 1;
            }
        }


       /* .loader{
            width: 150px;
            height: 150px;
            margin: 40px auto;
            transform: rotate(-45deg);
            font-size: 0;
            line-height: 0;
            animation: rotate-loader 5s infinite;
            padding: 25px;
            border: 1px solid #cf303d;
        }
        .loader .loader-inner{
            position: relative;
            display: inline-block;
            width: 50%;
            height: 50%;
        }
        .loader .loading{
            position: absolute;
            background: #cf303d;
        }
        .loader .one{
            width: 100%;
            bottom: 0;
            height: 0;
            animation: loading-one 1s infinite;
        }
        .loader .two{
            width: 0;
            height: 100%;
            left: 0;
            animation: loading-two 1s infinite;
            animation-delay: 0.25s;
        }
        .loader .three{
            width: 0;
            height: 100%;
            right: 0;
            animation: loading-two 1s infinite;
            animation-delay: 0.75s;
        }
        .loader .four{
            width: 100%;
            top: 0;
            height: 0;
            animation: loading-one 1s infinite;
            animation-delay: 0.5s;
        }
        @keyframes loading-one {
            0% {
                height: 0;
                opacity: 1;
            }
            12.5% {
                height: 100%;
                opacity: 1;
            }
            50% {
                opacity: 1;
            }
            100% {
                height: 100%;
                opacity: 0;
            }
        }
        @keyframes loading-two {
            0% {
                width: 0;
                opacity: 1;
            }
            12.5% {
                width: 100%;
                opacity: 1;
            }
            50% {
                opacity: 1;
            }
            100% {
                width: 100%;
                opacity: 0;
            }
        }
        @keyframes rotate-loader {
            0% {
                transform: rotate(-45deg);
            }
            20% {
                transform: rotate(-45deg);
            }
            25% {
                transform: rotate(-135deg);
            }
            45% {
                transform: rotate(-135deg);
            }
            50% {
                transform: rotate(-225deg);
            }
            70% {
                transform: rotate(-225deg);
            }
            75% {
                transform: rotate(-315deg);
            }
            95% {
                transform: rotate(-315deg);
            }
            100% {
                transform: rotate(-405deg);
            }
        }*/


        .animationload {
            background-color: #fff;
            height: 100%;
            left: 0;
            position: fixed;
            top: 0;
            width: 100%;
            z-index: 10000;
        }
        .osahanloading {
            animation: 1.5s linear 0s normal none infinite running osahanloading;
            background: #fed37f none repeat scroll 0 0;
            border-radius: 50px;
            height: 50px;
            left: 50%;
            margin-left: -25px;
            margin-top: -25px;
            position: absolute;
            top: 50%;
            width: 50px;
        }
        .osahanloading::after {
            animation: 1.5s linear 0s normal none infinite running osahanloading_after;
            border-color: #85d6de transparent;
            border-radius: 80px;
            border-style: solid;
            border-width: 10px;
            content: "";
            height: 80px;
            left: -15px;
            position: absolute;
            top: -15px;
            width: 80px;
        }
        @keyframes osahanloading {
            0% {
                transform: rotate(0deg);
            }
            50% {
                background: #85d6de none repeat scroll 0 0;
                transform: rotate(180deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }
    </style>
    <?
}
if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/epilog_admin.php');
}
?>