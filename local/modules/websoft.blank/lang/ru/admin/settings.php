<?php
$MODULE_ID = "";
$dir = dirname(__FILE__);
for ($i = 1; $i < ($level = 4); $i++) { $dir = dirname($dir); }
include $dir.'/module_id.php';
$defOpt = \Bitrix\Main\Config\Option::getDefaults($MODULE_ID);
$class_up = $defOpt['class_up'].'_';

/** Установка значений языковый переменных */
$MESS[$class_up.'_EXAMPLE_MESS_SETTINGS'] = 'Тестовое сообщение страницы НАСТРОЕК';
$MESS[$class_up.'TAB_1'] = 'TAB_1';
$MESS[$class_up.'TAB_2'] = 'TAB_2';

$MESS[$class_up.'CalendarDate'] = 'CalendarDate';
$MESS[$class_up.'CheckBox'] = 'CheckBox';
$MESS[$class_up.'TextArea'] = 'TextArea';
$MESS[$class_up.'Input'] = 'Input';
$MESS[$class_up.'Select'] = 'Select';
$MESS[$class_up.'Select2'] = 'Select2';
$MESS[$class_up.'SelectMultiple'] = 'SelectMultiple';
$MESS[$class_up.'CheckBoxMultiple'] = 'CheckBoxMultiple';
$MESS[$class_up.'RadioButtom'] = 'RadioButtom';

/** Системные вкладки */
$MESS[$class_up.'TAB_0'] = 'Режим разработки';
$MESS[$class_up.'Btn1'] = 'Синхронизация <b style="color: red;">файлов</b> в папке <b>/install/</b> с публичной частью (если ';
$MESS[$class_up.'Btn1'] .= 'файлы в модуле будут изменены, то модуль зафиксирует эти изменения и перезапишет файлы в ';
$MESS[$class_up.'Btn1'] .= 'публичной части)';
$MESS[$class_up.'Btn2'] = 'Синхронизация <b style="color: red;">сущностей</b> в папке <b>/lib/entity/</b> (если изменились ';
$MESS[$class_up.'Btn2'] .= 'поля, таблица будет удалена и заново создавана, <b><i>данные в ней будут утеряны</i></>!!!)';
?>