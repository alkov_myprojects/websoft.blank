<?php
$MODULE_ID = "";
$dir = dirname(__FILE__);
for ($i = 1; $i < ($level = 4); $i++) { $dir = dirname($dir); }
include $dir.'/module_id.php';
$defOpt = \Bitrix\Main\Config\Option::getDefaults($MODULE_ID);
$class_up = $defOpt['class_up'].'_';

/** Установка значений языковый переменных */
$MESS[$class_up.'GLOBAL_MENU_TEXT'] = 'Болванка модуля';
$MESS[$class_up.'DESCRIPTION_TITLE'] = 'Документация';
$MESS[$class_up.'SETTINGS_TITLE'] = 'Настройки';
$MESS[$class_up.'ENTITY_HL_TITLE'] = 'Сущности HL';
?>