<?php
defined('B_PROLOG_INCLUDED') || die;
$MESS['TestEntity2Table'] = 'Тестовая Сущность 2';
$MESS['ID'] = 'ID_2';
$MESS['NAME'] = 'Название_2';
$MESS['ASSIGNED_BY'] = 'Ответственный_2';
$MESS['DATE_TIME'] = 'Дата и время_2';
$MESS['TEST_FIELD'] = 'Тестовое поле типа строка_2';
$MESS['BOOL'] = 'Булеан_2';
$MESS['FLOAT'] = 'Число с плавающей точкой_2';
$MESS['ENUM'] = 'Список_2';
$MESS['TEXT'] = 'Текст_2';


//region Добавление элемента
$MESS['NameOneElementToAdd'] = 'Элемента тестовой сущности 2'; // Добавление кого?
//endregion