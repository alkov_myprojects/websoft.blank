<?php
/**
 * Позволяет хранить настройки по умолчанию в одном месте и легко их
 * изменять
 *
 * Вместо
 * Option::get("some_module", "some_param", 128);
 *
 * используйте
 * COption::get("some_module", "some_param"); // + default_option.php
 */
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
$MODULE_ID = "";
$dir = dirname(__FILE__);
for ($i = 1; $i < ($level = 1); $i++) { $dir = dirname($dir); }
include $dir.'/module_id.php';
$MODULE_ID = str_replace(".","_",$MODULE_ID);
$module_class_name = $MODULE_ID;
${$module_class_name.'_default_option'} = array(
    'class' => $module_class_name,
    'class_up' => strtoupper($module_class_name),
);