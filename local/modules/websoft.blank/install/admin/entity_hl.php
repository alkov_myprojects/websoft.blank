<?
require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
$basename = basename(__FILE__);
$MODULE_ID = explode("_",$basename);
$MODULE_ID = $MODULE_ID[0];
if (CModule::IncludeModule($MODULE_ID)) {
    $dir = COption::GetOptionString($MODULE_ID, "dir");
    require_once($dir."/admin/entity_hl.php");
}
?>