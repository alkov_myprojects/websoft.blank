<?
    if(!check_bitrix_sessid()) {
        return;
    }
    $aMsg_module =  COption::GetOptionString('alkov_entities', "LastError");
    foreach ($aMsg_module as $error) {
        echo CAdminMessage::ShowMessage(array( "MESSAGE" => $error ));
    }
?>