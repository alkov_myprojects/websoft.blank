<?
/**
 * <b><i>Болванка партнёрского модуля</i></b>
 *
 * Обязательные действия, при создании нового модуля:
 * <ul>
 *      <li>
 *          <b><i>Изменить название папки модуля <u>партнер.модуль</u></i></b> - латиницей в нижнем регистре
 *      </li>
 *      <li>
 *          <b><i>В папке lib у классов поменять простраства имён</i></b> -> начало <Партнер>\<Модуль>
 *          далее можно любое подпространство, главное чтобы начала namespace было именно таким, иначе
 *          autoloader D7 не подключит класс.
 *          Так же как памятка: имя файла в нижнем регистра, а название класса не зависит от регистра,
 *          но они должны совпадать.
 *      </li>
 * </ul>
 *
 * Пример получения экземпляра класса модуля:
 * <code>
 * $module = 'websoft.blank';
 *    if(CModule::includeModule($module)) {
 *    $Module = CModule::CreateModuleObject($module);
 * }
 * </code>
 */
class websoft_blank extends CModule {

    //region Публичны
    /** @var int индекс сортировки модуля в общем списке */
    public $MODULE_SORT = 100;

    /** @var string путь до корневой папки модуля */
    public $dir = '';
    //endregion

    //region Приватные переменные экзепляра класса модуля
    /** @var array - Папки с файлами, которые нужно копировать/удалить при установке */
    private $directories_files = array();
    private $directories_files_2 = array();
    private $listEvents = array();
    //endregion

    function __construct() {

        $MODULE_ID = "";
        $dir = dirname(__FILE__);
        for ($i = 1; $i < ($level = 2); $i++) { $dir = dirname($dir); }
        $this->dir = $dir;
        include ($dir.'/module_id.php');
        
        /**
         * @var string идентификатор модуля в фомате <код_партнёра>.<код_модуля>, обязательно в нижнем регистре
         * без нижнего подчеркивания “_”, иначе не будет воркать includeModule
         */
        $this->MODULE_ID = $MODULE_ID;

        /** version.php - номер и дата версии. Использовать только двойные кавычки!!!! Одинарные воркать не будут… */
        $arModuleVersion = array();
        include($dir.'/install/version.php');

        /**
         * @var string версия модуля в формате XX.XX.XX, определяется в файле /install/version.php
         * в корневой папке модуля, минимальная версия модуля 0.0.1
         */
        $this->MODULE_VERSION = $this->GetValArr($arModuleVersion,"VERSION");

        /** @var string - Дата версии модуля, в формате YYYY-MM-DD HH:MI:SS; */
        $this->MODULE_VERSION_DATE = $this->GetValArr($arModuleVersion,"VERSION_DATE");

        //region папка lang
        $listCodeConstLang = array(
            'MODULE_NAME',        // Наименование модуля
            'MODULE_DESCRIPTION', // Краткое описание модуля
            'PARTNER_NAME',       // Наименование партнера(разработчик модуля)
            'PARTNER_URI',        // Адрес партнера(разработчик модуля)
        );
        $class_up = strtoupper(get_class());
        foreach ($listCodeConstLang as $code) {
            if($val = GetMessage($class_up.'_'.$code)) {
                $this->$code = $val;
            }
        }
        //endregion

        //region Инициализация путей для копирования/удаления папок c файлами

        $this->directories_files[$dir.'/install/js/'] = '/bitrix/js/'.$MODULE_ID.'/';
        $this->directories_files[$dir.'/install/css/'] = '/bitrix/css/'.$MODULE_ID.'/';
        $this->directories_files[$dir.'/install/ajax/'] = '/bitrix/ajax/'.$MODULE_ID.'/';
        $this->directories_files[$dir.'/install/images/'] = '/bitrix/images/'.$MODULE_ID.'/';
        $this->directories_files[$dir.'/install/pub/'] = '/';
        $this->directories_files[$dir.'/install/components_add/'] = '/bitrix/components/'.$MODULE_ID.'/';

        // Уникальное копироавние, файлы будут переименованы перед копирование <партнер>.<модуль>_<файл>
        $this->directories_files_2[$dir.'/install/admin/'] = '/bitrix/admin/';

        //endregion

        //region COption
        COption::SetOptionString($MODULE_ID,"dir",$dir);
        if(!empty($this->directories_files)) {
            $json = json_encode($this->directories_files,true);
            COption::SetOptionString($MODULE_ID,"directories_files",$json);
        }
        //endregion

        //region Регистрация событий
        $this->listEvents = array(
            'main' => array(
                'OnBeforeProlog' => array(
                    array( 'class' => "Handler", 'method' => 'init' ),
                    //array('class'=>"Admin\CustomAdminTags",'method'=>'test'),
                ),
                //region class User
                'OnBeforeUserUpdate' => array(
                    array( 'class' => 'Classes\User', 'method' => 'OnBeforeUserUpdate' ),
                )
                //endregion
            )
        );
        //endregion

        /** Настройка закладок и опций формы настройки модуля */

    }

    //region Обязателные методы модуля (запускаются при Установке/Удалении)
    function DoInstall() {

        // Перед установкой, нужно проверить папки на права, убедиться что папки /bitrix/css/, /bitrix/admin/ и
        // /bitrix/images/ доступны для записи... иначе не все файлы модуля зальются корректно.

        global $APPLICATION;
        $dir = dirname(__FILE__);
        for ($i = 1; $i < ($level = 2); $i++) { $dir = dirname($dir); }

        /** Установка */
        // Install events
        RegisterModule($this->MODULE_ID);
        if($this->InstallFiles()) {
            $this->InstallEvents();
            $APPLICATION->IncludeAdminFile(GetMessage('DO_INSTALL')." {$this->MODULE_ID}", $dir.'/install/step.php');
        } else {
            /** Вывод ошибок */
            $aMsg = array('Ошибка копирования файлов');
            COption::SetOptionString($this->MODULE_ID,"LastError", $aMsg);
            $APPLICATION->IncludeAdminFile( GetMessage('ERRORS_INSTALL'), $dir.'/install/error_install.php' );
        }
        return true;
    }
    function DoUninstall() {
        global $APPLICATION;
        //$dir = dirname(__FILE__,2);
        $dir = dirname(__FILE__);
        for ($i = 1; $i < ($level = 2); $i++) { $dir = dirname($dir); }
        //UnRegisterModuleDependences("iblock","OnAfterIBlockElementUpdate","entities","cMainentities","onBeforeElementUpdateHandler");
        $this->UnInstallFiles();
        $this->UnInstallEvents();
        UnRegisterModule($this->MODULE_ID);
        $APPLICATION->IncludeAdminFile(GetMessage('DO_UNINSTALL')." {$this->MODULE_ID}", $dir.'/install/unstep.php');
        $this->UnInstallDB();
        return true;
    }
    //endregion

    //region Не Обязателные методы модуля (-//-)
    function InstallDB() {
        // Тут не воркало... запихнул в Handler
        /*Loader::includeModule('websoft.blank');
        $db = Application::getConnection();
        $storeEntity = TestTable::getEntity();
        if (!$db->isTableExists($storeEntity->getDBTableName())) {
            $storeEntity->createDbTable();
        }*/
        /*$dir = dirname(__FILE__);
        for ($i = 1; $i < ($level = 2); $i++) { $dir = dirname($dir); }
        $module_id = basename($dir);
        Loader::includeModule($module_id);
        $db = \Bitrix\Main\Application::getConnection();
        foreach ($this->listEntity as $entityName) {
            $entity = $entityName::getEntity();
            if (!$db->isTableExists($entity->getDBTableName())) {
                $entity->createDbTable();
            }
        }*/
    }
    function UnInstallDB() {}
    //endregion

    //region Файлы
    function InstallFiles() {
        foreach ($this->directories_files as $IN => $TO) {
            if(is_array($TO)) {
                if(!CopyDirFiles($IN, $_SERVER['DOCUMENT_ROOT'].$TO['path_to'], $TO['rewrite'])) {
                    return false;
                }
            } else {
                if(!CopyDirFiles($IN, $_SERVER['DOCUMENT_ROOT'].$TO, true, true)) {
                    return false;
                }
            }
        }
        foreach ($this->directories_files_2 as $IN => $TO) {
            if(is_array($TO)) {
                if(!$this->CopyDirFiles_custom(
                    $IN, $_SERVER['DOCUMENT_ROOT'].$TO['path_to'], $TO['rewrite'],
                    true,false,"",$this->MODULE_ID.'_'
                )) { return false; }
            } else {
                if(!$this->CopyDirFiles_custom($IN, $_SERVER['DOCUMENT_ROOT'].$TO, true, true,
                    false,false,$this->MODULE_ID.'_')) {
                    return false;
                }
            }
        }
        return true;
    }
    function UnInstallFiles(){
        foreach ($this->directories_files as $IN => $TO) {
            if(is_array($TO)) {
                DeleteDirFiles($IN, $TO['path_to']);
            } else {
                DeleteDirFilesEx($TO);
            }
        }
        return true;
    }
    function CopyDirFiles_custom(
        $path_from, $path_to, $ReWrite = True, $Recursive = False,
        $bDeleteAfterCopy = False, $strExclude = "",$prefix = ""
    ) {
        if (strpos($path_to."/", $path_from."/")===0 || realpath($path_to) === realpath($path_from))
            return false;

        if (is_dir($path_from)) { CheckDirPath($path_to."/"); }
        elseif(is_file($path_from)) {
            $p = bxstrrpos($path_to, "/");
            $path_to_dir = substr($path_to, 0, $p);
            CheckDirPath($path_to_dir."/");

            if (file_exists($path_to) && !$ReWrite)
                return False;

            @copy($path_from, $path_to);
            if(is_file($path_to))
                @chmod($path_to, BX_FILE_PERMISSIONS);

            if ($bDeleteAfterCopy)
                @unlink($path_from);

            return true;
        }
        else { return true; }

        if ($handle = @opendir($path_from)) {
            while (($file = readdir($handle)) !== false) {
                if ($file == "." || $file == "..")
                    continue;

                if (strlen($strExclude)>0 && substr($file, 0, strlen($strExclude))==$strExclude)
                    continue;

                if (is_dir($path_from."/".$file) && $Recursive) {
                    CopyDirFiles($path_from."/".$file, $path_to."/".$file, $ReWrite, $Recursive, $bDeleteAfterCopy, $strExclude);
                    if ($bDeleteAfterCopy)
                        @rmdir($path_from."/".$file);
                }
                elseif (is_file($path_from."/".$file)) {
                    if (file_exists($path_to."/".$file) && !$ReWrite)
                        continue;

                    @copy($path_from."/".$file, $path_to."/{$prefix}".$file);
                    @chmod($path_to."/".$file, BX_FILE_PERMISSIONS);

                    if($bDeleteAfterCopy)
                        @unlink($path_from."/".$file);
                }
            }
            @closedir($handle);

            if ($bDeleteAfterCopy)
                @rmdir($path_from);

            return true;
        }
        return false;
    }
    //endregion

    //region События
    function InstallEvents() {
        foreach ($this->listEvents as $module => $events) {
            foreach ($events as $event => $list) {
                foreach ($list as $data) {
                    \Bitrix\Main\EventManager::getInstance()->registerEventHandler(
                        $module, $event, $this->MODULE_ID, $this->GetNameSpace().$data['class'], $data['method']
                    );
                }
            }
        }
    }
    function UnInstallEvents() {
        foreach ($this->listEvents as $module => $events) {
            foreach ($events as $event => $list) {
                foreach ($list as $data) {
                    \Bitrix\Main\EventManager::getInstance()->unRegisterEventHandler(
                        $module, $event, $this->MODULE_ID, $this->GetNameSpace().$data['class'], $data['method']
                    );
                }
            }
        }
    }
    //endregion

    //region Вспомогательные приватные методы класса

    /**
     * Проверяет параметр $arr на массив, ищет ключ $key в массиве, если не массив или нет ключа возвращает mixed|null,
     * иначе возворащает значение массива по ключу $key.
     * @param $arr - ассоциативный массив
     * @param $key - ключ этого массива, по нему извлекаем занчение массива $arr
     * @return mixed|null
     */
    private function GetValArr($arr,$key) {
        if (is_array($arr) && array_key_exists($key, $arr)) { return $arr[$key]; }
        return null;
    }

    /**
     * Получить пространство имён текущего модуля
     * @return string
     */
    public function GetNameSpace() {
        $arrMODULE_ID = explode(".",$this->MODULE_ID);
        $partner = ucfirst($arrMODULE_ID[0]);
        $module = ucfirst($arrMODULE_ID[1]);
        $namespace = "\\".$partner."\\".$module."\\";
        return $namespace;
    }
    //endregion

    //region Публичные методы

        //region COption
        public function GetOption($code) {
            $value = \COption::GetOptionString($this->MODULE_ID,$code);
            $Handler = $this->GetNameSpace().'Handler';
            if($Handler::isJson($value)) {
                return json_decode($value,true);
            }
            return $value;
        }
        public function SetOption($code,$value) {
            if(is_array($value)) {
                $value = json_encode($value);
            }
            \COption::SetOptionString($this->MODULE_ID,$code,$value);
        }
        public function GetAllOptions() {
            $options = array();
            global $DB;
            $results = $DB->Query("SELECT o.MODULE_ID, o.NAME, o.VALUE FROM b_option o WHERE `MODULE_ID`='{$this->MODULE_ID}'");
            while ($row = $results->Fetch()) {
                $option = array( 'CODE' => $row['NAME'], 'VALUE' => $row['VALUE']);
                $Handler = $this->GetNameSpace().'Handler';
                if($Handler::isJson($option['VALUE'])) {
                    $option['VALUE'] = json_decode($option['VALUE'],true);
                }
                $options[] = $option;
            }
            return $options;
        }
        //endregion

    //endregion
}