<?php require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");

//region Входящие данные
$data = $_REQUEST['data'];
//endregion

//region $module_id
$dir = dirname(__FILE__);
for ($i = 1; $i < ($level = 2); $i++) { $dir = dirname($dir); }
$module_id = basename($dir);
//endregion

if(\CModule::includeModule($module_id)) {

    $Module = CModule::CreateModuleObject($module_id);

    /** @var \Websoft\Blank\Handler $Handler */
    $Handler = $Module->GetNameSpace().'Handler';

    /** @var \Websoft\Blank\Classes\CHLBlock $CHLBlock */
    $CHLBlock = $Module->GetNameSpace().'Classes\CHLBlock';

    //region Удалить таблицу БД
    $table_id = $CHLBlock::GetID_ByTableName($data['DB']['TABLE_NAME']);
    if($table_id) {
        $CHLBlock::DelTableByID($table_id);
    } else {
        echo 'Таблица HL с именем "'.$data['DB']['TABLE_NAME'].'" не найдена';
    }
    //endregion

    //region Удалить два файла (класс и структуру)
    $AllPathFilesEntityHL = $Handler::GetAllPathFilesEntityHL();
    foreach ($AllPathFilesEntityHL as $path) {
        foreach ($data['FILES'] as $nameFile) {
            if(strstr($path,$nameFile)) {
                unlink($path);
            }
        }
    }
    //endregion

}