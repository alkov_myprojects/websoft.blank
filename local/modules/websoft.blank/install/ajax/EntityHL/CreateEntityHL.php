<? require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");

//region генерация файла сущности
/**
 * 1) Как можно будет изменять данные сущности:
 *      1.1 Наименование
 *      1.2 Поля (множественное,символьный код,обязательное(Y/N),тип)
 * 2) Как привязывать сущности друг к другу?
 * 3) Как сделать одну большую сущность из нескольких?
 *
 */
//endregion

//region Входящие данные
$data = $_REQUEST['data'];
//endregion

//region $module_id
$dir = dirname(__FILE__);
for ($i = 1; $i < ($level = 2); $i++) { $dir = dirname($dir); }
$module_id = basename($dir);
//endregion


if(\CModule::includeModule($module_id)) {

    //region Подключение библиотек
    $Module = CModule::CreateModuleObject($module_id);
    $path_entity_hl = $Module->dir.'/lib/entity_hl/';

    /** @var \Websoft\Blank\Handler $Handler */
    $Handler = $Module->GetNameSpace().'Handler';

    /** @var \Websoft\Blank\Classes\CHLBlock $CHLBlock */
    $CHLBlock = $Module->GetNameSpace().'Classes\CHLBlock';
    //endregion

    //$listEntityStructure = $Handler::GetStructureAllEntityHL();

    //region Добавление талицы и полей сущности HL

    //... Таблица
    $DB_HL = $data['DB'];
    $ID_HL = $CHLBlock::AddTable($DB_HL['NAME'],$DB_HL['TABLE_NAME'],$DB_HL['NAME_RU']);

    //... Поля
    $UF_FIELDS = $data['DB']['UF_FIELDS'];
    foreach ($UF_FIELDS as $UF_FIELD) {
        $UF_FIELD['EDIT_FORM_LABEL']['ru'] = $UF_FIELD['NAME'];
        $UF_FIELD['FIELD_NAME'] = 'UF_'.$UF_FIELD['FIELD_NAME'];
        unset($UF_FIELD['NAME']);
        $errorsAdd = $CHLBlock::UF_Field_Add($ID_HL,$UF_FIELD);
        if(!empty($errorsAdd)) {

            echo '<b>$UF_FIELD</b>';
            echo '<pre>';
            print_r($UF_FIELD);
            echo '</pre>';

            echo '<b>$errorsAdd</b>';
            echo '<pre>';
            print_r($errorsAdd);
            echo '</pre>';

            echo '<hr>';
        }
    }

    //endregion

    //region Сохранение файла СТРУКТУРЫ сущности HL
    $Handler::SaveArrayToFile($path_entity_hl.'structures/'.$data['FILES']['STRUCTURE'],$data);
    //endregion

    //region создание КЛАССА сущности HL
    //$template = file_get_contents($path_entity_hl.'template_class_entity_hl.txt');
    //endregion

}