<?php require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");

//region Входящие данные
$data = $_REQUEST['data'];
//endregion

//region $module_id
$dir = dirname(__FILE__);
for ($i = 1; $i < ($level = 1); $i++) { $dir = dirname($dir); }
$module_id = basename($dir);
//endregion

if(\CModule::includeModule($module_id)) {
    $Module = \CModule::CreateModuleObject($module_id);
    $Module->SetOption($data['CODE'],$data['VALUE']);
}