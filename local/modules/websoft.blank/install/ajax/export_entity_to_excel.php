<?
require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Entity\IntegerField;   // TODO:: Целочисленное
use Bitrix\Main\Entity\ReferenceField; // TODO:: Свзять поля сущности с другой сущностью/элементом, работает в паре с IntegerField
use Bitrix\Main\Entity\StringField;    // TODO:: Строка
use Bitrix\Main\Entity\DatetimeField;  // TODO:: ДатаВремя
use Bitrix\Main\Entity\BooleanField;   // TODO:: Булеан
use Bitrix\Main\Entity\FloatField;     // TODO:: Число с плавающей точкой
use Bitrix\Main\Entity\EnumField;      // TODO:: Список
use Bitrix\Main\Entity\TextField;      // TODO:: Текстовое поле, Тоже самое что и строка, но можно определить тип

$dir = dirname(__FILE__);
for ($i = 1; $i < ($level = 1); $i++) { $dir = dirname($dir); }
$module_id = basename($dir);
if(CModule::IncludeModule($module_id)) {

    // Получение дефолтных настроек полудя (д/подключения сторонних бибилиотек, которые расположены в модуле)
    $defaultOptions = \Bitrix\Main\Config\Option::getDefaults($module_id);

    // Подключение бибилиотеки PHPExcel
    include ($defaultOptions['dir'].'/lib_add/PHPExcel-1.8/Classes/PHPExcel.php');

    // Получить сущность
    $entity = "";
    $arrNameSpace = json_decode($_REQUEST['DATA']['ENTITY']);
    foreach ($arrNameSpace as $nameSpace) { if($nameSpace) { $entity .= "\\".$nameSpace; } }

    // Получить шапку грида
    $grid_options = new CGridOptions($_REQUEST['DATA']['GRID_ID']);
    $visibleColumns = $grid_options->GetVisibleColumns();

    // Получить данные из БД
    $elements = $entity::getList(array('select' => $visibleColumns))->fetchAll();

    // Подготовить данные для excel таблицы
    $convertDate = new convertDate();
    //... Шапка
    $headerGrid = $entity::getHeaderGrid();

    //region Столбцы
    foreach ($visibleColumns as $code) {
        $name = '';
        foreach ($headerGrid as $item) { if($item['id'] == $code) { $name = $item['name']; break; } }
        $convertDate->setOneCol($name,$code);
    }
    //endregion

    //region Строки
    $NameAndType = $entity::GetColumnNameAndType();
    $NameAndType_UF = $entity::GetColumnNameAndType_UF();
    $GetAllEnums = $entity::GetAllEnums();
    foreach ($elements as &$element) {
        // Проверка на тип
        foreach ($element as $code => &$value) {
            // Простые
            if(isset($NameAndType[$code]) && $NameAndType[$code] instanceof BooleanField) {
                if($value) { $value = 'Да'; } else { $value = 'Нет'; }
            }
            if(isset($NameAndType[$code]) && $NameAndType[$code] instanceof EnumField) {
                if($value) { $value = $GetAllEnums[$code][$value]; }
            }
            // UF_*
            if(isset($NameAndType_UF[$code]) && $NameAndType_UF[$code] == 'file') {
                //$arrFile = CFile::GetFileArray($value);
                //$makeFileArray = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"].$arrFile['SRC']);
                if($value) { $value = 'Есть'; } else { $value = 'Нет'; }
            }
        }
        $convertDate->setOneRow($element); // кушает ассоциативыне массивы, где ключ это CODE столбца
    }
    //endregion

    //region Создание Excel
    $cols = $convertDate->getCols();
    $rows = $convertDate->getRows();
    $createExcelFile = new createExcelFile($cols,$rows);
    $resultCreateFile = $createExcelFile->createFile();
    //endregion

    //region Результирующий массив
    $result = array('link' => $resultCreateFile['link'],'nameFile' => $resultCreateFile['nameFile']);
    echo json_encode($result,true);
    //endregion
}

// .... Класс для занесения данных (автоматически преобразует столбцы,
// .... под размер контента, определять столбец [A,B,C и т.д.])
class convertDate {
    protected $cols = array();
    protected $rows = array();
    protected $alafvitExcel = array(
        'A','B','C','D','E','F','G',
        'H','I','J','K','L','M','N',
        'O','P','Q','R','S','T','U',
        'V','W','X','Y','Z'
    );

    // ********* Сеттеры *********
    //... добавить один столбец (его описание)
    public function setOneCol($name,$code) {
        $count = count($this->cols);
        $cnt_col = $count;     // кол-во текущих столбцов + текущий добавляемый
        $cnt_alf = count($this->alafvitExcel); // кол-во букв в алфавите
        $cnt = floor( ($cnt_col / $cnt_alf) + 1); // частота повторений буквы
        // первые 26-ть по одному A,B,C и т.д.
        // вторые 26-ть по два AA, BB, CC и т.д.
        // третьи 26-ть по три AAA, BBB, CCC и т.д.
        $ind = $cnt_col;
        if($cnt > 1) { $ind = $cnt_col - ($cnt_alf * ($cnt - 1)); }
        $ID = '';
        for ($i=0;$i<$cnt;$i++) { $ID .= $this->alafvitExcel[$ind]; }
        $this->cols[] = array('ID_EXCEL' => $ID, 'CODE' => $code, 'WIDTH' => $this->width($name), 'NAME' => $name);
    }
    //... добавить одну строку
    public function setOneRow($val_colums) {
        // Должен приходить ассоциативный массив, даже если он переменшан, мы ориентируемся на столбцы, которые
        // занесли ранее.
        $row = array();
        // Получить структру столбцов
        foreach ($this->cols as &$col) {
            $code = $col['CODE'];
            $val = $val_colums[$code];

            // Нужно расчитать ширину столбца в зависимости от контента.
            $width_content = $this->width($val);
            if($col['WIDTH'] < $width_content) { $col['WIDTH'] = $width_content; }

            // Занесение значения
            $val = str_replace('<br>', '; ', $val);
            $row[] = $val;
        }
        $this->rows[] = $row;
    }

    // Геттеры
    public function getCols() { return $this->cols; }
    public function getRows() { return $this->rows; }

    // ********* Расчётные формулы *********
    protected function width($str) {
        if(strlen($str) > 5) { return round(strlen($str) * 1.15); } else { return round(strlen($str) * 2); }
    }
}

//.... Класс для создания Excel файла
class createExcelFile {
    private $public_dir = '/upload/'; // Папка куда будет лежать сгенерированный файл EXCEL
    private $cols;
    private $rows;
    public function __construct($cols = array(),$rows = array()) { $this->cols = $cols; $this->rows = $rows; }
    public function createFile() {
        $tmp_dir = $_SERVER['DOCUMENT_ROOT'].$this->public_dir;
        $name = $this->RandomString() . '.XLSX';
        $excel_out = $tmp_dir.'/'.$name;
        try {
            $excel_format = "Excel2007"; //$excel_format = "Excel5"; //$excel_format = "HTML";
            $objPHPExcel = new PHPExcel();
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, $excel_format);
            $objPHPExcel->setActiveSheetIndex(0);
            $WorkSheet = $objPHPExcel->getActiveSheet();

            // Установить ширину столбцов
            foreach ($this->cols as $col) { $WorkSheet->getColumnDimension($col['ID_EXCEL'])->setWidth($col['WIDTH']); }

            //$WorkSheet->getColumnDimension('A')->setWidth(200);

            // Установить заначения первой строки (головы)
            $row_position = 1;
            foreach ($this->cols as $index => $col) {
                $type = PHPExcel_Cell_DataType::TYPE_STRING;
                $WorkSheet->getCellByColumnAndRow($index, $row_position)->setValueExplicit($col['NAME'], $type);
                $WorkSheet->getCellByColumnAndRow( $index, $row_position
                )->getStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $WorkSheet->getCellByColumnAndRow($index, $row_position
                )->getStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
                $WorkSheet->getCellByColumnAndRow($index, $row_position
                )->getStyle()->getAlignment()->setWrapText(true); // Перенос по словам
            };
            $row_position++;

            // Установить остальные строки
            foreach ($this->rows as $row) {
                foreach ($row as $index => $val_col) {
                    $type = PHPExcel_Cell_DataType::TYPE_STRING;

                    if(is_numeric($val_col)) { $type = PHPExcel_Cell_DataType::TYPE_NUMERIC; }
                    $WorkSheet->getCellByColumnAndRow($index, $row_position)->setValueExplicit($val_col, $type);

                    /*
                    $objPHPExcel->getActiveSheet()->getCell('B'.$row)->getHyperlink()->setUrl('http://www.www.com');
                    $objPHPExcel->getActiveSheet()->getCell('B'.$row)->getHyperlink()->setTooltip('Your title');
                     */

                    $WorkSheet->getCellByColumnAndRow($index, $row_position
                    )->getStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }
                $row_position++;
            }
            $objWriter->save($excel_out);
            return array( 'link' => $_SERVER['HTTP_ORIGIN'].$this->public_dir.$name, 'nameFile' => $name );
        } catch (ErrorException $e) { @unlink($excel_out); throw $e; }
    }
    private function RandomString(){
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randstring = '';
        for ($i = 0; $i < 10; $i++) { $randstring .= $characters[rand(0, strlen($characters))]; }
        return $randstring;
    }
}