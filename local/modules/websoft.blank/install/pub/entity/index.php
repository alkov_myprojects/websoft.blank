<?php require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php'); ?>
<?php
/** Пример комплексного компонента (ещё не доработан...) тут нужно настроить грид таблицу, просмотр и редактирование,
 *  но они есть уже =) */
$APPLICATION->IncludeComponent(
    'websoft.blank:entity', '', array(
        'MODE' => 'Y',
        'FOLDER' => '/entity/',
        'URL_TEMPLATES' => array(
            'list' => 'list/#ENTITY_UF_ID#/',
            //'details' => '#ID#/',
            'edit' => '#ENTITY_UF_ID#/#ID#/edit/',
        ),
        'ListEntity' => array(
            '\Websoft\Blank\Entity\TestEntity1Table',
            '\Websoft\Blank\Entity\TestEntity2Table',
        )
    ), false
);

/** Пример универсального списка, вроде только для инфоблоков */
/*$APPLICATION->IncludeComponent(
    "bitrix:lists",
    "",
    Array(
        "CACHE_TIME" => "3600",
        "CACHE_TYPE" => "A",
        "IBLOCK_TYPE_ID" => "lists",
        "SEF_MODE" => "N",
        "VARIABLE_ALIASES" => Array(
            "ID" => "ID",
            "document_state_id" => "document_state_id",
            "element_id" => "element_id",
            "field_id" => "field_id",
            "file_id" => "file_id",
            "list_id" => "list_id",
            "mode" => "mode",
            "section_id" => "section_id",
            "task_id" => "task_id"
        )
    )
);*/
?>
<?php require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php'); ?>