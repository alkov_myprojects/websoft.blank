/* ---------------------------------- Пространство имён ----------------------------------------- */

var MyAjax = BX.namespace('MyAjax');

/* ---------------------------------- Конструктор ----------------------------------------- */

/**
 * Конструктор класс для сокрашённых Ajax запросов и их обработчиков и путей до них
 * @param path_to_ajax_files - путь до корневой папки с обработчкиками
 * @constructor
 */
BX.MyAjax = function (path_to_ajax_files) {
    this.path_to_ajax_files = path_to_ajax_files;
    this.getParams = '';
    console.log('BX.MyAjax.this');
    console.log(this);
    console.log('----------------------------------------------------------');
};

/* ------------------------------------ Методы --------------------------------------------- */

/**
 * Сокращённых метод Ajax запроса
 * @param nameFile - наименование файла обработчика без расширения
 * @param data - Данные которые будут отпраленны обработчику -> массив тип { 'key':val , 'key2':val2 }
 * @param dataType - тип данных, которые пришлёт обработчик, по умолчанию "html".
 * @param funcDone - функция, которая будет вызвана в случае успеха, аргументы "data, textStatus, jqXHR"
 * @param funcFail - функция, которая будет вызвана в случае провала, аргументы "jqXHR, textStatus, error"
 * @param funcAlways - функция, которая будет вызвана в любом случае, аргументы "dataOrjqXHR, textStatus, jqXHRorErrorThrown"
 */
BX.MyAjax.prototype.Get = function (nameFile,data,dataType,funcDone,funcFail,funcAlways) {
    if(nameFile) {
        if(!data) { data = {}; }
        if(!dataType) { dataType = 'html'; }
        $.ajax({
            url: this.path_to_ajax_files + nameFile + '.php', dataType: dataType,data: data
        }).done(function(data, textStatus, jqXHR) { /** Успех */
            if(funcDone) { funcDone(data, textStatus, jqXHR); }
        }).fail(function(jqXHR, textStatus, error) { /** Провал */
            if(funcFail) { funcFail(jqXHR, textStatus, error); }
        }).always(function(dataOrjqXHR, textStatus, jqXHRorErrorThrown) { /** Всегда */
            if(funcAlways) { funcAlways(dataOrjqXHR, textStatus, jqXHRorErrorThrown); }
        });
    } else {
        console.log('Был вызван Ajax запрос, где не указанно имя файла!!!');
    }
};

BX.MyAjax.prototype.Get2 = function (nameFile,data,dataType,funcDone,funcFail,funcAlways) {
    if(nameFile) {
        if(!data) { data = {}; }
        if(!dataType) { dataType = 'html'; }
        $.ajax({
            url: this.path_to_ajax_files + nameFile, dataType: dataType,data: data
        }).done(function(data, textStatus, jqXHR) { /** Успех */
            if(funcDone) { funcDone(data, textStatus, jqXHR); }
        }).fail(function(jqXHR, textStatus, error) { /** Провал */
            if(funcFail) { funcFail(jqXHR, textStatus, error); }
        }).always(function(dataOrjqXHR, textStatus, jqXHRorErrorThrown) { /** Всегда */
            if(funcAlways) { funcAlways(dataOrjqXHR, textStatus, jqXHRorErrorThrown); }
        });
    } else {
        console.log('Был вызван Ajax запрос, где не указанно имя файла!!!');
    }
};

/* -------------- Вызовы методов класса при определённых действиях пользователя  ------------------- */