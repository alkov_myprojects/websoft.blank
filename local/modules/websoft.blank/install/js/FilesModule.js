/** ---------------------------------- Пространство имён ----------------------------------------- */

var FilesModule = BX.namespace('FilesModule');

/** ---------------------------------- Конструктор ----------------------------------------- */
BX.FilesModule = function () {
    this.MyPopupBX = new BX.MyPopupBX();
};

/** ------------------------------------ Методы --------------------------------------------- */
BX.FilesModule.prototype.Compare = function () {
    this.MyPopupBX.Message('AnalizeFilesModule','',BX.message('FilesModule_AnalizeFiles'),false);
    BX.MyAjax.myGet('AnalizeFilesModule',{},'json',
        function (data, textStatus, jqXHR) {
            console.log('data');
            console.log(data);
            if(data.length > 0 ) {
                // GetMessage("ERROR_MODULE_NOT_FOUND", Array ("#MODULE#" => "blog")
                html = '<table class="adm-main-wrap">' +
                    '    <tr style="padding-bottom: 5px; border-bottom: 1px;">' +
                    '        <td>Имя файла</td>' +
                    '        <td>Путь до файла в модуле</td>' +
                    '        <td>Путь до файла в публичке</td>' +
                    '    </tr>';

                for(i=0;i<data.length;i++) {
                    html += '    <tr>' +
                        '        <td>' + data[i]['NameFile'] + '</td>' +
                        '        <td>Путь до файла в модуле</td>' +
                        '        <td>Путь до файла в публичке</td>' +
                        '    </tr>';
                }



                html += '</table>';
                BX.FilesModule.MyPopupBX.Message(
                    'SuccessAjaxFilesModule',
                    'Список не синхронизованных файлов',
                    html,
                    true
                );
            }
            else {
                BX.FilesModule.MyPopupBX.Message( 'SuccessAjaxFilesModule', '', 'Все файлы синхронизированы', true );
            }
        },
        function (jqXHR, textStatus, error) {
            log = '<b>textStatus: </b>' + textStatus + '<br>';
            log += '<b>error: </b>' + error;
            BX.FilesModule.MyPopupBX.Message(
                'FailAjaxFilesModule',BX.message('FilesModule_AnalizeFiles_Fail'),
                BX.message('FilesModule_AnalizeFiles_Fail_LogError') + log,
                true
            );
        }
    );
};

/** -------------- Вызовы методов класса при определённых действиях пользователя  ------------------- */