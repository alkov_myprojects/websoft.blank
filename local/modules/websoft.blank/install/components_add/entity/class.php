<?php
defined('B_PROLOG_INCLUDED') || die;

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

class CEntityComponent extends CBitrixComponent {
    public function executeComponent() {

        /** Локальные переменные: */
        $FOLDER = $this->arParams['FOLDER'];
        $URL_TEMP = is_array($this->arParams['URL_TEMPLATES']) ? $this->arParams['URL_TEMPLATES'] : array();
        $getMes = 'getMessage';
        $MODE = $this->arParams['MODE'];
        $nameSpace = explode(':',$this->getName());
        $nameSpace = $nameSpace[0];

        /** Проерки: */
        if (!Loader::includeModule($nameSpace)) { $this->error('NO_MODULE',array('MID'=>$nameSpace)); return; }
        if (empty($MODE) || $MODE != 'Y') { ShowError(Loc::{$getMes}('NOT_ENABLED')); return; }
        if (empty($FOLDER)) { ShowError(Loc::{$getMes}('BASE_EMPTY')); return; }
        foreach ($this->arParams['ListEntity'] as $className) {
            if (!class_exists($className)) {
                ShowError(Loc::{$getMes}('EntityNotExists'), array('CLASS'=>$className)); return;
            }
        }

        /** ... */
        global $arVariables;
        $page = CComponentEngine::parseComponentPath( $FOLDER, $URL_TEMP, $arVariables );
        if (empty($page)) { $page = 'list'; } // Если нет страницы, то по умолчанию редирект на главную.

        /** Карта компонентов: */
        $urlTemplates = array(
            'LIST' => $FOLDER.$URL_TEMP['list'],'VIEW' => $FOLDER.$URL_TEMP['view'],'EDIT' => $FOLDER.$URL_TEMP['edit']
        );

        /** Подключение и передача данных компоненту: */
        $this->arResult = array(
            'URL_TEMPLATES' => $urlTemplates,
            'arVariables' => $arVariables,
            'nameSpace' => $nameSpace,
            'ListEntity' => $this->arParams['ListEntity'],
            'arVariables' => $arVariables,
            'ToAjax' => $this->arParams
        );
        $this->includeComponentTemplate($page);
    }
}