<?php
defined('B_PROLOG_INCLUDED') || die;

use Bitrix\Main\Localization\Loc;

/** @var CBitrixComponentTemplate $this */

$APPLICATION->SetTitle(Loc::getMessage('TITLE'));

//region Панелька CRM (Старт, Лиды, Сделки и т.д.), полезно если сюда вставляем ссылку на текущий компонет
/*$APPLICATION->IncludeComponent(
    'bitrix:crm.control_panel', '', array('ID' => 'ENTITY', 'ACTIVE_ITEM_ID' => 'ENTITY'), $component
);*/


//$APPLICATION->IncludeComponent(
//    'bitrix:crm.control_panel',
//    '',
//    array(
//        'ID' => 'DEAL_LIST',
//        'ACTIVE_ITEM_ID' => 'DEAL',
//        'PATH_TO_COMPANY_LIST' => isset($arResult['PATH_TO_COMPANY_LIST']) ? $arResult['PATH_TO_COMPANY_LIST'] : '',
//        'PATH_TO_COMPANY_EDIT' => isset($arResult['PATH_TO_COMPANY_EDIT']) ? $arResult['PATH_TO_COMPANY_EDIT'] : '',
//        /*'PATH_TO_CONTACT_LIST' => isset($arResult['PATH_TO_CONTACT_LIST']) ? $arResult['PATH_TO_CONTACT_LIST'] : '',
//        'PATH_TO_CONTACT_EDIT' => isset($arResult['PATH_TO_CONTACT_EDIT']) ? $arResult['PATH_TO_CONTACT_EDIT'] : '',
//        'PATH_TO_DEAL_WIDGET' => isset($arResult['PATH_TO_DEAL_WIDGET']) ? $arResult['PATH_TO_DEAL_WIDGET'] : '',
//        'PATH_TO_DEAL_LIST' => isset($arResult['PATH_TO_DEAL_LIST']) ? $arResult['PATH_TO_DEAL_LIST'] : '',
//        'PATH_TO_DEAL_EDIT' => isset($arResult['PATH_TO_DEAL_EDIT']) ? $arResult['PATH_TO_DEAL_EDIT'] : '',
//        'PATH_TO_DEAL_CATEGORY' => isset($arResult['PATH_TO_DEAL_CATEGORY']) ? $arResult['PATH_TO_DEAL_CATEGORY'] : '',
//        'PATH_TO_DEAL_WIDGETCATEGORY' => isset($arResult['PATH_TO_DEAL_WIDGETCATEGORY']) ? $arResult['PATH_TO_DEAL_WIDGETCATEGORY'] : '',
//        'PATH_TO_LEAD_LIST' => isset($arResult['PATH_TO_LEAD_LIST']) ? $arResult['PATH_TO_LEAD_LIST'] : '',
//        'PATH_TO_LEAD_EDIT' => isset($arResult['PATH_TO_LEAD_EDIT']) ? $arResult['PATH_TO_LEAD_EDIT'] : '',
//        'PATH_TO_QUOTE_LIST' => isset($arResult['PATH_TO_QUOTE_LIST']) ? $arResult['PATH_TO_QUOTE_LIST'] : '',
//        'PATH_TO_QUOTE_EDIT' => isset($arResult['PATH_TO_QUOTE_EDIT']) ? $arResult['PATH_TO_QUOTE_EDIT'] : '',
//        'PATH_TO_INVOICE_LIST' => isset($arResult['PATH_TO_INVOICE_LIST']) ? $arResult['PATH_TO_INVOICE_LIST'] : '',
//        'PATH_TO_INVOICE_EDIT' => isset($arResult['PATH_TO_INVOICE_EDIT']) ? $arResult['PATH_TO_INVOICE_EDIT'] : '',
//        'PATH_TO_REPORT_LIST' => isset($arResult['PATH_TO_REPORT_LIST']) ? $arResult['PATH_TO_REPORT_LIST'] : '',
//        'PATH_TO_DEAL_FUNNEL' => isset($arResult['PATH_TO_DEAL_FUNNEL']) ? $arResult['PATH_TO_DEAL_FUNNEL'] : '',
//        'PATH_TO_EVENT_LIST' => isset($arResult['PATH_TO_EVENT_LIST']) ? $arResult['PATH_TO_EVENT_LIST'] : '',
//        'PATH_TO_PRODUCT_LIST' => isset($arResult['PATH_TO_PRODUCT_LIST']) ? $arResult['PATH_TO_PRODUCT_LIST'] : '',*/
//        //'COUNTER_EXTRAS' => array('DEAL_CATEGORY_ID' => $categoryID)
//    ),
//    $component
//);
//endregion

//region Автофильтр - например "5 сделок требущих внимания"
/*$isBitrix24Template = SITE_TEMPLATE_ID === 'bitrix24';
if($isBitrix24Template) { $this->SetViewTarget('below_pagetitle', 0); }
$APPLICATION->IncludeComponent( 'bitrix:crm.entity.counter.panel', '', array(
    'ENTITY_TYPE_NAME' => CCrmOwnerType::DealName,
    //'EXTRAS' => array('DEAL_CATEGORY_ID' => $categoryID),
    //'PATH_TO_ENTITY_LIST' => $PATH_TO_ENTITY_LIST
));*/
//endregion

//region Какой то переключатель =)
/*if($isBitrix24Template) { $this->SetViewTarget('inside_pagetitle', 100); }
$APPLICATION->IncludeComponent( 'bitrix:crm.entity.list.switcher', '', array(
    'ENTITY_TYPE' => \CCrmOwnerType::Deal,
    'NAVIGATION_ITEMS' => array(
        array(
            'id' => 'list',
            'name' => GetMessage('CRM_DEAL_LIST_SWITCHER_LIST'),
            'active' =>$arResult['IS_RECURRING'] !== 'Y',
            'url' =>  $categoryID < 0
                ? $arResult['PATH_TO_DEAL_LIST']
                : CComponentEngine::makePathFromTemplate(
                    $arResult['PATH_TO_DEAL_CATEGORY'],
                    array('category_id' => $categoryID)
                )
        ),
        array(
            'id' => 'recur',
            'name' => GetMessage('CRM_DEAL_LIST_SWITCHER_RECUR'),
            'active' => $arResult['IS_RECURRING'] === 'Y',
            'url' =>  $categoryID < 0
                ? $arResult['PATH_TO_DEAL_RECUR']
                : CComponentEngine::makePathFromTemplate(
                    $arResult['PATH_TO_DEAL_RECUR_CATEGORY'],
                    array('category_id' => $categoryID)
                )
        )
    )
), $component );*/
//endregion

//region Автонастриваемый грид
$APPLICATION->IncludeComponent(
    $arResult['nameSpace'].':entity.list', '', array(
        'namespaceAndClassNameEntity' => $arResult['namespaceAndClassNameEntity'],
        'nameSpace' => $arResult['nameSpace'],
        'URL_TEMPLATES' => $arResult['URL_TEMPLATES'],
        'ToAjax' => $arResult['ToAjax']
    ), $this->getComponent(), array('HIDE_ICONS' => 'Y')
);
//endregion