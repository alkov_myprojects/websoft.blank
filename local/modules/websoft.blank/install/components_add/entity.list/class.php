<?php
defined('B_PROLOG_INCLUDED') || die;

use Academy\CrmStores\Entity\StoreTable;
use Bitrix\Main\Context;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\UI\PageNavigation;
use Bitrix\Main\UserTable;
use Bitrix\Main\Grid;
use Bitrix\Main\UI\Filter;
use Bitrix\Main\Web\Json;
use Bitrix\Main\Web\Uri;

class CEntityListComponent extends CBitrixComponent {

    //const SUPPORTED_ACTIONS = array('delete','e');
    const SUPPORTED_SERVICE_ACTIONS = array('GET_ROW_COUNT');

    private static $GRID_ID = 'ENTITY_LIST';
    private static $CLASS_ENTITY;
    private static $headers;
    private static $filterFields;
    private static $filterPresets;
    private static $SORTABLE_FIELDS;
    private static $FILTERABLE_FIELDS;

    public function __construct(CBitrixComponent $component = null) {

        global $USER;
        parent::__construct($component);

        //region Получение класса сущности
        $arVariables = $this->__parent->arResult['arVariables'];
        $className = $this->__parent->arResult['ListEntity'][0];
        self::$CLASS_ENTITY = $className;
        if($arVariables && isset($arVariables['ENTITY_UF_ID'])){
            $ENTITY_UF_ID = $arVariables['ENTITY_UF_ID'];
            foreach ($this->__parent->arResult['ListEntity'] as $entity) {
                if(strtolower($entity::getUfId()) == $ENTITY_UF_ID) { self::$CLASS_ENTITY = $entity; }
            }
        }
        //endregion

        // Проверка существования класса сущности
        if(class_exists(self::$CLASS_ENTITY)) {
            // Генерация идентификатора грида
            $className = explode("\\",self::$CLASS_ENTITY);
            $className = $className[count($className)-1];
            self::$GRID_ID .= '_'.strtoupper($className);

            /** Настройка грида */
            $entityClass = self::$CLASS_ENTITY;
            self::$headers = $entityClass::getHeaderGrid(); // ... столбцы доступные для отображения в гриде
            self::$SORTABLE_FIELDS = $entityClass::sortTableFieldsGrid();
            self::$FILTERABLE_FIELDS = $entityClass::filterTableFieldsGrid();
            self::$filterFields = $entityClass::filterFieldsGrid();
            self::$filterPresets = array(
                'my_stores' => array(
                    'name' => Loc::getMessage('CRMSTORES_FILTER_PRESET_MY_STORES'),
                    'fields' => array(
                        'ASSIGNED_BY_ID' => $USER->GetID(),
                        'ASSIGNED_BY_ID_name' => $USER->GetFullName(),
                    )
                )
            );
        }
    }

    public function executeComponent() {
        /** Локальные переменные: */
        $entityClass = self::$CLASS_ENTITY;

        /** Проверки: */
        if ($entityClass === '') { $this->error('NO_ENTITY'); return; }
        if (!class_exists($entityClass)){ $this->error('EntityNotExists', array('CLASS'=>$entityClass)); return; }

        /** Настройка грида */
        $context = Context::getCurrent();
        $request = $context->getRequest();

        $grid = new Grid\Options(self::$GRID_ID);

        //region Формирование сортировки (убираем мусор)
        $gridSort = $grid->getSorting();
        $sort = array_filter(
            $gridSort['sort'],function ($field){ return in_array($field,self::$SORTABLE_FIELDS); },
            ARRAY_FILTER_USE_KEY
        );
        if (empty($sort)) { $sort = array('ID' => 'asc'); }
        //endregion

        //region формирование фильтра (убираем мусор)
        $gridFilter = new Filter\Options(self::$GRID_ID, self::$filterPresets);
        $gridFilterValues = $gridFilter->getFilter(self::$filterFields);

        foreach ($gridFilterValues as $k => $v) {
            if (preg_match('/(.*)_from$/i'.BX_UTF_PCRE_MODIFIER, $k, $arMatch)) {
                \Bitrix\Crm\UI\Filter\Range::prepareFrom($gridFilterValues, $arMatch[1], $v);
            }
            elseif (preg_match('/(.*)_to$/i'.BX_UTF_PCRE_MODIFIER, $k, $arMatch)) {
                \Bitrix\Crm\UI\Filter\Range::prepareTo($gridFilterValues, $arMatch[1], $v);
            }
        }
        
        $gridFilterValues = array_filter(
            $gridFilterValues, function ($fieldName) { return in_array($fieldName, self::$FILTERABLE_FIELDS); },
            ARRAY_FILTER_USE_KEY
        );
        //endregion

        //region Обработка команд пользователя
        $this->processGridActions($gridFilterValues);
        $this->processServiceActions($gridFilterValues);
        //endregion

        //region Pagination
        $gridNav = $grid->GetNavParams();
        $pager = new PageNavigation('');
        $pager->setPageSize($gridNav['nPageSize']);
        $pager->setRecordCount($entityClass::getCount($gridFilterValues));
        if ($request->offsetExists('page')) {
            $currentPage = $request->get('page');
            $pager->setCurrentPage($currentPage > 0 ? $currentPage : $pager->getPageCount());
        }
        else { $pager->setCurrentPage(1); }
        //endregion

        //region Сбор данных (строки д/грида)
        $stores = $this->getData(array(
            'filter' => $gridFilterValues,'limit' => $pager->getLimit(),'offset' => $pager->getOffset(),'order' => $sort
        ));
        //endregion

        $requestUri = new Uri($request->getRequestedPage());
        $requestUri->addParams(array('sessid' => bitrix_sessid()));
        
        //region arResult
        $this->arResult = array(
            'GRID_ID' => self::$GRID_ID,
            'STORES' => $stores,
            'HEADERS' => self::$headers,
            'PAGINATION' => array(
                'PAGE_NUM' => $pager->getCurrentPage(),
                'ENABLE_NEXT_PAGE' => $pager->getCurrentPage() < $pager->getPageCount(),
                'URL' => $request->getRequestedPage(),
            ),
            'SORT' => $sort,
            'FILTER' => self::$filterFields,
            'FILTER_PRESETS' => self::$filterPresets,
            'ENABLE_LIVE_SEARCH' => false,
            'DISABLE_SEARCH' => true,
            'SERVICE_URL' => $requestUri->getUri(),
            'ThisEntity' => $entityClass,
            'SelectEntityUfId' => $entityClass::getUfId(),
            'ListEntity' => $this->__parent->arResult['ListEntity'],
            'URL_TEMPLATES' => $this->__parent->arResult['URL_TEMPLATES'],
            'arVariables' => $this->__parent->arResult['arVariables'],

            'menuCounter' => $this->arParams['ToAjax']['ToAjax']['menuCounter'],
            'mainNameSpace' => $this->arParams['ToAjax']['ToAjax']['mainNameSpace'],
            'listGroupEntity' => $this->arParams['ToAjax']['ToAjax']['listGroupEntity'],
        );
        //endregion arResult

        $this->includeComponentTemplate();
    }

    /** Системные методы */

    private function getData($params = array()) {

        $params['select'] = array('*','UF_*');
        $entityClass = self::$CLASS_ENTITY;

        //region Хак с булеанами
        $AllBooleans = $entityClass::GetAllBooleans();
        foreach ($AllBooleans as $codeBooleanField) {
            if(isset($params['filter'][$codeBooleanField])) {
                if($params['filter'][$codeBooleanField] == 'Y') { $params['filter'][$codeBooleanField] = true; }
                else { $params['filter'][$codeBooleanField] = false;  }
            }
        }
        //endregion

        $dbStores = $entityClass::getList($params);
        $stores = $dbStores->fetchAll();

        $userIds = array_column($stores, 'ASSIGNED_BY_ID');
        $userIds = array_unique($userIds);
        $userIds = array_filter( $userIds, function ($userId) { return intval($userId) > 0; } );

        $dbUsers = UserTable::getList(array( 'filter' => array('=ID' => $userIds) ));
        $users = array();
        foreach ($dbUsers as $user) { $users[$user['ID']] = $user; }
        $AllEnums = $entityClass::GetAllEnums();
        foreach ($stores as &$store) {
            if(intval($store['ASSIGNED_BY_ID']) > 0){ $store['ASSIGNED_BY'] = $users[$store['ASSIGNED_BY_ID']]; }
            foreach ($store as $key => &$value) { if(isset($AllEnums[$key])) { $value = $AllEnums[$key][$value]; } }
        }

        return $stores;
    }
    private function processGridActions($currentFilter) {

        if (!check_bitrix_sessid()) { return; }

        $entityClass = self::$CLASS_ENTITY;

        //region Получение Экшена
        $context = Context::getCurrent();
        $request = $context->getRequest();
        $idAction = 'action_button_' . self::$GRID_ID;
        $action = $request->get($idAction);
        $controls = $request->get('controls');
        if(isset($controls[$idAction]) && $controls[$idAction]) { $action = $controls[$idAction]; }
        //endregion

        //region Пометка "Для всех"
        $allRows = $request->get('action_all_rows_' . self::$GRID_ID) == 'Y';
        if ($allRows) {
            $dbStores = $entityClass::getList(array( 'filter' => $currentFilter,  'select' => array('ID'), ));
            $storeIds = array();
            foreach ($dbStores as $store) { $storeIds[] = $store['ID']; }
        } else {
            $storeIds = $request->get('ID');
            if (!is_array($storeIds)) { $storeIds = array(); }
        }
        //endregion

        switch ($action) {
            //region Массовое удаление элементов
            case 'delete':
                foreach ($storeIds as $storeId) { $entityClass::delete($storeId); }
            break;
            //endregion
            //region Массовое редактирование элементов
            case 'edit':
                $FIELDS = $request->get('FIELDS');
                foreach ($FIELDS as $id => $fields) {
                    $fields_before_checked = $fields; // Д/дебага
                    $checkedFields = $entityClass::CheckingCorrectValueFieldsBeforeInputToDB($fields);
                    if(!empty($checkedFields)){
                        foreach ($checkedFields as $code => $new_value){ $fields[$code] = $new_value; }
                    }
                    try {

                        \Bitrix\Main\Diag\Debug::writeToFile(array(
                            '$fields_before' => $fields_before_checked,
                            '$fields' => $fields,
                            '$checkedFields' => $checkedFields,
                        ), "", "log.txt");

                        $entityClass::update($id,$fields);

                    } catch (Exception $e) {
                        \Bitrix\Main\Diag\Debug::writeToFile(array(
                            'Exception' => $e
                        ), "", "log.txt");
                    }
                }
            break;
            //endregion
            //region Экспорт в Excel
            case 'printToExcel':
                //foreach ($storeIds as $storeId) {
                    //$entityClass::delete($storeId);
                //}
            break;
            //endregion
            //region Если нет обработчика, то то вызватьобработчик сущности
            default:
                $entityClass::processGridActions($action,$allRows,$request);
            break;
            //endregion
        }
    }
    private function processServiceActions($currentFilter){

        $entityClass = self::$CLASS_ENTITY;
        global $APPLICATION;
        if (!check_bitrix_sessid()) { return; }

        $context = Context::getCurrent();
        $request = $context->getRequest();

        $params = $request->get('PARAMS');

        if (empty($params['GRID_ID']) || $params['GRID_ID'] != self::$GRID_ID) { return; }

        $action = $request->get('ACTION');

        if (!in_array($action, self::SUPPORTED_SERVICE_ACTIONS)) { return; }

        $APPLICATION->RestartBuffer();
        header('Content-Type: application/json');

        switch ($action) {
            case 'GET_ROW_COUNT':
                $count = $entityClass::getCount($currentFilter);
                echo Json::encode(array(
                    'DATA' => array(
                        'TEXT' => Loc::getMessage('CRMSTORES_GRID_ROW_COUNT', array('#COUNT#' => $count))
                    )
                ));
            break;

            default:
            break;
        }

        die;
    }
    private function error($code,$arg=array()) { ShowError(Loc::getMessage($code,$arg)); }
}