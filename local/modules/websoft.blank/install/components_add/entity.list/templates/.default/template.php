<?php
defined('B_PROLOG_INCLUDED') || die;

use Bitrix\Main\Grid\Panel\Snippet;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Page\Asset;
use Bitrix\Main\Web\Json;

/** @var CBitrixComponentTemplate $this */

//region Системаные данные
if (!Loader::includeModule('crm')) { ShowError(Loc::getMessage('NO_MODULE')); return; }
$asset = Asset::getInstance();
$asset->addJs('/bitrix/js/crm/interface_grid.js');
$gridManagerId = $arResult['GRID_ID'];
$ActionsEntity = $arResult['ThisEntity']::getActions();
$APPLICATION->SetTitle(Loc::getMessage('TITLE',array('NAME'=>$arResult['ThisEntity']::getStringName())));

//endregion

//region Подключение вкусных плющек из коробки =)
Bitrix\Main\Page\Asset::getInstance()->addJs('/bitrix/js/crm/progress_control.js');
Bitrix\Main\Page\Asset::getInstance()->addJs('/bitrix/js/crm/activity.js');
Bitrix\Main\Page\Asset::getInstance()->addJs('/bitrix/js/crm/interface_grid.js');
Bitrix\Main\Page\Asset::getInstance()->addJs('/bitrix/js/crm/autorun_proc.js');
Bitrix\Main\Page\Asset::getInstance()->addJs('/bitrix/js/crm/batch_conversion.js');
Bitrix\Main\Page\Asset::getInstance()->addCss('/bitrix/js/crm/css/autorun_proc.css');
//endregion

//region Кнопки в правом верхнем углу
$BUTTONS = array();
if(in_array('add',$ActionsEntity)) {
    $BUTTONS[] = array(
        'TEXT' => Loc::getMessage('ADD_ELEMENT'), 'TITLE' => Loc::getMessage('ADD_ELEMENT'),
        'LINK' => CComponentEngine::makePathFromTemplate($arResult['URL_TEMPLATES']['EDIT'], array(
            'ID' => 0,
            'ENTITY_UF_ID' => strtolower($arResult['ThisEntity']::getUfId())
        )),
    );
}
//endregion

//region Шарнир (в правом верхнем углу)
if(
    in_array('export_excel',$ActionsEntity) || in_array('export_csv',$ActionsEntity) ||
    in_array('import',$ActionsEntity) || in_array('migration',$ActionsEntity)
) { $BUTTONS[] = array( 'NEWBAR' => true ); }

//region Экспорт

?>
<?
$arrThisEntity = $arResult['ThisEntity'];
$arrThisEntity = explode("\\",$arrThisEntity);
/*$headerToExportExcel = array();
foreach ($arResult['HEADERS'] as $col) { $headerToExportExcel[] = $col['id']; }*/

// Получить пользовательские настройки

?>
    <script>
        function export_to_excel() {
            PopupBX = new BX.MyPopupBX();
            PopupBX.Message('wait_export_to_excel','','Создание Excel файла, подождите...');
            BX.MyAjax.Get('export_entity_to_excel',{
                'DATA':{ 'ENTITY':'<?=json_encode($arrThisEntity)?>', 'GRID_ID':'<?=$arResult['GRID_ID']?>' }
            },'json',function (data, textStatus, jqXHR) {
                PopupBX.CloseLastPopup();
                console.log(data);
                //region Скачивание файла
                if(true) {
                    var link = document.createElement('a');
                    link.setAttribute('href', data['link']);
                    link.setAttribute('download', data['nameFile']);
                    onload = link.click();
                }
                //endregion

            },function (jqXHR, textStatus, error) {
                alert('Ошибка!!! Подробней в консоли (F12)');
                console.log('------ jqXHR -------');
                console.log(jqXHR);

                console.log('------ textStatus -------');
                console.log(textStatus);

                console.log('------ error -------');
                console.log(error);
            });
        }
    </script>
<?
if( in_array('export_excel',$ActionsEntity) ) {
    $BUTTONS[] = array( 'TEXT' => Loc::GetMessage('EXPORT_EXCEL'), 'ICON' => 'btn-export', 'ONCLICK' => 'export_to_excel();' );
}
if( in_array('export_csv',$ActionsEntity) ) {
    $BUTTONS[] = array( 'TEXT' => Loc::GetMessage('EXPORT_CSV'), 'ICON' => 'btn-export','ONCLICK' => 'alert("Экспорт в CSV");' );
}
if(in_array('export_excel',$ActionsEntity) || in_array('export_csv',$ActionsEntity)) {
    $BUTTONS[] = array( 'SEPARATOR' => true );
}
//endregion

//region Импорт
if( in_array('import',$ActionsEntity) ) {
    $BUTTONS[] = array('TEXT' => Loc::GetMessage('IMPORT'),'ICON' => 'btn-export','ONCLICK' => 'alert("Импорт");');
}
if( in_array('migration',$ActionsEntity) ) {
    $BUTTONS[] = array('TEXT' => Loc::GetMessage('MIGRATION'),'ICON' => 'btn-export','ONCLICK' => 'alert("Миграция");');
}
//endregion

$APPLICATION->IncludeComponent(
    'bitrix:crm.interface.toolbar', 'title', array(
    'TOOLBAR_ID' => 'TOOLBAR', 'BUTTONS' => $BUTTONS
    ), $this->getComponent(), array('HIDE_ICONS' => 'Y')
);
//endregion

//region Строки грида
$getHeaderGrid = $arResult['ThisEntity']::getHeaderGrid();
$rows = array();
foreach ($arResult['STORES'] as $element) {
    $viewUrl = CComponentEngine::makePathFromTemplate(
        $arParams['URL_TEMPLATES']['DETAIL'], array('STORE_ID' => $store['ID'])
    );
    $editUrl = CComponentEngine::makePathFromTemplate(
        $arParams['URL_TEMPLATES']['EDIT'], array('STORE_ID' => $store['ID'])
    );
    //region Действие "Удалить"
    $deleteUrlParams = http_build_query(array(
        'action_button_' . $arResult['GRID_ID'] => 'delete',
        'ID' => array($element['ID']),
        'sessid' => bitrix_sessid()
    ));
    $deleteUrl = $arParams['SEF_FOLDER'] . '?' . $deleteUrlParams;
    //endregion
    //region Действие "Редактировать"
    $editUrlParams = http_build_query(array(
        'action_button_' . $arResult['GRID_ID'] => 'edit',
        'ID' => array($element['ID']),
        'sessid' => bitrix_sessid()
    ));
    $editUrl = $arParams['SEF_FOLDER'] . '?' . $editUrlParams;

    $editUrlParams = CComponentEngine::makePathFromTemplate(
        $arParams['URL_TEMPLATES']['EDIT'], array(
            'ENTITY_UF_ID' => strtolower($arResult['ThisEntity']::getUfId()),
            'ID' => $element['ID']
        )
    );
    //echo '$editUrlParams: '.$editUrlParams.'<hr>';

    //endregion
    //region Уникальное отображение + запара с булеаном и файлом... =(
        $colums = $arResult['ThisEntity']::uniqueDisplayColumn($element);
        $CodeAllBooleans = $arResult['ThisEntity']::GetAllBooleans();
        foreach ($element as $code => &$value) { if(in_array($code,$CodeAllBooleans)) { $value = $value ? 'Y' : 'N'; } }
        foreach ($getHeaderGrid as $itemHG) {
            if($itemHG['type'] === 'file' && isset($element[$itemHG['id']]) && $element[$itemHG['id']]) {
                $file = $arrFile = \CFile::GetFileArray($element[$itemHG['id']]);
                $arCONTENT_TYPE = explode('/',$file['CONTENT_TYPE']);
                $arCONTENT_TYPE = $arCONTENT_TYPE[0];
                if($arCONTENT_TYPE == 'image') {
                    $colums[$itemHG['id']] = '<a href="'.$file['SRC'].'" download><img src="'.$file['SRC'].'" width="100"></a>';
                } else {
                    $colums[$itemHG['id']] = '<a href="'.$file['SRC'].'" download>'.$file['ORIGINAL_NAME'].'</a>';
                }
            }
        }
        
    //endregion
    //region Действия над строками
    $actionsRows = array(
        /*array(
            'TITLE' => Loc::getMessage('CRMSTORES_ACTION_VIEW_TITLE'),
            'TEXT' => Loc::getMessage('CRMSTORES_ACTION_VIEW_TEXT'),
            'ONCLICK' => 'BX.Crm.Page.open(' . Json::encode($viewUrl) . ')',
            'DEFAULT' => true
        ),
        array(
            'TITLE' => Loc::getMessage('CRMSTORES_ACTION_EDIT_TITLE'),
            'TEXT' => Loc::getMessage('CRMSTORES_ACTION_EDIT_TEXT'),
            'ONCLICK' => 'BX.Crm.Page.open(' . Json::encode($editUrl) . ')',
        ),
        array(
            'TITLE' => Loc::getMessage('CRMSTORES_ACTION_DELETE_TITLE'),
            'TEXT' => Loc::getMessage('CRMSTORES_ACTION_DELETE_TEXT'),
            'ONCLICK' => 'BX.CrmUIGridExtension.processMenuCommand(' . Json::encode($gridManagerId) . ', BX.CrmUIGridMenuCommand.remove, { pathToRemove: ' . Json::encode($deleteUrl) . ' })',
        ),
        array(
            'TITLE' => GetMessage('DropDownElement'),
            'TEXT' => GetMessage('DropDownElement'),
            'MENU' => array(
                array('TITLE' => GetMessage('Point1'), 'TEXT' => GetMessage('Point1'), 'ONCLICK' => 'alert(1);'),
                array('TITLE' => GetMessage('Point2'), 'TEXT' => GetMessage('Point2'), 'ONCLICK' => 'alert(2);'),
                array('TITLE' => GetMessage('Point3'), 'TEXT' => GetMessage('Point3'), 'ONCLICK' => 'alert(3);'),
            )
        )*/
    );
    if( in_array('edit',$ActionsEntity) ) {
        $actionsRows[] = array(
            'TITLE' => Loc::getMessage('CRMSTORES_ACTION_EDIT_TITLE'),
            'TEXT' => Loc::getMessage('CRMSTORES_ACTION_EDIT_TEXT'),
            //'ONCLICK' => 'BX.Crm.Page.open(' . Json::encode($editUrlParams) . ')',
            'ONCLICK' => 'goToPage(' . Json::encode($editUrlParams) . ')',

            //'ONCLICK' => 'BX.CrmUIGridExtension.processMenuCommand(' . Json::encode($gridManagerId) . ', BX.CrmUIGridMenuCommand.remove, { pathToRemove: ' . Json::encode($editUrlParams) . ' })',
        );
    }
    if( in_array('delete',$ActionsEntity) ) {
        $actionsRows[] = array(
            'TITLE' => Loc::getMessage('CRMSTORES_ACTION_DELETE_TITLE'),
            'TEXT' => Loc::getMessage('CRMSTORES_ACTION_DELETE_TEXT'),
            'ONCLICK' => 'BX.CrmUIGridExtension.processMenuCommand(' . Json::encode($gridManagerId) . ', BX.CrmUIGridMenuCommand.remove, { pathToRemove: ' . Json::encode($deleteUrl) . ' })',
        );
    }
    /*echo '<b>$actionsRows</b>';
    echo '<pre>';
    print_r($actionsRows);
    echo '</pre>';
    echo '<hr>';*/
    //endregion

    $rows[] = array(
        //region Идентификатор строки
        'id' => $element['ID'],
        //endregion

        'actions' => $actionsRows,
        //region Чистые данные из БД
        'data' => $element,
        //endregion
        //region Особые столбцы
        'columns' => $colums
        //endregion
    );
}
//endregion

//region Меню переключения между сущностями
$NAVIGATION_BAR = array();
if(count($arResult['ListEntity']) > 1) {
    foreach ($arResult['ListEntity'] as $entityList) {
        $NAVIGATION_BAR[] = array(
            'id' => $entityList::getUfId(),
            'elementId' => $entityList::getUfId(),
            'name' => $entityList::getStringName(),
            'active' => ($entityList::getUfId() == strtoupper($arResult['SelectEntityUfId'])) ? true : false,
            'url' => CComponentEngine::makePathFromTemplate($arResult['URL_TEMPLATES']['LIST'], array(
                'ENTITY_UF_ID' => strtolower($entityList::getUfId())
            ))
        );
    }
}
//endregion

//region Кнопки управления под гридом
$snippet = new Snippet();
$prefix = $arResult['GRID_ID'];
$controlPanel = array();
if( in_array('delete',$ActionsEntity) ) {
    $controlPanel[] = $snippet->getRemoveButton(); // Кнопка "Удалить"
}
if( in_array('edit',$ActionsEntity) ) {
    $controlPanel[] = $snippet->getEditButton(); // Кнопка "Редактировать"
}
    //action_assigned_by_search + _control
    //Prefix control will be added by main.ui.grid
    $APPLICATION->IncludeComponent(
        'bitrix:intranet.user.selector.new',
        '',
        array(
            'MULTIPLE' => 'N',
            'NAME' => "{$prefix}_ACTION_ASSIGNED_BY",
            'INPUT_NAME' => 'action_assigned_by_search_control',
            'SHOW_EXTRANET_USERS' => 'NONE',
            'POPUP' => 'Y',
            'SITE_ID' => SITE_ID,
            'NAME_TEMPLATE' => ''
        ),
        null,
        array('HIDE_ICONS' => 'Y')
    );

    //region Выпадашка "Группа дополнительных действий"
        //region Кнопка "Применить"
        $applyButton = $snippet->getApplyButton(array('ONCHANGE' => array( array(
            'ACTION' => Bitrix\Main\Grid\Panel\Actions::CALLBACK,
            'DATA' => array(array('JS' => "BX.CrmUIGridExtension.processApplyButtonClick('{$gridManagerId}')"))
        ))));
        //endregion
        $CustomActions = $arResult['ThisEntity']::addCustomAction($gridManagerId,$applyButton);
        if(!empty($CustomActions)) {
            $actionList = array(array('NAME' => GetMessage('CHOOSE_ACTION'), 'VALUE' => 'none'));
            foreach ($CustomActions as $customAction) { $actionList[] = $customAction; }
            //region Добавление пунктов в группу
            $controlPanel[] = array(
                "TYPE" => \Bitrix\Main\Grid\Panel\Types::DROPDOWN,
                "ID" => "action_button_{$prefix}",
                "NAME" => "action_button_{$prefix}",
                "ITEMS" => $actionList
            );
            //endregion
        }
    //endregion

//$controlPanel[] = $snippet->getForAllCheckbox(); // CheckBox "Для всех"
//endregion

$ListWarning = $arResult['ThisEntity']::ListWarning();
echo '<div class="box_warnings">';
if(!empty($ListWarning)) {
    foreach ($ListWarning as $warning) { ?><div class="warning_massage danger"><?=$warning?></div><?}
    echo '<hr style="    border-color: white;margin-bottom: 15px;margin-top: 15px;">';
}
?>
</div>
<style>
    div.warning_massage.danger {
        color: #721c24;
        background-color: #f8d7da;
        border: 1px solid #f5c6cb;
        position: relative;
        padding: .75rem 1.25rem;
        border-radius: .25rem;
        display: inline-block;
        margin: 15px 0 0 15px;
    }
    div.warning_massage.danger a:hover {
        outline: #1a60a9 1px solid;
    }
</style>
<?
//region Передача данных в грид
$APPLICATION->IncludeComponent(
    'bitrix:crm.interface.grid', 'titleflex', array(
        'GRID_ID' => $arResult['GRID_ID'],
        'HEADERS' => $arResult['HEADERS'],
        'ROWS' => $rows,
        'PAGINATION' => $arResult['PAGINATION'],
        'SORT' => $arResult['SORT'],
        'FILTER' => $arResult['FILTER'],
        'FILTER_PRESETS' => $arResult['FILTER_PRESETS'],
        'IS_EXTERNAL_FILTER' => false,
        'ENABLE_LIVE_SEARCH' => $arResult['ENABLE_LIVE_SEARCH'],
        'DISABLE_SEARCH' => $arResult['DISABLE_SEARCH'],
        'ENABLE_ROW_COUNT_LOADER' => true,
        'NAVIGATION_BAR' => array( 'ITEMS' => $NAVIGATION_BAR ),
        'AJAX_ID' => '',
        'AJAX_OPTION_JUMP' => 'N',
        'AJAX_OPTION_HISTORY' => 'N',
        'AJAX_LOADER' => null,
        'ACTION_PANEL' => array('GROUPS'=>array(array('ITEMS'=>$controlPanel))),
        'EXTENSION' => array(
            'ID' => $gridManagerId,
            'CONFIG' => array(
                'ownerTypeName' => 'STORE',
                'gridId' => $arResult['GRID_ID'],
                'serviceUrl' => $arResult['SERVICE_URL'],
            ),
            'MESSAGES' => array(
                'deletionDialogTitle' => Loc::getMessage('CRMSTORES_DELETE_DIALOG_TITLE'),
                'deletionDialogMessage' => Loc::getMessage('CRMSTORES_DELETE_DIALOG_MESSAGE'),
                'deletionDialogButtonTitle' => Loc::getMessage('CRMSTORES_DELETE_DIALOG_BUTTON'),
            )
        ),
    ), $this->getComponent(), array('HIDE_ICONS' => 'Y')
);
//endregion


/*echo strtolower($arResult['ThisEntity']::getUfId());
echo '<hr>';*/
$mainNameSpace = explode("\\",$arResult['mainNameSpace']);
?>

<script>
    var originalBxOnCustomEvent = BX.onCustomEvent;
    BX.onCustomEvent = function (eventObject, eventName, eventParams, secureParams) {
        var logData = {eventObject:eventObject,eventName:eventName,eventParams:eventParams,secureParams:secureParams};
        if (eventObject !== null && typeof eventObject == 'object' && eventObject.constructor) {
            logData['eventObjectClassName'] = eventObject.constructor.name;
        }
        if(logData['eventName'] == 'Grid::headerUpdated') {
            BX.MyAjax.Get('getWarningEntity',{
                'ENTITY':'<?=json_encode($arrThisEntity);?>',
                'mainNameSpace':'<?=json_encode($mainNameSpace)?>',
                'menuCounter':'<?=json_encode($arResult['menuCounter'])?>',
                'listGroupEntity':'<?=json_encode($arResult['listGroupEntity'])?>'
            },'json',function (data, textStatus, jqXHR) {
                console.log('data');
                console.log(data);
                $('.box_warnings').html(data['HTML']);
                if(!data['HTML']) { $('.crm-view-switcher-list-item-active').css('color','#545c6a'); }
                else { $('.crm-view-switcher-list-item-active').css('color','red'); }

                //if(data['CountError'] > 0) {
                    console.log(data['CountError']);
                    for (var k in data['CountError']){
                        if (data['CountError'][k]) {
                            $('#test_panel_menu_crm_' + k + ' .main-buttons-item-counter').html(data['CountError'][k]);
                        } else {
                            $('#test_panel_menu_crm_' + k + ' .main-buttons-item-counter').css('display','none');
                        }
                    }
                    //$('#test_panel_menu_crm_report .main-buttons-item-counter').html(data['CountError']);
                //} else {
                    //$('#test_panel_menu_crm_report .main-buttons-item-counter').css('display','none');
                //}
            },function (jqXHR, textStatus, error) { /*alert('Ошибка запроса: ' + textStatus);*/ });
        }
        originalBxOnCustomEvent.apply(null, [eventObject, eventName, eventParams, secureParams]);
    };
</script>