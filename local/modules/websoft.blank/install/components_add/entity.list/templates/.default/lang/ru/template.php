<?php
defined('B_PROLOG_INCLUDED') || die;

$MESS['NO_MODULE'] = 'Модуль CRM не установлен.';
$MESS['CRMSTORES_ACTION_VIEW_TITLE'] = 'Просмотреть элемент';
$MESS['CRMSTORES_ACTION_VIEW_TEXT'] = 'Просмотреть';
$MESS['CRMSTORES_ACTION_EDIT_TITLE'] = 'Редактировать элемент';
$MESS['CRMSTORES_ACTION_EDIT_TEXT'] = 'Редактировать';
$MESS['CRMSTORES_ACTION_DELETE_TITLE'] = 'Удалить элемент';
$MESS['CRMSTORES_ACTION_DELETE_TEXT'] = 'Удалить';
$MESS['CRMSTORES_GRID_ACTION_DELETE_TITLE'] = 'Удалить отмеченные элементы';
$MESS['CRMSTORES_GRID_ACTION_DELETE_TEXT'] = 'Удалить';
$MESS['CRMSTORES_DELETE_DIALOG_TITLE'] = 'Удалить элемент';
$MESS['CRMSTORES_DELETE_DIALOG_MESSAGE'] = 'Вы уверены, что хотите удалить выбранный элемент?';
$MESS['CRMSTORES_DELETE_DIALOG_BUTTON'] = 'Удалить';
$MESS['CRM_ALL'] = 'Всего';
$MESS['CRM_SHOW_ROW_COUNT'] = 'Показать количество';


$MESS['DropDownElement'] = 'Выпадающее меню в меню элемента';
$MESS['Point1'] = 'Пунктк 1';
$MESS['Point2'] = 'Пунктк 2';
$MESS['Point3'] = 'Пунктк 3';

//region Выпадашка под гридом
$MESS["CHOOSE_ACTION"] = "Выберите действие";
$MESS["ONE_ACTION"] = "Одно действие";
//endregion