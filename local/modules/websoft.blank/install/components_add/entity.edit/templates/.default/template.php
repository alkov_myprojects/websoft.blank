<?php
defined('B_PROLOG_INCLUDED') || die;

use Bitrix\Main\Config\Option;
use Bitrix\Main\Error;
use Bitrix\Main\ErrorCollection;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

/** @var CBitrixComponentTemplate $this */

/** @var ErrorCollection $errors */
$errors = $arResult['ERRORS'];
foreach ($errors as $error) { /** @var Error $error */ ShowError($error->getMessage()); }

//topblock
ob_start();
?>
    <div class="bx-crm-view-menu" id="toolbar_lead_edit">
        <a class="bx-context-button crm-btn-new" href="<?=$arResult['BACK_TO_LIST'];?>" title="Назад">
            <span class="bx-context-button-icon" style="background-position: center -1235px;"></span><span>Назад</span>
        </a>
    </div>
<?
$APPLICATION->AddViewContent('topblock', ob_get_contents());
ob_end_clean();

//region Пример составления полей
$fields = array(
    array(
        'id' => 'section_store',
        'name' => Loc::getMessage('CRMSTORES_FIELD_SECTION_STORE'),
        'type' => 'section',
        'isTactile' => true,
    ),
    array(
        'id' => 'NAME',
        'name' => Loc::getMessage('CRMSTORES_FIELD_NAME'),
        'type' => 'text',
        'value' => $arResult['STORE']['NAME'],
        'isTactile' => true,
    ),
    array(
        'id' => 'ADDRESS',
        'name' => Loc::getMessage('CRMSTORES_FIELD_ADDRESS'),
        'type' => 'text',
        'value' => $arResult['STORE']['ADDRESS'],
        'isTactile' => true,
    ),
    array(
        'id' => 'ASSIGNED_BY',
        'name' => Loc::getMessage('CRMSTORES_FIELD_ASSIGNED_BY'),
        'type' => 'intranet_user_search',
        'value' => $arResult['STORE']['ASSIGNED_BY_ID'],
        'componentParams' => array(
            'NAME' => 'crmstores_edit_responsible',
            'INPUT_NAME' => 'ASSIGNED_BY_ID',
            'SEARCH_INPUT_NAME' => 'ASSIGNED_BY_NAME',
            'NAME_TEMPLATE' => CSite::GetNameFormat()
        ),
        'isTactile' => true,
    ),
    array(
        'id' => 'CHECKBOX',
        'name' => 'checkbox',
        'type' => 'checkbox',
        'value' => 'Y',
        'isTactile' => true,
    ),
    array(
        'id' => 'TEXTAREA',
        'name' => 'textarea',
        'type' => 'textarea',
        'value' => 'Hello worl =)',
        'isTactile' => true,
    ),
    array(
        'id' => 'LIST',
        'name' => 'list',
        'type' => 'list',
        'value' => 'KEY_2',
        'items' => array(
            '' => 'Нет значения',
            'KEY_1' => 'Значение 1',
            'KEY_2' => 'Значение 2',
            'KEY_3' => 'Значение 3',
        ),
        'isTactile' => true,
    ),
    array(
        'id' => 'DATE',
        'name' => 'date',
        'type' => 'date',
        'value' => '21.02.2016 18:00:59',
        'isTactile' => true,
    )
);
//endregion
$APPLICATION->IncludeComponent(
    'bitrix:crm.interface.form', 'edit', array(
        'GRID_ID' => $arResult['GRID_ID'],
        'FORM_ID' => $arResult['FORM_ID'],
        'ENABLE_TACTILE_INTERFACE' => 'Y',
        'SHOW_SETTINGS' => 'Y',
        'TITLE' => $arResult['TITLE'],
        'IS_NEW' => $arResult['IS_NEW'],
        'DATA' => $arResult['DATA'],
        'TABS' => array(
            array(
                'id' => 'tab_1',
                'name' => Loc::getMessage('CRMSTORES_TAB_STORE_NAME'),
                'title' => Loc::getMessage('CRMSTORES_TAB_STORE_TITLE'),
                'display' => false,
                'fields' => $arResult['FIELDS']
            ),
        ),
        'BUTTONS' => array(
            'back_url' => $arResult['BACK_URL'],
            'standard_buttons' => true
        ),
    ),
    $this->getComponent(),
    array('HIDE_ICONS' => 'Y')
);
?>
<style>
    /*input[name='saveAndAdd'] { display: none !important; }*/
</style>
