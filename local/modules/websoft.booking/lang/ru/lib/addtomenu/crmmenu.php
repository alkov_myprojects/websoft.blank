<?php
defined('B_PROLOG_INCLUDED') || die;
$MODULE_ID = "";
$dir = dirname(__FILE__);
for ($i = 1; $i < ($level = 4); $i++) { $dir = dirname($dir); }
include $dir.'/module_id.php';
$defOpt = \Bitrix\Main\Config\Option::getDefaults($MODULE_ID);
$class_up = $defOpt['class_up'].'_';

/** ********************************************* Названия меню ***************************************************** */
$MESS[$class_up.'_MENU_ITEM_ENTITY'] = 'Болванка сущности';