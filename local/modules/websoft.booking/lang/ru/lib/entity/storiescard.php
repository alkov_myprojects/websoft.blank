<?php
defined('B_PROLOG_INCLUDED') || die;
$MESS['NegotiatedTable'] = 'История изменений карточки бронирования';
$MESS['ID'] = 'ID';
$MESS['DATE_TIME'] = ''; // Дата изменения
$MESS['USER_ID'] = 'Кто изменил';
$MESS['DESCRIPTION'] = 'Описание изменения';
$MESS['BEFORE'] = 'Значение до';
$MESS['AFTER'] = 'Значение после';

$MESS['Activate'] = 'Активировать';
$MESS['DeActivate'] = 'Деактивировать';

//region Добавление элемента
$MESS['NameOneElementToAdd'] = 'Истории изменения'; // Добавление кого?
//endregion