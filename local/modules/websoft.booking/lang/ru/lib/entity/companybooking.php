<?php
defined('B_PROLOG_INCLUDED') || die;
$MESS['СompanyTable'] = 'Компании';
$MESS['ID'] = 'ID';
$MESS['NAME'] = 'Название';
$MESS['ACTIVE'] = 'Активность';
$MESS['ASSIGNED_BY'] = 'Администратор компании';
$MESS['ASSIGNED_BY_ID'] = 'Администратор компании';


$MESS['Activate'] = 'Активировать';
$MESS['DeActivate'] = 'Деактивировать';

//region Добавление элемента
$MESS['NameOneElementToAdd'] = 'Компании'; // Добавление кого?
//endregion

$MESS['UF_LOGO'] = 'Логотип';
$MESS['UF_SNAP_NEGOTIATION'] = 'Привзяка к переговорным';