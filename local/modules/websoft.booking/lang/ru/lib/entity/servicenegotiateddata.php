<?php
defined('B_PROLOG_INCLUDED') || die;
$MESS['NegotiatedTable'] = 'Переговорные';
$MESS['ID'] = 'ID';
$MESS['NAME'] = 'Название';
$MESS['ACTIVE'] = 'Активность';


$MESS['STATUS_DESCRIPTION'] = 'Общее описание подготовки';
$MESS['RATING_1'] = 'Организация и сопровождение мероприятия';
$MESS['RATING_2'] = 'Подготовка оборудования';
$MESS['RATING_3'] = 'Питаниe';

//region Добавление элемента
$MESS['NameOneElementToAdd'] = 'Переговорной'; // Добавление кого?
//endregion