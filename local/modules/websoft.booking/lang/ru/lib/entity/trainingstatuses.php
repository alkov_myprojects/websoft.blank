<?php
defined('B_PROLOG_INCLUDED') || die;
$MESS['TrainingStatusesTable'] = 'Статусы подготовки';
$MESS['ID'] = 'ID';
$MESS['NAME'] = 'Название';
$MESS['ACTIVE'] = 'Активность';
$MESS['DEFAULT'] = 'По умолчанию';


$MESS['Activate'] = 'Активировать';
$MESS['DeActivate'] = 'Деактивировать';

//region Добавление элемента
$MESS['NameOneElementToAdd'] = 'Статуса подготовки'; // Добавление кого?
//endregion