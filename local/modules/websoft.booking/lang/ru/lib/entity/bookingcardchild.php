<?php
defined('B_PROLOG_INCLUDED') || die;
$MESS['BookingCardChild'] = 'Дочерние карточки бронирования';
$MESS['ID'] = 'ID';
$MESS['NAME'] = 'Наименование мероприятия';
$MESS['DESCRIPTION'] = 'Описание';
$MESS['DATE_TIME_BEGIN'] = 'Дата начала';
$MESS['DATE_TIME_END'] = 'Дата окончания';
$MESS['BOOKING_TYPE_ID'] = 'Вид бронирования';
$MESS['COMMENT_TO_EVENT'] = 'Комментраий к мероприятию';
$MESS['NOTIFY'] = 'Уведомлять участников о начале мероприятия';
$MESS['NOTIFY_TIME'] = 'Время за которое отправлять уведомления о начале';
$MESS['EXECUTOR_ID'] = 'Организатор';
$MESS['EXECUTOR_BY_ID'] = 'Организатор';
$MESS['UF_COMPANIES_ID'] = 'Компании которые участвуют в мероприятии';
$MESS['DATE_START'] = 'Дата начала';
$MESS['DATE_END'] = 'Дата окончания';
$MESS['DATE_CREATE'] = 'Дата создания мероприятия';
$MESS['EXECUTOR_COMPANY_ID'] = 'Компания Организатора';
$MESS['EXECUTOR_BY_ID'] = 'Организатор';
$MESS['EXECUTOR_BY'] = 'Организатор';
$MESS['COMMENT_EVENT'] = 'Комментраий к мероприятию';
$MESS['COMMENT_SERVICE_TYPES'] = 'Комментраий к сопровождению мероприятия';
$MESS['PRIVATE_EVENT'] = 'Частное событие';
$MESS['UF_SERVICES_DATA_ID'] = 'Частное событие';


$MESS['PARENT_ID'] = 'Родительская карточка бронирования';
$MESS['NEGOTIATED_ID'] = 'ID переговорной';
$MESS['RESPONSIBLE_ID'] = 'ID Ответсвенного за контроль качества';
$MESS['TRAINING_STATUS_ID'] = 'Статус подготовки';
$MESS['STATUS_DESCRIPTION'] = 'Общее описание подготовки';
$MESS['SUM_RATING'] = 'Общая оценка';
$MESS['PARENT_EVENT_ID'] = 'ID родительского события в календаре';