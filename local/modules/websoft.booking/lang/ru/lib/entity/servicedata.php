<?php
defined('B_PROLOG_INCLUDED') || die;
$MESS['NegotiatedTable'] = 'Переговорные';
$MESS['ID'] = 'ID';
$MESS['NAME'] = 'Название';
$MESS['ACTIVE'] = 'Активность';


$MESS['Activate'] = 'Активировать';
$MESS['DeActivate'] = 'Деактивировать';

//region Добавление элемента
$MESS['NameOneElementToAdd'] = 'Переговорной'; // Добавление кого?
//endregion