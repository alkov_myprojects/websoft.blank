<?php
defined('B_PROLOG_INCLUDED') || die;
$MESS['ServiceTypesTable'] = 'Сервисы сопровождения';
$MESS['ID'] = 'ID';
$MESS['NAME'] = 'Название';
$MESS['ACTIVE'] = 'Активность';
$MESS['UF_ICON'] = 'Иконка';
$MESS['UF_ICON_ACTIVE'] = 'Иконка(Активная)';
$MESS['UF_SORT'] = 'Сортировка';
$MESS['UF_STATUS_ID'] = 'Статус готовности';


$MESS['Activate'] = 'Активировать';
$MESS['DeActivate'] = 'Деактивировать';

//region Добавление элемента
$MESS['NameOneElementToAdd'] = 'Типа сервиса сопровождения'; // Добавление кого?
//endregion