<?php
defined('B_PROLOG_INCLUDED') || die;
$MESS['NegotiatedTable'] = 'Переговорные';
$MESS['ID'] = 'ID';
$MESS['NAME'] = 'Название';
$MESS['ACTIVE'] = 'Активность';
$MESS['COMPANY_ID'] = 'Компания';
$MESS['CITY_ID'] = 'Город';
$MESS['ADDRESS'] = 'Адрес';
$MESS['COUNT_PLACE'] = 'Кол-во мест';
$MESS['FLOOR'] = 'Этаж';
$MESS['PHONE'] = 'Телефон';
$MESS['UF_SERVICE_TYPES'] = 'Сервисы сопровождения';
$MESS['ASSIGNED_BY_ID'] = 'Администратор переговорной';
$MESS['ASSIGNED_BY'] = 'Администратор переговорной';


$MESS['Activate'] = 'Активировать';
$MESS['DeActivate'] = 'Деактивировать';

//region Добавление элемента
$MESS['NameOneElementToAdd'] = 'Переговорной'; // Добавление кого?
//endregion

$MESS['UF_PREVIEW_IMAGE_NAME'] = 'Превью изображение переговорной';
$MESS['UF_PREVIEW_IMAGE'] = 'Превью переговорной';