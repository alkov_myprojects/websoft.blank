<?php
defined('B_PROLOG_INCLUDED') || die;
$MESS['TestEntity1Table'] = 'Диспетчер писем';
$MESS['ID'] = 'ID';
$MESS['NAME'] = 'Название';
$MESS['TEST_STRING'] = 'Тестовая строка';


$MESS['CONTROL_SEND_ASSIGNED_BY_ID'] = 'Инициатор отправки';
$MESS['CONTROL_SEND_ASSIGNED_BY'] = 'Инициатор отправки';

$MESS['CONTROL_SEND_ASSIGNED_BY_2_ID'] = 'Получатель';
$MESS['CONTROL_SEND_ASSIGNED_BY_2'] = 'Получатель';

$MESS['DATE_CREATE'] = 'Дата создания';
$MESS['DATE_SEND'] = 'Дата отправки';
$MESS['SEND'] = 'Отправлено?';
$MESS['TAGS'] = 'Теги письма';
$MESS['BOOKING_ID'] = 'ID бронирования';
$MESS['CODE_MAIL_TEMPLATE'] = 'Код почтового шаблона';
$MESS['ACTION_MARK'] = 'Метка действия';
$MESS['LOG'] = 'Лог';




$MESS['TestEntity1Table_CustomAction1'] = 'Одношаговое Кастомное действие над выбранными элементами';
$MESS['TestEntity1Table_CustomAction2'] = 'Назначить ответсвенного';
$MESS['TestEntity1Table_CustomAction3'] = 'Двух шаговое Кастомное действие над выбранными элементами';

//region Добавление элемента
$MESS['NameOneElementToAdd'] = 'Элемента тестовой сущности 1'; // Добавление кого?
//endregion

//region UF
$MESS['UF_TestString'] = 'Тестовая строка';
//endregion