<?php
defined('B_PROLOG_INCLUDED') || die;
$MESS['BreakTable'] = 'Перерывы';
$MESS['ID'] = 'ID';
$MESS['NAME'] = 'Название';
$MESS['ACTIVE'] = 'Активность';
$MESS['START'] = 'Время начала';
$MESS['END'] = 'Время окнчания';
$MESS['BOOKING_CARD_ID'] = 'ID Карточки бронирования';

$MESS['Activate'] = 'Активировать';
$MESS['DeActivate'] = 'Деактивировать';

//region Добавление элемента
$MESS['NameOneElementToAdd'] = 'Перерыв'; // Добавление кого?
//endregion