<?php
defined('B_PROLOG_INCLUDED') || die;
$MESS['BookingTypeTable'] = 'Виды бронирования';
$MESS['ID'] = 'ID';
$MESS['NAME'] = 'Название';
$MESS['ACTIVE'] = 'Активность';
$MESS['DESCRIPTION'] = 'Описание';
$MESS['UF_SERVICE_TYPES'] = 'Связанные сервисы сопровождения';
$MESS['UF_ICON'] = 'Иконка';


$MESS['Activate'] = 'Активировать';
$MESS['DeActivate'] = 'Деактивировать';

//region Добавление элемента
$MESS['NameOneElementToAdd'] = 'Вида бронирования'; // Добавление кого?
//endregion