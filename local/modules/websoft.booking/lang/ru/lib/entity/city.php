<?php
defined('B_PROLOG_INCLUDED') || die;
$MESS['CityTable'] = 'Города';
$MESS['ID'] = 'ID';
$MESS['NAME'] = 'Название';
$MESS['ACTIVE'] = 'Активность';
$MESS['TEST_STRING'] = 'Тестовая строка';
$MESS['ASSIGNED_BY'] = 'Ответственный';
$MESS['DATE_TIME'] = 'Дата и время';
$MESS['TEST_FIELD'] = 'Тестовое поле типа строка';
$MESS['BOOL'] = 'Булеан';
$MESS['FLOAT'] = 'Число с плавающей точкой';
$MESS['ENUM'] = 'Список';
$MESS['TEXT'] = 'Текст';
$MESS['STRING'] = 'Строка';


$MESS['TestEntity1Table_CustomAction1'] = 'Одношаговое Кастомное действие над выбранными элементами';
$MESS['TestEntity1Table_CustomAction2'] = 'Назначить ответсвенного';
$MESS['TestEntity1Table_CustomAction3'] = 'Двух шаговое Кастомное действие над выбранными элементами';

//region Добавление элемента
$MESS['NameOneElementToAdd'] = 'Города'; // Добавление кого?
//endregion

//region UF
$MESS['UF_TestString'] = 'Тестовая строка';
//endregion