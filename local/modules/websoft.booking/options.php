<?
require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
//$MODULE_ID = basename(dirname(__FILE__,1));
$dir = dirname(__FILE__);
for ($i = 1; $i < ($level = 1); $i++) { $dir = dirname($dir); }
$MODULE_ID = basename($dir);
if (CModule::IncludeModule($MODULE_ID)) {
    $dir = COption::GetOptionString($MODULE_ID, "dir");
    require_once($dir."/admin/settings.php");
}
?>