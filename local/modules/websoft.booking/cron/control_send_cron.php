<?
$dir = dirname(__FILE__);
for ($i = 1; $i < ($level = 3); $i++) { $dir = dirname($dir); }
$_SERVER["DOCUMENT_ROOT"] = realpath($dir."/../..");
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

@set_time_limit(0);
@ignore_user_abort(true);

CAgent::CheckAgents();
define("BX_CRONTAB_SUPPORT", true);
define("BX_CRONTAB", true);
CEvent::CheckEvents();

if (CModule::IncludeModule("websoft.booking"))
{
    //region cron запускается каждую минуту, за минуту можно отослать не более N писем, которым приша пора отправится
    $limit = 2;
    $sends = \Websoft\Booking\Entity\ControlSendTable::getList(array(
        'select' => array( 'ID', 'CODE_MAIL_TEMPLATE', 'TAGS' ),
        'filter' => array( '<=DATE_SEND' => new \Bitrix\Main\Type\DateTime(), 'SEND' => false ),
        'limit' => $limit
    ))->fetchAll();

    foreach ($sends as $send) {

        //region Отправка
        $send['TAGS'] = json_decode($send['TAGS'],true);
        CEvent::Send($send['CODE_MAIL_TEMPLATE'], SITE_ID, $send['TAGS']);
        //endregion

        //region Пометка о том что отправка осуществелна
        \Websoft\Booking\Entity\ControlSendTable::update($send['ID'],array('SEND'=>true));
        //endregion

    }
    //endregion

    //region Пример из документации https://dev.1c-bitrix.ru/learning/course/index.php?COURSE_ID=43&LESSON_ID=2943&LESSON_PATH=3913.4776.4620.4978.2943
    //$cPosting = new CPosting;
    //$cPosting->AutoSend();
    //endregion
}
?>