<?
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_before.php');
if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_after.php');
}

use \Bitrix\Main\Config\Option;
use Bitrix\Main\Localization\Loc;

use Websoft\Booking\Admin\CustomAdminTags; /** TODO:: Обязательно поменять namespace, если не соответсвует */

Loc::loadMessages(__FILE__);

$MODULE_ID = "";
$dir = dirname(__FILE__);
for ($i = 1; $i < ($level = 2); $i++) { $dir = dirname($dir); }
include $dir.'/module_id.php';
$module_id = $MODULE_ID;
if ($module_id && CModule::IncludeModule($module_id)) {
    if ($_SERVER['REQUEST_METHOD']==='GET'){$GLOBALS['APPLICATION']->SetTitle(GetMessage($class_up.'SETTINGS_TITLE'));}

    /** подключим все необходимые файлы и константы: */
    require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
    $defOpt = Option::getDefaults($module_id);
    $class_up = $defOpt['class_up'].'_';

    /** права доступа текущего пользователя на модуль */
    $POST_RIGHT = $APPLICATION->GetGroupRight($module_id);
    if ($POST_RIGHT == "D") { $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED")); }

    /** сформируем список закладок */
    $aTabs = array(
        array("CODE"=>"TAB_0","DIV"=>"edit0","TAB"=>GetMessage($class_up.'TAB_0'),"ICON"=>"main_user_edit","TITLE"=>''),
        array("CODE"=>"TAB_1","DIV"=>"edit1","TAB"=>GetMessage($class_up.'TAB_1'),"ICON"=>"main_user_edit","TITLE"=>''),
        array("CODE"=>"TAB_2","DIV"=>"edit2","TAB"=>GetMessage($class_up.'TAB_2'),"ICON"=>"main_user_edit","TITLE"=>''),
    );
    $tabControl = new CAdminTabControl("tabControl", $aTabs);

    /**____________________________ОБРАБОТКА ФОРМЫ______________________________*/
    $CustomAdminTags = new CustomAdminTags(
        GetMessage($class_up.'SETTINGS_TITLE'),$defOpt['class_up'].'_FORM_SETTINGS',$module_id,
        'POST',check_bitrix_sessid(),$POST_RIGHT,$_SERVER['REQUEST_METHOD'],$_REQUEST['save'],$_REQUEST['apply']
    );

    // ---- TAB_0
    $CustomAdminTags->SetCheckBox(
            'TAB_0',GetMessage($class_up.'Btn1'),'SynchronizeFilesModule',
            true,false,false,15,85
    );
    $CustomAdminTags->SetCheckBox(
            'TAB_0',GetMessage($class_up.'Btn2'),'SynchronizeEntity',
            true,false,false,15,85
    );
    //$CustomAdminTags->SetCheckBox('TAB_0',GetMessage($class_up.'Btn2'),'SynchronizeEntity',true);

    // ---- TAB_1
    $CustomAdminTags->SetCalendarDate('TAB_1',GetMessage($class_up.'CalendarDate'),'CalendarDate');
    $CustomAdminTags->SetCheckBox('TAB_1',GetMessage($class_up.'CheckBox'),'CheckBox');
    $CustomAdminTags->SetTextArea('TAB_1',GetMessage($class_up.'TextArea'),'TextArea');
    $CustomAdminTags->SetInput('TAB_1',GetMessage($class_up.'Input'),'Inputtest');

    // ---- TAB_2
    $CustomAdminTags->SetRadioButtom('TAB_2',GetMessage($class_up.'RadioButtom'),'RadioButtom');
    $CustomAdminTags->SetSelect('TAB_2',GetMessage($class_up.'Select'),'Select',array(
        ''=>'нет значения','KEY_1'=>'VAL_1','KEY_2'=>'VAL_2','KEY_3'=>'VAL_3'
    ));
    $CustomAdminTags->SetSelectMultiple(
        'TAB_2',GetMessage($class_up.'SelectMultiple'),'SelectMultiple',array(
        ''=>'нет значения','KEY_1'=>'VAL_1','KEY_2'=>'VAL_2','KEY_3'=>'VAL_3'
    ));
    $CustomAdminTags->SetCheckBoxMultiple(
    'TAB_2',GetMessage($class_up.'CheckBoxMultiple'),'CheckBoxMultiple',array(
        'KEY_1'=>'VAL_1','KEY_2'=>'VAL_2','KEY_3'=>'VAL_3'
    ));

    /**_____________________ПРОВЕРКА ОШИБОК ВХОДНЫХ ДАННЫХ______________________________*/
    $message = null; // сообщение об ошибке
    if ($CustomAdminTags->GetCange()) {
        $nameError = ''; // Название ошибки
        $strError = ''; // Описание ошибки
        $ListValOption = $CustomAdminTags->GetListValOption(); // Значения после обновления
        if($strError === '') { $_REQUEST['mess'] = 'ok'; } // Проверка наличия текста ошибки
        else {
            $aMsg = array();
            $arrErr = explode("<br>",$strError);
            reset($arrErr);
            while (list(,$err)=each($arrErr)) $aMsg[]['text']=$err;
            $e = new CAdminException($aMsg);
            $GLOBALS["APPLICATION"]->ThrowException($e);
            $message = new CAdminMessage($nameError, $e);
        }
    }

    /**______________________________ВЫВОД ФОРМЫ______________________________*/
    $CustomAdminTags->SetTitle();
    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

    /** конфигурация административного меню */
    /*$aMenu = array(
        array(
            "TEXT" => GetMessage("rub_list"),
            "TITLE" => GetMessage("rub_list_title"),
            "LINK" => "rubric_admin.php?lang=" . LANG,
            "ICON" => "btn_list",
        )
    );

    if ($ID > 0) {
        $aMenu[] = array("SEPARATOR" => "Y");
        $aMenu[] = array(
            "TEXT" => GetMessage("rub_add"),
            "TITLE" => GetMessage("rubric_mnu_add"),
            "LINK" => "rubric_edit.php?lang=" . LANG,
            "ICON" => "btn_new",
        );
        $aMenu[] = array(
            "TEXT" => GetMessage("rub_delete"),
            "TITLE" => GetMessage("rubric_mnu_del"),
            "LINK" => "javascript:if(confirm('" . GetMessage("rubric_mnu_del_conf") . "'))window.location='rubric_admin.php?ID=" . $ID . "&action=delete&lang=" . LANG . "&" . bitrix_sessid_get() . "';",
            "ICON" => "btn_delete",
        );
        $aMenu[] = array("SEPARATOR" => "Y");
        $aMenu[] = array(
            "TEXT" => GetMessage("rub_check"),
            "TITLE" => GetMessage("rubric_mnu_check"),
            "LINK" => "template_test.php?lang=" . LANG . "&ID=" . $ID
        );
    }

    // создание экземпляра класса административного меню
    $context = new CAdminContextMenu($aMenu);

    // вывод административного меню
    $context->Show();*/
    ?>

    <? if(isset($_REQUEST['mid'])): ?>
        <?php
        $arModules = array(
            "main"=>array(
                "PAGE"=>$_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/options.php",
                "NAME"=> 'Главный модуль',
                "SORT"=>-1,
            )
        );
        $adminPage->Init();
        foreach($adminPage->aModules as $module) {
            if($APPLICATION->GetGroupRight($module) < "R")
                continue;
            if($module == "main")
                continue;
            $ifile = getLocalPath("modules/".$module."/install/index.php");
            $ofile = getLocalPath("modules/".$module."/options.php");
            if($ifile !== false && $ofile !== false) {
                $info = CModule::CreateModuleObject($module);
                $arModules[$module]["PAGE"] = $_SERVER["DOCUMENT_ROOT"].$ofile;
                $arModules[$module]["NAME"] = $info->MODULE_NAME;
                $arModules[$module]["SORT"] = $info->MODULE_SORT;
            }
        }
        uasort($arModules, create_function('$a, $b', 'if($a["SORT"] == $b["SORT"]) return strcasecmp($a["NAME"], $b["NAME"]); return ($a["SORT"] < $b["SORT"])? -1 : 1;'));
        $mid = $_REQUEST["mid"];
        if($mid == "" || !isset($arModules[$mid]) || !file_exists($arModules[$mid]["PAGE"]))
            $mid = "main";
        ?>
        <form action="">
            <select name="mid" onchange="window.location='settings.php?lang=<?=LANGUAGE_ID.($_REQUEST["mid_menu"]<>""? "&amp;mid_menu=1":"")?>&amp;mid='+this[this.selectedIndex].value;">
                <?foreach($arModules as $k=>$m):?>
                    <option value="<?echo htmlspecialcharsbx($k)?>"<?if($mid == $k) echo " selected"?>><?echo htmlspecialcharsbx($m["NAME"])?></option>
                <?endforeach;?>
            </select>
        </form>
        <br />
    <? endif; ?>

    <?$CustomAdminTags->Messages($message); // если есть сообщения об ошибках или об успешном сохранении - выведем их.?>
    <? $getParams = ''; if(isset($_REQUEST['mid'])) { $getParams = '?lang=ru&mid=websoft.blank'; } ?>
    <form method="POST" Action="<?=$APPLICATION->GetCurPage().$getParams;?>" ENCTYPE="multipart/form-data" name="<?=$CustomAdminTags->NameForm();?>">
        <?
        echo bitrix_sessid_post(); // проверка идентификатора сессии
        $tabControl->Begin(); // отобразим заголовки закладок
        $CustomAdminTags->printOptions($tabControl); // Печать опций модуля
        $tabControl->Buttons( array( // завершение формы - вывод кнопок сохранения изменений
            "disabled" => ($POST_RIGHT < "W"),     // Есть у пользователя не достаточно прав на изменение, то блокирует кнопки
            //"back_url" => $_REQUEST["back_url"], // При клике на кнопку отмены осуществит редирект на эту ссылку
            //"btnApply" => false,                 // не показывать кнопку применить
            "btnSave" => false,                    // не показывать кнопку сохранить
        ));
        ?>
    <? $tabControl->End(); // завершаем интерфейс закладок ?>
    <?
    if($CustomAdminTags->issetМandatoryOption()) {
        $CustomAdminTags->Note(GetMessage("REQUIRED_FIELDS"),true); // Предупреждение
    }
    ?>
    <? //<style>.adm-title { display: none !important; }</style> ?>
    <? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php"); // завершение страницы
}
if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/epilog_admin.php');
}
?>