/** ---------------------------------- Конструктор ----------------------------------------- */
var MyPopupBX = BX.namespace('MyPopupBX');
/**
 * Класс позволяет манипулировать несколькими типами модальных окон:
 *   1) "Сообщение" - несёт сугубо информативных характер
 *   2) "Вопрос" - необходимо получить ответ на вопрос или какие либо данные от пользователя
 * @constructor
 */
BX.MyPopupBX = function () {
    this.LastPopup = '';
    this.Buttons = [];
};

/** ------------------------------------ Методы --------------------------------------------- */

/**
 * Модальное окно типа "Сообщение" несёт только информативный характер
 * @param id - Идентификтор модального окна, по нему можно отлавливать событие BX
 * @param title - Заголовок
 * @param text - Текст контента, может быть html
 * @param btnClose - Показать кнопку закрытия окна?
 */
BX.MyPopupBX.prototype.Message = function (id,title,text,btnClose) {
    if(this.LastPopup) {
        this.LastPopup.close();
        this.LastPopup.destroy();
    }

    // Сообщение об обработке файла, просьба дождаться результата
    content = this.generateContentHTML(title,text);
    buttons = [];

    //
    if(this.Buttons.length > 0) {
        for (i=0;i<this.Buttons.length;i++) {
            buttons.push(this.Buttons[i]);
        }
    }

    if(btnClose === true) {
        buttons.push(new BX.PopupWindowButton({ text:BX.message('MyPopUpBX_CLOCSE'),
            events:{click:function(){this.popupWindow.close();this.popupWindow.destroy();}}
        }));
    }
    var Popup = new BX.PopupWindow( id, null, {
        closeByEsc:false,closeIcon:false,overlay:{opacity:38,backgroundColor:'#000'},content:content,buttons:buttons
    });
    Popup.show();
    this.LastPopup = Popup;
};
BX.MyPopupBX.prototype.AddBtn = function (text,funcClick,accecClass) {
    if(accecClass) {
        button = new BX.PopupWindowButton({text:text,className: 'popup-window-button-accept',events:{click:funcClick}});
    } else { button = new BX.PopupWindowButton({ text:text, events:{click:funcClick}}); }
    this.Buttons.push(button);
};

BX.MyPopupBX.prototype.CloseLastPopup = function () { this.LastPopup.close(); this.LastPopup.destroy(); };

BX.MyPopupBX.prototype.SetContent = function (content) {
    // Сообщение об обработке файла, просьба дождаться результата
    contentRes = this.generateContentHTML('',content);
    this.LastPopup.setContent(contentRes);
};

BX.MyPopupBX.prototype.generateContentHTML = function (title,text) {
    content =
        '<div class="bx-viewer-confirm" style="display: block; width: auto;">' +
        '<div class="bx-viewer-confirm-title" style="padding: 1px 0 0px;">' +
        title +
        '</div>' +
        '<div class="bx-viewer-confirm-text-wrap" >' +
        '<span class="bx-viewer-confirm-text-alignment"></span>' +
        '<span class="bx-viewer-confirm-text" style="width: auto !important;">' + text +  '</span>' +
        '</div>' +
        '</div>';
    return content;
};