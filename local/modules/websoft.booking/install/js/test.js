/** ---------------------------------- Конструктор ----------------------------------------- */
BX.TestJS = function (name,soname) {
    this.name = name;
    this.soname = soname;
};

/** ------------------------------------ Методы --------------------------------------------- */
BX.TestJS.prototype.init = function () {
    console.log(BX.TestJS);
    this.sayHi();
};
BX.TestJS.prototype.sayHi = function () {
    console.log('Hi =) Я ' + this.name + ' ' + this.soname + ' =)');
};

/** -------------- Вызовы методов класса при определённых действиях пользователя  ------------------- */