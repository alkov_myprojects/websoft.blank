<?php
require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
$dir = dirname(__FILE__);
for ($i = 1; $i < ($level = 1); $i++) { $dir = dirname($dir); }
$MODULE_ID = basename($dir);
if(CModule::IncludeModule($MODULE_ID)) {
    function tmlFile($dir,$listFiles = array()){
        $ok = scandir($dir, 0);
        foreach($ok as $k=>$v){
            if($v != '.' && $v != '..'){
                if(is_dir($dir.$v) == true){ tmlFile($dir.$v.'/',$listFiles); }
                elseif(is_file($dir.$v) == true){ $listFiles[] = $dir.$v; }
            }
        }
        return $listFiles;
    }
    $directiories_files = COption::GetOptionString($MODULE_ID, "directiories_files");
    $directiories_files = json_decode($directiories_files,true);
    $ListUnequalFiles = array();
    foreach ($directiories_files as $pathDirModuleAbs => $pathiDirPulic) {
        //echo '$pathDirModuleAbs: <b>'.$pathDirModuleAbs.'</b><br><br>';
        $listFiles = tmlFile($pathDirModuleAbs);
        foreach ($listFiles as $pathTofile) {
            $HashMD5_module = hash_file('md5', $pathTofile);
            $fileName = str_replace($pathDirModuleAbs,'',$pathTofile);
            $pathTofilePublic = $_SERVER['DOCUMENT_ROOT'].$pathiDirPulic.$fileName;
            $HashMD5_public = hash_file('md5', $pathTofilePublic);
            if($HashMD5_module !== $HashMD5_public) {
                $ListUnequalFiles[] = array(
                    'NameFile' => $fileName,
                    'PathToModule' => $pathTofile,
                    'PathToPublic' => $pathTofilePublic,
                );
            }
        }
    }
    echo json_encode($ListUnequalFiles);
}
?>