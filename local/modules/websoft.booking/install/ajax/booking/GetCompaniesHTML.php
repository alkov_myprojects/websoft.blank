<?php
require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
$dir = dirname(__FILE__);
for ($i = 1; $i < ($level = 2); $i++) { $dir = dirname($dir); }
$MODULE_ID = basename($dir);

use \Websoft\Booking\Entity\NegotiatedTable as Negotiated;
use \Websoft\Booking\Entity\CompanyBookingTable as CompanyBooking;

if(CModule::IncludeModule($MODULE_ID)) {
    //... Получить все корректные переговорные
    $listCorrectNeg = Negotiated::GetCorrectElements();
    //...... Ивзлечь из них компании
    $idsCompany = array();
    foreach ($listCorrectNeg as $neg) { $idsCompany[] = $neg['COMPANY_ID']; }
    $listCompany = CompanyBooking::getListPure(true,array('ID'=>$idsCompany),array('ID','NAME','UF_LOGO'));
    echo '<form>';
    foreach ($listCompany as $company): ?>
        <input type="radio" id="UF_COMPANIES_ID_AJAX_<?=$company['ID'];?>" name="UF_COMPANIES_ID_AJAX" value="<?=$company['ID'];?>" />
        <label for="UF_COMPANIES_ID_AJAX_<?=$company['ID'];?>"><?=$company['NAME'];?></label>
    <? endforeach;
    echo '</form>';
}