<?
require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
$result = array('success' => true, 'data' => array());
$element = \Websoft\Booking\Entity\BookingCardTable::getListPure(false,array('ID'=>$_REQUEST['ID']));
$result_db = \Websoft\Booking\Entity\BookingCardTable::update($_REQUEST['ID'],array('ACTIVE_BOOKING'=>'N'));

//region Получить дочерние карточки
$childs = \Websoft\Booking\Entity\BookingCardChildTable::getListPure(
    true,array('PARENT_ID'=>$element['ID']),array('PARENT_EVENT_ID'));
foreach ($childs as $child) {
    // Удаление бронирования переговорной в сервисах (стандартный модуль Bitrix)

    \Websoft\Booking\Handler::removeEvent($child['PARENT_EVENT_ID']); // Удаление события в календаре

}
//endregion

if (!$result_db->isSuccess(true)) {
    $result['success'] = false;
    $result['data'] = $result->getErrors();
}

use \Websoft\Booking\Entity\BookingCardTable as BookingCard;
use \Websoft\Booking\Entity\BookingCardChildTable as BookingCardChild;
use \Websoft\Booking\Entity\NegotiatedTable as Negotiated;
use \Websoft\Booking\Entity\CompanyBookingTable as CompanyBooking;

if($result['success']) {
    echo 'Текущая бронь, успешно аннулирована.';

    $BookingCard_ID = $_REQUEST['ID'];

    $BookingCard = BookingCard::getListPure(
        false,array('ID'=>$BookingCard_ID),
        array('ID','EXECUTOR_BY_ID','UF_RESPONSIBLE_EVENT','UF_COMPANIES_ID','NAME')
    );
    $BookingCardsChild = BookingCardChild::getListPure(
        true,array('PARENT_ID' => $BookingCard['ID']), array('ID','NEGOTIATED_ID','UF_PARTICIPANTS')
    );

    $users = array();

    //region 1. организатор
        $users[] = $BookingCard['EXECUTOR_BY_ID'];
    //endregion

    //region 2. ответственный за мероприятие
        if($BookingCard['UF_RESPONSIBLE_EVENT']) { $users[] = $BookingCard['UF_RESPONSIBLE_EVENT']; }
    //endregion

    //region 3. администраторы компаний
        if(empty($BookingCard['UF_COMPANIES_ID'])) {
            foreach ($BookingCardsChild as $child_card) {
                $neg = Negotiated::getListPure(false,array('ID'=>$child_card['NEGOTIATED_ID']),array('COMPANY_ID'));
                $company = CompanyBooking::getListPure(false,array('ID'=>$neg['COMPANY_ID']),array('ASSIGNED_BY_ID'));
                $users[] = $company['ASSIGNED_BY_ID'];
            }
        } else {
            foreach ($BookingCard['UF_COMPANIES_ID'] as $company_id) {
                $company = CompanyBooking::getListPure(false,array('ID'=>$company_id),array('ASSIGNED_BY_ID'));
                $users[] = $company['ASSIGNED_BY_ID'];
            }
        }
    //endregion

    //region 4. ответсвенные за переговорные
        foreach ($BookingCardsChild as $child_card) {
            $neg = Negotiated::getListPure(false,array('ID'=>$child_card['NEGOTIATED_ID']),array('ASSIGNED_BY_ID'));
            $users[] = $neg['ASSIGNED_BY_ID'];
        }
    //endregion

    //region 5. участники
        foreach ($BookingCardsChild as $bookingCardChild) {
            foreach ($bookingCardChild['UF_PARTICIPANTS'] as $part_user_id) { $users[] = $part_user_id; }
        }
    //endregion

    //region 6. ответственным за сервисы
        foreach ($BookingCardsChild as $bookingCardChild) {
            $SearchServices = \Websoft\Booking\Entity\ServiceDataTable::getListPure(
                true, array('UF_SNAP_CHILD_CARD' => $bookingCardChild['ID']), array('RESPONSIBLE_ID'));
            foreach ($SearchServices as $searchService) { $users[] = $searchService['RESPONSIBLE_ID']; }
        }
    //endregion

    $users = array_unique($users);

    // ... сбор данных для отправки
    $TAGS = array(
        'MAIL_USER' => 'Email получателя сообщения',
        'BOOKING_ID' => $BookingCard_ID,
        'BOOKING_NAME' => $BookingCard['NAME'],
    );

    global $USER;
    foreach ($users as $user_id) {

        //region $TAGS['MAIL_USER']
        $arUser = \CUser::GetByID($user_id)->Fetch();
        $TAGS['MAIL_USER'] = $arUser['EMAIL'];
        //endregion

        \Websoft\Booking\Entity\ControlSendTable::add(array(
            'BOOKING_ID' => $BookingCard_ID,
            'CONTROL_SEND_ASSIGNED_BY_ID' => $USER->GetID(),
            'CONTROL_SEND_ASSIGNED_BY_2_ID' => $user_id,
            'DATE_CREATE' => new \Bitrix\Main\Type\DateTime(),
            'DATE_SEND' => new \Bitrix\Main\Type\DateTime(),
            'ACTION_MARK' => 'local/modules/websoft.booking/install/ajax/booking/CancelBooking.php => [110]',
            'CODE_MAIL_TEMPLATE' => 'BOOKING_CANCEL',
            'SEND' => false,
            'TAGS' => json_encode($TAGS,true), //$TAGS
        ));
    }
}
else { echo 'Что то пошло не так...';}
?>