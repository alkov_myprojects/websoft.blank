<?php
require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
$dir = dirname(__FILE__);
for ($i = 1; $i < ($level = 2); $i++) { $dir = dirname($dir); }
$MODULE_ID = basename($dir);
if(CModule::IncludeModule($MODULE_ID)) {
    $code = 'CompanyUsers';
    $UF_COMPANIES_ID = $_REQUEST['UF_COMPANIES_ID'];
    $USER_ID = $_REQUEST['USER_ID'];
    $value = \COption::GetOptionString($MODULE_ID,$code);
    $value = json_decode($value,true);
    if(!is_array($value)) { $value = array(); }
    $find = false; foreach ($value as $item) { if($item['USER_ID'] == $USER_ID) { $find = true; break; } }
    if(!$find) { $value[] = array( 'USER_ID' => $USER_ID, 'UF_COMPANIES_ID' => $UF_COMPANIES_ID ); }
    $value = json_encode($value,true);
    \COption::SetOptionString($MODULE_ID, $code,$value);

}