<?php

require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
use Websoft\Booking\Entity\NegotiatedTable as Negotiated;
use Websoft\Booking\Entity\BookingTypeTable as BookingType;
use Websoft\Booking\Entity\BookingCardTable as BookingCard;

$result = array('success' => true, 'data' => array());

/** Добавление карточки бронирования */
$element = array(
    'NAME' => '',                // Наименование
    'DESCRIPTION' => '',         // Описание
    'BOOKING_TYPE_ID' => '',     // Вид бронирования
    'NOTIFY' => '',              // Уведомлять участников о начале мероприятия
    //'UF_NOTIFY_TIME' => array(), // Время отправки уведомлений о начале (Тут массив секунд)
    'PRIVATE_EVENT' => '',       // Частное событие
    'EXECUTOR_COMPANY_ID' => $_REQUEST['EXECUTOR_COMPANY_ID'], // Компания Организатора
    'EXECUTOR_BY_ID' => $_REQUEST['EXECUTOR_BY_ID'],           // Организатор
);


/*echo '<b>$_REQUEST</b>';
echo '<pre>';
print_r($_REQUEST);
echo '</pre>';
echo '<hr>';*/

//region Частное событие
$element['PRIVATE_EVENT'] = isset($_REQUEST['PRIVATE_EVENT']) ? true : false;
//endregion

//region Уведомлять участников о начале мероприятия + Время отправки уведомлений о начале (множественное)
$element['NOTIFY'] = isset($_REQUEST['NOTIFY_TIME']) ? true : false;
//$element['UF_NOTIFY_TIME'] = isset($_REQUEST['NOTIFY_TIME']) ? $_REQUEST['NOTIFY_TIME'] : array();
//endregion

//region Прверка Вида бронирования + установка называния мероприятия
$BOOKING_TYPE_ID = isset($_REQUEST['filter']['BOOKING_TYPE_ID']) ? $_REQUEST['filter']['BOOKING_TYPE_ID'] : '';
if($BOOKING_TYPE_ID) {
    if($BOOKING_TYPE_ID == 'on' && !$_REQUEST['DESCRIPTION']) {
        $error = 'Был выбран вид бронирования "Другой", но не было введено его описание.';
        $result['success'] = false; $result['data']['errors'][] = $error;
    }
    else {
        if($BOOKING_TYPE_ID == 'on') { $BOOKING_TYPE_ID = 0; $element['NAME'] = 'Другое...'; }
        else {
            $bookingType = BookingType::getListPure(false,array('ID'=>$BOOKING_TYPE_ID),array('NAME'));
            $element['NAME'] = $bookingType['NAME'];
        }
        $element['BOOKING_TYPE_ID'] = $BOOKING_TYPE_ID;
        $element['DESCRIPTION'] = $_REQUEST['DESCRIPTION'];
    }
} else { $result['success'] = false; $result['data']['errors'][] = 'Не выбран вид бронирования.'; }
//endregion

//region Компании которые участвуют в мероприятии (из фильтра)
$UF_COMPANIES_ID = isset($_REQUEST['filter']['UF_COMPANIES_ID']) ? $_REQUEST['filter']['UF_COMPANIES_ID'] : array();
//endregion

//region Проверка участников переговорных
$UF_PARTICIPANTS = array(); //isset($_REQUEST['filter']['NEG_IDS']) ? $_REQUEST['filter']['NEG_IDS'] : array();
foreach ($_REQUEST['filter']['NEG_IDS'] as $NEG_ID) {
    $UF_PARTICIPANTS[$NEG_ID] = array();
    if(isset($_REQUEST['NEG_ID_'.$NEG_ID])) {
        foreach ($_REQUEST['NEG_ID_'.$NEG_ID] as $user) {
            if(isset($user['ID'])){ $UF_PARTICIPANTS[$NEG_ID][] = $user['ID']; }
        }
    }
}

if(!empty($UF_PARTICIPANTS)) {
    foreach($UF_PARTICIPANTS as $neg_id => &$users) {
        $negData = Negotiated::getListPure(false,array('ID'=>$neg_id),array('ID','NAME'));
        if(empty($negData)) {
            $result['success'] = false; $result['data']['errors'][] = 'Переговорная с ID = '.$neg_id.' не найдена.';
            break;
        } else if(empty($users)) {
            $result['success'] = false; $result['data']['errors'][] = 'В переговорной "'.$negData['NAME'].'" нет ни '.
                'одного участника, необходим хотя бы один.';
        } else {
            // Проверка на пустые элементы массива
            $users_new = array();
            foreach ($users as $user_id) { if($user_id) { $users_new[] = $user_id; } }
            $users = $users_new;
            if(count($users) == 0) {
                $result['success'] = false; $result['data']['errors'][] = 'В переговорной '.$negData['NAME'].' нет ни '.
                    'одного участника.';
            }
        }
    }
}
else { $result['success'] = false; $result['data']['errors'][] = 'Нет ни одной переговорной'; }
//endregion

//region Сервисы сопровождения (которые нужны Организатору д/всех переговорных)
$UF_SERVICE_TYPES_ID = isset($_REQUEST['filter']['UF_SERVICE_TYPES_ID']) ? $_REQUEST['filter']['UF_SERVICE_TYPES_ID'] : array();
//endregion

//region Периодичность
$RRule = false;
if(isset($_REQUEST['EVENT_RRULE']) && $_REQUEST['EVENT_RRULE_EX']['FREQ'] != 'NONE') {
    $RRule = array( 'FREQ' => $_REQUEST['EVENT_RRULE_EX']['FREQ'], 'INTERVAL' => $_REQUEST['EVENT_RRULE']['INTERVAL'] );
    if($_REQUEST['rrule_endson'] != 'never') {
        if($_REQUEST['rrule_endson'] == 'count') { $RRule['COUNT'] = $_REQUEST['EVENT_RRULE']['COUNT']; }
        if($_REQUEST['rrule_endson'] == 'until') { $RRule['UNTIL'] = $_REQUEST['EVENT_RRULE']['UNTIL']; }
    }
}
//endregion

//region Уведомлять участников
$element['NOTIFY'] = isset($_REQUEST['NOTIFY']) ? true : false;
$Remind = false;
if($element['NOTIFY'] && isset($_REQUEST['NOTIFY_TIME'])) {
    $Remind = array();
    foreach ($_REQUEST['NOTIFY_TIME'] as $NOTIFY_TIME) { $Remind[] = array('type'=>'min','count'=>$NOTIFY_TIME); }
}
//endregion

//region Дата начала/окончания + дата создания мероприятия
$DATE_BEGIN = $_REQUEST['filter']['DATE_BEGIN'];
$TIME_START = $_REQUEST['filter']['TIME_START'];
$TIME_END = $_REQUEST['filter']['TIME_END'];
$element['DATE_START'] = $DATE_BEGIN ? $DATE_BEGIN.' '.$TIME_START.':00' : '';
$element['DATE_END'] = $DATE_BEGIN ? $DATE_BEGIN.' '.$TIME_END.':00' : '';
 //date('m/d/Y h:i:s', time());
//endregion

/*echo '<b>Before $element</b>';
echo '<pre>';
print_r($element);
echo '</pre>';
echo '<hr>';*/

/*\Bitrix\Main\Diag\Debug::writeToFile(array(
    '$element' => $element,
    '$UF_PARTICIPANTS' => $UF_PARTICIPANTS,
    '$UF_COMPANIES_ID' => $UF_COMPANIES_ID,
    '$UF_SERVICE_TYPES_ID' => $UF_SERVICE_TYPES_ID,
    '$RRule' => $RRule,
    '$Remind' => $Remind
), "", "log123.txt");*/

// Создание основной карточки бронирования
if($result['success']) {
    $CorrectValueFieldsBeforeInputToDB = BookingCard::CheckingCorrectValueFieldsBeforeInputToDB($element);
    foreach ($CorrectValueFieldsBeforeInputToDB as $key => $value) {
        if(isset($element[$key])) { $element[$key] = $value; }
    }

    //region Кастыль (DON`T TOUCH ME, BITCH=)!!!)
    $value = date('m/d/Y h:i:s', time());
    $date = strtotime($value);
    $time_string = ConvertTimeStamp($date, "FULL");
    $new_value = new \Bitrix\Main\Type\DateTime($time_string);
    $element['DATE_CREATE'] = $new_value;
    //endregion

    $resAddBookingCard = BookingCard::addCard(
        $element,$UF_PARTICIPANTS,$UF_COMPANIES_ID,$UF_SERVICE_TYPES_ID,$RRule,$Remind
    );
    if(!$resAddBookingCard['success']) { $result['success'] = false; }
    $result['data'] = $resAddBookingCard['data'];
}

$result['data']['$_REQUEST'] = $_REQUEST;
$result['data']['$element'] = $element;
$result['data']['$RRule'] = $RRule;
$result['data']['$Remind'] = $Remind;

/*echo '<b>$result</b>';
echo '<pre>';
print_r($result);
echo '</pre>';
echo '<hr>';*/

echo json_encode($result,true);

// Создание дочерних карточке бронирования