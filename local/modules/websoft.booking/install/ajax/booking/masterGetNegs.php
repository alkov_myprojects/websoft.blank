<? require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");

use \Websoft\Booking\Entity\NegotiatedTable as Negotiated;


$COUNT = $_REQUEST['COUNT'] ? $_REQUEST['COUNT'] : 0; // Кол-во участников
$UF_SERVICE_TYPES_ID = $_REQUEST['UF_SERVICE_TYPES_ID'] ? $_REQUEST['UF_SERVICE_TYPES_ID'] : array(); // Сервисы сопровождения
$UF_COMPANIES_ID = $_REQUEST['UF_COMPANIES_ID'] ? $_REQUEST['UF_COMPANIES_ID'] : array(); // Компании

// Генерация фильтра
$arFilter = array();
if(!empty($UF_COMPANIES_ID)) { $arFilter['COMPANY_ID'] = $UF_COMPANIES_ID; }
if(!empty($COUNT)) { $arFilter['>=COUNT_PLACE'] = $COUNT; }
//... Определить переговорные, которые выбрал Организатор
$UF_PARTICIPANTS = $_REQUEST['UF_PARTICIPANTS'] ? $_REQUEST['UF_PARTICIPANTS'] : array();
if(!empty($UF_PARTICIPANTS)) { foreach ($UF_PARTICIPANTS as $idNeg => $users) {
    // ... Удалить из выборки те переговорные, которые он выбрал
    $arFilter['!=ID'][] = $idNeg;
    // ... + Если выбрали переговорную(ые), то другие переговорные из этой(этих) компании не нужны (логика клиента)
    $neg_one = Negotiated::getListPure(false, array('ID'=>$idNeg), array('COMPANY_ID'));
    if($neg_one) {if(($key = array_search($neg_one['COMPANY_ID'],$arFilter['COMPANY_ID'])) !== false){
        unset($arFilter['COMPANY_ID'][$key]);
    }}
    if($neg_one && !isset($arFilter['COMPANY_ID'])) { $arFilter['!=COMPANY_ID'][] = $neg_one['COMPANY_ID']; }
}}

echo '<b>$arFilter</b>';
echo '<pre>';
print_r($arFilter);
echo '</pre>';
echo '<hr>';

//... Определить минимальные сервисы, которыми должна обладать переговорная
$UF_SERVICE_TYPES = $_REQUEST['UF_SERVICE_TYPES_ID'] ? $_REQUEST['UF_SERVICE_TYPES_ID'] : array();
if(!empty($UF_SERVICE_TYPES)) {
    $arFilter['LOGIC'] = 'AND';
    foreach ($UF_SERVICE_TYPES as $SERVICE_TYPE) { $arFilter[] = array( '=UF_SERVICE_TYPES' => $SERVICE_TYPE ); }
}

// Поиск подходящей переговорной
$negs = Negotiated::getListPure(true,$arFilter,array('*','UF_*'));

echo '<pre>';
print_r($_REQUEST);
echo '</pre>';

echo '<hr>';

$DATE_BEGIN = $_REQUEST['DATE_BEGIN'].$_REQUEST['TIME_START'].':00';
echo '$DATE_BEGIN: '.$DATE_BEGIN.'<br>';

$DATE_END = $_REQUEST['DATE_BEGIN'].$_REQUEST['TIME_END'].':00';
echo '$DATE_END: '.$DATE_END.'<br>';

echo '<hr>';

// Учёт расписания (Занятые переговорные на это время)
$negsCategories = array('FREE' => array(), 'BUSY' => array());
foreach ($negs as $neg) {

    // Тут будует логика распределения переговорных (Свободные/Занятые)
    $negsCategories['FREE'][] = $neg;
}

// Печать (Тестовая, тут можно забрать массив или распечатать HTML)
foreach ($negsCategories['FREE'] as $neg) { // Свободные переговорные

    echo '<b>Свободные</b> - $negsCategories[\'FREE\']';


    echo '<div class="test_block_neg_'.$neg['ID'].'">';
    $img = $neg['UF_PREVIEW_IMAGE'];$img = CFile::GetFileArray($img);$img = $img['SRC'];
    
    echo '<pre>';
    print_r($neg);
    echo '</pre>';
    
    ?>
    <button onclick="select_neg('<?=$neg['ID']?>','<?=$img?>','<?=$neg['NAME']?>');">Выбрать переговорную "<?=$neg['NAME']?>"</button>
    <?
    echo '</div>';
}
if(!empty($negsCategories['BUSY'])) { // Занятые переговорные переговорные
    echo '<hr>';

    echo '<b>Занятые</b> - $negsCategories[\'BUSY\']';

    echo '<pre>';
    print_r($neg);
    echo '</pre>';
    echo '<hr>';
}
