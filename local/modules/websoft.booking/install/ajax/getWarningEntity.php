<? require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
$entity = "";
$arrNameSpace = json_decode($_REQUEST['ENTITY']);
foreach ($arrNameSpace as $nameSpace) { if($nameSpace) { $entity .= "\\".$nameSpace; } }
$ListWarning = $entity::ListWarning();
$HTML = "";
if(!empty($ListWarning)) {
    ob_start();
    foreach ($ListWarning as $warning) { ?><div class="warning_massage danger"><?=$warning?></div><? }
    $HTML = ob_get_contents();
    ob_end_clean();
}

$menuCounter = json_decode($_REQUEST['menuCounter'],true);

foreach ($menuCounter as $key => &$value) { $value = 0; }

$mainNameSpace = "";
$mainNameSpace__ARR = json_decode($_REQUEST['mainNameSpace']);
foreach ($mainNameSpace__ARR as $nameSpace) { if($nameSpace) { $mainNameSpace .= "\\".$nameSpace; } }

$listGroupEntity = json_decode($_REQUEST['listGroupEntity'],true);
foreach ($listGroupEntity as $group => $entities) {
    foreach ($entities as $entity_name) {
        $entityLocal = $mainNameSpace."\\".$entity_name.'Table';
        $ListWarning = $entityLocal::ListWarning();
        if(!empty($ListWarning)) { $menuCounter[$group] += count($ListWarning); }
    }
}

$newmenuCounter = array();
foreach ($menuCounter as $keyCounter => $valueCounter) { $newmenuCounter[strtolower($keyCounter)] = $valueCounter; }

$result = array( 'CountError' => $newmenuCounter, 'HTML' => $HTML );
echo json_encode($result);