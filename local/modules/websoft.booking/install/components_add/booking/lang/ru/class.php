<?php
defined('B_PROLOG_INCLUDED') || die;

$MESS['SEF_NOT_ENABLED'] = 'Режим ЧПУ должен быть включен.';
$MESS['SEF_BASE_EMPTY'] = 'Не указан каталог ЧПУ.';
$MESS['SEF_BASE_EMPTY'] = 'Не указан каталог ЧПУ.';

$MESS['EntityNotExists'] = 'Класса "CLASS" не существует!';
$MESS['NotAccess'] = 'Вы не имеете права доступа к текущему разделу, обратитесь к администратору сайта.';
$MESS['NotCorrectIDCard'] = 'Не корректный ID карточки бронирования';
$MESS['NotFoundCard'] = "Карточка бронирования с ID = VAR_ID, не найдена";

$MESS['SETTING'] = 'Системные настройки';