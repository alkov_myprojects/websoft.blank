<?php
defined('B_PROLOG_INCLUDED') || die;

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Grid\Panel\Snippet;
use Bitrix\Main\Loader;
use Bitrix\Main\Page\Asset;
use Bitrix\Main\Web\Json;

/** @var CBitrixComponentTemplate $this */
$APPLICATION->SetTitle(Loc::getMessage('TITLE_BOOKING'));
$APPLICATION->IncludeComponent(
    'websoft.booking:booking.main', '', array(
    'MODE' => 'Y',
    'FOLDER' => '/booking/',
    'URL_TEMPLATES' => array(
        'main' => '',

        'handbook' => 'handbook/',
        'handbook_list_entity' => 'handbook/#ENTITY_UF_ID#/',
        'handbook_edit_entity' => 'handbook/#ENTITY_UF_ID#/#ID#/edit/',

        'setting' => 'setting/',
        'setting_list_entity' => 'setting/#ENTITY_UF_ID#/',
        'setting_edit_entity' => 'setting/#ENTITY_UF_ID#/#ID#/edit/',

        'report' => 'report/',

        'master' => 'master/',

        //'list' => 'list/#ENTITY_UF_ID#/',
        //'details' => '#ID#/',
        //'edit' => '#ENTITY_UF_ID#/#ID#/edit/',
    ),
    /*'ListEntity' => array(
        '\Websoft\Blank\Entity\TestEntity1Table',
        '\Websoft\Blank\Entity\TestEntity2Table',
    )*/
), false
);
?>