<?php
defined('B_PROLOG_INCLUDED') || die;

use Bitrix\Main\Localization\Loc;

/** @var CBitrixComponentTemplate $this */
global $APPLICATION; /*$APPLICATION->ShowCSS();*/
$APPLICATION->IncludeComponent(
    'websoft.booking:booking.master', '', array(
        'PATH_EDIT'=>$arParams['URL_TEMPLATES']['card_edit'],
        'PATH_CALENDAR'=>$arParams['FOLDER'],
        'Card' => $arResult['Card'],
        'COMPANY_ID_THIS_USER' => $arResult['COMPANY_ID_THIS_USER']
    ), false
);
