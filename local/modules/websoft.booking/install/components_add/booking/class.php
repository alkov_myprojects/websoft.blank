<?php
defined('B_PROLOG_INCLUDED') || die;

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Page\Asset;
use \Bitrix\Main\Data\Cache;

use Websoft\Booking\Entity\BookingCardTable as BookingCard;
use Websoft\Booking\Entity\BookingCardChildTable as BookingCardChild;

class CBookingComponent extends CBitrixComponent {

    public function __construct(CBitrixComponent $component = null) {
        parent::__construct($component);
        //$name = $this->GetTemplateName();
        //echo '$name: '.$name.'<hr>';
    }

    public function executeComponent() {

        /** Локальные переменные: */
        $FOLDER = $this->arParams['FOLDER'];
        $URL_TEMP = is_array($this->arParams['URL_TEMPLATES']) ? $this->arParams['URL_TEMPLATES'] : array();
        $getMes = 'getMessage';
        $MODE = $this->arParams['MODE'];
        $nameSpace = explode(':',$this->getName());
        $nameSpace = $nameSpace[0];

        /** Проерки: */
        if (!Loader::includeModule($nameSpace)) { $this->error('NO_MODULE',array('MID'=>$nameSpace)); return; }
        if (empty($MODE) || $MODE != 'Y') { ShowError(Loc::{$getMes}('NOT_ENABLED')); return; }
        if (empty($FOLDER)) { ShowError(Loc::{$getMes}('BASE_EMPTY')); return; }
        foreach ($this->arParams['ListEntity'] as $className) {
            if (!class_exists($className)) {
                ShowError(Loc::{$getMes}('EntityNotExists'), array('CLASS'=>$className)); return;
            }
        }

        /** Кеш */
        //$cache = Cache::createInstance(); // получаем экземпляр класса
        $cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
        $cacheTtl = 7200;

        /** ... */
        global $arVariables;
        $page = CComponentEngine::parseComponentPath( $FOLDER, $URL_TEMP, $arVariables );
        if (empty($page)) { $page = 'index'; } // Если нет страницы, то по умолчанию редирект на главную.

        if($page == 'handbook_list_entity' || $page == 'handbook_edit_entity') { $page = 'handbook'; }
        if($page == 'setting_list_entity' || $page == 'setting_edit_entity') { $page = 'setting'; }
        if($page == 'report_list_entity' || $page == 'report_edit_entity') { $page = 'report'; }
        if($page != 'index' && $page != 'master' && $page != 'card' && $page != 'card_edit') {
            global $USER;
            if(!$USER->IsAdmin()) { // Текущий пользователь не администратор
                //region Проверить на администратора модуля
                $isBookingAdmin = \Websoft\Booking\Entity\UsersTable::getListPure(false,array(
                    'ASSIGNED_BY_ID'=>$USER->GetID(), 'GROUP' => 'BOOKING_ADMIN' ));
                if(!$isBookingAdmin) { ShowError(Loc::{$getMes}('NotAccess')); return; }
                //endregion
            }
        }


        /** Хак, только для этого проекта, возможно и для других подойдёт... но нужно его оптимизировать */
        $menuCounter = array( 'MAIN' => 0, 'SETTING' => 0, 'HANDBOOK' => 0, 'REPORT' => 0 );
        // Проверить ошибки в сущностях
        $mainNameSpace = "\Websoft\Booking\Entity";
        $listGroupEntity = array(
            'HANDBOOK' => array('Negotiated','CompanyBooking','ResponsibleSupportServices'),
            'SETTING' => array(),
        );

        global $APPLICATION;
        $listEntity = array();
        $dir = COption::GetOptionString($nameSpace, "dir").'/lib/entity/';
        if(is_dir($dir)) {
            $TreeFiles = self::TreeDirAndFiles($dir);
            foreach ($TreeFiles as $file) { $listEntity[] = self::get_namespace($file); }
        }

        ob_start();?><style><? foreach ($listGroupEntity as $group => $entities) { foreach ($entities as $entity_name) {
            $entityLocal = $mainNameSpace."\\".$entity_name.'Table';

            $cacheId = $entityLocal;
            if ($cache->read($cacheTtl, $cacheId)) { $ListWarning = $cache->get($cacheId); }
            else {
                $ListWarning = $entityLocal::ListWarning();
                $cache->set($cacheId, $ListWarning); // записываем в кеш
            }

            $UfId = $entityLocal::getUfId();
            $UfId = strtolower($UfId);
            if(!empty($ListWarning)) {
                $menuCounter[$group] += count($ListWarning);
                foreach ($listEntity as $gEntity) {
                    $UfId_2 = strtolower($gEntity::getUfId());
                    echo '.crm-view-switcher-list-item#entity_list_'.$UfId_2.'table_'.$UfId.' { color: red; } ';
                }
            }
        }}?></style><? $CSS = ob_get_clean(); $APPLICATION->AddHeadString($CSS);

        /** Карта компонентов: */
        $urlTemplates = array(
            'MAIN' => $FOLDER.$URL_TEMP['main'],
            'SETTING' => $FOLDER.$URL_TEMP['setting'],
            'MASTER' => $FOLDER.$URL_TEMP['master'],
            'HANDBOOK' => $FOLDER.$URL_TEMP['handbook'],
            'REPORT' => $FOLDER.$URL_TEMP['report'],
            //'VIEW' => $FOLDER.$URL_TEMP['view'],
            //'EDIT' => $FOLDER.$URL_TEMP['edit']
        );

        /** Определение пользователя (из какой он компании) */
        $CompanyUsers = COption::GetOptionString($nameSpace, "CompanyUsers");
        $CompanyUsers = json_decode($CompanyUsers,true);
        global $USER;
        $COMPANY_ID_THIS_USER = '';
        if(is_array($CompanyUsers) && count($CompanyUsers) > 0) { foreach ($CompanyUsers as $item) {
            if($USER->GetID() == $item['USER_ID']) { $COMPANY_ID_THIS_USER = $item['UF_COMPANIES_ID']; }
        }};

        $Card = array();
        if($page=='card' || $page == 'card_edit') {
            $ID = $arVariables['ID'];
            if(!$ID) { ShowError(Loc::{$getMes}('NotCorrectIDCard')); return; }
            else {
                // Поиск карточки бронирования
                $BookingCard = BookingCard::getListPure(false,array('ID'=>$ID),array('*','UF_*'));
                if($BookingCard) {
                    // Получить дочерние карточки
                    $BookingCardChild = BookingCardChild::getListPure(
                        true,array('PARENT_ID' => $BookingCard['ID']),array('*','UF_*')
                    );
                    $BookingCard['ITMES'] = $BookingCardChild;

                    // Передать шаблону
                    $Card = $BookingCard;
                } else { ShowError(Loc::{$getMes}('NotFoundCard', array('VAR_ID'=>$ID))); return; }
            }
        }

        //... Получить сервисы сопровождения, связанные с кокретной датой
        if(isset($_REQUEST['DATE'])) {
            $ITMES_New = array();
            foreach ($Card['ITMES'] as $item) {

                $UF_SERVICES_DATA_ID_NEW = array();
                $item_L = $item;

                //region Получаем сервисы сервисы сопровождения по дате в $_REQUEST
                $SearchServices = \Websoft\Booking\Entity\ServiceDataTable::getListPure(
                    true,array(
                        'UF_SNAP_CHILD_CARD' => $item_L['ID'], 'UF_CODE_DATE_BEGIN' => $_REQUEST['DATE']
                    ), array('ID') );
                if(!empty($SearchServices)) {
                    foreach ($SearchServices as $searchService){ $UF_SERVICES_DATA_ID_NEW[] = $searchService['ID']; }
                } else {
                    $UF_SERVICES_DATA_ID_NEW = \Websoft\Booking\Classes\RuleServiceData::generateServiceDateByDay(
                        $item_L['ID'],$_REQUEST['DATE']
                    );
                }
                //endregion

                $item_L['UF_SERVICES_DATA_ID'] = $UF_SERVICES_DATA_ID_NEW;
                $ITMES_New[] = $item_L;

            }
            $Card['ITMES'] = $ITMES_New;
        }

        /** Подключение и передача данных компоненту: */

        $this->arResult = array(

            'COMPANY_ID_THIS_USER' => $COMPANY_ID_THIS_USER,

            'URL_TEMPLATES' => $urlTemplates,
            'arVariables' => $arVariables,
            'nameSpace' => $nameSpace,

            'menuCounter' => $menuCounter,
            'mainNameSpace' => $mainNameSpace,
            'listGroupEntity' => $listGroupEntity,

            'Card' => $Card

            //'ListEntity' => $this->arParams['ListEntity'],
        );

        $nameTempalte = $this->GetTemplateName();
        $path = $this->__path.'/templates/'.$nameTempalte;
        //\Bitrix\Main\Diag\Debug::dump($path . "/js/slick.js",'$path');
        Asset::getInstance()->addJs($path . "/js/jquery-3.3.1.min.js");
        Asset::getInstance()->addJs($path . "/js/jquery-ui.js");
        Asset::getInstance()->addJs($path . "/js/jquery.datetimepicker.full.min.js");
        Asset::getInstance()->addJs($path . "/js/slick.js");

        Asset::getInstance()->addCss($path . "/css/fonts.css");
        Asset::getInstance()->addCss($path . "/css/jquery-ui.css");
        Asset::getInstance()->addCss($path . "/css/slick-theme.css");
        Asset::getInstance()->addCss($path . "/css/jquery.datetimepicker.min.css");
        Asset::getInstance()->addCss($path . "/css/slick.css");
        $this->includeComponentTemplate($page);
    }

    private static function TreeDirAndFiles($directory) {
        if (! is_dir($directory)) { return false; }
        $files = array();
        $dir = dir($directory);
        while (false !== ($file = $dir->read())) {
            if ($file != '.' and $file != '..') {
                $checkDir = $directory . $file;
                if (is_dir($checkDir)) { $files = array_merge($files,self::TreeDirAndFiles($checkDir . '/')); }
                else { $files[] = $directory . $file; }
            }
        }
        $dir->close();
        return $files;
    }
    private static function get_namespace($path) {
        $content = file_get_contents($path);
        $tokens = token_get_all($content);
        $namespace = '';
        for ($index = 0; isset($tokens[$index]); $index++) {
            if (!isset($tokens[$index][0])) {
                continue;
            }
            if (T_NAMESPACE === $tokens[$index][0]) {
                $index += 2; // Skip namespace keyword and whitespace
                while (isset($tokens[$index]) && is_array($tokens[$index])) {
                    $namespace .= $tokens[$index++][1];
                }
            }
            if (T_CLASS === $tokens[$index][0]) {
                $index += 2; // Skip class keyword and whitespace
                $fqcns[] = $namespace.'\\'.$tokens[$index][1];
                $namespace .= '\\'.$tokens[$index++][1];
            }
        }
        return '\\'.$namespace;
    }
}