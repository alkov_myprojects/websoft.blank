<?php
defined('B_PROLOG_INCLUDED') || die;

use Bitrix\Main\Localization\Loc;

$arComponentParameters = array(
    'PARAMETERS' => array(
        'SEF_MODE' => array(
            'details' => array(
                'NAME' => Loc::getMessage('DETAILS_URL_TEMPLATE'),
                'DEFAULT' => '#ID#/',
                'VARIABLES' => array('ID')
            ),
            'edit' => array(
                'NAME' => Loc::getMessage('EDIT_URL_TEMPLATE'),
                'DEFAULT' => '#ID#/edit/',
                'VARIABLES' => array('ID')
            )
        )
    )
);
