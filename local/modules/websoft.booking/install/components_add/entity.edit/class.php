<?php
defined('B_PROLOG_INCLUDED') || die;

use Academy\CrmStores\Entity\StoreTable;
use Bitrix\Main\Context;
use Bitrix\Main\Error;
use Bitrix\Main\ErrorCollection;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\UserTable;

class EntityEditComponent extends CBitrixComponent {
    const FORM_ID = 'ENTITY_EDIT';
    private $errors;
    private $ThisEntity;
    public function __construct(CBitrixComponent $component = null) {
        parent::__construct($component);
        $this->errors = new ErrorCollection();
        $ENTITY_UF_ID = $this->__parent->arResult['arVariables']['ENTITY_UF_ID'];
        $ListEntity = $this->__parent->arResult['ListEntity'];
        foreach ($ListEntity as $entity){
            if(strtolower($entity::getUfId()) == $ENTITY_UF_ID){ $this->ThisEntity = $entity; }
        }
        //CBitrixComponent::includeComponentClass('academy.crmstores:stores.list');
    }

    public function executeComponent() {
        global $APPLICATION;
        $entity = $this->ThisEntity;

        // Получить поля сущности
        $ID = '';
        if (isset($this->arParams['arVariables']['ID']) && intval($this->arParams['arVariables']['ID']) > 0) {
            $ID = $this->arParams['arVariables']['ID'];
            $title = Loc::getMessage('SHOW_TITLE_EDIT',array('NAME_ONE_ELEMENT'=>$entity::getNameOneElement()));
            $APPLICATION->SetTitle($title);
        } else {
            $title = Loc::getMessage('SHOW_TITLE_DEFAULT',array('NAME_ONE_ELEMENT'=>$entity::getNameOneElement()));
            $APPLICATION->SetTitle($title);
        }
        $DataToRequest = $this->GetDataToRequest();
        $fields = $entity::getFieldsToEdit($ID,$DataToRequest);
        $AllEnums = $entity::GetAllEnums();
        foreach ($AllEnums as $code => $enum) {
            foreach ($fields as &$arr) { if($arr['id'] == $code) { $arr['items'] = $enum; } }
        }
        
        if (isset($this->arParams['arVariables']['ID']) && intval($this->arParams['arVariables']['ID']) > 0) {
            $element = $entity::getList(array(
                'filter' =>array('ID'=>$ID),'select' => array('*','UF_*'))
            )->fetchAll();
            if(!empty($element)) { $element = $element[0]; }
            if (empty($element)) {
                ShowError(Loc::getMessage('NOT_FOUND'));
                return;
            } else {
                foreach ($fields as &$arr) {
                    if(isset($element[$arr['id']]) && $element[$arr['id']]) { $arr['value'] = $element[$arr['id']]; }
                    elseif(
                        isset($arr['componentParams'])
                        && isset($element[$arr['componentParams']['INPUT_NAME']])
                        && $element[$arr['componentParams']['INPUT_NAME']]
                    ) { $arr['value'] = $element[$arr['componentParams']['INPUT_NAME']]; }
                }
            }
        }
        else {
            $dataToRequest = $this->GetDataToRequest();
            foreach($dataToRequest as $code => $value) {
                if($value) {
                    foreach ($fields as &$field) {
                        if($field['id'] == $code) { $field['value'] = $value; }
                        elseif(isset($field['componentParams']) && $field['componentParams']['INPUT_NAME'] == $code) {
                            $field['value'] = $value;
                        }
                    }
                }
            }
        }
        $AllBooleans = $entity::GetAllBooleans();
        foreach ($AllBooleans as $code) { foreach ($fields as &$arr) {
            if($arr['id'] == $code){ if($arr['value']){ $arr['value'] = 'Y'; } else{ $arr['value'] = 'N'; } }
        }}
        
        if (!empty($element['ID']) && isset($element['NAME'])) {
            $title = Loc::getMessage(
                'CRMSTORES_SHOW_TITLE',
                array(
                    '#ID#' => $element['ID'],
                    '#NAME#' => $element['NAME']
                )
            );
        }

        $URL_TEMPLATES = $this->__parent->arParams['URL_TEMPLATES'];
        $BACK_TO_LIST = $this->__parent->arParams['FOLDER'].CComponentEngine::makePathFromTemplate(
            $URL_TEMPLATES['list'], array('ENTITY_UF_ID' => $this->arParams['arVariables']['ENTITY_UF_ID'])
        );
        $BACK_TO_VIEW = $this->__parent->arParams['FOLDER'].CComponentEngine::makePathFromTemplate(
            $URL_TEMPLATES['edit'], array(
                'ID' => $this->arParams['arVariables']['ID'],
                'ENTITY_UF_ID' => $this->arParams['arVariables']['ENTITY_UF_ID']
            )
        );

        if (self::isFormSubmitted()) {
            $savedStoreId = $this->processSave($element);
            if ($savedStoreId > 0) {
                if(self::isApply()) { LocalRedirect($BACK_TO_VIEW); }
                else {  LocalRedirect($BACK_TO_LIST); }
            }
            $submittedStore = $this->getSubmittedStore();
            $element = array_merge($element, $submittedStore);
        }
        if (isset($this->arParams['arVariables']['ID']) && intval($this->arParams['arVariables']['ID']) > 0) {
            $element['ID'] = $this->arParams['arVariables']['ID'];
        }

        $fieldsCustom = $entity::getFieldsToEdit($ID,$fields);
        foreach ($fieldsCustom as $one_fieldsCustom) {
            if($one_fieldsCustom['type'] == 'custom') {
                foreach ($fields as &$field) {
                    if($field['id'] == $one_fieldsCustom['id']) {
                        $field['value'] = $one_fieldsCustom['value'];
                    }
                }
            }
        }
        
        $this->arResult =array(
            'FORM_ID' => self::FORM_ID,
            //'GRID_ID' => CAcademyCrmStoresStoresListComponent::GRID_ID,
            'IS_NEW' => empty($element['ID']),
            'TITLE' => $title,
            'DATA' => $element,
            'BACK_URL' => $this->getRedirectUrl(),
            'BACK_TO_LIST' => $BACK_TO_LIST,
            'ERRORS' => $this->errors,
            'FIELDS' => $fields
        );

        $this->includeComponentTemplate();
    }

    private function processSave($initial) {
        $entity = $this->ThisEntity;
        $element = $this->getSubmittedStore();
        
        //$element = $this->GetDataToRequest();
        
        $ID = $this->arParams['arVariables']['ID'];
        $element['ID'] = $ID;
        $elementConverted = $entity::CheckingCorrectValueFieldsBeforeInputToDB($element);
        
        foreach ($element as $code => &$value123) {
            if(isset($elementConverted[$code])) { $value123 = $elementConverted[$code]; }
        }
        $this->errors = self::validate($element,$entity);
        if (!$this->errors->isEmpty()) { return false; }
        
        // Удалить ключи, которые не участвуют в форме редактирования.
        $FieldsToEdit = array();
        $FieldsToEditElements = $entity::getFieldsToEdit();
        foreach ($FieldsToEditElements as $fieldToEditEl) { $FieldsToEdit[] = $fieldToEditEl['id']; }
        $destroyKeys = array();
        foreach ($element as $key_unique => $value) { if(!in_array($key_unique,$FieldsToEdit)) {
            if($key_unique != 'ASSIGNED_BY_ID' && $key_unique != 'ADMIN_BY_ID' && $key_unique != 'ADMIN_BY') { $destroyKeys[] = $key_unique; }
        }}

        $element_new = array();
        //foreach ($destroyKeys as $keyDestroyToElement) { unset($element[$keyDestroyToElement]); }
        foreach ($element as $key_destroy => $value_destroy) {
            if(!in_array($key_destroy,$destroyKeys)) { $element_new[$key_destroy] = $value_destroy; }
        }
        $element = $element_new;
        
        
        foreach ($elementConverted as $key_conv => $val_conv)  { $element[$key_conv] = $val_conv; }

        /*echo '$ID: '.$ID.'<br>';
        echo '<b>$element</b>';
        echo '<pre>';
        print_r($element);
        echo '</pre>';
        echo '<hr>';
        die();*/

        if(isset($element['COMPANY_ID'])) {
            $element['COMPANY_ID'] = strval($element['COMPANY_ID']);

            /*echo '<b>$element</b>';
            echo '<pre>';
            print_r($element);
            echo '</pre>';
            echo '<hr>';

            die();*/
        }
        if ($ID) {
            $result = $entity::update($ID, $element);

            if (!$result->isSuccess()) {
                $errors = $result->getErrors();
                echo '<pre>';
                print_r($errors);
                echo '</pre>';
                die();
            }

            $this->clearSession();
        }
        else {
            $result = $entity::add($element);
            $this->clearSession();
        }
        if (!$result->isSuccess()) { $this->errors->add($result->getErrors()); }
        return $result->isSuccess() ? $result->getId() : false;
    }

    private function getSubmittedStore() {

        $context = Context::getCurrent();
        $request = $context->getRequest();
        $entity = $this->ThisEntity;

        //region Поля сущности
        $ColumnNameAndType = $entity::GetColumnNameAndType();
        $submitted = array();
        foreach ($ColumnNameAndType as $code => $field) { $submitted[$code] = $request->get($code); }
        //endregion

        //region Пользовательские поля сущности
        $GetDataToRequest = $this->GetDataToRequest();
        $ColumnNameAndType_UF = $entity::GetColumnNameAndType_UF();
        foreach ($ColumnNameAndType_UF as $fieldUF_Code => $fieldUF_Type) {
            if($fieldUF_Type == 'file') {
                $fid = $GetDataToRequest[$fieldUF_Code];
                if($fid) {
                    $arrFile = CFile::GetFileArray($fid);
                    $makeFileArray = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"].$arrFile['SRC']);
                    $submitted[$fieldUF_Code] = $makeFileArray;
                }
            }
            else { $submitted[$fieldUF_Code] = $request->getPost($fieldUF_Code); }
        }
        //endregion

        /*\Bitrix\Main\Diag\Debug::writeToFile(array(
            '$file' => $file,
            'method' => 'getSubmittedStore',
            '$submitted,' => $submitted,
            '$ColumnNameAndType,' => $ColumnNameAndType,
            '$request' => $request,
        ), "", "log.txt");*/

        return $submitted;
    }
    private function clearSession() {
        $entity = $this->ThisEntity;
        $ColumnNameAndType_UF = $entity::GetColumnNameAndType_UF();
        foreach ($ColumnNameAndType_UF as $fieldUF_Code => $fieldUF_Type) {
            if($fieldUF_Type == 'file') { $_SESSION['old_fid_'.$fieldUF_Code] = ''; }
        }
    }
    private function GetDataToRequest() {
        $context = Context::getCurrent();
        $request = $context->getRequest();
        $files = $request->getFileList();
        $entity = $this->ThisEntity;
        $ColumnNameAndType = $entity::GetColumnNameAndType();

        $submitted = array();
        foreach ($ColumnNameAndType as $code => $field) { if($request->get($code)) { $submitted[$code] = $request->get($code); } }

        //region Пользовательские поля сущности
        $ColumnNameAndType_UF = $entity::GetColumnNameAndType_UF();
        foreach ($ColumnNameAndType_UF as $fieldUF_Code => $fieldUF_Type) {
            if($fieldUF_Type == 'file') {
                $file = $files->offsetGet($fieldUF_Code);
                if($file) {
                    $fid = CFile::SaveFile($file,'');
                    $delete = $request->getPost($fieldUF_Code.'_del');
                    if($delete === 'Y') { $_SESSION['old_fid_'.$fieldUF_Code] = ''; }
                    $old_fid = $_SESSION['old_fid_'.$fieldUF_Code];
                    if($fid) { $_SESSION['old_fid_'.$fieldUF_Code] = $fid; }
                    if(!$fid && $old_fid && $request->get('saveAndView')) { $fid = $old_fid; }
                    if(($request->get('apply') || $request->get('saveAndView')) && $delete !== 'Y' && !$fid) {
                        if(isset($this->arParams['arVariables']['ID']) && intval($this->arParams['arVariables']['ID']) > 0){
                            $ID = $this->arParams['arVariables']['ID'];
                            $element = $entity::getList(array(
                                    'filter' =>array('ID'=>$ID),'select' => array($fieldUF_Code))
                            )->fetchAll();
                            if(!empty($element)) { $element = $element[0]; }
                            $fid = $element[$fieldUF_Code];
                        }
                    }
                    $submitted[$fieldUF_Code] = $fid;
                }
            }
            else { $submitted[$fieldUF_Code] = $request->get($fieldUF_Code); }
        }
        //endregion

        return $submitted;
    }

    private static function validate($element,$entity) {
        $errors = new ErrorCollection();
        return $entity::validateFormEdit($element,$errors);
    }
    private static function isFormSubmitted() {
        $context = Context::getCurrent();
        $request = $context->getRequest();
        $saveAndView = $request->get('saveAndView');
        $saveAndAdd = $request->get('saveAndAdd');
        $apply = $request->get('apply');
        return !empty($saveAndView) || !empty($saveAndAdd) || !empty($apply);
        //return !empty($saveAndView) || !empty($saveAndAdd) ;
    }
    private static function isApply() {
        $context = Context::getCurrent();
        $request = $context->getRequest();
        $apply = $request->get('apply');
        return !empty($apply);
    }
    private function getRedirectUrl($savedStoreId = null) {
        $context = Context::getCurrent();
        $request = $context->getRequest();

        if (!empty($savedStoreId) && $request->offsetExists('apply')) {
            return CComponentEngine::makePathFromTemplate(
                $this->arParams['URL_TEMPLATES']['EDIT'],
                array('STORE_ID' => $savedStoreId)
            );
        } elseif (!empty($savedStoreId) && $request->offsetExists('saveAndAdd')) {
            return CComponentEngine::makePathFromTemplate(
                $this->arParams['URL_TEMPLATES']['EDIT'],
                array('STORE_ID' => 0)
            );
        }

        $backUrl = $request->get('backurl');
        if (!empty($backUrl)) {
            return $backUrl;
        }

        if (!empty($savedStoreId) && $request->offsetExists('saveAndView')) {
            return CComponentEngine::makePathFromTemplate(
                $this->arParams['URL_TEMPLATES']['DETAIL'],
                array('STORE_ID' => $savedStoreId)
            );
        } else {
            return $this->arParams['SEF_FOLDER'];
        }
    }
}