<?php
defined('B_PROLOG_INCLUDED') || die;

/*$MESS['CRMSTORES_NO_MODULE'] = 'Модуль "Торговые точки CRM" не установлен.';*/
$MESS['NOT_FOUND'] = 'Элемент не найден.';
$MESS['SHOW_TITLE_DEFAULT'] = 'Добавление "NAME_ONE_ELEMENT"';
$MESS['SHOW_TITLE_EDIT'] = 'Редактирование "NAME_ONE_ELEMENT"';
$MESS['CRMSTORES_SHOW_TITLE'] = 'ID:#ID# &mdash; #NAME#';

/*$MESS['CRMSTORES_ERROR_EMPTY_NAME'] = 'Название торговой точки не задано.';
$MESS['CRMSTORES_ERROR_EMPTY_ASSIGNED_BY_ID'] = 'Не указан ответственный.';
$MESS['CRMSTORES_ERROR_UNKNOWN_ASSIGNED_BY_ID'] = 'Указанный ответственный сотрудник не существует.';*/