<?php
defined('B_PROLOG_INCLUDED') || die;

$MESS['NO_MODULE'] = 'Модуль "MID" не установлен.';
$MESS['NO_ENTITY'] = 'Не указана сущность';
$MESS['EntityNotExists'] = 'Класса "CLASS" не существует!';
$MESS['CRMSTORES_HEADER_ID'] = 'ID';
$MESS['CRMSTORES_HEADER_NAME'] = 'Название';
$MESS['CRMSTORES_HEADER_ASSIGNED_BY'] = 'Ответственный';
$MESS['CRMSTORES_HEADER_ADDRESS'] = 'Адрес';
$MESS['CRMSTORES_FILTER_FIELD_ID'] = 'ID';
$MESS['CRMSTORES_FILTER_FIELD_NAME'] = 'Название';
$MESS['CRMSTORES_FILTER_FIELD_ASSIGNED_BY'] = 'Ответственный';
$MESS['CRMSTORES_FILTER_FIELD_ADDRESS'] = 'Адрес';
$MESS['CRMSTORES_FILTER_PRESET_MY_STORES'] = 'Мои торговые точки';
$MESS['CRMSTORES_GRID_ROW_COUNT'] = 'Всего: #COUNT#';


$MESS['EXPORT_EXCEL'] = 'Экспорт в Excel';
$MESS['EXPORT_CSV'] = 'Экспорт в CSV';
$MESS['IMPORT'] = 'Импорт';
$MESS['MIGRATION'] = 'Миграция';
$MESS['ADD_ELEMENT'] = 'Добавить';


