// $("#datepicker_from").datepicker({
//     showOn: "button",
//     monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь',
//         'Октябрь', 'Ноябрь', 'Декабрь'
//     ],
//     dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
//     dateFormat: "dd.mm.yy h:i",
//     beforeShow:function(textbox, instance){
//         console.log(this);
//         // $().closest(.)
//         $(this).closest('.content_right').append($('#ui-datepicker-div'));
//     }
// });

function setDateBegin() {
    date = $('#datepicker_date_begin').val();
    app.card.DATE_START = date + ' ' + $('#time_start_begin').val() + ':00';
    app.card.DATE_END = date + ' ' + $('#time_end_begin').val() + ':00';
}


function convertDateToString(date) {
    return getDate(date) + ' ' + getTime(date) + ':' + ('0' + (date.getSeconds())).slice(-2);
}
function convertStingDateToNewDate(str_date) {
    
    console.log('str_date: ' + str_date);
    
    arr_date = str_date.split(' ');
    time = arr_date[1];
    ar_time = time.split(':');
    arr_date = arr_date[0].split('.');

    console.log('arr_date');
    console.log(arr_date);

    date = arr_date[2] + '-' + arr_date[1] + '-' + arr_date[0];    
    return new Date( arr_date[2], arr_date[1] - 1, arr_date[0], parseInt(ar_time[0]), parseInt(ar_time[1]));
    // return new Date( date + ' ' + time);
}
function getDate(date) {
    return ('0' + date.getDate()).slice(-2) + '.' + ('0' + (date.getMonth() + 1)).slice(-2) + '.' + date.getFullYear();
}
function getTime(date) {
    return ('0' + (date.getHours())).slice(-2) + ':' + ('0' + (date.getMinutes())).slice(-2);
}
function initApp(settings) {

    var app = new Vue({
        el: '#app', data: settings, created: function (){},mounted: function () {

            var self = this;

            DATE_START = convertStingDateToNewDate(self.card.DATE_START);
            console.log('DATE_START: ' + DATE_START);
            
            DATE_END = convertStingDateToNewDate(self.card.DATE_END);
            console.log('DATE_END ' + DATE_END);
            // console.log(self.card.DATE_START);
            // console.log(self.card.DATE_END);
            
            //region Установка стартового времени
            $("#datepicker_date_begin").val(getDate(DATE_START));
            $("#time_start_begin").val(getTime(DATE_START));
            $("#time_end_begin").val(getTime(DATE_END));
            //endregion

            //region Обработчики событий по изменению времени начала/окончания
            $("#datepicker_date_begin").datetimepicker({ timepicker:false, format:'d.m.Y' }).on('change', function(){
                setDateBegin();
            });
            $("#time_start_begin").datetimepicker({ datepicker:false, format:'H:i' }).on('change', function(){
                setDateBegin();
            });
           
            $("#time_end_begin").datetimepicker({ datepicker:false, format:'H:i' }).on('change', function(){
                setDateBegin();
            });

            // перерывы
            
            // $("#time_start_begin").on('change', function(){ setDateBegin(); });
            // $("#time_end_begin").on('change', function(){ setDateBegin(); });
            //endregion

            /*$("#datepicker_from").datetimepicker({
                onShow: function (ct) {
                    this.setOptions({ maxDate: jQuery('#datepicker_to').val() ? jQuery('#datepicker_to').val() : false})
                },
                step: 30,
                format: 'd.m.Y H:i',
                closeOnWithoutClick: false,
                lang: 'ru'
            }).on('change', function(e){
                self.card.DATE_START = $('#datepicker_from').val();
            });

            $("#datepicker_to").datetimepicker({
                onShow: function (ct) {
                    this.setOptions({
                        minDate: jQuery('#datepicker_from').val() ? jQuery('#datepicker_from').val() : false
                    })
                },
                step: 30,
                format: 'd.m.Y H:i',
                closeOnWithoutClick: false,
                lang: 'ru'
            }).on('change', function(e){
                self.card.DATE_END = $('#datepicker_to').val();
            });*/

            $('.onlytime').datetimepicker({ datepicker: false,  step: 30,  format: 'H:i' });
            $('.card_slider').slick({
                infinite: false,
                arrows: true,
                slidesToShow: 3,
                slidesToScroll: 1,
                appendArrows: $('.slide_card'),
                responsive: [{
                        breakpoint: 1500,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 1,
                            infinite: true,
                        }
                    },
                    {
                        breakpoint: 1300,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                ]
            });
        },
        updated: function (){
            $("#BREAKS .onlytime").datetimepicker({ datepicker:false, format:'H:i' }).on('change', function(){

                //region Получить индекс перерыва
                parent = $(this).parents().eq(2);
                index = parent.index();
                //endregion

                //region Поределить, какая это граница (левая или правая)
                parent2 = $(this).parents().eq(1);
                leftOrRight = parent2.index();
                //endregion

                //region Получить установленное время
                time = $(this).datetimepicker('getValue');
                if(time) {
                    const newTime = getTime(time);
                    if(leftOrRight === 1) { app.card.BREAKS[index]['START'] = newTime; }
                    else { app.card.BREAKS[index]['END'] = newTime; }
                }
                //endregion

                /*test = $(this).data("DateTimePicker");
                console.log(test);
                console.log('------------');*/

            });

            /*.on('dp.change', function(e){
                console.log(e.date);
                test = $(this).data("DateTimePicker").date();
                console.log(test);
                console.log('------------');
            });*/

            /*.on('change', function(e){
                console.log(e.date);
                console.log(this);
            });*/
        },
        methods: {
            CancelBooking: function () {
                var PopupBX = new BX.MyPopupBX();
                PopupBX.Message('cancel_booking','','Аннулирование бронирования, ожидайте...');
                BX.MyAjax.Get('booking/CancelBooking', {'ID':this.card_Id}, 'html', function (data) {
                    PopupBX.CloseLastPopup();
                    var PopupBX_2 = new BX.MyPopupBX();
                    PopupBX_2.AddBtn('Закрыть',function(){ document.location.href = "/booking/"; /*PopupBX_2.CloseLastPopup();*/ },true);
                    PopupBX_2.Message('result_cancel_booking','Результат аннулирования',data);
                });
            },
            changeRating: function (rating,id_service) {
                var self = this;
                index = this.selectCard;
                for(i=0; i<self.card.ITEMS[index].SERVICES_DATA.length; i++) {
                    if(self.card.ITEMS[index].SERVICES_DATA[i]['ID'] == id_service) {
                        self.card.ITEMS[index].SERVICES_DATA[i]['RATING'] = rating;
                    }
                }
            },
            deletBreak: function (index) {this.card.BREAKS.splice(index, 1);},
            addBreak: function () {
                console.log('addBreak');
                this.card.BREAKS.push({
                    NAME: "",
                    START: "",
                    END: ""
                });
            },
            updateRaiting: function () {
                /*SERVICES_DATA = app.card.ITEMS[app.selectCard].SERVICES_DATA;
                $( '.raty-icons' ).each(function( index, element ) {
                    data_id_service = $(element).data('id-service');
                    jQuery(element).raty({
                        'score': parseInt(SERVICES_DATA[index]['RATING']),
                        'target': '.rates', 'click': function (score, evt) {
                            id_service = $(this).attr( "data-id-service" );
                            app.changeRating(score,id_service);
                        }
                    });
                });
                */

                RATING_1 = app.card.ITEMS[app.selectCard].RATING_1;
                jQuery('#RATING_1').raty({ 'score': RATING_1, 'target': '.rates', 'click': function (score, evt) {
                    app.card.ITEMS[app.selectCard].RATING_1 = score; }});

                RATING_2 = app.card.ITEMS[app.selectCard].RATING_2;
                jQuery('#RATING_2').raty({ 'score': RATING_2, 'target': '.rates', 'click': function (score, evt) {
                    app.card.ITEMS[app.selectCard].RATING_2 = score; }});

                RATING_3 = app.card.ITEMS[app.selectCard].RATING_3;
                jQuery('#RATING_3').raty({ 'score': RATING_3, 'target': '.rates', 'click': function (score, evt) {
                        app.card.ITEMS[app.selectCard].RATING_3 = score; }});

                jQuery('.rates').hide();
            },
            selectSubCard: function (subcard, index,e) {
                $('.card_slider-item_inner').removeClass('active');
                $(e.target).addClass('active');
                this.selectCard = index;
                app.updateRaiting();
            },
            showPhone: function (e) {
                $(e.target).parent().find('.company-details').fadeToggle().toggleClass("active");
            },
            save: function () {
                errors = [];

                //region Наименование переговорной обязательно должно быть заполненным
                if(!this.card.NAME) {
                    errors.push('Поле <b>Наименование мероприятия</b> не доложно быть пустым.');
                }
                //endregion

                //region Анализ перерывоы
                BREAKS = this.card.BREAKS;
                NAME = true; END = true; START = true; // То что нужно проверить в перерывах
                for (var ind in BREAKS) {
                    if(!BREAKS[ind].NAME && NAME) {
                        errors.push('Не для всех перерывов установлено <b>наименование</b>.'); NAME = false;
                    }
                    if(!BREAKS[ind].END && END) {
                        errors.push('Не для всех перерывов установлено <b>время начала</b>.'); END = false;
                    }
                    if(!BREAKS[ind].START && START) {
                        errors.push('Не для всех перерывов установлено <b>время окончания</b>.'); START = false;
                    }
                }
                //endregion

                if(errors.length > 0 ) { printErrors(errors); }
                else {

                    //region Получить ids файлов которые нужно удалить
                    old_files = [];
                    $( "input.old_files" ).each(function( index, element ) {
                        if($(element).prop("checked")) { old_files.push($(element).data('id-file')); }
                    });
                    this.card.OLD_FILES = old_files;
                    //endregion

                    //region Получить ids новых файлов
                    new_files = []; new_files_serialize = $('.boxAddNewFiles').serializeArray();
                    for(var i=0; i < new_files_serialize.length; i++) { new_files.push(new_files_serialize[i]['value']); }
                    this.card.NEW_FILES = new_files;
                    //endregion

                    var PopupBX = new BX.MyPopupBX();
                    PopupBX.Message('reload','','Применение изменений, ожидайте...');

                    //region Сбор системных данных, для сохранения
                    this.card.CARD_ID = this.card_Id;
                    this.card.EXECUTOR_BY_ID = $('input[name="EXECUTOR_BY_ID"]').val();
                    this.card.UF_RESPONSIBLE_EVENT = $('input[name="UF_RESPONSIBLE_EVENT"]').val();
                    //endregion

                    $.ajax({
                        url: this.path +'ajax.php', type: 'post', dataType: 'json', contentType: 'application/json',
                        success: function (data) {
                            if(data['success']) { location.reload(); }
                            else { PopupBX.CloseLastPopup(); printErrors(data['data']['errors']); }
                        }, data: JSON.stringify(this.card)
                    });
                }



            }
        }
    });

    return app;
}
jQuery.datetimepicker.setLocale('ru');

function printErrors(errors) {
    var PopupBX = new BX.MyPopupBX();
    errors_str = ''; for (i=0;i<errors.length;i++) { errors_str += ' - ' + errors[i] + '<br>'; } /*errors_str += '';*/
    PopupBX.Message('errors_edit_booking','',errors_str,true);
}