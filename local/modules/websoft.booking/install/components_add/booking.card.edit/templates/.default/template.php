<?php
defined('B_PROLOG_INCLUDED') || die;

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Grid\Panel\Snippet;
use Bitrix\Main\Loader;
use Bitrix\Main\Page\Asset;
use Bitrix\Main\Web\Json;

/** @var CBitrixComponentTemplate $this */
$APPLICATION->SetTitle('Карточка бронирования(редактирование)');
//Asset::getInstance()->addString('<script src="https://cdn.jsdelivr.net/npm/vue@2.5.17/dist/vue.js"></script>');
Asset::getInstance()->addJs($this->__folder . "/js/vue.js");
Asset::getInstance()->addJs($this->__folder . "/js/jquery.raty.mini.js");
Asset::getInstance()->addJs($this->__folder . "/js/main.js");
Asset::getInstance()->addCss($this->__folder . "/css/style.css");
Asset::getInstance()->addCss($this->__folder . "/css/jquery.rating.css");

$pathToAjax = $templateFolder.'/ajax.php';
$CARD_ID = $arResult['CARD_ID'];
$arData = $arResult['DATA_TO_EDIT'];
$EXECUTOR_ID = $arResult['EXECUTOR_ID'];
$UF_RESPONSIBLE_EVENT = $arResult['UF_RESPONSIBLE_EVENT'];

?>


<div class="form_responsible_event">
    <?
    $user = \CUser::GetByID($UF_RESPONSIBLE_EVENT)->Fetch();
    $APPLICATION->IncludeComponent(
        'bitrix:tasks.widget.member.selector',
        '',
        array(
            'TEMPLATE_CONTROLLER_ID' => 'UF_RESPONSIBLE_EVENT',
            'MAX' => 1,
            'MIN' => 1,
            'MAX_WIDTH' => 786,
            'TYPES' => array('USER', 'USER.EXTRANET'),
            'INPUT_PREFIX' => 'UF_RESPONSIBLE_EVENT',
            'SOLE_INPUT_IF_MAX_1' => 'Y',
            'DATA' => $user ? array($user) : '',
            'PATH_TO_USER_PROFILE' => \Bitrix\Main\Config\Option::get('intranet', 'path_user', '', SITE_ID) ,
            'READ_ONLY' => 'N',
        ),
        false,
        array("HIDE_ICONS" => "Y", "ACTIVE_COMPONENT" => "Y")
    );
    ?>
</div>

<div class="form_excecutor">
    <?
    $user = \CUser::GetByID($EXECUTOR_ID)->Fetch();
    $APPLICATION->IncludeComponent(
        'bitrix:tasks.widget.member.selector',
        '',
        array(
            'TEMPLATE_CONTROLLER_ID' => 'originator',
            'MAX' => 1,
            'MIN' => 1,
            'MAX_WIDTH' => 786,
            'TYPES' => array('USER', 'USER.EXTRANET'),
            'INPUT_PREFIX' => 'EXECUTOR_BY_ID',
            'SOLE_INPUT_IF_MAX_1' => 'Y',
            'DATA' => array($user),
            'PATH_TO_USER_PROFILE' => \Bitrix\Main\Config\Option::get('intranet', 'path_user', '', SITE_ID) ,
            'READ_ONLY' => 'N',
        ),
        false,
        array("HIDE_ICONS" => "Y", "ACTIVE_COMPONENT" => "Y")
    );
    ?>
</div>

<div class="form_add_new_files">
    <?$APPLICATION->IncludeComponent("bitrix:main.file.input", "drag_n_drop",
        array(
            "INPUT_NAME"=>"TEST_NAME_INPUT",
            "MULTIPLE"=>"Y",
            "MODULE_ID"=>"main",
            "MAX_FILE_SIZE"=>"",
            "ALLOW_UPLOAD"=>"A",
            "ALLOW_UPLOAD_EXT"=>""
        ), false
    );?>
</div>

<main id="app">
    <div class="center_side">
        <!--
        <div class="breadcrumbs">
            <li><a href="/booking/">Бронирование переговорных</a></li>
            <li><a href="#" class="active">Карточка бронирования</a></li>
        </div>
        -->
        <div class="content_sides">
            <div class="content_side_center">

                <!-- Наименование мероприятия -->
                <div class="content_item">
                    <div class="content_left ">
                        <div class="content_left_item  m-h70">
                            <p>Наименование мероприятия</p>
                        </div>
                    </div>
                    <div class="content_right ">
                        <div class="content_right_item m-h70">
                            <div class="input_wrap active ">
                                <input type="text" class="w-100" v-model="card.NAME" style="width: 798px;">
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Описание мероприятия -->
                <div class="content_item">
                    <div class="content_left  ">
                        <div class="content_left_item item_start m-h120">
                            <p>Описание мероприятия</p>
                        </div>
                    </div>
                    <div class="content_right">
                        <div class="content_right_item m-h120 p-15">
                            <div class="input_wrap active ">
                                <textarea v-model="card.DESCRIPTION" id=""></textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Ответственный за мероприятие -->
                <div class="content_item">
                    <div class="content_left ">
                        <div class="content_left_item  m-h50">
                            <p>Ответственный за мероприятие</p>
                        </div>
                    </div>
                    <div class="content_right ">
                        <div class="content_right_item m-h50 space_between pr-10">
                            <div class="boxToResponsibleEvent"></div>
                        </div>
                    </div>
                </div>

                <!-- Организатор -->
                <div class="content_item">
                    <div class="content_left ">
                        <div class="content_left_item  m-h50">
                            <p>Организатор</p>
                        </div>
                    </div>
                    <div class="content_right ">
                        <div class="content_right_item m-h50 space_between pr-10">
                            <div class="boxToChangeExcecutor"></div>
                        </div>
                    </div>
                </div>
                
                <!-- Период проведения -->
                <div class="content_item">
                    <div class="content_left ">
                        <div class="content_left_item  m-h50">
                            <p>Период проведения</p>
                        </div>
                    </div>
                    <div class="content_right">
                        <div class="content_right_item m-h50 space_between pr-10">

                            <div class="input_wrap active ">
                                <input type="text" class="w-290" id="datepicker_date_begin" >
                            </div>

                            <div class="input_wrap active w-170 mr-10" style="width: 105px">
                                <div class="input_item">
                                    <input type="text" step="300" class="bb-none onlytime" id="time_start_begin" readonly>
                                </div>
                            </div>

                            <div class="input_wrap active w-170 mr-10" style="width: 105px">
                                <div class="input_item">
                                    <input type="text" step="300" class="bb-none onlytime" id="time_end_begin" readonly>
                                </div>
                            </div>

                            <!--<div class="input_wrap active ">
                                <label for="">C</label>
                                <input type="text" class="w-290" id="datepicker_from" v-model="card.DATE_START">
                            </div>
                            <div class="input_wrap active ">
                                <label for="">До</label>
                                <input type="text" class="w-290" id="datepicker_to"  v-model="card.DATE_END">
                            </div>-->
                        </div>
                    </div>
                </div>

                <!-- Перерывы -->
                <div class="content_item">
                    <div class="content_left ">
                        <div class="content_left_item  m-h60">
                            <p>Перерывы</p>
                        </div>
                    </div>
                    <div class="content_right " id="BREAKS">
                        <div class="content_right_item m-h60  pr-10" v-for="(break_info, index) in card.BREAKS">
                            <div class="input_wrap active w-170 mr-10">
                                <div class="input_item">
                                    <input type="text" v-model="break_info.NAME" placeholder="Наименование">
                                </div>
                            </div>
                            <div class="input_wrap active w-170 mr-10" style="width: 105px">
                                <div class="input_item">
                                    <label for="">C</label>
                                    <input type="text" step="300" class="bb-none onlytime" v-model="break_info.START">
                                </div>
                            </div>
                            <div class="input_wrap active w-170 mr-10" style="width: 105px">
                                <div class="input_item">
                                    <label for=""><p>До</p></label>
                                    <input type="text" step="300" class="bb-none onlytime"  v-model="break_info.END">
                                </div>
                            </div>                                
                            <div class="input_wrap w-170 mr-10">
                                <div class="input_item">
                                    <div class="delete_brek" @click="deletBreak(index)">x</div>
                                </div>
                            </div>                                
                        </div>
                        <div class="w-100 add_break" @click="addBreak()"><div class="add_break_pluss">+</div> <span class="add_break_text">Добавить перерыв...</span></div>
                    </div>
                </div>

                 <!-- Комментарий к мероприятию -->
                 <div class="content_item">
                    <div class="content_left  ">
                        <div class="content_left_item item_start m-h120">
                            <p>Комментарий к мероприятию</p>
                        </div>
                    </div>
                    <div class="content_right ">
                        <div class="content_right_item m-h120 p-15">
                            <div class="input_wrap active ">
                                <textarea v-model="card.COMMENT_EVENT"></textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <? if($arResult['ThisUsersAccess']): ?>
                    <!-- Файлы -->
                    <div class="content_item">
                    <div class="content_left ">
                        <div class="content_left_item item_start m-h55 ai_center">
                            <p>Файлы мероприятия</p>
                        </div>
                    </div>
                    <div class="content_right">
                        <div class="content_right_item m-h55 p-15">
                            <div class="input_wrap ">
                                <?
                                if(!empty($arResult['FILES'])): ?>
                                    <div class="old_files">
                                        <span>Ранее загруженные файлы, при пометке удаляется:</span>
                                        <br>
                                        <? foreach ($arResult['FILES'] as $FILE): ?>
                                            <lable>
                                                <input type="checkbox" class="old_files" data-id-file="<?=$FILE['ID']?>">
                                                <a download href="<?=$FILE['SRC']?>"><?=$FILE['ORIGINAL_NAME']?></a>
                                            </lable><br>
                                        <? endforeach; ?>
                                        <hr>
                                    </div>
                                <? endif; ?>
                                <form class="boxAddNewFiles"></form>
                            </div>
                        </div>
                    </div>
                </div>
                <? endif; ?>

                <!-- Сопутствующие карточки -->
                <div class="content_item">
                    <div class="content_left " style="z-index: 1;">
                        <div class="content_left_item slide_card m-h80">
                            <p>Сопутствующие карточки</p>
                        </div>
                    </div>
                    <div class="content_right" style="overflow:hidden;">
                        <div class=" m-h80  pr-10">
                            <div class="" style="    max-width: 805px;margin: 0 auto;">
                                <div class="card_slider">
                                    <div class="card_slider-item" v-for="(subcard, index) in card.ITEMS">
                                        <span :class="[{active:subcard.SELECTED},'card_slider-item_inner']"@click="selectSubCard(subcard, index, $event)">
                                            {{subcard.NAME}}
                                        </span>
                                    </div>                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <? /*
                <!-- Статус подготовки -->
                <div class="content_item">
                    <div class="content_left ">
                        <div class="content_left_item  m-h50">
                            <p>Статус подготовки</p>
                        </div>
                    </div>
                    <div class="content_right ">
                        <div class="content_right_item m-h50 space_between pr-10">
                            <select v-model="card.ITEMS[selectCard].TRAINING_STATUS.SELECT" class="input_wrap active ">
                                <option :value="training.ID" v-for="training in card.ITEMS[selectCard].TRAINING_STATUS.LIST_TRAINING_STATUS">{{training.NAME}}</option>
                            </select>
                            <!-- <input type="text" class="w-290" v-model="card.ITEMS[selectCard].TRAINING_STATUS.SELECT"> -->
                        </div>
                    </div>
                </div>
                */ ?>

                <!-- Общее описание подготовки -->
                <div class="content_item">
                    <div class="content_left  ">
                        <div class="content_left_item item_start m-h120">
                            <p>Общее описание подготовки</p>
                        </div>
                    </div>
                    <div class="content_right ">
                        <div class="content_right_item m-h120 p-15">
                            <div class="input_wrap active ">
                                <textarea v-model="card.ITEMS[selectCard].STATUS_DESCRIPTION"></textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <? if($arData['FINISH']): ?>
                    <!-- Организация и сопровождение мероприятия -->
                    <div class="content_item">
                        <div class="content_left">
                            <div class="content_left_item item_start">
                                <p>Организация и сопровождение мероприятия</p>
                            </div>
                        </div>
                        <div class="content_right ">
                            <div class="content_right_item p-15">
                                <div class="rating1">
                                    <div id="RATING_1" class="star green raty-icons" data-target="pid_1" data-score="5.00"></div>
                                    <input id="pid_1" v-model="card.ITEMS[selectCard].RATING_1" type="text" class="rates" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Подготовка оборудования -->
                    <div class="content_item">
                        <div class="content_left">
                            <div class="content_left_item item_start">
                                <p>Подготовка оборудования</p>
                            </div>
                        </div>
                        <div class="content_right ">
                            <div class="content_right_item p-15">
                                <div class="rating1">
                                    <div id="RATING_2" class="star green raty-icons" data-target="pid_2" data-score="5.00"></div>
                                    <input id="pid_2" v-model="card.ITEMS[selectCard].RATING_2" type="text" class="rates" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Питание -->
                    <div class="content_item">
                        <div class="content_left">
                            <div class="content_left_item item_start">
                                <p>Питание</p>
                            </div>
                        </div>
                        <div class="content_right ">
                            <div class="content_right_item p-15">
                                <div class="rating1">
                                    <div id="RATING_3" class="star green raty-icons" data-target="pid_3" data-score="5.00"></div>
                                    <input id="pid_3" v-model="card.ITEMS[selectCard].RATING_3" type="text" class="rates" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Комментарий к сопровождению мероприя -->
                    <div class="content_item">
                        <div class="content_left  ">
                            <div class="content_left_item item_start m-h55 ai_center">
                                <p>Комментарий <br> к сопровождению мероприя</p>
                            </div>
                        </div>
                        <div class="content_right ">
                            <div class="content_right_item m-h55 p-15">
                                <div class="input_wrap active ">
                                    <textarea v-model="card.COMMENT_SERVICE_TYPES" id="" class="mh-45"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                <? endif; ?>

                <!-- Сервисы сопровождения -->
                <div class="content_item">
                    <div class="content_left  ">
                        <div class="content_left_item item_start m-h120">
                            <p>Сервисы сопровождения</p>
                        </div>
                    </div>
                    <div class="content_right ">
                        <div class="content_right_item m-h120 p-15">
                            <table>
                                <tr>
                                    <th>Сервис</th>
                                    <? if(!$arData['FINISH']): ?>
                                        <th>Статус готовности</th>
                                    <? endif; ?>
                                    <? if($arData['FINISH']): ?>
                                        <!--<th>Оценка</th>-->
                                    <? endif; ?>
                                    <th>Ответственный</th>
                                </tr>
                                <tr v-for="services in card.ITEMS[selectCard].SERVICES_DATA">
                                    <td><input type="text" readonly="readonly" :value="services.NAME"></td>
                                    <? if(!$arData['FINISH']): ?>
                                    <td>
                                        <select v-model="services.STATUS.ID">
                                            <option v-for="(name, index) in services.STATUS.LIST"
                                                    :value="index">{{ name }}</option>
                                        </select>
                                    </td>
                                    <? endif; ?>
                                    <? if($arData['FINISH']): ?>
                                        <!--<td>
                                            <div class="stars_wrap text-left">
                                                <div class="rating1">
                                                    <div class="star green raty-icons"
                                                        id="pid_974-raty"
                                                        data-target="pid_974"
                                                        data-score="5.00"
                                                        :data-id-service="services.ID"
                                                    >
                                                    </div>
                                                    <input  id="pid_974" v-model="services.RATING" type="text" class="rates" />
                                                </div>
                                            </div>
                                        </td>-->
                                    <? endif; ?>
                                    <td class="text-left">
                                        <span class="phone_num">{{services.RESPONSIBLE.FIO}}</span>
                                        <span href="#" class="phone_ic" @click="showPhone($event)">
                                            <div class="company-details">
                                                <p>{{services.RESPONSIBLE.PHONE}}</p>
                                            </div>
                                        </span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                    </div>
                </div>

                <!-- Control buttons -->
                <div class="content_item">
                    <div class="content_right ">                        
                            <div class="footer_buttons">
                                <a class="btns blue" href="javascript::void(0)" @click="save()">СОХРАНИТЬ</a>
                                <a class="btns black" href="/booking/card/<?=$CARD_ID?>/?DATE=<?=$_REQUEST['DATE']?>">НАЗАД</a>
                                <a class="btns violet" href="javascript:void(0);" onclick="app.CancelBooking();">Аннулировать</a>
                            </div>
                    </div>
                </div>

            </div>
            <div class="right_side"></div>
        </div>
    </div>
</main>
<script>
    app = '';
    $(document).ready(function () {
        var settings = {
            path: '<?=$this->__folder?>/',
            card_Id: <?=$CARD_ID?>,
            card:<?=CUtil::PhpToJSObject($arData)?>,
            selectCard: 0
        }
        app = initApp(settings);
        app.updateRaiting();

        //$('.form_excecutor') // Тут сгенерированная форма
        //$('.boxToChangeExcecutor') // Сюда её нужно положить
        $('.boxToChangeExcecutor').append($('.form_excecutor'));
        $('.form_excecutor').css('display','block');

        $('.boxAddNewFiles').append($('.form_add_new_files'));
        $('.form_add_new_files').css('display','block');

        $('.boxToResponsibleEvent').append($('.form_responsible_event'));
        $('.form_responsible_event').css('display','block');
    });

    /*<![CDATA[*/
    $.fn.raty.defaults.path =  '<?=$this->__folder?>/img';
    $.fn.raty.defaults.targetKeep = true;
    $.fn.raty.defaults.targetType = 'number';
    $.fn.raty.defaults.hints = ['bad', 'poor', 'regular', 'good', 'gorgeous'];
    $.fn.raty.defaults.noRatedMsg = 'Not rated yet!';
    $.fn.raty.defaults.cancelHint = 'Cancel this rating!';

    /*]]>*/
    jQuery(function ($) {
        /*console.log('app');
        console.log(app);
        $( '.raty-icons' ).each(function( index, element ) {
            data_id_service = $(element).attr('data-id-service');
            console.log('data_id_service: ' + data_id_service);
            jQuery(element).raty({
                'score': parseInt(app.SERVICES_DATA[index]['RATING']),
                'target': '.rates', 'click': function (score, evt) {
                    id_service = $(this).attr( "data-id-service" );
                    app.changeRating(score,id_service);
                }
            });
        });*/
        /*jQuery('.raty-icons').raty({
            'click': function (score, evt) {
                id_service = $(this).data( "id-service" );
                app.changeRating(score,id_service);
            }, 'score': '0', 'target': '.rates'
        });
        jQuery('.rates').hide();*/



    });
</script>