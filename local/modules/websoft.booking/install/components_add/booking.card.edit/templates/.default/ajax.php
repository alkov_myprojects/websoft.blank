<?
require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");

$json = file_get_contents("php://input");
$request = json_decode($json,true);

$CARD_ID = $request['CARD_ID'];
unset($request['CARD_ID']);

$DATA = $request;

//region Определить занятость переговорной

    $DATE_BEGIN = $DATA['DATE_START'];
    $DATE_END = $DATA['DATE_END'];

    $time = strtotime($DATE_END);
    $time = $time - (1 * 60); // Минус минута
    $date = date("d.m.Y H:i:s", $time);
    $DATE_END = $date;

    $ITEMS = $DATA['ITEMS'];

    $arFilter = array('ID'=>array());
    foreach ($ITEMS as $ITEM) {
        $ExcludeChildCardsFromSearch = array('ID'=>$ITEM['ID']);
        $arFilter['ID'] = $ITEM['NEG_ID'];
    }

    //region Поиск подходящей переговорной
    CBitrixComponent::includeComponentClass("websoft.booking:booking.master");
    $CBookingMasterComponent = 'CBookingMasterComponent';
    $negsCategories = $CBookingMasterComponent::getNegotiationCategories($arFilter,$DATE_BEGIN,$DATE_END,$ExcludeChildCardsFromSearch);
    //endregion

//endregion
$result = array('success' => true, 'data' => array('errors'));
if(empty($negsCategories['BUSY'])) { $result = \Websoft\Booking\Entity\BookingCardTable::changeCard($CARD_ID,$DATA); }
else {
    $result['success'] = false;
    foreach ($negsCategories['BUSY'] as $neg) { $result['data']['errors'][] = $neg['NAME'].' занята'; }
}

$result['data']['$negsCategories'] = $negsCategories;
$result['data']['$arFilter'] = $arFilter;
$result['data']['$DATE_BEGIN'] = $DATE_BEGIN;
$result['data']['$DATE_END'] = $DATE_END;
$result['data']['$DATA'] = $DATA;

echo json_encode($result,true);
?>