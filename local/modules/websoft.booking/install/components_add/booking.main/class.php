<?php
defined('B_PROLOG_INCLUDED') || die;

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use \Websoft\Booking\Entity\CompanyBookingTable as CompanyBooking;
use \Websoft\Booking\Entity\NegotiatedTable as Negotiated;
use \Websoft\Booking\Entity\ServiceTypesTable as ServiceTypes;

class CBookingMainComponent extends CBitrixComponent {
    public static function dateIntervals() {
        return array(
            'Date_time_start' => '07:00:00',
            'Date_time_end' => '21:00:00',
            'DateInterval_Min' => 30
        );
    }
    public function executeComponent() {

        /** Локальные переменные: */
        $FOLDER = $this->arParams['FOLDER'];
        $URL_TEMP = is_array($this->arParams['URL_TEMPLATES']) ? $this->arParams['URL_TEMPLATES'] : array();
        $getMes = 'getMessage';
        $MODE = $this->arParams['MODE'];
        $nameSpace = explode(':', $this->getName());
        $nameSpace = $nameSpace[0];

        /** Проерки: */
        if (!Loader::includeModule($nameSpace)) {$this->error('NO_MODULE', array('MID' => $nameSpace));return;}
        if (empty($MODE) || $MODE != 'Y') {ShowError(Loc::{$getMes}('NOT_ENABLED'));return;}
        if (empty($FOLDER)) {ShowError(Loc::{$getMes}('BASE_EMPTY'));return;}
        foreach ($this->arParams['ListEntity'] as $className) {
            if (!class_exists($className)) {
                ShowError(Loc::{$getMes}('EntityNotExists'), array('CLASS' => $className));return;
            }
        }

        /** Кеш */
        //$cache = Cache::createInstance(); // получаем экземпляр класса
        $cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
        $cacheTtl = 7200;

        /** ... */
        global $arVariables;
        $page = CComponentEngine::parseComponentPath($FOLDER, $URL_TEMP, $arVariables);
        if (empty($page)) {$page = 'index';} // Если нет страницы, то по умолчанию редирект на главную.

        if ($page == 'handbook_list_entity' || $page == 'handbook_edit_entity') {$page = 'handbook';}
        if ($page == 'setting_list_entity' || $page == 'setting_edit_entity') {$page = 'setting';}

        /** Хак, только для этого проекта, возможно и для других подойдёт... но нужно его оптимизировать */
        $menuCounter = array(
            'MAIN' => 0,
            'SETTING' => 0,
            'HANDBOOK' => 0,
            'REPORT' => 0,
        );
        // Проверить ошибки в сущностях
        $mainNameSpace = "\Websoft\Booking\Entity";
        $listGroupEntity = array(
            'HANDBOOK' => array('Negotiated', 'CompanyBooking', 'ResponsibleSupportServices'),
            'SETTING' => array(),
        );

        global $APPLICATION;
        $listEntity = array();
        $dir = COption::GetOptionString($nameSpace, "dir") . '/lib/entity/';
        if (is_dir($dir)) {
            $TreeFiles = self::TreeDirAndFiles($dir);
            foreach ($TreeFiles as $file) {$listEntity[] = self::get_namespace($file);}
        }

        ob_start();?>
        <style><?
        foreach ($listGroupEntity as $group => $entities) {foreach ($entities as $entity_name) {
            $entityLocal = $mainNameSpace . "\\" . $entity_name . 'Table';

            //$ListWarning = $entityLocal::ListWarning();
            $cacheId = $entityLocal;
            if ($cache->read($cacheTtl, $cacheId)) { $ListWarning = $cache->get($cacheId); }
            else {
                $ListWarning = $entityLocal::ListWarning();
                $cache->set($cacheId, $ListWarning); // записываем в кеш
            }

            $UfId = $entityLocal::getUfId();
            $UfId = strtolower($UfId);
            if (!empty($ListWarning)) {
                $menuCounter[$group] += count($ListWarning);
                foreach ($listEntity as $gEntity) {
                    $UfId_2 = strtolower($gEntity::getUfId());
                    echo '.crm-view-switcher-list-item#entity_list_' . $UfId_2 . 'table_' . $UfId . ' { color: red; } ';
                }
            }
        }}?></style><?$CSS = ob_get_clean();
        $APPLICATION->AddHeadString($CSS);

        /** Карта компонентов: */
        $urlTemplates = array(
            'MAIN' => $FOLDER . $URL_TEMP['main'],
            'SETTING' => $FOLDER . $URL_TEMP['setting'],
            'MASTER' => $FOLDER . $URL_TEMP['master'],
            'HANDBOOK' => $FOLDER . $URL_TEMP['handbook'],
            'REPORT' => $FOLDER . $URL_TEMP['report'],
            'CARD' => $FOLDER . $URL_TEMP['card'],

            //'VIEW' => $FOLDER.$URL_TEMP['view'],
            //'EDIT' => $FOLDER.$URL_TEMP['edit']
        );

        /** Определение пользователя (из какой он компании) */
        $CompanyUsers = COption::GetOptionString($nameSpace, "CompanyUsers");
        $CompanyUsers = json_decode($CompanyUsers, true);
        global $USER;
        $COMPANY_ID_THIS_USER = '';
        if (is_array($CompanyUsers) && count($CompanyUsers) > 0) {foreach ($CompanyUsers as $item) {
            if ($USER->GetID() == $item['USER_ID']) {$COMPANY_ID_THIS_USER = $item['UF_COMPANIES_ID'];}
        }}

        /** Новое */
        $arCompany = self::getCompnyList();
        // \Bitrix\Main\Diag\Debug::dump($arCompany,'$arCompany');
        $arNegotiated =  self::getNegotiated();
        /** END */

        //region Получить расписание переговорных
        $date = $_REQUEST['DATE'] ? $_REQUEST['DATE'] : date_format(date_create(), 'd.m.Y'); //date_format(date_create(), 'd.m.Y'); // Сегодняшний день
        //$date = '16.11.2018';

        // ...
        $dateIntervals = self::dateIntervals();
        $date_time_start = date_format(date_create($date), 'Y-m-d').' '.$dateIntervals['Date_time_start'];
        $date_time_end = date_format(date_create($date), 'Y-m-d').' '.$dateIntervals['Date_time_end'];

        $begin = new \DateTime( $date_time_start );
        $end = new \DateTime( $date_time_end );
        //$end->modify( '+1 day' );
        //$end->modify( '+24 hour' );
        $interval = new \DateInterval('PT'.$dateIntervals['DateInterval_Min'].'M');
        $daterange = new \DatePeriod($begin, $interval ,$end);


        //\Websoft\Booking\Handler::dd('$arNegotiated',$arNegotiated);

        foreach ($arNegotiated as &$neg) {
            $Interpreter = Negotiated::GetInterpreter($neg['ID'],$date,$daterange);
            $neg['ITEMS'] = $Interpreter;
        }
        //endregion

        $daterange_clear = array();
        foreach ($daterange as $date) { $daterange_clear[] = $date->format('H:i'); }

        /** Подключение и передача данных компоненту: */
        $toDay = date_format(date_create(), 'd.m.Y');
        $this->arResult = array(

            'DATE' => $_REQUEST['DATE'] ? $_REQUEST['DATE'] : $toDay, //date_format($date,'d.m.Y'),
            'COMPANY_ID_THIS_USER' => $COMPANY_ID_THIS_USER,
            'COMPANIES' => $arCompany,
            'NEGOTIATED' => $arNegotiated,

            'URL_TEMPLATES' => $urlTemplates,
            'arVariables' => $arVariables,
            'nameSpace' => $nameSpace,

            'menuCounter' => $menuCounter,
            'mainNameSpace' => $mainNameSpace,
            'listGroupEntity' => $listGroupEntity,

            'daterange' => $daterange_clear

            //'ListEntity' => $this->arParams['ListEntity'],
        );

        $this->includeComponentTemplate();
    }
    private static function TreeDirAndFiles($directory)
    {
        if (!is_dir($directory)) {return false;}
        $files = array();
        $dir = dir($directory);
        while (false !== ($file = $dir->read())) {
            if ($file != '.' and $file != '..') {
                $checkDir = $directory . $file;
                if (is_dir($checkDir)) {$files = array_merge($files, self::TreeDirAndFiles($checkDir . '/'));} else { $files[] = $directory . $file;}
            }
        }
        $dir->close();
        return $files;
    }
    private static function get_namespace($path)
    {
        $content = file_get_contents($path);
        $tokens = token_get_all($content);
        $namespace = '';
        for ($index = 0;isset($tokens[$index]); $index++) {
            if (!isset($tokens[$index][0])) {
                continue;
            }
            if (T_NAMESPACE === $tokens[$index][0]) {
                $index += 2; // Skip namespace keyword and whitespace
                while (isset($tokens[$index]) && is_array($tokens[$index])) {
                    $namespace .= $tokens[$index++][1];
                }
            }
            if (T_CLASS === $tokens[$index][0]) {
                $index += 2; // Skip class keyword and whitespace
                $fqcns[] = $namespace . '\\' . $tokens[$index][1];
                $namespace .= '\\' . $tokens[$index++][1];
            }
        }
        return '\\' . $namespace;
    }
    private static function getCompnyList()
    {

        /** Кеш */
        //$cache = Cache::createInstance(); // получаем экземпляр класса
        $cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
        $cacheTtl = 7200;

        $idsCompany = array();

        //region кеш $listCorrectNeg
        //$listCorrectNeg = Negotiated::GetCorrectElements();
        $cacheId = 'booking_main_listCorrectNeg';
        if ($cache->read($cacheTtl, $cacheId)) { $listCorrectNeg = $cache->get($cacheId); }
        else {
            $listCorrectNeg = Negotiated::GetCorrectElements();
            $cache->set($cacheId, $listCorrectNeg); // записываем в кеш
        }
        //endregion

        // \Bitrix\Main\Diag\Debug::dump($listCorrectNeg,'$listCorrectNeg');
        // die();
        foreach ($listCorrectNeg as $neg) {$idsCompany[] = $neg['COMPANY_ID'];}

        //region кеш $listCompany
        //$listCompany = CompanyBooking::getListPure(true, array('ID' => $idsCompany), array('ID', 'NAME', 'UF_LOGO'));
        $cacheId = 'booking_main_listCompany';
        if ($cache->read($cacheTtl, $cacheId)) { $listCompany = $cache->get($cacheId); }
        else {
            $listCompany = CompanyBooking::getListPure(
                true, array('ID' => $idsCompany), array('ID', 'NAME', 'UF_LOGO'));
            $cache->set($cacheId, $listCompany); // записываем в кеш
        }
        //endregion

        foreach ($listCompany as $key => $value) {
            if ((int) $value['UF_LOGO'] > 0) {
                $listCompany[$key]['UF_LOGO'] = CFile::GetPath($value['UF_LOGO']);
            }
            $listCompany[$key]['ACTIVE'] = false;
            $listCompany[$key]['COUNT_NEG'] = Negotiated::getCount(['COMPANY_ID' => $value['ID']]);
        }
        return $listCompany;
    }
    public static function getNegotiated($company_id = false) {

        /** Кеш */
        //$cache = Cache::createInstance(); // получаем экземпляр класса
        $cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
        $cacheTtl = 7200;

        $arFilter = array();
        $arSelect = array('*', 'UF_*');
        if ($company_id) { $arFilter['COMPANY_ID'] = $company_id; }


        //region кеш $arNegotiated
        //$arNegotiated = Negotiated::getListPure(true, $arFilter, $arSelect);
        $cacheId = 'booking_main_arServices';
        if ($cache->read($cacheTtl, $cacheId)) { $arNegotiated = $cache->get($cacheId); }
        else {
            $arNegotiated = Negotiated::getListPure(true, $arFilter, $arSelect);
            $cache->set($cacheId, $arNegotiated); // записываем в кеш
        }
        //endregion

        foreach ($arNegotiated as $key => &$negotiated) {
            $resizePrevie = CFile::ResizeImageGet($negotiated['UF_PREVIEW_IMAGE'],array('width'=>185,'height'=>115),BX_RESIZE_IMAGE_EXACT);
            $negotiated['UF_PREVIEW_IMAGE'] = array('ID'=>$negotiated['UF_PREVIEW_IMAGE'],'SRC'=>$resizePrevie['src']);
            if ($negotiated['UF_SERVICE_TYPES']) {

                //region кеш $arServices
                $arServices = ServiceTypes::getList(array(
                    'filter'=>array( 'ID' => $negotiated['UF_SERVICE_TYPES'] ), 'select'=>array('*', 'UF_*')
                ))->fetchAll();
                //endregion

                foreach ($arServices as $key => $value) {
                    if($value['UF_ICON'] > 0){
                        //$arServices[$key]['UF_ICON'] = CFile::GetPath($value['UF_ICON']);
                        $arServices[$key]['UF_ICON'] = CFile::GetPath($value['UF_ICON_ACTIVE']);
                    }
                }
                $negotiated['SERVICE'] = $arServices;
                unset($negotiated['UF_SERVICE_TYPES']);
            }
        }
        return $arNegotiated;
    }
}