<?php
defined('B_PROLOG_INCLUDED') || die;

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Grid\Panel\Snippet;
use Bitrix\Main\Loader;
use Bitrix\Main\Page\Asset;
use Bitrix\Main\Web\Json;

/** @var CBitrixComponentTemplate $this */
$APPLICATION->SetTitle(Loc::getMessage('TITLE_BOOKING'));
?>

<script>
    function redirect(page) { window.location = page; }
    function activate(obj) {
        className = 'active';
        if($(obj).hasClass(className)) { $(obj).removeClass(className); }
        else { $(obj).addClass(className); }

    }
</script>
<style>
    .Company, .Negotiated{
        padding: 10px 5px; border: 2px solid; cursor: pointer; display: inline-block; margin: 10px; user-select: none;
        text-align: center;
    }
    .Company.active,  .Negotiated.active { border-color: red; }
</style>

<? ob_start(); ?>
<button onclick="redirect('<?=$arResult['URL_TEMPLATES']['SETTING']?>');">Административная часть</button>
<button style="margin-left: 10px;" onclick="redirect('<?=$arResult['URL_TEMPLATES']['MASTER']?>');">Мастер бронирования</button>

<?
$APPLICATION->AddViewContent('pagetitle', ob_get_contents()); ob_end_clean();?>

<?
global $APPLICATION; $APPLICATION->ShowCSS(); $APPLICATION->ShowHeadScripts(); $APPLICATION->ShowHeadStrings();
CJSCore::Init(array("jquery","date"));
$date = date_create();
$date = date_format($date, 'd.m.Y');
?>

<?
use \Websoft\Booking\Entity\NegotiatedTable as Negotiated;
use \Websoft\Booking\Entity\CompanyBookingTable as CompanyBooking;
//... Получить все корректные переговорные
$listCorrectNeg = Negotiated::GetCorrectElements();
//...... Ивзлечь из них компании
$idsCompany = array();
foreach ($listCorrectNeg as $neg) { $idsCompany[] = $neg['COMPANY_ID']; }
$listCompany = CompanyBooking::getListPure(true,array('ID'=>$idsCompany),array('ID','NAME','UF_LOGO'));
//...... Извлечь перегворные для вывода на странице
$idsNeg = array();
foreach ($listCorrectNeg as $neg) { $idsNeg[] = $neg['ID']; }
$listNeg = Negotiated::getListPure(true,array('ID'=>$idsNeg),array('ID','NAME','UF_PREVIEW_IMAGE'));
?>

<div class="main">
    <div class="CompanyProfile">
        <b>Компании:</b><br>
        <? $firsCompany = true; ?>
        <? foreach ($listCompany as $company): ?>
            <?$logo = $company['UF_LOGO'];$logo = CFile::GetFileArray($logo);$logo = $logo['SRC'];?>
            <div data-id="<?=$company['ID'];?>"
                 class="Company"><img src="<?=$logo?>" height="50" alt=""><hr><?=$company['NAME'];?></div>
        <? endforeach; ?>
    </div>
    <hr>
    <div class="DateAndSearch">
        <div class="date">
            <?
            $date = date_create();
            $date = date_format($date, 'd.m.Y');

            ?>
            Дата: <input type="text"
                         value="<?=$date;?>"
                         name="start"
                         onclick="BX.calendar({node: this, field: this, bTime: false});">
        </div>
        <hr>
        <div class="Interactive">
                <b>Переговорные:</b><br>
                <? foreach ($listNeg as $neg): ?>
                    <div data-id="<?=$neg['ID']?>" class="Negotiated">
                        <?$logo = $neg['UF_PREVIEW_IMAGE'];$logo = CFile::GetFileArray($logo);$logo = $logo['SRC'];?>
                        <img src="<?=$logo?>" height="50" alt=""><hr>
                        <?=$neg['NAME']?>
                    </div>
                <? endforeach; ?>
        </div>
    </div>
    <hr>
    Расписание:
    <? foreach ($listNeg as $neg): ?>
        <?
        // Получить расписнаие переговорной
        $Interpreter = Negotiated::GetInterpreter($neg['ID'],$date);
        ?>
    <? endforeach; ?>
</div>

<script>

</script>