<?php
defined('B_PROLOG_INCLUDED') || die;

use Bitrix\Main\Localization\Loc;

/** @var CBitrixComponentTemplate $this */

$APPLICATION->SetTitle(Loc::getMessage('TITLE'));

//region Панелька CRM (Старт, Лиды, Сделки и т.д.), полезно если сюда вставляем ссылку на текущий компонет
$APPLICATION->IncludeComponent(
    'bitrix:crm.control_panel', '', array('ID' => 'ENTITY', 'ACTIVE_ITEM_ID' => 'ENTITY'), $component
);
//endregion

//region Автофильтр - например "5 сделок требущих внимания"
/*$isBitrix24Template = SITE_TEMPLATE_ID === 'bitrix24';
if($isBitrix24Template) { $this->SetViewTarget('below_pagetitle', 0); }
$APPLICATION->IncludeComponent( 'bitrix:crm.entity.counter.panel', '', array(
    'ENTITY_TYPE_NAME' => CCrmOwnerType::DealName,
    //'EXTRAS' => array('DEAL_CATEGORY_ID' => $categoryID),
    //'PATH_TO_ENTITY_LIST' => $PATH_TO_ENTITY_LIST
));*/
//endregion

//region Какой то переключатель =)
/*if($isBitrix24Template) { $this->SetViewTarget('inside_pagetitle', 100); }
$APPLICATION->IncludeComponent( 'bitrix:crm.entity.list.switcher', '', array(
    'ENTITY_TYPE' => \CCrmOwnerType::Deal,
    'NAVIGATION_ITEMS' => array(
        array(
            'id' => 'list',
            'name' => GetMessage('CRM_DEAL_LIST_SWITCHER_LIST'),
            'active' =>$arResult['IS_RECURRING'] !== 'Y',
            'url' =>  $categoryID < 0
                ? $arResult['PATH_TO_DEAL_LIST']
                : CComponentEngine::makePathFromTemplate(
                    $arResult['PATH_TO_DEAL_CATEGORY'],
                    array('category_id' => $categoryID)
                )
        ),
        array(
            'id' => 'recur',
            'name' => GetMessage('CRM_DEAL_LIST_SWITCHER_RECUR'),
            'active' => $arResult['IS_RECURRING'] === 'Y',
            'url' =>  $categoryID < 0
                ? $arResult['PATH_TO_DEAL_RECUR']
                : CComponentEngine::makePathFromTemplate(
                    $arResult['PATH_TO_DEAL_RECUR_CATEGORY'],
                    array('category_id' => $categoryID)
                )
        )
    )
), $component );*/
//endregion

//region Автонастриваемый грид
$APPLICATION->IncludeComponent(
    $arResult['nameSpace'].':entity.list', '', array(
        'namespaceAndClassNameEntity' => $arResult['namespaceAndClassNameEntity'],
        'nameSpace' => $arResult['nameSpace'],
        'URL_TEMPLATES' => $arResult['URL_TEMPLATES'],

        'menuCounter' => $arResult['menuCounter'],
        'mainNameSpace' => $arResult['mainNameSpace'],
        'listGroupEntity' => $arResult['listGroupEntity'],
        'ToAjax' => array(
            'menuCounter' => $arResult['menuCounter'],
            'mainNameSpace' => $arResult['mainNameSpace'],
            'listGroupEntity' => $arResult['listGroupEntity'],
        )

    ), $this->getComponent(), array('HIDE_ICONS' => 'Y')
);
//endregion