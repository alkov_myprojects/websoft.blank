<?php
defined('B_PROLOG_INCLUDED') || die;

use Bitrix\Main\Localization\Loc;

/** @var CBitrixComponentTemplate $this */

global $APPLICATION; $APPLICATION->ShowCSS(); $APPLICATION->ShowHeadScripts(); $APPLICATION->ShowHeadStrings();


CJSCore::Init(array("jquery","date"));
$date = date_create();
$date = date_format($date, 'd.m.Y');

use \Websoft\Booking\Entity\NegotiatedTable as Negotiated;
use \Websoft\Booking\Entity\CompanyBookingTable as CompanyBooking;
use \Websoft\Booking\Entity\ServiceTypesTable as ServiceTypes;
use \Websoft\Booking\Entity\BookingTypeTable as BookingType;

//... Получить все корректные переговорные
$listCorrectNeg = Negotiated::GetCorrectElements();
//...... Ивзлечь из них компании
$idsCompany = array();
foreach ($listCorrectNeg as $neg) { $idsCompany[] = $neg['COMPANY_ID']; }
$listCompany = CompanyBooking::getListPure(true,array('ID'=>$idsCompany),array('ID','NAME','UF_LOGO'));

// Определение текущего пользователя как Организатора
global $USER;
$UserCompanyID = $arResult['COMPANY_ID_THIS_USER'];
?>

<table>
    <tr>
        <td style="vertical-align: top;">
            <form name="BOOKING_CARD" action="">

                <h2>Системнные данные (скрыть при интеграции вёрстки):</h2>
                ID Компании Организатора (тот кто создаёт бронь), из настроек модуля, если нет, то появляется
                модальное окно с вопросом "Из какой вы компании?": <input name="EXECUTOR_COMPANY_ID" type="text" value="<?=$UserCompanyID?>"><br>
                USER_ID Организатора (тот кто создаёт бронь): <input name="EXECUTOR_BY_ID" type="text" value="<?=$USER->GetID()?>"><br>

                <h2>Компании:</h2>
                <? foreach ($listCompany as $company): ?>
                    <?$logo = $company['UF_LOGO'];$logo = CFile::GetFileArray($logo);$logo = $logo['SRC'];?>
                    <input type="checkbox" id="UF_COMPANIES_ID_<?=$company['ID'];?>" name="UF_COMPANIES_ID[]" value="<?=$company['ID'];?>" />
                    <label for="UF_COMPANIES_ID_<?=$company['ID'];?>"><?=$company['NAME'];?></label>
                <? endforeach; ?>
                <hr>

                <h2>Дата начала и окончания мероприятия:</h2>
                <div class="date">
                    Дата: <input type="text" value="<?=$date;?>"
                                 name="DATE_BEGIN" onclick="BX.calendar({node: this, field: this, bTime: false});">
                    <br>
                    Время начала: <input
                            type="time" id="appt-time" name="TIME_START" min="9:00" max="18:00" required value="12:00"/>
                    <br>
                    Время окончания: <input
                            type="time" id="appt-time" name="TIME_END" min="9:00" max="18:00" required value="14:00" />
                </div>
                <hr>

                <b>Кол-во участников:</b> <input type="number" id="COUNT" name="COUNT" value="5" step="1" min="0" max="20" />
                <hr>

                <b>Вид бронирования:</b>
                <? $BookingType = BookingType::getListPure(
                    true,array('ACTIVE'=>true),array('ID','NAME','DESCRIPTION','UF_SERVICE_TYPES')
                ); ?>
                <? foreach ($BookingType as $bookingType): ?>
                    <input onclick="setBookingType(<?=$bookingType['ID'];?>)"
                           type="radio" id="BOOKING_TYPE_ID_<?=$bookingType['ID'];?>" name="BOOKING_TYPE_ID"
                           value="<?=$bookingType['ID'];?>" />
                    <label for="BOOKING_TYPE_ID_<?=$bookingType['ID'];?>"><?=$bookingType['NAME'];?></label>
                <? endforeach; ?>
                <input onclick="setBookingType('')" type="radio" id="BOOKING_TYPE_ID_0" name="BOOKING_TYPE_ID" />
                <label for="BOOKING_TYPE_ID_0">Другое</label>
                <script>
                    BookingType = <?=json_encode($BookingType)?>;
                    function setBookingType(ID) {
                        if(ID) {
                            for(var index in BookingType) {
                                if(BookingType[index].ID == ID) {
                                    // Установить описание
                                    DESCRIPTION = BookingType[index].DESCRIPTION;
                                    $('textarea[name="DESCRIPTION"]').val(DESCRIPTION);
                                    // Установить сервисы сопровождения
                                    $('input[name="UF_SERVICE_TYPES_ID[]"]').attr("checked",false);
                                    UF_SERVICE_TYPES = BookingType[index].UF_SERVICE_TYPES;
                                    if(UF_SERVICE_TYPES.length > 0) {
                                        for (i=0;i<UF_SERVICE_TYPES.length;i++) {
                                            $('input[id="SERVICE_TYPE_' + UF_SERVICE_TYPES[i] + '"]').attr("checked",true);
                                        }
                                    }
                                }
                            }
                        } else { $('textarea[name="DESCRIPTION"]').val(''); }
                    }
                </script>
                <hr>

                <b>Описание:</b>
                <textarea name="DESCRIPTION" id="" cols="30" rows="10" placeholder="Введите описание..."></textarea>
                <hr>

                <b>Сервисы сопровождения:</b>
                <? $ServiceTypes = ServiceTypes::getListPure(true,array('ACTIVE'=>true),array('ID','NAME')); ?>
                <? foreach ($ServiceTypes as $serviceType): ?>
                    <input type="checkbox" id="SERVICE_TYPE_<?=$serviceType['ID'];?>" name="UF_SERVICE_TYPES_ID[]"
                           value="<?=$serviceType['ID'];?>" />
                    <label for="SERVICE_TYPE_<?=$serviceType['ID'];?>"><?=$serviceType['NAME'];?></label>
                <? endforeach; ?>
                <hr>

                <b>Дополниетельные настройки:</b><br><br>
                Повторяемость: <select name="EVENT_RRULE[FREQ]">
                    <option value="NONE" >Не повторяется</option>
                    <option value="DAILY"   data-first="кадый" data-last="день">Каждый день</option>
                    <option value="WEEKLY"  data-first="кадую" data-last="неделю">Каждую неделю</option>
                    <option value="MONTHLY" data-first="кадый" data-last="месяц">Каждый месяц</option>
                    <option value="YEARLY"  data-first="кадый" data-last="год">Каждый год</option>
                </select>
                <script>
                    $('select[name="EVENT_RRULE[FREQ]"]').on('change', function() {
                        var value = $(this).val();
                        if(value == 'NONE') {
                            $('div#INTERVAL').css('display','none');
                            $('div#COUNT').css('display','none');
                        }
                        else {
                            var data_first = $(this).children(":selected").attr('data-first');
                            var data_last = $(this).children(":selected").attr('data-last');
                            $('div#INTERVAL span#FIRST').html(data_first);
                            $('div#INTERVAL span#LAST').html(data_last);
                            $('div#INTERVAL').css('display','inline-block');
                            $('div#COUNT').css('display','block');
                        }

                        if(value == 'WEEKLY') { $('div#BYDAY').css('display','block'); }
                        else { $('div#BYDAY').css('display','none'); }
                    });
                </script>
                <div id="INTERVAL" style="display:none;">
                    <span id="FIRST"></span>
                    <select name="EVENT_RRULE[INTERVAL]">
                        <? $days = 35; for ($i=1;$i<=$days;$i++):?>
                            <option value="<?=$i?>"><?=$i?></option>
                        <? endfor; ?>
                    </select>
                    <span id="LAST"></span>
                </div>
                <div id="BYDAY" style="display:none;">
                    <label><input name="EVENT_RRULE[BYDAY][]" type="checkbox" value="MO">Пн</label>
                    <label><input name="EVENT_RRULE[BYDAY][]" type="checkbox" value="TU">Вт</label>
                    <label><input name="EVENT_RRULE[BYDAY][]" type="checkbox" value="WE">Ср</label>
                    <label><input name="EVENT_RRULE[BYDAY][]" type="checkbox" value="TH">Чт</label>
                    <label><input name="EVENT_RRULE[BYDAY][]" type="checkbox" value="FR">Пт</label>
                    <label><input name="EVENT_RRULE[BYDAY][]" type="checkbox" value="SA">Сб</label>
                    <label><input name="EVENT_RRULE[BYDAY][]" type="checkbox" value="SU">Вс</label>
                </div>
                <div id="COUNT" style="display:none;">

                    Окончание:<br>

                    <label for="never">
                        <input name="rrule_endson" id="never" type="radio"  value="never" checked="checked">
                        Никогда
                    </label>
                    <br>

                    <label for="endson_count">
                        <input id="endson_count" name="rrule_endson" type="radio" value="count">
                        После <input type="text" name="EVENT_RRULE[COUNT]" placeholder="10" value="10"> повторений
                    </label>
                    <br>

                    <label for="rrule_endson">
                        <input id="rrule_endson" name="rrule_endson" type="radio" value="until">
                        <input name="EVENT_RRULE[UNTIL]" type="text" placeholder="дата" onclick="BX.calendar({node: this, field: this, bTime: false});">
                    </label>

                </div>

                <br>
                <br>

                <label>
                    <input name="NOTIFY" type="checkbox" value=""> Уведомлять участников
                </label>
                <br>
                <br>

                Уведомить за: <br>
                <? $listTime = array( // Тут минуты
                    'В момент события' => 0,
                    'За 5 минут до события' => 5,
                    'За 10 минут до события' => 10,
                    'За 30 минут до события' => 30,
                    'За 1 час до события' => 60,
                    'За 2 часа до события' => 60,
                    'За день до события' => (60*24),
                    'За 2 дня до события' => ((60*24)*2),
                ); ?>
                <? foreach ($listTime as $decsription => $time): ?>
                    <label><input type="checkbox" name="NOTIFY_TIME[]" value="<?=$time?>"/><?=$decsription?></label><br>
                <? endforeach; ?>
                <br>

                <label type="text"><input name="PRIVATE_EVENT" type="checkbox" value="">Частное событие</label>

                <div id="PARTICIPANTS" style="display:none;">
                    <br><hr><div class="listGroups"></div>
                </div>

                <hr>
                <button onclick="searchNeg(); return false;">Тестовый поиск доступных переговорных</button>
                <button style="margin-left: 10px;" onclick="reservation(); return false;">Забронировать</button>
                <button style="margin-left: 10px;" onclick="redirect('<?=$arResult['URL_TEMPLATES']['MAIN']?>');  return false;">Отмена</button>

            </form>
        </td>
        <td style="vertical-align: top;">
            <h2>Доступные переговорные:</h2>
            <div id="DIV_SELECT_NEGS" style="display: none">
                <hr>
                <form id="SELECT_NEGS">
                    <b>Выбранные переговорные:</b><br>
                    <div class="Interactive">
                        <!--<div data-id="5" class="Negotiated">
                            <div class="del_select_neg" onclick="del_select_neg(5);"></div>
                            <img src="/upload/uf/f48/4-720x450.jpg" height="50" alt=""><hr>Переговорная 1
                            <input type="hidden" name="SELECT_NEG[]" value="5">
                        </div>

                        <div data-id="6" class="Negotiated">
                            <div class="del_select_neg" onclick="del_select_neg(6);"></div>
                            <img src="/upload/uf/f48/4-720x450.jpg" height="50" alt=""><hr>Переговорная 1
                            <input type="hidden" name="SELECT_NEG[]" value="6">
                        </div>-->
                    </div>
                </form>
                <hr>
                <br>
            </div>
            <div id="result_search_neg">
                Поиск по переговорным ещё не запускался...
            </div>
        </td>
    </tr>
</table>

<script>
    /** Забронировать перговорную(ые) */
    function reservation() {

        // Сбор данных:
        // ... Получить переговорные, которые пользователь решил забронировать
        listIdNegs = [];
        $('#SELECT_NEGS .Interactive .Negotiated').each(function( index, element ) {
            id = $( element ).attr('data-id');
            listIdNegs.push(id);
        });
        // ... Получить компании которые выбрал пользователь (для проверки)
        UF_COMPANIES_ID = [];//$('input[name="UF_COMPANIES_ID[]"]').toJSON();
        $('input[name^="UF_COMPANIES_ID"]:checked').each(function() { UF_COMPANIES_ID.push($(this).val()); });

        //... Получить все данные с фильтра
        getParams = $('form[name="BOOKING_CARD"]').serialize();

        // Валидация данных
        if(listIdNegs.length > 0) {
            // Проверить выбранные компании
            if(UF_COMPANIES_ID.length > listIdNegs.length) {
                // Если компаний больше, чем переговорных, то пользователь не знает какую переговорную выбрать.
                // нужно уведомить администратора компании, чтобы он установил переговорную
                var PopupBX = new BX.MyPopupBX();
                PopupBX.AddBtn('Принимаю',function () {
                    PopupBX.CloseLastPopup();
                    _reservation(getParams,listIdNegs);
                },true);
                PopupBX.Message(
                    'not_full_data','Не полноценные данные',
                    'Не для всех компаний были назначены переговорные.<br><br>' +
                    'Бронирование будет на рассмотрение администратора. После того как ' +
                    'администратор назначит оставшиеся переговорные, бронь вступит в полную силу.<br><br>' +
                    'На момент рассмотрения брони администратором, выбранные переговорные будут зарезервированы ' +
                    'за вами, т.е. их ни кто не займёт.<br><br>' +
                    'Если вы согласны на данные условия, то нажмите "Принять", иначе "Закрыть".',
                    true
                );

            } else { _reservation(getParams,listIdNegs); }
        } else { alert('Вы не выбрали ни одну переговорную'); }
    }
    function _reservation(getParams,listIdNegs) {
        var PopupBX = new BX.MyPopupBX();
        PopupBX.Message('wait_reservation','','Создание бронирования, ожидайте....');
        BX.MyAjax.Get2('booking/addBookingCard.php?' + getParams,{listIdNegs:listIdNegs},'html',function (data) {
            PopupBX.CloseLastPopup();

            console.log('data');
            console.log(data);

            // Определить наличие ошибок

            // Если всё хорошо, то вывести сообщение об успехе и дать выбрать:
            //   1) Перейти на главную
            //   2) Перейти на карточку бронирования

        });
    }
    function select_neg(ID_NEG,SRC_IMG,NAME,obj) {
        /** Выбор переговорной */

        html = '<div data-id="'+ID_NEG+'" class="Negotiated">' +
               '   <div class="del_select_neg" onclick="del_select_neg('+ID_NEG+');"></div>' +
               '    <img src="'+SRC_IMG+'" height="50" alt=""><hr>' + NAME +
               '    <input type="hidden" name="SELECT_NEG[]" value="'+ID_NEG+'">' +
               '</div>';

        htmlOld = $('form#SELECT_NEGS div.Interactive').html();
        html = htmlOld + html;
        $('form#SELECT_NEGS div.Interactive').html(html);

        // Проверить кол-во выбранных переговорных, если = 0, то скрыть блок выбранных переговорных
        $("div#DIV_SELECT_NEGS").css('display','block');

        /** Генерация группы пользователей которая будет в этой переговорной */
        html2 = '<div class="group" data-id="'+ID_NEG+'">' +
                '   Участники "<span class="name_neg">'+NAME+'</span>":' +
                '   <input type="number" name="UF_PARTICIPANTS['+ID_NEG+'][]" value="">' +
                '   <input type="number" name="UF_PARTICIPANTS['+ID_NEG+'][]" value="">' +
                '   <input type="number" name="UF_PARTICIPANTS['+ID_NEG+'][]" value="">' +
                '</div>';

        html2_Old = $("#PARTICIPANTS .listGroups").html();
        html2 = html2_Old + html2;
        $("#PARTICIPANTS .listGroups").html(html2);
        $("#PARTICIPANTS").css('display','block');

        // Удалить переговорную, которую выборали
        $('.test_block_neg_' + ID_NEG).remove();
        searchNeg();
    }
    function del_select_neg(ID_NEG) {
        $("form#SELECT_NEGS").find('.Negotiated[data-id="'+ID_NEG+'"]').remove();
        // Проверить кол-во выбранных переговорных, если = 0, то скрыть блок выбранных переговорных
        countSelectNeg = $("form#SELECT_NEGS").find('.Negotiated').length;
        if(countSelectNeg === 0) { $("div#DIV_SELECT_NEGS").css('display','none'); }

        $('.group[data-id='+ID_NEG+']').remove();
        countGroupPartNeg = $("#PARTICIPANTS .listGroups").find('.group').length;
        if(countGroupPartNeg === 0) { $("#PARTICIPANTS").css('display','none'); }

        searchNeg();
    }
    function redirect(page) { window.location = page; }
    function searchNeg() {
        getParams = $('form[name="BOOKING_CARD"]').serialize();
        BX.MyAjax.Get2('booking/masterGetNegs.php?' + getParams,{},'html',function (data) {
            $('div#result_search_neg').html(data);
        });
    }
    BX.ready(function() {
        // системаные скрипты
        UserCompanyID = '<?=$UserCompanyID?>';
        if (!UserCompanyID) {
            BX.MyAjax.Get('booking/GetCompaniesHTML', {}, '', function (data) {
                var PopupBX = new BX.MyPopupBX();
                PopupBX.AddBtn('Сохранить',function () {
                    form = $(this.popupWindow.contentContainer).find('form').serializeArray();
                    if(form.length > 0) {
                        UF_COMPANIES_ID = form[0]['value'];
                        BX.MyAjax.Get('booking/SnapUserToCompany', {
                            'UF_COMPANIES_ID': UF_COMPANIES_ID, 'USER_ID': <?=$USER->GetID()?>
                        });
                        $('input[name="EXECUTOR_COMPANY_ID"]').val(UF_COMPANIES_ID);
                        PopupBX.CloseLastPopup();
                    } else { alert('Вы не выбрали компанию'); }
                },true);
                PopupBX.Message('what_company_do_you_belong_to','К какой компании вы принадлежите?',data);
            });
        }
    });
</script>

<style>
    .Company, .Negotiated {
        padding: 10px 5px; border: 2px solid; cursor: pointer; display: inline-block; margin: 10px; user-select: none;
        text-align: center; position: relative;
    }
    .Company.active, .Negotiated.active { border-color: red; }
    .Interactive .del_select_neg {
        position: absolute; top: -5px; right: -5px; width: 20px; height: 20px; background: grey;border: 1px solid;
        border-radius: 10px; line-height: 18px; cursor: pointer;
    }
    .Interactive .del_select_neg:hover { background: red; }
    .Interactive .del_select_neg:before { content: "X"; }
</style>