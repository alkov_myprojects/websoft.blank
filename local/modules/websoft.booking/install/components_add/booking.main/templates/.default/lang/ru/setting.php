<?php
defined('B_PROLOG_INCLUDED') || die;
$MESS['TITLE'] = 'Настройка';

$MESS['MAIN'] = 'Главная';
$MESS['SETTING'] = 'Системные настройки';
$MESS['HANDBOOK'] = 'Справочники';
$MESS['REPORT'] = 'Отчёты';