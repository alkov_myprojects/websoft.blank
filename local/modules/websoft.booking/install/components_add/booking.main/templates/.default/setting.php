<?php
defined('B_PROLOG_INCLUDED') || die;

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\UI\Extension;

Extension::load('ui.buttons');
Extension::load('ui.buttons.icons');

/** @var CBitrixComponentTemplate $this */


global $APPLICATION;
$dir = $APPLICATION->GetCurDir();

/*
?>
<script> function redirect(page) { window.location = page; } </script>
<style> .ui-btn-main.active { background-color: var(--ui-btn-bg-primary-hover) !important; color: #fff; } </style>
<? ob_start(); ?>
<div class="ui-btn-double ui-btn-primary">
    <?
    $menu = array('MAIN','SETTING','HANDBOOK','REPORT');
    foreach ($menu as $one_menu) {
        $class_active = '';
        if(strstr($dir,strtolower($one_menu))) { $class_active = 'active'; }
        ?>
        <button id="toolbar_lead_details_6_convert_label"
                onclick="redirect('<?=$arResult['URL_TEMPLATES'][$one_menu]?>');"
                class="ui-btn-main <?=$class_active?>"><?=Loc::getMessage($one_menu)?></button>
    <? } ?>
</div>
<?
//sidebar
//sidebar_tools_1
//sidebar_tools_2
//below_pagetitle *
// inside_pagetitle
$APPLICATION->AddViewContent('above_pagetitle', ob_get_contents(),0); ob_end_clean();
*/
?>

<?

$APPLICATION->SetAdditionalCSS("/bitrix/js/crm/css/crm.css");
$isBitrix24 = SITE_TEMPLATE_ID === "bitrix24";
if ($isBitrix24) { $this->SetViewTarget("above_pagetitle"); }
$ITEMS = array(
    /*array(
        'TEXT' => 'Текст менюхи',
        'URL' => '/booking/handbook/',
        'CLASS' => 'crm-menu- crm-menu-item-wrap',
        'CLASS_SUBMENU_ITEM' => 'crm-menu-more-',
        'ID' => 'menu_crm_test',
        'SUB_LINK' => '',
        'COUNTER' => '',
        'COUNTER_ID' => '',
        'IS_ACTIVE' => '1',
        'IS_LOCKED' => '',
        'IS_DISABLED' => '',
    )*/
);

$menu = array('MAIN','SETTING','HANDBOOK','REPORT');
foreach ($menu as $one_menu) {
    $class_active = '';
    if(strstr($dir,strtolower($one_menu))) {
        $APPLICATION->SetTitle(Loc::getMessage($one_menu));
        $class_active = '1';
    }
    $ITEMS[] = array(
        'TEXT' => Loc::getMessage($one_menu),
        'URL' => $arResult['URL_TEMPLATES'][$one_menu],
        'CLASS' => 'crm-menu- crm-menu-item-wrap',
        'CLASS_SUBMENU_ITEM' => 'crm-menu-more-',
        'ID' => 'menu_crm_'.$one_menu,
        'SUB_LINK' => '',
        'COUNTER' => $arResult['menuCounter'][$one_menu],
        'COUNTER_ID' => '',
        'IS_ACTIVE' => $class_active,
        'IS_LOCKED' => '',
        'IS_DISABLED' => '',
    );
}
$APPLICATION->IncludeComponent( "bitrix:main.interface.buttons", "", array( "ID" => 'TEST_PANEL', "ITEMS" => $ITEMS ) );
if ($isBitrix24) { $this->EndViewTarget("sidebar"); }

?>

<?
$APPLICATION->IncludeComponent(
    'websoft.booking:entity', '', array(
        'MODE' => 'Y',
        'FOLDER' => '/booking/setting/',
        'URL_TEMPLATES' => array(
            'list' => '#ENTITY_UF_ID#/',
            'edit' => '#ENTITY_UF_ID#/#ID#/edit/',
        ),
        'ListEntity' => array(
            '\Websoft\Booking\Entity\UsersTable',            // Пользователи
            '\Websoft\Booking\Entity\BookingCardTable',      // Карточки бронирования
            '\Websoft\Booking\Entity\BookingCardChildTable', // Дочерние карточки бронирования
        ),
        'ToAjax' => array(
            'menuCounter' => $arResult['menuCounter'],
            'mainNameSpace' => $arResult['mainNameSpace'],
            'listGroupEntity' => $arResult['listGroupEntity'],
        )
    ), false
);
?>
