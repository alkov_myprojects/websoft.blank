<?
require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
if(CModule::IncludeModule("calendar")) {

    $CBookingMainComponent = CBitrixComponent::includeComponentClass("websoft.booking:booking.main");
    $CBookingMainComponent = '\CBookingMainComponent';
    $dateIntervals = $CBookingMainComponent::dateIntervals();
    
    // Получить переговрные (дата,компании,переговорны)
    $date = isset($_REQUEST['DATE']) ? $_REQUEST['DATE'] : date_format(date_create(), 'd.m.Y');
    $companies = isset($_REQUEST['COMPANIES_ID']) ? $_REQUEST['COMPANIES_ID'] : array();
    $negs = isset($_REQUEST['NEGOTIATED_ID']) ? $_REQUEST['NEGOTIATED_ID'] : array();
    $date_time_start = isset($_REQUEST['DATE_TIME_START']) ? $_REQUEST['DATE_TIME_START'] :
        date_format(date_create($date), 'Y-m-d').' '.$dateIntervals['Date_time_start'];

    $date_time_end = isset($_REQUEST['DATE_TIME_START']) ? $_REQUEST['DATE_TIME_START'] :
        date_format(date_create($date), 'Y-m-d').' '.$dateIntervals['Date_time_end'];


    // Собрать данные расписания по переговорным
    $arNegotiated = $CBookingMainComponent::getNegotiated($companies);
    $begin = new \DateTime($date_time_start);
    $end = new \DateTime($date_time_end);
    //$end->modify('+1 day');
    $interval = new \DateInterval('PT'.$dateIntervals['DateInterval_Min'].'M');
    $daterange = new \DatePeriod($begin, $interval, $end);
    foreach ($arNegotiated as &$neg) {
        $Interpreter = \Websoft\Booking\Entity\NegotiatedTable::GetInterpreter($neg['ID'], $date, $daterange);
        $neg['ITEMS'] = $Interpreter;
    }

    // Распечатать результат
    $result = array();
    if (empty($negs)) {
        foreach ($arNegotiated as $neg_2) {
            if(!$_REQUEST['SEARCH_STR']) {  $result[] = $neg_2; }
            else if(strstr(mb_strtolower($neg_2['ADDRESS']),mb_strtolower ($_REQUEST['SEARCH_STR']))) { $result[] = $neg_2; }
        }
    }
    else { foreach ($arNegotiated as $neg_2) { if (in_array($neg_2['ID'], $negs)) {
        if(!$_REQUEST['SEARCH_STR']) {  $result[] = $neg_2; }
        else if(strstr(mb_strtolower($neg_2['ADDRESS']),mb_strtolower ($_REQUEST['SEARCH_STR']))) { $result[] = $neg_2; }
    } } }

    echo json_encode($result);
    //echo json_encode($arNegotiated);
    //echo json_encode(array());
}
?>