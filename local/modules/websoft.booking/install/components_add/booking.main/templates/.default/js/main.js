function chunkArray(myArray, chunk_size){
    var index = 0;
    var arrayLength = myArray.length;
    var tempArray = [];
    for (index = 0; index < arrayLength; index += chunk_size) {
        myChunk = myArray.slice(index, index+chunk_size);
        tempArray.push(myChunk);
    }
    return tempArray;
}

window.onscroll = function() {
    var scrolled = window.pageYOffset || document.documentElement.scrollTop;
    $('.time_slider .slick-arrow').css('top',scrolled/2 + window.innerHeight/2);
};

function initBooking(settings) {

    var bookingDateRange = { // диапозон слева
        props: ['range'],
        data: function () { return {} },
        template: '<div class="aside_time">' +
                    '<div class="aside_time_title">Выбрать время</div>' +
                    '<transition-group name="daterange">'+
                        '<div class="time_laps" v-for="item,index in range" v-bind:key="index" >' +
                            '<div class="time_item">' +
                                '<span>{{item}}</span>' +
                            '</div>' +
                        '</div>' +
                    '</transition-group>'+
                '</div>'
    };
    var bookingTimeWrapTitle = { // заголовок переговорной
        props: ['item'],
        data: function () {
            return {
                //companies: arCompanies,
            }
        },
        template:
            '<div class="time_title">' +
                '<p>{{item.NAME}} <br> <span style="font-family: \'Gotham-Regular\'; font-size: 10px;">{{item.ADDRESS}}</span> </p>' +
                '<div class="icons_wrap">' +
                    '<a href="#" v-for="service in services[0]"><img :src="service.UF_ICON" width="16" :title="service.NAME"></a>' +
                '</div>' +
            '<span class="more_info" v-if="services[1]"></span>'+
            '<div class="more_info_dropdown">'+
                '<span class="more_info"></span>'+
                '<div class="inner_info_wrap">'+
                    '<a href="#" v-for="service in services[1]"><img :src="service.UF_ICON" width="16" :alt="service.NAME"></a>' +
                '</div>'+
            '</div>'+
            '</div>',
        computed: { services: function(){ return chunkArray(this.item.SERVICE,6); } },
    };
    var bookingBlockOrder = { // заголовок переговорной
        props: ['block'],
        data: function () {
            return {
                //companies: arCompanies,
            }
        },
        template: '<div class="order active">' +
                    '<div :class="[{ last: index == items.length-1 }, \'time_main_item active\']" v-for="item, index in items">' +
                        '<img v-if="item.SRC" width="16px" :src="item.SRC">'+
                        '<span :class="item.CLASS" v-if="item.TEXT">{{item.TEXT}}</span>' +
                    '</div>' +
                    '<span class="more_info" @click="showInfo($event)"></span>' +
                    '<div class="more_info_dropdown" style="display: none;">'+
                    '<span class="more_info" @click="showInfo($event)"></span>'+

                        //region Информация
                        '<div class="inner_info_wrap active">'+
                            '<p class="" @click="showDetail($event)">Информация <span class="more_click">-</span></p>'+
                            '<div class="inner_info active">' +

                                '<div class="inner_item">' +
                                    '<span>Организатор:</span>' +
                                    '<b>{{block.INFORMATION.EXECUTOR.FIO}}</b>' +
                                '</div>' +

                                '<div class="inner_item">' +
                                    '<span>Телефон организатора:</span>' +
                                    '<b>{{block.INFORMATION.EXECUTOR.PERSONAL_PHONE}}</b>' +
                                '</div>' +

                                '<div class="inner_item">'+
                                    '<span>Количество участников:</span>'+
                                    '<b> {{block.INFORMATION.COUNT_OWNERS}}</b>'+
                                '</div>'+
                                '<div class="inner_item" v-if="block.INFORMATION.BREAKS.length > 0">'+
                                    '<span class="item_full">Перерывы:</span>'+
                                    '<div v-for="break_info in block.INFORMATION.BREAKS" style="height: 13px"><b>{{break_info.NAME}} с {{break_info.START}} до {{break_info.END}}</b></div>'+
                                '</div>'+
                                '<div class="inner_item">'+
                                    '<span class="item_full">Ответственный за сопровождение:</span>'+
                                    '<b v-for="services in block.INFORMATION.SERVICES">{{services.NAME}} - {{services.ASSIGNED_BY.FIO}} ({{services.ASSIGNED_BY.PERSONAL_PHONE}})</b>'+
                            ' </div>'+
                        ' </div>'+
                        '</div>'+
                        //endregion

                        //region Изменить
                        '<div class="inner_info_wrap">'+
                            '<a :href="block.LINK_EDIT"><p>Изменить</p></a>'+                                    
                        '</div>'+
                        //endregion

                        //region Просмотреть
                        '<div class="inner_info_wrap">'+
                            '<a :href="block.LINK_VIEW"><p>Просмотреть</p></a>'+                                    
                        '</div>'+
                        //endregion

                        //region Аннулировать
                        ' <div class="inner_info_wrap">'+
                                '<p @click="CancelBooking($event)">Аннулировать</p>   '+
                        ' </div>'+
                        //endregion

                ' </div>'+
                '</div>',
        computed: {
            items: function () {
                var res = []
                for (let index = 1; index <= this.block.COUNT_TIME_SECTION; index++) {
                    if (index == 1) {
                        res.push({
                            TEXT: this.block.TIME_START + ' - ' + this.block.TIME_END,
                            CLASS:['time_ic']
                        })
                    } else if (index == 2) {
                        res.push({
                            TEXT: this.block.BOOKING_TYPE.NAME,
                            SRC: this.block.BOOKING_TYPE.SRC_ICON,
                            CLASS:['booking_type']
                        })
                    } else {
                        res.push({
                            TEXT: false
                        })
                    }
                }
                return res;
            }
        },
        methods:{
            CancelBooking:function (e) {
                ID_MAIN_BOOKING_CARD = this.$options.propsData.block.ID_MAIN_BOOKING_CARD;

                var PopupBX = new BX.MyPopupBX();
                PopupBX.Message('cancel_booking','','Аннулирование бронирования, ожидайте...');
                $.ajax({
                    url: '/bitrix/ajax/websoft.booking/booking/CancelBooking.php',
                    dataType: "html", // "html","json"
                    data: {'ID':ID_MAIN_BOOKING_CARD},
                    success: function(data) {
                        PopupBX.CloseLastPopup();
                        var PopupBX_2 = new BX.MyPopupBX();
                        PopupBX_2.AddBtn('Закрыть',function(){ document.location.href = "/booking/"; /*PopupBX_2.CloseLastPopup();*/ },true);
                        PopupBX_2.Message('result_cancel_booking','Результат аннулирования',data);
                    }
                });
                /*BX.MyAjax.Get('booking/CancelBooking', {'ID':ID_MAIN_BOOKING_CARD}, 'html', function (data) {
                    PopupBX.CloseLastPopup();
                    var PopupBX_2 = new BX.MyPopupBX();
                    PopupBX_2.AddBtn('Закрыть',function(){ document.location.href = "/booking/"; /!*PopupBX_2.CloseLastPopup();*!/ },true);
                    PopupBX_2.Message('result_cancel_booking','Результат аннулирования',data['data']);
                });*/
            },
            showInfo: function(e){
                console.log(e);
                
                var parent = $(e.target).closest('.order');
                parent.find('.more_info_dropdown').slideToggle();
                //parent.toggleClass("active").find('.more_info_dropdown').slideToggle();
                // $(".order.active").not(parent).find('.more_info_dropdown').removeClass("active").slideUp();
            },
            showDetail: function (e, text) {
                //$(e.target).closest('.inner_info_wrap').toggleClass("active");
                /*if(text){
                    if ($(e.target).text() == "+") { $(this).text("-"); }
                    else { $(e.target).text("+"); }
                }*/
                //$(e.target).toggleClass("active");
                //var parent = $(e.target).closest('.order');
                //parent.find('.more_info_dropdown').toggleClass("active");
                //parent.toggleClass("active").find('.inner_info').slideToggle().toggleClass('active');
            }
        }
    }
    var bookingBlockEmpty = { // заголовок переговорной
        props: ['block'],
        data: function () {
            return {
                //companies: arCompanies,
            }
        },
        template: '<div class="time_main_item booking">' +
                    '<div class="booking_txt" @click="openLink()">Забронировать</div></a>' +
                '</div>',
        methods:{
            openLink: function(){
                var params = {
                    DATE: this.block.DATE,
                    ID_NEG: this.block.ID_NEG
                }
                var url = booking_main.params.URL_TEMPLATES.master+'?';
                // location(url+$.param(params))
                window.open(
                    url+$.param(params),
                '_self' // <- This is what makes it open in a new window.
                );
                console.log(url+$param(params))
            }
        }
    }
    var bookingBlock = { 
        props: ['block'],
        data: function () {
            return {
                //companies: arCompanies,
            }
        },
        template: '<div>' +
            '<booking-block-order :block="block.DATA" v-if="block.TYPE == \'EVENT\'"></booking-block-order>' +
            '<booking-block-empty :block="block.DATA" v-if="block.TYPE == \'EMPTY\'"></booking-block-empty>' +
            '</div>',
        components: {
            'booking-block-order': bookingBlockOrder,
            'booking-block-empty': bookingBlockEmpty
        }
    }
    var bookingTimeWrap = {
        props: ['negotiated'],
        data: function () {
            return {
                items: this.negotiated,
            }
        },
        template: '<div class="time_slider"><div class="time_wrap" v-for="item in negotiated">' +
            '<bookingwrap-title :item="item"></bookingwrap-title>' +
            // '<pre>{{item.ITEMS}}</pre>'+
            '<booking-block v-for="block in item.ITEMS" :block="block"></booking-block>' +
            '</div>' +
            // '<pre>{{negotiated}}</pre>' +
            '</div>',
        components: {
            'bookingwrap-title': bookingTimeWrapTitle,
            'booking-block': bookingBlock
        },
        beforeUpdate: function () {
            $('.time_slider').slick('unslick');
        },
        created: function () {},
        mounted: function () {},
        updated: function () {
            $('.time_slider').slick({
                dots: true,
                infinite: false,
                speed: 300,
                slidesToShow: 5,
                slidesToScroll: 1
            });
        },
    }
    var bookingSearchNeg = {
        props: ['negotiated'],
        template: '<div class="choose_dropdown">'+
                    '<div class="items_wrap" v-for="negs in items">'+
                        '<div v-for="item in negs" class="choose_item" @click="selectNegotiated(item, $event)">'+
                            '<div class="item_img">'+
                                '<div class="img_wrap">'+
                                    '<div class="over_lay_img">'+
                                        '<b>{{item.NAME}}</b>'+
                                    '</div>'+
                                    '<a href="javascript::void(0)" class="close_item">x</a>'+
                                    '<img :src="item.UF_PREVIEW_IMAGE.SRC" alt="">'+
                                '</div>'+
                            '</div>'+
                            '<div class="item_descr">'+
                                '<p><b>{{item.NAME}}</b> {{item.ADDRESS}}</p>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                '</div>',
        computed:{
            items: function(){
                return chunkArray(this.negotiated,4);
            }
            
        },
        methods: {

            selectNegotiated: function (negotiated, e) {
                if(!$(e.target).hasClass('choose_item')){
                    console.log($(e.target));
                    $(e.target).closest('.choose_item').toggleClass('active');
                    $(e.target).closest('.choose_item').find($(".item_descr")).slideToggle("slow");
                }else{
                    $(e.target).toggleClass('active');
                    $(e.target).find($(".item_descr")).slideToggle("slow");
                }
                if (negotiated.SELECT) { negotiated.SELECT = false; }
                else { negotiated.SELECT = true; }
                negotiated_id = [];
                for (i=0;i<this.negotiated.length;i++) { if(this.negotiated[i].SELECT) { negotiated_id.push(this.negotiated[i].ID); } }
                this.$parent.selectNegotiated = negotiated_id;
            }
        }
    }
    var bookingCompany = {
        props: ['companies'],
        data: function () {
            return {
                items: this.companies,
            }
        },
        mounted: function() {
            $('.company_slider').slick({
                infinite: false,
                arrows: true,
                slidesToShow: 6,
                slidesToScroll: 1,
                appendArrows: $('.slider_title'),
                responsive: [{
                        breakpoint: 1500,
                        settings: {
                            slidesToShow: 4,
                            slidesToScroll: 4,
                            infinite: true,
                        }
                    },
                    {
                        breakpoint: 1300,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                ]
            });
        },
        template: '<div class="company_slider">' +
            '<div v-for="company in items" :class="[{ active: company.ACTIVE }, \'company_slider-item\']">' +
            '<a href="javascript::void(0)" class="close_slide" @click="selectCompany(company)">x</a>' +
            '<img :src="company.UF_LOGO" alt="">' +
            '<div class="company-details">' +
            '<p>{{company.NAME}}</p>' +
            '<p>Переговоных <b>{{company.COUNT_NEG}}</b></p>' +
            '<p><span class="comp_btn" @click="selectCompany(company)">Выбрать</span></p>' +
            '</div>' +
            '</div>' +
            '</div>',
        methods: {
            selectCompany: function (company, e) {
                if (company.ACTIVE) { company.ACTIVE = false; }
                else { company.ACTIVE = true; }
                company_id = [];
                for (i=0;i<this.items.length;i++) { if(this.items[i].ACTIVE) { company_id.push(this.items[i].ID); } }
                this.$parent.selectCompany = company_id;
                this.$parent.selectNegotiated = [];

                // Перерисовать переговорные, вывесте, только те, которые относятся к этим

            }
        }
    }
    var booking_main = new Vue({
        el: '#booking_main',
        data: settings,
        components: {
            'booking-company': bookingCompany,
            'booking-timewrap': bookingTimeWrap,
            'booking-daterange': bookingDateRange,
            'booking-search-neg': bookingSearchNeg,
            // 'block-fields': blockFields,
            // 'block-cblock': CIBlock,
            // 'block-iblock': Iblock,
            // 'block-filter': dataFilter,
            // 'alert-block':alert
        },
        mounted: function () {
            // alert()
            $("#datepicker").datepicker({
                showOn: "button",
                monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь',
                    'Октябрь', 'Ноябрь', 'Декабрь'
                ],
                dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
                buttonImage: this.path+"img/arr_calend-hove.png",
                buttonImageOnly: true,
                dateFormat: "dd.mm.yy",
                onSelect: function (date, datepicker) {
                    booking_main.date = date;
                },
            });
        },
        watch: {
            date: function (newVal, oldVal) {
                this.updateNegotiated();
            },
            selectCompany: function (newVal, oldVal) {
                this.updateNegotiated('company');                
            },
            selectNegotiated: function (newVal, oldVal) {
                this.updateNegotiated();                
            }
        },
        methods: {
            searchNegByName: function () {
                searchStr = $('#search_input').val();
                this.SEARCH_STR = searchStr;
                this.updateNegotiated();
            },
            updateNegotiated: function (type) {
                var params = {
                    DATE: this.date,
                    COMPANIES_ID: this.selectCompany,
                    NEGOTIATED_ID: this.selectNegotiated,
                    //NEGS_ID: this.selectNegotiated,
                    SEARCH_STR: this.SEARCH_STR
                };
                console.log('params');
                console.log(params);
                var self = this;
                $('#preloader').fadeIn();
                BX.MyAjax.Get('ajax', params, 'json', function (data) {
                    self.NEGOTIATED = data;
                    if(type == 'company') {
                        self.NEGOTIATED_ALL = data;
                    }
                    $('#preloader').fadeOut();
                    self.logNeg();
                })
                // console.log(self.NEGOTIATED);
                // this.NEGOTIATED = self.NEGOTIATED;
            },
            logNeg: function () {

            },
        }
    })

    $('.company_slider-item').click(function () {

        //region Убарть всплывашку выбора у всех
        /*var container = $(".company_slider-item");
        var parent = $(container).parent();
        parent.find('.company-details').slideUp().removeClass("active");
        $(".order.active").not(parent).find('.more_info_dropdown').removeClass("active").slideUp();*/
        /*$(".company_slider-item").parent().find('.company-details').slideUp(function () {
            $(this).removeClass("active");
        });*/
        //endregion

        //region Открыть всплывашку выбора у текущего элемента
        var parent = $(this).parent();
        parent.find('.company-details').slideToggle().toggleClass("active");
        $(".order.active").not(parent).find('.more_info_dropdown').removeClass("active").slideUp();
        //endregion
    });

    $(document).mouseup(function (e) {

        var container = $(".company-details.active");
        setTimeout(function () {
            if (container.has(e.target).length === 0) {
                $(container).slideUp().removeClass("active");
            }
        },100);

        //region кнопка "Выбрать переговорную", закрывать, если открыта и кликнули вне области выбора
        var container_2 = $(".choose_object");
        setTimeout(function () {
            if (container_2.has(e.target).length === 0) {
                $(".choose_object").removeClass('active');
                $(".choose_object .choose_dropdown ").removeClass('active');
            }
        },200);
        //endregion

    });

    $(".ui-datepicker-trigger").on('click', function () {
        var src = $(this).attr('src');
        var newsrc = (src == settings.path+'img/arr_calend-hove.png') ? settings.path+'img/arr_calend_up.png' : settings.path+'img/arr_calend-hove.png';
        $(this).attr('src', newsrc);
    });
    /*$(".search_btn, .choose_object span").on('click', function () {*/
    $(".choose_object span").on('click', function () {
        $('.choose_dropdown').toggleClass('active');
        $('.choose_object').toggleClass('active');

    });

    $('.time_title .more_info').click(function () {
        var parent = $(this).parent();
        parent.addClass("active").find('.more_info_dropdown').slideToggle();
        $(".time_title.active").not(parent).find('.more_info_dropdown').removeClass("active").slideUp();
    });
    $('.bookin .more_info').click(function () {
        var parent = $(this).parent().parent();
        parent.find('.more_info_dropdown').slideToggle().toggleClass("ordering");
        $(".order.active").not(parent).find('.more_info_dropdown').removeClass("active").slideUp();
    });
    $('.close_slide').click(function (event) {
        var parent = $(this).parent();
        parent.removeClass("active");
        parent.find('.company-details').removeClass('active');
        return false;
    });

    $(document).click(function (e) {
        if (!$(e.target).closest(".order.active").length) {
            $(".order.active").removeClass("active").find('.more_info_dropdown').slideUp();
        }
    });
    $(document).click(function (e) {
        if (!$(e.target).closest(".more_info_dropdown").length) { $(".more_info_dropdown").removeClass("active"); }
    })
    $(document).click(function (e) { // событие клика по веб-документу
        var div = $(".inner_info_wrap");
        if (!div.is(e.target) && div.has(e.target).length === 0) { // и не по его дочерним элементам
            //div.removeClass("active");
            //$('.more_click').removeClass('active').text("+");
            //$('p.active').removeClass('active');
            //$('.inner_info').slideUp().removeClass("active"); // скрываем его
        }
    });
    $(document).click(function (e) { // событие клика по веб-документу
        var div2 = $(".bookin"); // тут указываем ID элемента
        if (!div2.is(e.target) && div2.has(e.target).length === 0) { // и не по его дочерним элементам
            div2.find('.more_info_dropdown').slideUp();
            $(".more_info_dropdown").removeClass("ordering");
        }
    });
    $(window).on('resize load', function () {
        var get_width_img = $('.item_img img').width();
        var get_height_img = $('.item_img img').height();
        $('.over_lay_img').css("width", get_width_img);
        $('.over_lay_img').css("height", get_height_img);
    });

    $('.time_slider').slick({
        dots: true,
        infinite: false,
        speed: 300,
        slidesToShow: 5,
        slidesToScroll: 1,
    });

    return booking_main;
};