<?php
defined('B_PROLOG_INCLUDED') || die;

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Grid\Panel\Snippet;
use Bitrix\Main\Loader;
use Bitrix\Main\Page\Asset;
use Bitrix\Main\Web\Json;

/** @var CBitrixComponentTemplate $this */
$APPLICATION->SetTitle(Loc::getMessage('TITLE_BOOKING'));
//Asset::getInstance()->addString('<script src="https://cdn.jsdelivr.net/npm/vue@2.5.17/dist/vue.js"></script>');
Asset::getInstance()->addJs($this->__folder . "/js/vue.js");
Asset::getInstance()->addJs($this->__folder . "/js/main.js");
Asset::getInstance()->addCss($this->__folder . "/css/jquery-ui.css");
Asset::getInstance()->addCss($this->__folder . "/css/slick.css");
Asset::getInstance()->addCss($this->__folder . "/css/style.css");
Asset::getInstance()->addCss($this->__folder . "/css/slick-theme.css");

?>

<style>
    @media all and (-ms-high-contrast:none) {

        /* IE10 */
        /*#pagetitle { color: green !important; }*/

        /* IE11 */
        *::-ms-backdrop, #search_input { right: 295px !important; top: 12px; }
        *::-ms-backdrop, .search_btn { margin-right: -282px; }

    }
</style>

<main id="booking_main">
    <div id="preloader">Загрузка</div>
    <div class="right_side">
        <div class="top">
            <!-- <h1>Бронирование переговорных</h1> -->
            <? ob_start(); ?>
            <?
            $acces = true;
            global $USER;
            if(!$USER->IsAdmin()) { // Текущий пользователь не администратор
                // Проверить на администратора модуля
                $isBookingAdmin = \Websoft\Booking\Entity\UsersTable::getListPure(false,array(
                    'ASSIGNED_BY_ID'=>$USER->GetID(), 'GROUP' => 'BOOKING_ADMIN' ));
                if(!$isBookingAdmin) { $acces = false; }
            }
            ?>
            <? if($acces): ?>
            <a href="<?=$arResult['URL_TEMPLATES']['SETTING']?>" class="btn_master">АДМИНИСТРАТИВНАЯ ЧАСТЬ</a>
            <span style="width: 20px"></span>
            <? endif; ?>

            <a href="<?=$arResult['URL_TEMPLATES']['MASTER']?>" class="btn_master blink">ЗАБРОНИРОВАТЬ ПЕРЕГОВОРНУЮ</a>
            <?$APPLICATION->AddViewContent('pagetitle', ob_get_contents()); ob_end_clean();?>
        </div>
        <div class="slider_wrap">
            <div class="slider_title">
                <span>Выбрать Компанию</span>
            </div>
            <booking-company v-bind:companies="<?=CUtil::PhpToJSObject($arResult['COMPANIES'])?>"></booking-company>
        </div>
        <div class="choose_block">
            <div class="datepicker">
                <p>
                    <img src="<?=$this->__folder ?>/img/datepick_ic.png" alt="">
                    <span>Выбрать дату</span>
                    <input type="text" v-model="date" id="datepicker" placeholder="22.02.2022г">
                </p>
            </div>
            <div class="choose_object">
                <span>Выбрать переговорную</span>
                <input type="text" id="search_input">
                <button class="search_btn"  onclick="app.searchNegByName()">поиск</button>
                <booking-search-neg v-bind:negotiated="NEGOTIATED_ALL"></booking-search-neg>
            </div>
        </div>
        <div class="info_block">
            <booking-daterange :range="<?=CUtil::PhpToJSObject($arResult['daterange'])?>"></booking-daterange>
            <booking-timewrap ref="neg" v-bind:negotiated="NEGOTIATED"></booking-timewrap>
        </div>
    </div>
</main>

<script>
    app = '';
    $(document).ready(function () {
        dateNew = '<?=$arResult['DATE']?>';
        var settings = {
            date: dateNew,
            selectCompany:[], 
            NEGOTIATED:<?=CUtil::PhpToJSObject($arResult['NEGOTIATED'])?>,
            NEGOTIATED_ALL:<?=CUtil::PhpToJSObject($arResult['NEGOTIATED'])?>,
            selectNegotiated:[],
            params:<?=CUtil::PhpToJSObject($arParams)?>,
            path: '<?=$this->__folder?>/'
        };
        setTimeout( function () { app = initBooking(settings); }, 1000);
        BX.MyAjax.path_to_ajax_files = '<?=$this->__folder?>/';
    });

    function redirect(page) { window.location = page; }
    function activate(obj) {
        className = 'active';
        if($(obj).hasClass(className)) { $(obj).removeClass(className); }
        else { $(obj).addClass(className); }
    }
</script>