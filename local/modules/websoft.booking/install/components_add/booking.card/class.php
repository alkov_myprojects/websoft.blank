<?php
defined('B_PROLOG_INCLUDED') || die;

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

use Websoft\Booking\Entity\BookingCardTable as BookingCard;
use Websoft\Booking\Entity\BookingCardChildTable as BookingCardChild;
use Websoft\Booking\Entity\BookingTypeTable as BookingType;
use Websoft\Booking\Entity\CompanyBookingTable as CompanyBooking;

class CBookingCardComponent extends CBitrixComponent {
    public  function getCard($Card) {

        // Тип бронирования (иконка + наименование)
        $BOOKING_TYPE = BookingType::getListPure(
            false,array('ID'=>$Card['BOOKING_TYPE_ID']),array('NAME','UF_ICON')
        );

        // Перечень компаний
        $list_companyes = array();
        foreach ($Card['ITMES'] as $item) {
            $neg = \Websoft\Booking\Entity\NegotiatedTable::getListPure(
                false,array('ID'=>$item['NEGOTIATED_ID']),array('COMPANY_ID'));
            $company = \Websoft\Booking\Entity\CompanyBookingTable::getListPure(
                false,array('ID'=>$neg['COMPANY_ID']),array('*','UF_*'));

            $list_companyes[] = array('ID' => $company['ID'], 'NAME' => $company['NAME'], 'SRC' => $company['UF_LOGO']);
        }
        $EXECUTOR = \CUser::GetByID($Card['EXECUTOR_BY_ID'])->Fetch();
        $BREAKS = \Websoft\Booking\Entity\BreakTable::getListPure(
            true,array('BOOKING_CARD_ID'=>$Card['ID']),array('ID','NAME','START','END')
        );
        $pathEdit = \CComponentEngine::MakePathFromTemplate(
            $this->arParams['PATH_CALENDAR'].$this->arParams['PATH_EDIT'], array('ID' => $Card['ID'])
        );

        //region Определить каких переговорных не хватает
        $ADD_NEG_TO_COMPANY = array();
        foreach ($Card['UF_COMPANIES_ID'] as $company_id) {
            $add = true;
            foreach ($list_companyes as $arrCompany) { if($arrCompany['ID'] == $company_id) { $add = false; break; } }
            if($add) {
                $company = \Websoft\Booking\Entity\CompanyBookingTable::getListPure(
                    false,array('ID'=>$company_id),array('ID','NAME')
                );
                $ADD_NEG_TO_COMPANY[] = array('ID' => $company_id, 'NAME' => $company['NAME']);
            }
        }
        //endregion

        //region Компании ВКС
        $allCompanies = array();
        if(empty($Card['UF_COMPANIES_ID'])) {
            foreach ($Card['ITMES'] as $item) {
                //\Websoft\Booking\Entity\CompanyBookingTable::getListPure(false,array(),array())
                $neg = \Websoft\Booking\Entity\NegotiatedTable::getListPure(false,array('ID'=>$item['NEGOTIATED_ID']),array('COMPANY_ID'));
                $company = \Websoft\Booking\Entity\CompanyBookingTable::getListPure(
                    false,array('ID'=>$neg['COMPANY_ID']),array('ID','NAME','UF_LOGO')
                );
                $logo = CFile::ResizeImageGet(
                    $company['UF_LOGO'], array('width'=>107, 'height'=>45),BX_RESIZE_IMAGE_PROPORTIONAL
                );
                $allCompanies[] = array('ID' => $company_id, 'NAME' => $company['NAME'],'SRC' => $logo['src']);

            }
        } else {
            foreach ($Card['UF_COMPANIES_ID'] as $company_id) {
                $company = \Websoft\Booking\Entity\CompanyBookingTable::getListPure(
                    false,array('ID'=>$company_id),array('ID','NAME','UF_LOGO')
                );
                $logo = CFile::ResizeImageGet(
                    $company['UF_LOGO'], array('width'=>107, 'height'=>45),BX_RESIZE_IMAGE_PROPORTIONAL
                );
                $allCompanies[] = array('ID' => $company_id, 'NAME' => $company['NAME'],'SRC' => $logo['src']);
            }
        }
        //endregion
        
        //region История измениня
        /*$STORY = \Websoft\Booking\Entity\StoriesCardTable::getListPure(
            true,array('ENTITY_ID' => $Card['ID']),array('*','UF_*')
        );
        \Websoft\Booking\Handler::dd('$STORY',$STORY);*/
        //endregion

        //region получить строку "Дата начала/окочания из события"
        $PARENT_EVENT_ID = \Websoft\Booking\Entity\BookingCardChildTable::getListPure(
            false,array('PARENT_ID'=>$Card['ID']),array('PARENT_EVENT_ID')
        );
        $PARENT_EVENT_ID = $PARENT_EVENT_ID['PARENT_EVENT_ID'];

        $event = \Websoft\Booking\Classes\CustomEventsCalendar::GetEventByID($PARENT_EVENT_ID);

        $DT_BEGIN_END = $event['CUSTOM_DATA']['When'];
        if($event['CUSTOM_DATA']['Periodicity']) { $DT_BEGIN_END .= ' -> '.$event['CUSTOM_DATA']['Periodicity']; }
        //endregion
        
        $Card_new = array(
            // Системаные поля
            'ID' => $Card['ID'],
            'UF_COMPANIES_ID' => $Card['UF_COMPANIES_ID'],
            'FINISH' => false,

            // Поля для вывода
            'DT_BEGIN_END' => $DT_BEGIN_END,
            'ADD_NEG_TO_COMPANY' => $ADD_NEG_TO_COMPANY,
            'NAME' => $Card['NAME'],
            'PATH_EDIT' =>$pathEdit,
            'DESCRIPTION' => $Card['DESCRIPTION'],
            'DATE_CREATE' => $Card['DATE_CREATE']->toString(),
            'DATE_START' => $Card['DATE_START'],
            'DATE_END' => $Card['DATE_END'],
            'BREAKS' => $BREAKS,

            'BOOKING_TYPE' => $BOOKING_TYPE['NAME'] ? $BOOKING_TYPE['NAME'] : 'Другое',

            'COMMENT_EVENT' => $Card['COMMENT_EVENT'], // Комментраий к мероприятию
            'COMMENT_SERVICE_TYPES' => $Card['COMMENT_SERVICE_TYPES'], // Комментраий к сопровождению мероприятия

            'COMPANIES' => $allCompanies, //$list_companyes,
            'EXECUTOR' => array(
                'ID' => $EXECUTOR['ID'],
                'FIO' => \Websoft\Booking\Handler::fullNameUser($EXECUTOR['ID']),
                'PATH_TO_USER' => \Websoft\Booking\Handler::getPathUser($EXECUTOR['ID'])
            ),
            'ITEMS' => array(),
            'STORY' =>array(),
        );

        //region Определить какая переговорная будет выделена
        global $USER;
        $userId = $USER->GetID();
        $select = false;
        foreach ($Card['ITMES'] as $item) {

            $neg = \Websoft\Booking\Entity\NegotiatedTable::getListPure(
                false,array('ID'=>$item['NEGOTIATED_ID']),array('*','UF_*'));
            $company = \Websoft\Booking\Entity\CompanyBookingTable::getListPure(
                false,array('ID'=>$neg['COMPANY_ID']),array('*','UF_*'));
            $trainingStatus = \Websoft\Booking\Entity\TrainingStatusesTable::getListPure(
                false,array('ID'=>$item['TRAINING_STATUS_ID']),array('*','UF_*'));
            $UF_SERVICES_DATA = \Websoft\Booking\Entity\ServiceDataTable::getListPure(
                true,array('ID'=>$item['UF_SERVICES_DATA_ID']),array('*','UF_*'));
            
            /*echo '<b>$UF_SERVICES_DATA</b>';
            echo '<pre>';
            print_r($UF_SERVICES_DATA);
            echo '</pre>';
            echo '<hr>';*/

            $SERVICES_DATA = array();
            foreach ($UF_SERVICES_DATA as $service_data) {
                $arUser = \CUser::GetByID($service_data['RESPONSIBLE_ID'])->Fetch();
                $service = \Websoft\Booking\Entity\ServiceTypesTable::getListPure(
                    false,array('ID'=>$service_data['SERVICE_TYPE_ID']),array('*','UF_*'));

                $statuses = \Websoft\Booking\Entity\ServiceDataTable::GetStatuses();
                //\Websoft\Booking\Handler::dd('$service_data',$service_data);

                $SERVICES_DATA[] = array(
                    'ID' => $service_data['ID'],
                    'NAME' => $service['NAME'],
                    'RESPONSIBLE' => array(
                        'ID' => $arUser['ID'],
                        'FIO' => \Websoft\Booking\Handler::fullNameUser($arUser['ID']),
                        'PATH_TO_USER' => \Websoft\Booking\Handler::getPathUser($arUser['ID']),
                        'PHONE' => $arUser['WORK_PHONE']
                    ),
                    'COMMENT' => $service_data['COMMENT'],
                    'RATING' => $service_data['RATING'],
                    'STATUS' => array(
                        'ID' => $service_data['STATUS_ID'],
                        'NAME' => $statuses[$service_data['STATUS_ID']],
                        'LIST' => \Websoft\Booking\Entity\ServiceDataTable::GetStatuses()
                    )
                );
            }

            $select_L = false;
            if(!$select) { // Если ни одна переговорная не выделена, то проверить текущую, на выделение
                // Пользователь должен входить как участник или организатор
                if(isset($_REQUEST['neg_id']) && $_REQUEST['neg_id']) {
                    if($_REQUEST['neg_id'] == $neg['ID']) { $select = true; $select_L = true; }
                }
                else if(in_array($userId,$item['UF_PARTICIPANTS'])) { $select = true; $select_L = true; }
            }

            //region Статус подгтовки/Общее описание подготовки/общая сумма рейтингов
            $MAIN_TRAINING_STATUS = \Websoft\Booking\Entity\ServiceNegotiatedDataTable::GetMainStatus(0);
            $STATUS_DESCRIPTION = '';
            $ServiceNegotiatedData = \Websoft\Booking\Entity\ServiceNegotiatedDataTable::getListPure(
                false, array( 'SNAP_CARD_ID' => $item['ID'], 'DATE_BEGIN' => $_REQUEST['DATE'] ),
                array('STATUS_ID','STATUS_DESCRIPTION','STATUS_MAIN_ID','RATING_1','RATING_2','RATING_3') );
            if($ServiceNegotiatedData) {
                $MAIN_TRAINING_STATUS = \Websoft\Booking\Entity\ServiceNegotiatedDataTable::GetMainStatus(
                    $ServiceNegotiatedData['STATUS_MAIN_ID'] );
                $STATUS_DESCRIPTION = $ServiceNegotiatedData['STATUS_DESCRIPTION'];
            }
            else {
                \Websoft\Booking\Entity\ServiceNegotiatedDataTable::add(array(
                    'STATUS_DESCRIPTION' => $STATUS_DESCRIPTION,
                    'SNAP_CARD_ID' => $item['ID'],
                    'DATE_BEGIN' => $_REQUEST['DATE'],
                ));
            }
            //endregion

            //region Общий статус подготовки
            //$MAIN_TRAINING_STATUS = 'hello';
            //endregion

            //region UF_PARTICIPANTS - Участники
            $UF_PARTICIPANTS = array();
            foreach ($item['UF_PARTICIPANTS'] as $PARTICIPANT_ID) {
                $arUSer = \CUser::GetByID($PARTICIPANT_ID)->Fetch();
                $arUSer['FULL_NAME'] = \Websoft\Booking\Handler::fullNameUser($PARTICIPANT_ID);
                $arUSer['PATH_USER'] = \Websoft\Booking\Handler::getPathUser($PARTICIPANT_ID);
                $UF_PARTICIPANTS[] = $arUSer;
            }
            //endregion

            // Добавление дочерней карточки
            $logo = CFile::ResizeImageGet($company['UF_LOGO'], array('width'=>118, 'height'=>45),BX_RESIZE_IMAGE_PROPORTIONAL);
            $Card_new['ITEMS'][] = array(
                'ID' => $item['ID'],
                'SELECTED' => $select_L,
                'COMPANY' => array(
                    'ID' => $company['ID'],
                    'NAME' => $company['NAME'],
                    'SRC' => $logo['src'], //\CFile::GetPath($company['UF_LOGO']),
                ),
                'NEGOTIATION' => $neg['NAME'],
                'MAIN_TRAINING_STATUS' => $MAIN_TRAINING_STATUS,//$TRAINING_STATUS, //$trainingStatus['NAME'],
                'STATUS_DESCRIPTION' => $STATUS_DESCRIPTION, //$item['STATUS_DESCRIPTION'],
                'RATING_1' => $ServiceNegotiatedData['RATING_1'],
                'RATING_2' => $ServiceNegotiatedData['RATING_2'],
                'RATING_3' => $ServiceNegotiatedData['RATING_3'],
                'SERVICES_DATA' => $SERVICES_DATA,
                'UF_PARTICIPANTS' => $UF_PARTICIPANTS, //$item['UF_PARTICIPANTS']
            );
        }
        if(!$select) { $Card_new['ITEMS'][0]['SELECTED'] = true; }
        //endregion
        
        //region Получить историю
        // ... $Card
        $story = \Websoft\Booking\Entity\StoriesCardTable::getListPure(true,array('ENTITY_ID' => $Card['ID']),array('*'),array('DATE_TIME'=>'desc'));
        foreach ($story as $story_one) {
            $Card_new['STORY'][] = array(
                'DATE_TIME' => $story_one['DATE_TIME']->toString(),
                'CHANGE_BY' => array(
                    'ID' => $story_one['CHANGE_BY_ID'],
                    'NAME' => \Websoft\Booking\Handler::fullNameUser($story_one['CHANGE_BY_ID']),
                    'PATH_TO_PROFILE' => \Websoft\Booking\Handler::getPathUser($story_one['CHANGE_BY_ID']),
                ),
                'DESCRIPTION' => $story_one['DESCRIPTION'],
                'BEFORE' => $story_one['BEFORE'],
                'AFTER' => $story_one['AFTER'],
            );
        }
        //endregion

        //region Определелить окончание мероприятия
        if(isset($_REQUEST['DATE'])) {
            $objDateTime = new \Bitrix\Main\Type\DateTime();
            $objDateTime_2 = new \Bitrix\Main\Type\DateTime($_REQUEST['DATE']);
            if($objDateTime->getTimestamp() > $objDateTime_2->getTimestamp()) {
                $Card_new['FINISH'] = true;
            }
            $Card_new['DATE_EVENT'] = $_REQUEST['DATE'];

        }
        //endregion

        //region Карточка бронирования Анулирована или нет?
        $activeBookingCard = \Websoft\Booking\Entity\BookingCardTable::getListPure(
            false,array('ID'=>$Card['ID'],'ACTIVE_BOOKING'=>'Y')
        );
        $Card_new['ACTIVE_BOOKING'] = $activeBookingCard ? true : false;
        //endregion
        
        //region Файлы
        // ... Получить список пользователей, которые относятся к мероприятию (Организатор, администраторы
        //     компаний, администраторы переговорных, участники, ответсвенные за сервисы сопровождения)
        $idsUsersAccess = array();
        $idsUsersAccess[] = $Card_new['EXECUTOR']['ID']; // Организатор
        foreach ($Card_new['COMPANIES'] as $COMPANY) { // Администраторы компаний
            $company_L = \Websoft\Booking\Entity\CompanyBookingTable::getListPure(
                false,array('ID'=>$COMPANY['ID']),array('ASSIGNED_BY_ID'));
            $idsUsersAccess[] = $company_L['ASSIGNED_BY_ID'];
        }
        // Ответсвенные за сервисы сопровождения
        foreach ($Card_new['ITEMS'] as $ITEM) {
            foreach ($ITEM['SERVICES_DATA'] as $SERVICES_DATA){$idsUsersAccess[] = $SERVICES_DATA['RESPONSIBLE']['ID'];}
            foreach ($ITEM['UF_PARTICIPANTS'] as $PARTICIPANT) { $idsUsersAccess[] = $PARTICIPANT; } // Участники
        }
        $idsUsersAcces = array_unique($idsUsersAccess);
        if(in_array($userId,$idsUsersAcces)) {
            $Card_new['FILES'] = array();
            foreach ($Card['UF_FILES'] as $UF_FILE) { $Card_new['FILES'][] = CFile::GetFileArray($UF_FILE); }
        }
        //endregion
        
        //region ответственный за мероприятие
        $Card_new['UF_RESPONSIBLE_EVENT'] = $Card['UF_RESPONSIBLE_EVENT'] ? \Websoft\Booking\Handler::fullNameUser($Card['UF_RESPONSIBLE_EVENT']) : 'Не назначен';
        //endregion

        $Card_new['idsUsersAccess'] = $idsUsersAccess;
        return $Card_new;
    }
    public function executeComponent() {
        
        $Card = $this->arParams['Card'];
        $Card = $this->getCard($Card);

        if(!$Card['ACTIVE_BOOKING']) { ShowError('Бронирование аннулировано...'); return; }

        /** Подключение и передача данных компоненту: */
        $this->arResult = array(
            'Card' => $Card,
            'PATH_CALENDAR' => $this->arParams['PATH_CALENDAR']
        );

        $this->includeComponentTemplate();
    }
}