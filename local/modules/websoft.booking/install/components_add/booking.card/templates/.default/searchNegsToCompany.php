<?
require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");

$CARD_ID = $_REQUEST['CARD_ID'];
$COMPANY_ID = $_REQUEST['COMPANY_ID'];

// Поиск подходящей переговорной
$arCard = \Websoft\Booking\Entity\BookingCardTable::getListPure(false,array('ID' => $CARD_ID),array('*','UF_*'));
$DATE_BEGIN = $arCard['DATE_START'];
$DATE_END = $arCard['DATE_END'];
$arFilter = array( 'COMPANY_ID' => $COMPANY_ID );

CBitrixComponent::includeComponentClass("websoft.booking:booking.master");
$CBookingCardComponent = 'CBookingMasterComponent';
$negsCategories = $CBookingCardComponent::getNegotiationCategories($arFilter,$DATE_BEGIN,$DATE_END);

/*echo '<b>$negsCategories</b>';
echo '<pre>';
print_r($negsCategories);
echo '</pre>';
echo '<hr>';*/


// Печать (Тестовая, тут можно забрать массив или распечатать HTML)
foreach ($negsCategories as $cat => $arrNegs) {
    foreach ($arrNegs as $neg) {
        $resizeImg = CFile::ResizeImageGet($neg['UF_PREVIEW_IMAGE'],array('width'=>135,'height'=>73),BX_RESIZE_IMAGE_EXACT);
        $img = $resizeImg['src'];
        $company = \Websoft\Booking\Entity\CompanyBookingTable::getListPure(
            false,array('ID' => $neg['COMPANY_ID']),array('NAME'));
        $SERVICES = \Websoft\Booking\Entity\ServiceTypesTable::getListPure(
            true,array('ID'=>$neg['UF_SERVICE_TYPES']),array('UF_ICON_ACTIVE'));
        ?>
        <div class="avaible_item <?if($cat == 'BUSY'){?>ordered<?}?>"
             data-neg-id="<?=$neg['ID']?>"
             data-company-id="<?=$neg['COMPANY_ID']?>"
             onclick="selectNeg(this);"
        >
             <a href="javascript:void(0);" class="avaible_close" onclick="closeSelectNeg(this); event.stopPropagation();"></a>
             <div class="avaible_tags">
                 <? $avaible_hidden = array(); ?>
                 <? $avaible_count = 0; ?>
                 <? foreach ($SERVICES as $SERVICE): ?>
                     <? $avaible_count++; ?>
                     <? $src = CFile::GetFileArray($SERVICE['UF_ICON_ACTIVE']); $src = $src['SRC']; ?>
                     <? if($avaible_count <= 5): ?>
                         <img src="<?=$src?>" width="12">
                     <? else: ?>
                         <? $avaible_hidden[] = $src; ?>
                     <? endif; ?>
                 <? endforeach; ?>
                 <? if(!empty($avaible_hidden)): ?>
                     <div style="height: 100%; width: 10px; display: inline-block;"></div>
                     <span class="more_info">
                         <div class="box_info">
                             <? foreach ($avaible_hidden as $src2): ?>
                                 <div style="display: inline-block">
                                     <img src="<?=$src2?>" width="12">
                                 </div>
                             <? endforeach; ?>
                         </div>
                     </span>

                 <? endif; ?>
             </div>
             <div class="avaible_image">
                 <img src="<?=$img?>">
                 <?if($cat == 'BUSY'){?><div class="violet_wrap">Занято</div><?}?>
             </div>
             <div class="avaible_text">
                <div class="avaible_item-title">Название:</div>
                <div class="avaible_item-name"><b><?=$neg['NAME']?></b></div>
                <div class="avaible_item_descr">
                    <p>Компания:</p>
                    <p style="padding-bottom: 5px;"><b><?=$company['NAME']?></b></p>

                    <p>Кол-во мест:</p>
                    <p style="padding-bottom: 5px;"><b><?=$neg['COUNT_PLACE']?></b></p>

                    <p>Адрес:</p>
                    <p style="padding-bottom: 5px;"><b><?=$neg['ADDRESS']?></b></p>
                </div>
            </div>
        </div>
        <?
    }
} ?>

<style>
    .avaible_item:hover ,
    .avaible_item.active { border: 3px solid #52aeca; }
    .avaible_item {
        padding: 10px 8px;
        padding-top: 0;
        max-width: 157px;
        box-sizing: border-box;
        border: 3px solid transparent;
        position: relative;
        margin-bottom: 15px;
        transition: all 0.5s;
        min-height: 285px;
        float: left;
        margin-right: 20px;
    }
    .avaible_tags {
        height: 16px;
        display: flex;
        align-items: center;
        background: white;
        position: relative;
        top: -11px;
        padding: 0px 5px;
        display: inline-block;
    }
    .avaible_image { position: relative; }
    .avaible_text {
        color: #4b4b4b;
        font-family: "Gotham-Regular";
        font-size: 12px;
        margin-top: 10px;
    }
    .avaible_tags img { margin-right: 6px; }
    .avaible_item.ordered { border: 3px solid #7b60b3; }
    .violet_wrap {
        position: absolute;
        top: 0px;
        left: 0px;
        width: 135px;
        height: 73px;
        background: rgba(123, 96, 179,0.8);
        color: #fff;
        display: flex;
        align-items: center;
        justify-content: center;
        font-family: "Gotham-Regular";
        font-size: 10px;
        text-transform: uppercase;
        letter-spacing: 3px;
    }
    .avaible_item.active .avaible_close {
        width: 13px;
        height: 13px;
        background: #52aeca url(/bitrix/components/websoft.booking/booking.master/templates/.default/img/avaible_close.png) center no-repeat;
        position: absolute;
        top: -8px;
        right: -8px;
        border-radius: 50%;
    }
    .more_info {
        background: url(/bitrix/components/websoft.booking/booking.master/templates/.default/img/vertical_dots_mint.png) no-repeat;
        display: inline-block;
        width: 4px;
        height: 18px;
        z-index: 2;
        position: absolute;
        top: -1px;
        right: 10px;
        cursor: pointer;
    }
    .box_info {
        position: absolute;
        background: white;
        padding: 5px 8px 5px 5px;
        z-index: 99;
        border: 2px solid #52aeca;
        right: -2px;
        top: 20px;
        display: none;
        min-width: 104px;
        text-align: right;
    }
    .more_info:hover .box_info { display: block; }
    .avaible_item.ordered:hover .avaible_close{ display: none;
</style>

<script>

    /*$('.avaible_item').click(function (e) {
        console.log(123);
        if(!$(this).hasClass('ordered')) {
            var children = $(this);
            children.addClass("active");
            neg_id = $(this).data('neg-id');
            //setFilterParam('NEG_IDS',neg_id,true,true);
            SelectNeg = neg_id;
            company_id = $(this).data('company-id');
            $('.avaible_item[data-company-id="' + company_id + '"]').css('display','none');
            $(this).css('display','block');
            //upload_negs();
        }
    });
    $('.avaible_close').click(function (e) {
        e.stopPropagation();
        var children = $(this).parent();
        children.removeClass("active");
        neg_id = $(this).parent().data('neg-id');
        //removeFilterParam('NEG_IDS',neg_id,true);
        SelectNeg = '';
        company_id = $(this).parent().data('company-id');
        $('.avaible_item[data-company-id="' + company_id + '"]').css('display','block');
        //upload_negs();
    });*/
</script>
