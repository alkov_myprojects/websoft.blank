$('.company_slider').slick({
    infinite: false,
    arrows: true,
    slidesToShow: 5,
    slidesToScroll: 1,
    appendArrows: $('.slide_comp'),
    responsive: [{
            breakpoint: 1500,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 1,
                infinite: true,
            }
        },
        {
            breakpoint: 1300,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ]
});
$('.card_slider').slick({
    infinite: false,
    arrows: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    appendArrows: $('.slide_card'),
    responsive: [{
            breakpoint: 1500,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
                infinite: true,
            }
        },
        {
            breakpoint: 1300,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ]
});

function initApp(settings) {
    var app = new Vue({
        el: '#app',
        data: settings,
        created() { },
        mounted() { this.selectCard = this.subCard.filter(card => card.SELECTED == true)[0]; },
        methods:{
            selectSubCard: function(card){                
                var self = this;
                $.map(self.subCard, function (elementOrValue, indexOrKey) {
                    if(elementOrValue.SELECTED){ self.subCard[indexOrKey].SELECTED = false; }
                });
                card.SELECTED = true;
                this.selectCard = card;
                this.updateRaiting();
            },
            showPhone: function(e){
                var parent = $(e.target).parent();
                parent.find('.company-details').fadeToggle().toggleClass("active");
            },
            updateRaiting: function () {
                jQuery(function ($) {
                    /*cnt = 0;
                    summ = 0;
                    SERVICES_DATA = app.selectCard.SERVICES_DATA;
                    $( '.raty-icons' ).each(function( index, element ) {
                        data_id_service = $(element).attr('data-service-id');
                        summ += parseInt(SERVICES_DATA[index]['RATING']);
                        cnt++;
                        jQuery(element).raty({
                            'score':parseInt(SERVICES_DATA[index]['RATING']), 'target':'.rates', 'readOnly':true
                        });
                    });
                    jQuery('.rates').hide();
                    app.summ_rating = Math.ceil(summ/cnt);*/
                    
                    summ  = parseInt(app.selectCard.RATING_1);
                    summ += parseInt(app.selectCard.RATING_2);
                    summ += parseInt(app.selectCard.RATING_3);

                    jQuery('#RATING_1').raty({ 'score': app.selectCard.RATING_1, 'target': '.rates', 'readOnly':true});
                    jQuery('#RATING_2').raty({ 'score': app.selectCard.RATING_2, 'target': '.rates', 'readOnly':true});
                    jQuery('#RATING_3').raty({ 'score': app.selectCard.RATING_3, 'target': '.rates', 'readOnly':true});
                    jQuery('.rates').hide();

                    res =  Math.ceil(summ/3);
                    if(!isNaN(parseFloat(res)) && isFinite(res)) { app.summ_rating = res; }
                    else { app.summ_rating = 0; }


                });
            }
        },
        computed: {
            test2() {
                //region Получить все сервисы текущей переговорной
                return 'hhhhhhhhhhhhhhhhhh';
            }
        }
    });
    
    return app;
}

SelectNeg = '';
Users = '';
function addNegToCompany(COMPANY_ID,CARD_ID) {
    var PopupBX = new BX.MyPopupBX();
    PopupBX.Message('message_search_neg','', 'Поиск подходящих переговорных, подождите...');
    $.ajax({
        url: folder + 'searchNegsToCompany.php', dataType: "html", data: { 'CARD_ID':CARD_ID, 'COMPANY_ID':COMPANY_ID },
        success: function(data) {
            PopupBX.CloseLastPopup();
            var PopupBX_2 = new BX.MyPopupBX();
            PopupBX_2.AddBtn('Применить',function () {
                if(SelectNeg) {
                    //region Добавление участников
                    PopupBX_2.CloseLastPopup();
                    var PopupBX_4 = new BX.MyPopupBX();
                    PopupBX_4.Message('wait_form_add_users','','Подготовка формы для добавления участников, ожидайте...');
                    $.ajax({ url: folder + 'FormAddUsers.php', dataType: "html", success: function(data) {
                        PopupBX_4.CloseLastPopup();
                        var PopupBX_5 = new BX.MyPopupBX();
                        PopupBX_5.AddBtn('Применить',function () {
                            var PopupBX_6 = new BX.MyPopupBX();
                            
                            //region Получить пользователей с формы
                            Users = $('#box_to_list_users').serializeArray();
                            //endregion
                            
                            if(Users.length > 1) {
                                PopupBX_5.CloseLastPopup();
                                PopupBX_6.Message('wait_add_NegToCompany','','Добавление переговорной к карточке бронирования, ожидайте...');
                                //var elem = document.getElementById("box_to_list_users");
                                $.ajax({
                                    url: folder + 'addNegToCompany.php',
                                    dataType: "html", data:{ 'SelectNeg': SelectNeg, 'CARD_ID': CARD_ID,'Users':Users },
                                    success: function(data) {
                                        //PopupBX_6.CloseLastPopup();
                                        console.log('data');
                                        console.log(data);
                                        location.reload();
                                    }
                                });
                            } else { PopupBX_6.Message('Warning','','Вы не добавили участников.',true); }
                        },true);
                        PopupBX_5.AddBtn('Закрыть',function () {
                            $('body').append($('.FormAddUser_Bitrix'));
                            $('.FormAddUser_Bitrix').css('display','none');

                            SelectNeg = '';
                            Users = '';
                            PopupBX_5.CloseLastPopup();
                        });
                        PopupBX_5.Message('form_add_users','Участники',data);

                        //region Кастыль для формы добавления пользователей
                        /*top = $('#box_to_list_users').offset().top - $(window).scrollTop;
                        console.log('top: ');
                        console.log(top);

                        $('.FormAddUser_Bitrix').css('top',top + 'px');*/
                        //var elem = document.getElementById("box_to_list_users");
                        //Coords = getCoords(elem);
                        //console.log('Coords');
                        //console.log(Coords);
                        //$('.FormAddUser_Bitrix').css('top',Coords.top + 'px');
                        //$('.FormAddUser_Bitrix').css('left',Coords.left + 'px');
                        //$('.FormAddUser_Bitrix').css('display','block');
                        //html = $('.FormAddUser_Bitrix').html();
                        //$('#box_to_list_users').append(html);

                        $('#box_to_list_users').append($('.FormAddUser_Bitrix'));
                        $('.FormAddUser_Bitrix').css('display','block');


                            //endregion
                    }});
                    //endregion
                } else {
                    var PopupBX_3 = new BX.MyPopupBX();
                    PopupBX_3.Message('error_select_neg','', 'Вы не выбрали ни одной переговорной.',true);
                }
            },true);
            PopupBX_2.AddBtn('Закрыть',function () { PopupBX_2.CloseLastPopup(); SelectNeg = ''; });
            PopupBX_2.Message('listNegs','Доступные переговорные',data);
        }
    });
}

function getCoords(elem) { // кроме IE8-
    var box = elem.getBoundingClientRect();

    return {
        top: box.top + pageYOffset,
        left: box.left + pageXOffset
    };

}

function selectNeg(obj) {
    if(!$(obj).hasClass('ordered') && !$(obj).hasClass('active')) {
        $('.avaible_item').removeClass('active');
        $(obj).addClass('active');
        neg_id = $(obj).data('neg-id');
        SelectNeg = neg_id;
    }
}
function closeSelectNeg(obj) {
    var children = $(obj).parent();
    children.removeClass("active");
    neg_id = $(obj).parent().data('neg-id');
    SelectNeg = '';
    $('.avaible_item[data-neg-id="' + neg_id + '"]').removeClass('active');
}