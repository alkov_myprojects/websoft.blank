<?php
defined('B_PROLOG_INCLUDED') || die;

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Page\Asset;
use Bitrix\Main\Web\Json;

/** @var CBitrixComponentTemplate $this */
$APPLICATION->SetTitle('Карточка бронирования(просмотр)');
//Asset::getInstance()->addString('<script src="https://cdn.jsdelivr.net/npm/vue@2.5.17/dist/vue.js"></script>');
Asset::getInstance()->addJs($this->__folder . "/js/vue.js");
Asset::getInstance()->addJs($this->__folder . "/js/jquery.raty.mini.js");
Asset::getInstance()->addJs($this->__folder . "/js/main.js");
Asset::getInstance()->addCss($this->__folder . "/css/style.css");
Asset::getInstance()->addCss($this->__folder . "/css/jquery.rating.css");

$arCard = $arResult['Card'];
$date = $arCard['DATE_START']->format('d.m.Y');
$date_start = date('H:i', strtotime($arCard['DATE_START']->ToString()));
$date_end = date('H:i', strtotime($arCard['DATE_END']->ToString()));
$arSubCard = $arResult['Card']['ITEMS'];

//\Websoft\Booking\Handler::dd('$arCard["FILES"]',$arCard['FILES']);

?>

<main id="app">
    <div class="center_side">
        <!--<div class="breadcrumbs">
            <li><a href="/booking/">Бронирование переговорных</a></li>
            <li><a href="#" class="active">Карточка бронирования</a></li>
        </div>-->
        <div class="content_sides">
            <div class="content_side_center">

                <!-- Наименование мероприятия -->
                <div class="content_item">
                    <!--<div class="content_left ">-->
                    <div class="content_left">
                        <div class="content_left_item  m-h70">
                            <p>Наименование мероприятия</p>
                        </div>
                    </div>
                    <div class="content_right">
                        <div class="content_right_item m-h70">
                            <p class="descr_title"><?=$arCard['NAME']?></p>
                        </div>
                    </div>
                </div>

                <!-- Описание мероприятия -->
                <div class="content_item">
<!--                    <div class="content_left  ">-->
                    <div class="content_left">
                        <div class="content_left_item item_start">
                            <p>Описание мероприятия</p>
                        </div>
                    </div>
<!--                    <div class="content_right ">-->
                    <div class="content_right">
                        <div class="content_right_item">
                            <div class="input_wrap ">
                                <textarea readonly="readonly"><?=$arCard['DESCRIPTION']?></textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Дата создания мероприятия -->
                <div class="content_item">
                    <div class="content_left ">
                        <div class="content_left_item  m-h50">
                            <p>Дата создания мероприятия</p>
                        </div>
                    </div>
                    <div class="content_right">
                        <div class="content_right_item m-h50 space_between pr-10">
                            <div class="input_wrap ">
                                <input type="text" readonly class="w-290" v-model="card.DATE_CREATE">
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Ответственный за мероприятие-->
                <div class="content_item">
                    <div class="content_left ">
                        <div class="content_left_item  m-h50">
                            <p>Ответственный за мероприятие</p>
                        </div>
                    </div>
                    <div class="content_right ">
                        <div class="content_right_item m-h50 space_between pr-10">
                            <div class="input_wrap ">
                                <input type="text" readonly class="w-290" v-model="card.UF_RESPONSIBLE_EVENT">
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Организатор -->
                <div class="content_item">
                    <div class="content_left ">
                        <div class="content_left_item  m-h50">
                            <p>Организатор</p>
                        </div>
                    </div>
                    <div class="content_right ">
                        <div class="content_right_item m-h50 space_between pr-10">
                            <div class="input_wrap ">
                                <input type="text" readonly class="w-290" v-model="card.EXECUTOR.FIO">
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Период проведения -->
                <div class="content_item">
                    <div class="content_left ">
                        <div class="content_left_item  m-h50">
                            <p>Период проведения</p>
                        </div>
                    </div>
                    <div class="content_right">
                        <div class="content_right_item m-h50 space_between pr-10">
                            <div class="input_wrap ">
                                <input type="text" readonly class="w-290" value="<?=$arCard['DT_BEGIN_END']?>"
                                       style="width: 100%;">
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Перерывы -->
                <div class="content_item">
                    <div class="content_left ">
                        <div class="content_left_item m-h50">
                            <p>Перерывы</p>
                        </div>
                    </div>
                    <div class="content_right " style="margin-left: 7px;">
                        <div class="content_right_item m-h60 pr-10">
                            <div class="w-100">
                                <div class="input_wrap w-290">
                                    <div class="input_item" v-for="break_info in card.BREAKS">
                                        <span>{{ break_info.NAME }}</span>
                                        <input type="text" readonly class="bb-none"
                                               :value="'с '+break_info.START+ ' до '+break_info.END">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Вид бронирования -->
                <div class="content_item">
                    <div class="content_left ">
                        <div class="content_left_item  m-h50">
                            <p>Вид бронирования</p>
                        </div>
                    </div>
                    <div class="content_right">
                        <div class="content_right_item m-h50 space_between pr-10">
                            <div class="input_wrap ">
                                <input type="text" readonly class="w-290" v-model="card.BOOKING_TYPE">
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Комментарий к мероприятию -->
                <div class="content_item">
                    <div class="content_left  ">
                        <div class="content_left_item item_start">
                            <p>Комментарий к мероприятию</p>
                        </div>
                    </div>
                    <div class="content_right ">
                        <div class="content_right_item ">
                            <div class="input_wrap ">
                                <textarea name="" readonly="readonly" id="" v-model="card.COMMENT_EVENT"></textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <? if($arCard['FILES']): ?>
                    <!-- Файлы -->
                    <div class="content_item">
                        <div class="content_left ">
                            <div class="content_left_item item_start m-h55 ai_center">
                                <p>Файлы мероприятия</p>
                            </div>
                        </div>
                        <div class="content_right">
                            <div class="content_right_item m-h55 p-15">
                                <div class="input_wrap ">
                                    <? foreach ($arCard['FILES'] as $FILE): ?>
                                        <a download href="<?=$FILE['SRC']?>"><?=$FILE['ORIGINAL_NAME']?></a><br><br>
                                    <? endforeach; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <? endif; ?>

                <!-- Компании ВКС -->
                <div class="content_item">
                    <div class="content_left " style="z-index: 1;">
                        <div class="content_left_item slide_comp m-h80">
                            <p>Компании ВКС</p>
                        </div>
                    </div>
                    <div class="content_right " style="overflow:hidden;">
                        <div class="pr-10">
                            <div class="" style="max-width: 805px; margin: 0 auto;">
                                <div class="company_slider">
                                    <? foreach ($arCard['COMPANIES'] as $company): ?>
                                        <div class="company_slider-item">
                                            <a href="#" class="close_slide">x</a>
                                            <img src="<?=$company['SRC']?>" alt="<?=$company['NAME']?>">
                                        </div>
                                    <? endforeach; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Сопутсвующие карточки -->
                <div class="content_item">
                    <div class="content_left " style="z-index: 1;">
                        <div class="content_left_item slide_card m-h80">
                            <p>Сопутствующие карточки</p>
                        </div>
                    </div>
                    <div class="content_right" style="overflow:hidden;">
                        <div class=" pr-10">
                            <div class="" style="max-width: 805px; margin: 0 auto;">
                                <div class="card_slider">
                                    <div class="card_slider-item"  v-for="card in subCard">
                                        <span :class="[{active:card.SELECTED},'card_slider-item_inner']"
                                              @click="selectSubCard(card)">{{ card.NEGOTIATION }}</span>
                                    </div>
                                </div>
                                <? if(!empty($arCard['ADD_NEG_TO_COMPANY'])): ?>
                                        <div class="addNegToCompany">
                                            <? foreach ($arCard['ADD_NEG_TO_COMPANY'] as $arrData): ?>
                                                <a href="javascript:void(0);"
                                                   onclick="addNegToCompany('<?=$arrData['ID']?>','<?=$arCard['ID']?>')"
                                                   class="btns blue"
                                                >Добавить переговорную для компании "<?=$arrData['NAME']?>"</a>
                                            <? endforeach; ?>

                                        </div>
                                <? endif; ?>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Участники  -->
                <div class="content_item">
                    <div class="content_left ">
                        <div class="content_left_item  m-h50">
                            <p>Участники</p>
                        </div>
                    </div>
                    <div class="content_right ">
                        <div class="content_right_item m-h50 space_between pr-10">
                            <div class="input_wrap ">
                                <div class="participant" v-for="PARTICIPANT in selectCard.UF_PARTICIPANTS">
                                    <a :href="PARTICIPANT.PATH_USER">{{ PARTICIPANT.FULL_NAME }}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Статус подготовки  -->
                <div class="content_item">
                    <div class="content_left ">
                        <div class="content_left_item  m-h50">
                            <p>Общий статус подготовки</p>
                        </div>
                    </div>
                    <div class="content_right ">
                        <div class="content_right_item m-h50 space_between pr-10">
                            <div class="input_wrap ">
                                <input type="text" readonly class="w-100-procent" v-model="selectCard.MAIN_TRAINING_STATUS">
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Общее описание подготовки -->
                <div class="content_item">
                    <div class="content_left  ">
                        <div class="content_left_item item_start">
                            <p>Общее описание подготовки</p>
                        </div>
                    </div>
                    <div class="content_right ">
                        <div class="content_right_item">
                            <div class="input_wrap ">
                                <textarea readonly="readonly" v-model="selectCard.STATUS_DESCRIPTION"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <? if($arCard['FINISH']): ?>
                    <!-- Общее описание подготовки -->
                    <div class="content_item">
                        <div class="content_left  ">
                            <div class="content_left_item item_start">
                                <p>Общая оценка</p>
                            </div>
                        </div>
                        <div class="content_right ">
                            <div class="content_right_item">
                                <div class="input_wrap ">
                                    <textarea readonly="readonly" > {{summ_rating}} из 5</textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Организация и сопровождение мероприятия -->
                    <div class="content_item">
                        <div class="content_left">
                            <div class="content_left_item item_start">
                                <p>Организация и сопровождение мероприятия</p>
                            </div>
                        </div>
                        <div class="content_right ">
                            <div class="content_right_item p-15">
                                <div class="rating1">
                                    <div id="RATING_1" class="star green raty-icons" data-target="pid_1" data-score="5.00"></div>
                                    <input id="pid_1" v-model="selectCard.RATING_1" type="text" class="rates" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Подготовка оборудования -->
                    <div class="content_item">
                        <div class="content_left">
                            <div class="content_left_item item_start">
                                <p>Подготовка оборудования</p>
                            </div>
                        </div>
                        <div class="content_right ">
                            <div class="content_right_item p-15">
                                <div class="rating1">
                                    <div id="RATING_2" class="star green raty-icons" data-target="pid_2" data-score="5.00"></div>
                                    <input id="pid_2" v-model="selectCard.RATING_2" type="text" class="rates" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Питание -->
                    <div class="content_item">
                        <div class="content_left">
                            <div class="content_left_item item_start">
                                <p>Питание</p>
                            </div>
                        </div>
                        <div class="content_right ">
                            <div class="content_right_item p-15">
                                <div class="rating1">
                                    <div id="RATING_3" class="star green raty-icons" data-target="pid_3" data-score="5.00"></div>
                                    <input id="pid_3" v-model="selectCard.RATING_3" type="text" class="rates" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Комментарий к сопровождению мероприятия -->
                    <div class="content_item">
                        <div class="content_left ">
                            <div class="content_left_item item_start m-h55 ai_center">
                                <p>Комментарий <br> к сопровождению мероприятия</p>
                            </div>
                        </div>
                        <div class="content_right">
                            <div class="content_right_item m-h55 p-15">
                                <div class="input_wrap ">
                                <textarea readonly="readonly" class="mh-45"
                                          v-model="card.COMMENT_SERVICE_TYPES"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                <? endif; ?>

                <!-- Сервисы сопровождения -->
                <div class="content_item">
                    <div class="content_left  ">
                        <div class="content_left_item item_start m-h120">
                            <p>Сервисы сопровождения</p>
                        </div>
                    </div>
                    <div class="content_right ">
                        <div class="content_right_item m-h120 p-15">
                            <table>
                                <tr>
                                    <th>Сервис</th>
                                    <? if(!$arCard['FINISH']): ?><th>Статус готовности</th> <? endif; ?>
                                    <? if($arCard['FINISH']): ?><th>Оценка</th><? endif; ?>
                                    <th>Ответственный</th>
                                </tr>
                                <tr v-for="service in selectCard.SERVICES_DATA">
                                    <td><input type="text" class="" readonly="readonly" v-model="service.NAME"></td>
                                    <? if(!$arCard['FINISH']): ?><td><input type="text" class=""
                                                                            readonly="readonly"
                                                                            v-model="service.STATUS.NAME"></td>
                                    <? endif; ?>
                                    <? if($arCard['FINISH']): ?>
                                    <td>
                                        <div class="stars_wrap text-left">
                                            <div class="rating1">
                                                <div class="star green raty-icons" :data-service-id="service.ID" id="pid_974-raty"
                                                     data-target="pid_974" data-score="5.00">
                                                </div>
                                                <input id="pid_974" type="text" value="5" class="rates"
                                                       name="pid_974" v-model="service.RATING">
                                            </div>
                                        </div>
                                    </td>
                                    <? endif; ?>
                                    <td class="text-left">
                                        <span class="phone_num">{{ service.RESPONSIBLE.FIO }}</span>
                                        <span href="#" class="phone_ic" @click="showPhone($event)">
                                            <div class="company-details">
                                                <p>{{ service.RESPONSIBLE.PHONE }}</p>
                                            </div>
                                        </span>
                                    </td>
                                </tr>                                
                            </table>
                        </div>
                    </div>
                </div>

                <!-- История изменений -->
                <div class="content_item">
                    <div class="content_left ">
                        <div class="content_left_item item_start m-h150">
                            <p>История изменений</p>
                        </div>
                    </div>
                    <div class="content_right">
                        <div class="content_right_item item_start m-h150 space_between  fx-wrap">
                            <table class="table_changes">                                
                                <? if(empty($arCard['STORY'])): ?><tr><th>Нет истории изменения...</th></tr><? else: ?>
                                    <tr>
                                        <th>Дата изменения</th>
                                        <th>Кем изменено</th>
                                        <th>Описание изменения</th>
                                        <th>Значение до</th>
                                        <th>Значение после</th>
                                    </tr>
                                    <? foreach ($arCard['STORY'] as $story_one): ?>
                                        <tr>
                                            <td><?=$story_one['DATE_TIME']?></td>
                                            <td><a href="<?=$story_one['CHANGE_BY']['PATH_TO_PROFILE']?>"
                                                ><?=$story_one['CHANGE_BY']['NAME']?></a></td>
                                            <td><?=$story_one['DESCRIPTION']?></td>
                                            <td><?=$story_one['BEFORE']?></td>
                                            <td><?=$story_one['AFTER']?></td>
                                        </tr>
                                    <? endforeach; ?>
                                <? endif; ?>
                            </table>
                            <hr style="width:100%;margin:15px 25px;">
                            <div class="footer_buttons">
                                <a class="btns blue" href="<?=$arCard['PATH_EDIT']?>?DATE=<?=$_REQUEST['DATE']?>">Редактировать</a>
                                <a class="btns black" href="<?=$arParams['PATH_CALENDAR']?>">НАЗАД</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="right_side"></div>
        </div>
    </div>
</main>

<script>
    folder = '<?=$this->GetFolder()?>/';
    app = '';
    $(document).ready(function () {
        var settings = {
            card: <?=CUtil::PhpToJSObject($arCard)?>, subCard: <?=CUtil::PhpToJSObject($arSubCard)?>, selectCard: [],
            summ_rating: 0
        };
        app = initApp(settings);
        app.updateRaiting();
    });

    /*<![CDATA[*/
    $.fn.raty.defaults.path =  '<?=$this->__folder?>/img';
    $.fn.raty.defaults.targetKeep = true;
    $.fn.raty.defaults.targetType = 'number';
    $.fn.raty.defaults.hints = ['bad', 'poor', 'regular', 'good', 'gorgeous'];
    $.fn.raty.defaults.noRatedMsg = 'Not rated yet!';
    $.fn.raty.defaults.cancelHint = 'Cancel this rating!';
    /*]]>*/

    /*jQuery(function ($) {
        jQuery('.raty-icons').raty({'readOnly': true, 'score': '0', 'target': '.rates', 'click': function (score, evt) {
            productRatio(score, $(this).attr('data-target'));
            $(this).raty('readOnly', true);
        }});
        jQuery('.rates').hide();
    });*/
</script>

<div class="FormAddUser_Bitrix">
    <?php
    $APPLICATION->IncludeComponent(
        'bitrix:tasks.widget.member.selector', '', array(
        'MAX_WIDTH' => 1200,
        'TYPES' => array('USER', 'USER.EXTRANET', 'USER.MAIL'),
        'INPUT_PREFIX' => 'USERS_LIST',
        'ATTRIBUTE_PASS' => array('ID','NAME','LAST_NAME','EMAIL'),
    ), false, array("HIDE_ICONS" => "Y", "ACTIVE_COMPONENT" => "Y"));
    ?>
</div>