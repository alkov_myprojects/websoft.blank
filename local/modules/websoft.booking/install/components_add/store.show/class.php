<?php
defined('B_PROLOG_INCLUDED') || die;

use Academy\CrmStores\Entity\StoreTable;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

class CAcademyCrmStoresStoreShowComponent extends CBitrixComponent
{
    const FORM_ID = 'CRMSTORES_SHOW';

    public function __construct(CBitrixComponent $component = null)
    {
        parent::__construct($component);

        CBitrixComponent::includeComponentClass('academy.crmstores:stores.list');
        CBitrixComponent::includeComponentClass('academy.crmstores:store.edit');
    }

    public function executeComponent()
    {
        global $APPLICATION;

        $APPLICATION->SetTitle(Loc::getMessage('CRMSTORES_SHOW_TITLE_DEFAULT'));

        if (!Loader::includeModule('academy.crmstores')) {
            ShowError(Loc::getMessage('CRMSTORES_NO_MODULE'));
            return;
        }

        $dbStore = StoreTable::getById($this->arParams['STORE_ID']);
        $store = $dbStore->fetch();

        if (empty($store)) {
            ShowError(Loc::getMessage('CRMSTORES_STORE_NOT_FOUND'));
            return;
        }

        $APPLICATION->SetTitle(Loc::getMessage(
            'CRMSTORES_SHOW_TITLE',
            array(
                '#ID#' => $store['ID'],
                '#NAME#' => $store['NAME']
            )
        ));

        $this->arResult =array(
            'FORM_ID' => self::FORM_ID,
            'TACTILE_FORM_ID' => CAcademyCrmStoresStoreEditComponent::FORM_ID,
            'GRID_ID' => CAcademyCrmStoresStoresListComponent::GRID_ID,
            'STORE' => $store
        );

        $this->includeComponentTemplate();
    }
}