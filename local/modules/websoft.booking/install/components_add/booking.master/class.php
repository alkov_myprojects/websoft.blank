<?php
defined('B_PROLOG_INCLUDED') || die;

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

use Websoft\Booking\Entity\BookingCardTable as BookingCard;
use Websoft\Booking\Entity\BookingCardChildTable as BookingCardChild;
use Websoft\Booking\Entity\BookingTypeTable as BookingType;
use Websoft\Booking\Entity\CompanyBookingTable as CompanyBooking;

class CBookingMasterComponent extends CBitrixComponent {

    public static function convertToHoursMins($time, $format = '%d:%02d') {
        if ($time < 1) {
            return;
        }
        $hours = floor($time / 60);
        $minutes = ($time % 60);
        return sprintf($format, $hours, $minutes);
    }

    /**
     * Получение переговорных, которые распределены по категориям
     */
    public static function getNegotiationCategories($arFilter,$DATE_BEGIN,$DATE_END,$ExcludeChildCardsFromSearch = array()) {

        //region Поиск подходящей переговорной
        $negs = \Websoft\Booking\Entity\NegotiatedTable::getListPure(true,$arFilter,array(
            'ID','NAME','COUNT_PLACE','ADDRESS','COMPANY_ID','UF_PREVIEW_IMAGE','UF_SERVICE_TYPES'
        ));

        // Получить расписание переговорных на текущую дату
        CBitrixComponent::includeComponentClass("websoft.booking:booking.main");
        $CBookingMainComponent ='CBookingMainComponent';
        $dateIntervals = $CBookingMainComponent::dateIntervals();

        //region Получить переговрные (дата,компании,переговорны)

        $date_time_start = $date = date('d.m.Y',strtotime($DATE_BEGIN));

        //... Собрать данные расписания по переговорным
        $arNegotiated = $CBookingMainComponent::getNegotiated($arFilter['COMPANY_ID']);
        $begin = new \DateTime($date_time_start);
        $end = new \DateTime($date_time_start);
        $end->modify('+1 day');
        $interval = new \DateInterval('PT'.$dateIntervals['DateInterval_Min'].'M');
        $daterange = new \DatePeriod($begin, $interval, $end);
        foreach ($arNegotiated as &$negLink) {
            $Interpreter = \Websoft\Booking\Entity\NegotiatedTable::GetInterpreter($negLink['ID'], $date, $daterange,$ExcludeChildCardsFromSearch);
            $negLink['ITEMS'] = $Interpreter;
        }

        // Распечатать результат
        $BUSY_NEGS = array();
        foreach ($arNegotiated as $negInterpreter) {
            //echo 'ID: '.$negInterpreter['ID'].'<br>';
            foreach ($negInterpreter['ITEMS'] as $ITEM) {
                if($ITEM['TYPE'] != 'EMPTY') {
                    $DATE_FROM_L = strtotime($ITEM['DATA']['DATE_FROM']);
                    $DATE_TO_L = strtotime($ITEM['DATA']['DATE_TO']) - 1;
                    $DATE_BEGIN_L = strtotime($DATE_BEGIN);
                    $DATE_END_L = strtotime($DATE_END);
                    if(!( $DATE_BEGIN_L > $DATE_TO_L || $DATE_END_L < $DATE_FROM_L )) { $BUSY_NEGS[] = $negInterpreter['ID']; }
                }
            }
        }
        //endregion

        // Учёт расписания (Занятые переговорные на это время)
        $negsCategories = array('FREE' => array(), 'BUSY' => array());
        foreach ($negs as $neg) { // логика распределения переговорных (Свободные/Занятые)
            if(in_array($neg['ID'],$BUSY_NEGS)) { $negsCategories['BUSY'][] = $neg;}
            else { $negsCategories['FREE'][] = $neg; }
        }
        return $negsCategories;
    }

    public function executeComponent() {

        //region данные для результирующиего массива
        $DATE = array(); // Дата/начало/окнчание мероприятия
        $CountUsers = 1; // Кол-во участников

        // Список копаний
        $listCorrectNeg = \Websoft\Booking\Entity\NegotiatedTable::GetCorrectElements();
        $idsCompany = array();
        foreach ($listCorrectNeg as $neg) { $idsCompany[] = $neg['COMPANY_ID']; }
        $listCompany = \Websoft\Booking\Entity\CompanyBookingTable::getListPure(true,array('ID'=>$idsCompany),array('ID','NAME','UF_LOGO'));
        foreach ($listCompany as &$company) {
            //$logo = \CFile::GetFileArray($company['UF_LOGO']); $company['UF_LOGO'] = $logo['SRC'];
            $logo = CFile::ResizeImageGet($company['UF_LOGO'], array('width'=>118, 'height'=>45),BX_RESIZE_IMAGE_PROPORTIONAL);
            $company['UF_LOGO'] = $logo['src'];
            $company['SELECTED'] = false;
        }


        // Типы мероприятий
        $BookingType = \Websoft\Booking\Entity\BookingTypeTable::getListPure(
            true,array('ACTIVE'=>true),array('ID','NAME','DESCRIPTION','UF_SERVICE_TYPES')
        );
        foreach ($BookingType as &$bookingType) { $bookingType['SELECTED'] = false; }

        // Сервисы сопровождения
        $ServiceTypes = \Websoft\Booking\Entity\ServiceTypesTable::getListPure(
            true,array('ACTIVE'=>true),array('ID','NAME','UF_ICON','UF_ICON_ACTIVE'),array(
                //'UF_SORT' => 'ACS'
                'UF_SORT' => 'ASC', //'DESC'
            )
        );
        foreach ($ServiceTypes as &$serviceType) {
            $icon = \CFile::GetFileArray($serviceType['UF_ICON']);
            $serviceType['UF_ICON'] = $icon['SRC'];
            $icon_active = \CFile::GetFileArray($serviceType['UF_ICON_ACTIVE']);
            $serviceType['UF_ICON_ACTIVE'] = $icon_active['SRC'];
            $serviceType['SELECTED'] = false;
        }

        //endregion

        // Если есть рекуэст с ID переговорной и датой, то:
        if(isset($_REQUEST['DATE']) && isset($_REQUEST['ID_NEG'])) {
            
            //region Дата, время начала/окончания
            $date = date_create($_REQUEST['DATE']);
            $DATE['DATE_BEGIN'] = date_format($date, 'd.m.Y');
            $DATE['TIME_START'] =  (intval($date->format('H')) * 60) + intval($date->format('i'));
            $date_time_start = date_format($date, 'Y-m-d H:i');
            $end = new \DateTime($date_time_start);
            $end->modify( '+90 minutes' );

            if($DATE['TIME_START'] >= 1170) { $DATE['TIME_END'] = 1260; }
            else { $DATE['TIME_END'] = (intval($end->format('H')) * 60) + intval($end->format('i')); }
            //endregion
            
            $neg = \Websoft\Booking\Entity\NegotiatedTable::getListPure(false,array('ID' => $_REQUEST['ID_NEG']),array('*','UF_*'));
            
            //region Определение компанию/кол-во участников/сервисы сопровождения/тип мероприятия по переговорной
            foreach ($listCompany as &$company) { // Выделить компании
                if($company['ID'] == $neg['COMPANY_ID']) { $company['SELECTED'] = true; }
            }
            //$CountUsers = $neg['COUNT_PLACE']; // Кол-во участников

            // Выделить сервисы сопровождения
            //$SERVICE_TYPES = $neg['UF_SERVICE_TYPES'];
            //foreach ($ServiceTypes as &$serviceType) {
                //if(in_array($serviceType['ID'],$SERVICE_TYPES)) { $serviceType['SELECTED'] = true; }
            //}
        }
        //endregion


        // Выделить тип мероприятия
        foreach ($BookingType as &$bookingType_L) {
            if($bookingType_L['NAME'] == 'Другое') { $bookingType_L['SELECTED'] = true; break; } // Фишка заказчика
            /*$service_types_booking = $bookingType['UF_SERVICE_TYPES'];
            if(count($service_types_booking) != count($SERVICE_TYPES)) { continue;}
            else {
                $diff_1 = array_diff($service_types_booking,$SERVICE_TYPES);
                $diff_2 = array_diff($SERVICE_TYPES,$service_types_booking);
                if(count($diff_1) == 0 && count($diff_2) == 0 ) { $bookingType['SELECTED'] = true; break; }
            }*/
        }

        /** Подключение и передача данных компоненту: */
        $this->arResult = array(
            'UF_COMPANIES_ID' => $listCompany,
            'DATE' => $DATE,
            'COUNT' => 2,//$CountUsers,
            'BOOKING_TYPE_ID' => $BookingType,
            'UF_SERVICE_TYPES_ID' => $ServiceTypes,
            'COMPANY_ID_THIS_USER' => $this->arParams['COMPANY_ID_THIS_USER']
        );
        
        $this->includeComponentTemplate();
    }
}