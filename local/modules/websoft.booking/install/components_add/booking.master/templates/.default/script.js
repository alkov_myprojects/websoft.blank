/**
 * Удаление перерыва
 * @param obj - ссылка на текущеий html объект
 */
function removeBreak(obj) {
    $(obj).parent().parent().remove();
}

/**
 * Добавление перерыва
 * @param obj - ссылка на текущеий html объект
 */
function addBreak(folder) {
    console.log('folder: ' + folder);
    tr  = '<tr>';
    tr += '<td><input type="text" placeholder="Название"></td>' +
          '    <td><span>с</span></td>' +
          '    <td><input class="time" type="time" placeholder="00:00" pattern="[0-9]{2}:[0-9]{2}"></td>' +
          '    <td><span>по</span></td>' +
          '    <td><input class="time" type="time" placeholder="00:00" pattern="[0-9]{2}:[0-9]{2}"></td>' +
          '    <td>' +
          '        <a  href="#"' +
          '            class="break_input_delete"' +
          '            onclick="removeBreak(this); return false;"><img' +
          '            src="' + folder + '/img/close_lines.png"' +
          '            id="delete"></a>' +
          '    </td>';
    tr += '</tr>';

    $('#listBreaksItem').append(tr); //.appendTo(tr);
}

/**
 * Установка даты из браузера пользователя
 * */
function set_user_date() {

    date = new Date();

    //region Время
    start = (date.getHours()*60)+date.getMinutes();
    end = start + 90;
    jQuery("#slider_time").slider({values: [start, end]});
    $('#slider_time #minCost').html(convertMinsToHrsMins(start));
    $('#slider_time #maxCost').html(convertMinsToHrsMins(end));
    setFilterParam('TIME_START',convertMinsToHrsMins(start),false,true);
    setFilterParam('TIME_END',convertMinsToHrsMins(end),false,true);
    setIntervalTimePlus(90);
    //endregion

    //region Дата
    date = ('0' + date.getDate()).slice(-2) + '.' + ('0' + (date.getMonth() + 1)).slice(-2) + '.' + date.getFullYear();
    $('#datepicker').val(date);
    setFilterParam('DATE_BEGIN',date);
    //endregion
}

function sliceTime(time) { return ('0' + time).slice(-2); }

/**
 * Установка времени правого ползунка, по отношению к левому
 * @param minutesInterval - время на которое нужно сдвинут правый ползунок, от левого
 */
function setIntervalTimePlus(minutesInterval) {
    values = $('#slider_time').slider('values');
    value_2 = values[0]+minutesInterval;
    if(value_2 > 1260) { value_2 = 1260; }
    setIntervalTime(values[0],value_2);
}

/**
 * Установка точного времени
 * @param TimeStart - время левого ползунка
 * @param TimeEnd - время правого ползунка
 */
function setIntervalTime(TimeStart, TimeEnd) {
    $('.time_slider-item span').removeClass('active');
    $('.time_slider-item.' + (TimeEnd - TimeStart) + ' span').addClass('active');
    jQuery("#slider_time").slider({values: [TimeStart, TimeEnd]});
    $('.slider-time').html(convertMinsToHrsMins(TimeStart));
    $('.slider-time2').html(convertMinsToHrsMins(TimeEnd));
    setFilterParam('TIME_START',convertMinsToHrsMins(TimeStart));
    setFilterParam('TIME_END',convertMinsToHrsMins(TimeEnd));
}

/**
 * @param minutes - Минуты, преобразуются в часы:минуты
 * @returns {string} - формат 0:00
 */
function convertMinsToHrsMins(minutes) {
    var h = Math.floor(minutes / 60);
    var m = minutes % 60;
    h = h < 10 ? h : h;
    m = m < 10 ? '0' + m : m;
    return h + ':' + m;
}

/**
 * ------------------------ AJAX ---------------------
 */
FilterGlobal = {};

/**
 * Дабавить параметр фильтра
 * @param code - Символьный код (ключ,параметр фильтра) ассоциативного массива
 * @param val - Значение ассоциативного массива
 * @param add - bool, по умолчанию false, заменяет значение. Если установить в true, то дабавит значение в массив, иначе
 */
function setFilterParam(code,val,add,dontReload) {
    //region debug reload
    if(dontReload) { console.log('code: ' + code + '; no_reload'); }
    else { console.log('code: ' + code + '; reload'); }
    //endregion

    if(add) {
        if(!FilterGlobal[code]) { FilterGlobal[code] = []; }
        add = true;
        for(i=0;i<=FilterGlobal[code].length;i++) { if(val === FilterGlobal[code][i]) { add = false; break; } }
        if(add) { FilterGlobal[code].push(val); }
    } else { FilterGlobal[code] = val; }
    if(!dontReload) { getNegotiated(); }
    //removeFilterParam('NEG_IDS');
    //console.log('setFilterParam:');
    //console.log(FilterGlobal);
}

/**
 * Удалить значение параметра
 * @param code - Символьный код (ключ,параметр фильтра) ассоциативного массива
 * @param val - Значение ассоциативного массива
 */
function removeFilterParam(code,val,dontReload) {
    if(FilterGlobal[code]) {
        if(Array.isArray(FilterGlobal[code])) { if(val){FilterGlobal[code].remove(val);}else {FilterGlobal[code] = [];}}
        else { FilterGlobal[code] = val; }
    }
    if(!dontReload) { getNegotiated(); }
    //console.log(FilterGlobal);
}

/**
 * Получить ассоциативный массив д/передачи ajax-запросу
 * @returns {{}}
 */
function getFilter() { return FilterGlobal; }

/**
 * Удаление элемента массива по значение
 * @returns {Array}
 */
Array.prototype.remove = function() {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) { this.splice(ax, 1); }
    }
    return this;
};

folderGlobal = ''; // Папка где лежат обработчики ajax-запросов
ajax_request_neg = ""; // Д/прерывания старого ajax-запроса
function getNegotiated(folder) {
    if(folder && !folderGlobal) { folderGlobal = folder; }
    //$('#listNegs').fadeOut(function () { // Убрать старые данные
        $('#preloader').fadeIn(function () { // Показать
            if(ajax_request_neg){ ajax_request_neg.abort(); }
            ajax_request_neg = $.ajax({
                url: folderGlobal + '/ajax.php', dataType: "html", data: getFilter(), success: function(data) {
                    removeFilterParam('NEG_IDS',false,true);
                    upload_negs();
                    $('#listNegs').html(data); // Загрузить данные
                    $('#preloader').fadeOut(function () { $('#listNegs').fadeIn(); }); // скрыть прелоадер
                }
            });
        });
    //});
}


/**
 * ------------------------ Переговорные ---------------------
 */
function addNeg(neg_id) {

}
function delNeg(neg_id) {

}

ajax_request_neg_2 = false;
function upload_negs() {
    main = '#upload_negs ';
    $(main + "#preloader_2").stop();
    $(main + ".data_negs_users").stop();
    $(main + ".not_negs").stop();
    if(typeof(FilterGlobal['NEG_IDS']) != 'undefined' ) {
        if (FilterGlobal['NEG_IDS'].length == 0) {
            $(main + "#preloader_2").fadeOut(function () {
                $(main + ".data_negs_users").fadeOut(function () { $(main + ".not_negs").fadeIn(); });
            });
        } else {
            $(main + ".not_negs").fadeOut(function () {
                $(main + ".data_negs_users").fadeOut(function () {
                    $(main + "#preloader_2").fadeIn(function () {
                        $(main + ".data_negs_users table").css('display', 'none');
                        for (i = 0; i < FilterGlobal['NEG_IDS'].length; i++) {
                            $(main+".data_negs_users #table_NEG_ID_"+FilterGlobal['NEG_IDS'][i]).css('display','block');
                        }
                        $(main + "#preloader_2").fadeOut(function () { $(main + ".data_negs_users").fadeIn(); });
                    });
                });
            });
        }
    }
    after_reload_filter();
}

/**
 * ----------------------------- Контроль -----------------------
 * */
function controllMaxValue(obj) {
    max = $(obj).attr('max');
    if(max) {
        max = parseInt(max);
        value_int = parseInt($(obj).val());
        if(value_int > max) { $(obj).val(max); }
    }
}

/**
 * ----------------------------- Збаровнировать -----------------------
 * */
function printErrors(errors) {
    var PopupBX = new BX.MyPopupBX();
    errors_str = '<ul>'; for (i=0;i<errors.length;i++) { errors_str += errors[i]; } errors_str += '</ul>';
    PopupBX.Message('errors_add_booking','',errors_str,true);
}
function ToBook() {
    filter = getFilter();
    DESCRIPTION = $("#more_info").val();
    NedUsers = $('#NedUsers').serialize();

    //region Проверки
    errors = [];
    if(typeof(filter['NEG_IDS']) === 'undefined') { errors.push('<li> - Не выбранно ни одной переговорной.</li>'); }
    else if(!filter['NEG_IDS'].length) { errors.push('<li> - Не выбранно ни одной переговорной.</li>'); }
    if(typeof(filter['BOOKING_TYPE_ID']) === 'undefined') { errors.push('<li> - Не указан вид бронирования.</li>'); }
    //if(!DESCRIPTION) { errors.push('<li> - Поле <b>Описание мероприятия</b> обязательно для заполнения.</li>'); }
    //endregion
    
    if(errors.length == 0) {
        if(typeof(filter['UF_COMPANIES_ID']) !== 'undefined') {

            if(filter['UF_COMPANIES_ID'].length > filter['NEG_IDS'].length) {
                // Если компаний больше, чем переговорных, то пользователь не знает какую переговорную выбрать.
                // нужно уведомить администратора компании, чтобы он установил переговорную
                var PopupBX = new BX.MyPopupBX();
                PopupBX.AddBtn(
                    'Принимаю', function(){ PopupBX.CloseLastPopup(), _ToBook(filter,DESCRIPTION,NedUsers); },true
                );
                PopupBX.Message(
                    'not_full_data','Не полноценные данные',
                    'Не для всех компаний были назначены переговорные.<br><br>' +
                    'Бронирование будет на рассмотрение администратора. После того как ' +
                    'администратор назначит оставшиеся переговорные, бронь вступит в полную силу.<br><br>' +
                    'На момент рассмотрения брони администратором, выбранные переговорные будут зарезервированы ' +
                    'за вами, т.е. их ни кто не займёт.<br><br>' +
                    'Если вы согласны на данные условия, то нажмите "Принять", иначе "Закрыть".',
                    true
                );
            } else { _ToBook(filter,DESCRIPTION,NedUsers); }
        } else { _ToBook(filter,DESCRIPTION,NedUsers); }
    } else {
        printErrors(errors);
        /*var PopupBX = new BX.MyPopupBX();
        errors_str = '<ul>'; for (i=0;i<errors.length;i++) { errors_str += errors[i]; } errors_str += '</ul>';
        PopupBX.Message('errors_add_booking','',errors_str,true);*/
    }
}
function _ToBook(filter,DESCRIPTION,NedUsers) {

    //region получить RRule и Remind
    RRuleAdnRemind = $('#Repeatability').serialize();
    //endregion

    if(true) {
        var PopupBX = new BX.MyPopupBX();
        PopupBX.Message('wait_reservation','','Анализ данных, ожидайте....');
        BX.MyAjax.Get2('booking/addBookingCard.php?' + NedUsers + '&' + RRuleAdnRemind,{
            'EXECUTOR_COMPANY_ID':UserCompanyID, // Компания Организатора
            'EXECUTOR_BY_ID':UserID,           // Организатор
            filter:filter,
            DESCRIPTION:DESCRIPTION
        },'json',function (data) {
            PopupBX.CloseLastPopup();
            if(data['success']) {
                var PopupBX_2 = new BX.MyPopupBX();
                PopupBX_2.AddBtn('Расписание',function () {
                    document.location.href = "/booking/?DATE=" + filter['DATE_BEGIN'];
                });
                PopupBX_2.AddBtn('Просмотр брони',function () {
                    document.location.href = "/booking/card/" + data['data']['id'] + "/?DATE=" + filter['DATE_BEGIN'];
                });
                PopupBX_2.AddBtn('Редактирование брони',function () {
                    document.location.href = "/booking/card/"+data['data']['id']+"/edit/?DATE="+filter['DATE_BEGIN'];
                });
                PopupBX_2.AddBtn('Закрыть',function () { location.reload(); /*PopupBX_2.CloseLastPopup();*/ });
                PopupBX_2.Message('success_reservation','Успешное бронирование','Выберете дальнейшее действие:');
            } else { printErrors(data['data']['errors']); } // Вывод ошибок

        });
    }
}


