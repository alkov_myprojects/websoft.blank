<?php
defined('B_PROLOG_INCLUDED') || die;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Page\Asset;
use Bitrix\Main\Web\Json;

$APPLICATION->SetTitle('Мастер бронирования');

//region CSS
Asset::getInstance()->addCss($this->__folder . "/css/style.css");
Asset::getInstance()->addCss($this->__folder . "/css/BsMultiSelect.css");
Asset::getInstance()->addCss("/bitrix/js/calendar/cal-style.css");
//endregion

//region JS
//Asset::getInstance()->addJs('https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js');
Asset::getInstance()->addJs($this->__folder . "/js/vue.js");
Asset::getInstance()->addJs($this->__folder . "/js/BsMultiSelect.js");
//endregion

?>

<style>
    <? foreach ($arResult['UF_SERVICE_TYPES_ID'] as $serviceType): ?>
        .service_id_<?=$serviceType['ID']?>{ background: url(<?=$serviceType['UF_ICON']?>) center no-repeat; }
        .service_id_<?=$serviceType['ID']?>.active{ background: url(<?=$serviceType['UF_ICON_ACTIVE']?>) center no-repeat; }
    <? endforeach; ?>
</style>

<style>
    @media all and (-ms-high-contrast:none) {

        /* IE10 */
        /*#pagetitle { color: green !important; }*/

        /* IE11 */
        *::-ms-backdrop, .ui-slider { top: 31px !important; }
        *::-ms-backdrop, .sliderCont .ui-slider { top: 25px !important; }
        *::-ms-backdrop, #value { top: 31px !important; }
        *::-ms-backdrop, #minpeople { top: 22px !important; }
        *::-ms-backdrop, .content_left_item { padding-top: 10px; }
        *::-ms-backdrop, .btns { margin: 0 auto; }
    }
</style>

<script>
    UserID = '<?=$USER->GetID()?>'; // ID Организатора
    UserCompanyID = '<?=$arResult['COMPANY_ID_THIS_USER']?>'; // Компания Организатора
</script>

<? ob_start(); ?>
<a href="/booking/" class="btn_master">ГЛАВНАЯ СТРАНИЦА</a>
<?$APPLICATION->AddViewContent('pagetitle', ob_get_contents()); ob_end_clean();?>


<? /*
<div style="margin: 100px">
    <select name="states" id="example" class="form-control" multiple="multiple" >
        <option value="AL">Alabama</option>
        <option value="AK">Alaska</option>
        <option value="AZ">Arizona</option>
        <option value="AR">Arkansas</option>
        <option selected value="CA">California</option>
        <option value="CO">Colorado</option>
        <option value="CT">Connecticut</option>
        <option value="DE">Delaware</option>
        <option value="DC">District Of Columbia</option>
        <option value="FL">Florida</option>
        <option value="GA">Georgia</option>
        <option value="HI">Hawaii</option>
        <option value="ID">Idaho</option>
        <option value="IL">Illinois</option>
        <option value="IN">Indiana</option>
        <option value="IA">Iowa</option>
        <option value="KS">Kansas</option>
        <option value="KY">Kentucky</option>
        <option value="LA">Louisiana</option>
        <option value="ME">Maine</option>
        <option value="MD">Maryland</option>
        <option value="MA">Massachusetts</option>
        <option value="MI">Michigan</option>
        <option value="MN">Minnesota</option>
        <option value="MS">Mississippi</option>
        <option value="MO">Missouri</option>
        <option value="MT">Montana</option>
        <option value="NE">Nebraska</option>
        <option value="NV">Nevada</option>
        <option value="NH">New Hampshire</option>
        <option value="NJ">New Jersey</option>
        <option value="NM">New Mexico</option>
        <option value="NY">New York</option>
        <option value="NC">North Carolina</option>
        <option value="ND">North Dakota</option>
        <option value="OH">Ohio</option>
        <option value="OK">Oklahoma</option>
        <option value="OR">Oregon</option>
        <option selected value="PA">Pennsylvania</option>
        <option value="RI">Rhode Island</option>
        <option value="SC">South Carolina</option>
        <option value="SD">South Dakota</option>
        <option value="TN">Tennessee</option>
        <option value="TX">Texas</option>
        <option value="UT">Utah</option>
        <option value="VT">Vermont</option>
        <option value="VA">Virginia</option>
        <option value="WA">Washington</option>
        <option value="WV">West Virginia</option>
        <option value="WI">Wisconsin</option>
        <option value="WY">Wyoming</option>
    </select>
</div>
<script>$("select#example").bsMultiSelect();</script>

<style>
    .dropdown-menu {
        margin-top: 0;
        border: none;
        -webkit-box-shadow: 0 1px 4px rgba(0,0,0,0.3);
        box-shadow: 0 1px 4px rgba(0,0,0,0.3);
        position: absolute;
        top: 100%;
        left: 0;
        z-index: 1000;
        display: none;
        float: left;
        min-width: 10rem;
        padding: 0.5rem 0;
        margin: 0.125rem 0 0;
        font-size: 0.8125rem;
        color: #444;
        text-align: left;
        list-style: none;
        background-color: #fff;
        background-clip: padding-box;
        border: 1px solid rgba(0,0,0,0.15);
        border-radius: 0.25rem;
    }
    button.close {
        padding: 0;
        background-color: transparent;
        border: 0;
        -webkit-appearance: none;
    }
    .close:not(:disabled):not(.disabled) {
        cursor: pointer;
    }
    .close {
        float: right;
        font-size: 2.125rem;
        font-weight: 300;
        line-height: 1;
        color: #000;
        text-shadow: 0 1px 0 #fff;
        opacity: .5;
        line-height: 0.5;
        opacity: 0.6;
        -webkit-transition: all 0.2s;
        transition: all 0.2s;
    }
</style>
*/?>


<main>
    <div class="center_side">
        <!--
        <div class="breadcrumbs">
            <li><a href="/booking/">Бронирование переговорных</a></li>
            <li><a href="#" class="active">Мастер бронирования</a></li>
        </div>
        -->
        <div class="content_sides">
            <div class="content_side_center">

                <!-- Компания -->
                <div class="content_item">
                    <div class="content_left first_color">
                        <div class="content_left_item  m-h74">
                            <img src="<?=$this->__folder?>/img/company-ic.png" alt="">
                            <p>Компания</p>
                        </div>
                    </div>
                    <div class="content_right" style="overflow:hidden;">
                        <div class="content_right_item m-h74">
                            <div class="" style="width: 615px; margin-left: 35px">
                                <div class="company_slider">
                                    <? foreach ($arResult['UF_COMPANIES_ID'] as $company): ?>
                                        <div class="company_slider-item <?if($company['SELECTED']){?>active<?}?>"
                                             onclick="setFilterParam('UF_COMPANIES_ID','<?=$company['ID']?>',true);"
                                            >
                                            <a href="javascript:void(0);" class="close_slide"
                                               onclick="removeFilterParam('UF_COMPANIES_ID','<?=$company['ID']?>');">

                                            </a>

                                            <div class="test"style="
                                                    background-image: url(<?=$company['UF_LOGO']?>);
                                                    width: 98px;
                                                    height: 45px;
                                                    background-size: contain !important;
                                                    background-repeat: no-repeat !important;
                                                    background-position: center;
                                                    background-color: #ffffff;
                                                    "></div>
                                            <?/*<img src="<?=$company['UF_LOGO']?>" alt="">*/?>
                                        </div>
                                    <? endforeach; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Дата -->
                <div class="content_item">
                    <div class="content_left second_color ">
                        <div class="content_left_item m-h55">
                            <img src="<?=$this->__folder?>/img/calend_ic.png" alt="">
                            <p>Дата</p>
                        </div>
                    </div>
                    <div class="content_right bg_white">
                        <div class="datepicker">
                            <p><input type="text" id="datepicker"
                                      value="<?if(!empty($arResult['DATE'])){ echo $arResult['DATE']['DATE_BEGIN']; }?>"></p>
                        </div>
                    </div>
                </div>
                <div class="divider">
                    <div class="content_left"></div>
                    <div class="content_right"></div>
                </div>
                <script>
                    setFilterParam('DATE_BEGIN','<?=$arResult['DATE']['DATE_BEGIN']?>',false,true);
                </script>

                <!-- Время -->
                <div class="content_item">
                    <div class="content_left second_color">
                        <div class="content_left_item  m-h95 fs-start">
                            <img src="<?=$this->__folder?>/img/clock.png" alt="">
                            <p>Время</p>
                        </div>
                    </div>
                    <div class="content_right">
                        <div class="content_right_item  bg_white m-h95 space_between flex-column">
                            <div class="time_slider pl-40 " style="max-width: 663px;margin: 0 auto;">
                                <? $timeStart = $arResult['DATE']['TIME_START']; ?>
                                <!--<div class="time_slider-item 30"><span onclick="setIntervalTime(0,30);">0.5 часа</span></div>
                                <div class="time_slider-item 60"><span onclick="setIntervalTime(0,60);">1 час</span></div>-->
                                <div class="time_slider-item 90"><span onclick="setIntervalTimePlus(90);">1.5 часа</span></div>
                                <div class="time_slider-item 120"><span onclick="setIntervalTimePlus(120);">2 часа</span></div>
                                <div class="time_slider-item 150"><span onclick="setIntervalTimePlus(150);">2.5 часа</span></div>
                                <div class="time_slider-item 180"><span onclick="setIntervalTimePlus(180);">3 часа</span></div>
                                <div class="time_slider-item 210"><span onclick="setIntervalTimePlus(210);">3.5 часа</span></div>
                                <div class="time_slider-item 240"><span onclick="setIntervalTimePlus(240);">4 часа</span></div>
                                <div class="time_slider-item 270"><span onclick="setIntervalTimePlus(270);">4.5 часа</span></div>
                                <div class="time_slider-item 300"><span onclick="setIntervalTimePlus(300);">5 часов</span></div>
                                <div class="time_slider-item 330"><span onclick="setIntervalTimePlus(330);">5.5 часа</span></div>
                                <div class="time_slider-item 360"><span onclick="setIntervalTimePlus(360);">6 часов</span></div>
                                <div class="time_slider-item 420"><span onclick="setIntervalTimePlus(420);">7 часов</span></div>
                            </div>
                            <div class="sliderCont">
                                <div id="slider_time">
                                    <div class="triangle" id="col-1">
                                        <span class="slider-time" id="minCost"><?
                                        echo CBookingMasterComponent::convertToHoursMins($arResult['DATE']['TIME_START']);
                                        ?></span>
                                    </div>
                                    <div class="triangle" id="col-2">
                                        <span class="slider-time2" id="maxCost"><?
                                        echo CBookingMasterComponent::convertToHoursMins($arResult['DATE']['TIME_END']);
                                        ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="divider">
                    <div class="content_left"></div>
                    <div class="content_right"></div>
                </div>

                <!-- Кол-во участников -->
                <div class="content_item">
                    <div class="content_left second_color">
                        <div class="content_left_item  m-h70 lh_init">
                            <img src="<?=$this->__folder?>/img/peoples.png" alt="">
                            <p>Кол-во <br> участников</p>
                        </div>
                    </div>
                    <div class="content_right bg_white">
                        <div class="content_right_item m-h70 fx-center ">
                            <input type="text" id="minpeople" value="<?=$arResult['COUNT']?>">
                            <div class='sliderCont sec_cont mb0'>

                                <div id="slider_peoples"></div>
                                <div id="valuecontainer">
                                    <span id="value"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="divider">
                    <div class="content_left"></div>
                    <div class="content_right"></div>
                </div>

                <!-- Вид бронирования -->
                <div class="content_item">
                    <div class="content_left second_color">
                        <div class="content_left_item lh_init mb-init m-h70">
                            <img src="<?=$this->__folder?>/img/event_type.png" alt="">
                            <p>Вид<br>бронирования</p>
                        </div>
                    </div>
                    <div class="content_right bg_white">
                        <div class="content_right_item m-h50 space_between pr-10">
                            <div class="type_order">
                                <? foreach ($arResult['BOOKING_TYPE_ID'] as $bookingType): ?>
                                    <div
                                        class="order_item <?if($bookingType['SELECTED']){?>active<?}?>"
                                        data-arr-id-services="<?=json_encode($bookingType['UF_SERVICE_TYPES'])?>"
                                        data-id-booking="<?=$bookingType['ID']?>"
                                        data-description-type-booking="<?=$bookingType['DESCRIPTION']?>"
                                    ><?=$bookingType['NAME']?></div>
                                    <?if($bookingType['SELECTED']):?>
                                        <script>
                                            setFilterParam('BOOKING_TYPE_ID',<?=$bookingType['ID']?>);
                                        </script>
                                    <?endif;?>
                                <? endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="divider">
                    <div class="content_left"></div>
                    <div class="content_right"></div>
                </div>

                <!-- Сервисы сопровождения -->
                <div class="content_item">
                    <div class="content_left second_color" style="z-index: 1;">
                        <div class="content_left_item lh_init mb-init slide_comp m-h70">
                            <img src="<?=$this->__folder?>/img/cup_tea.png" alt="">
                            <p>Сервисы<br>сопровож-<br>дения</p>
                        </div>
                    </div>
                    <div class="content_right bg_white">
                        <div class="m-h70 pr-10">
                            <div class="service_wrap">
                                <? foreach ($arResult['UF_SERVICE_TYPES_ID'] as $serviceType): ?>
                                    <div class="service_item">
                                        <p data-service-id="<?=$serviceType['ID']?>"
                                           class="service_img service_id_<?=$serviceType['ID']?> <? if(/*$serviceType['SELECTED']*/false){?>activ<?}?>"></p>
                                        <div class="service_item-details"><?=$serviceType['NAME']?></div>
                                    </div>

                                    <? if($serviceType['SELECTED']):?>
                                        <script>
                                            //setFilterParam('UF_SERVICE_TYPES_ID',<?=$serviceType['ID']?>,true);
                                        </script>
                                    <? endif; ?>

                                <? endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="divider">
                    <div class="content_left"></div>
                    <div class="content_right"></div>
                </div>

                <!-- Описание мероприятия -->
                <div class="content_item">
                    <div class="content_left second_color">
                        <div class="content_left_item lh_init mb-init m-h110 fx-start">
                            <img src="<?=$this->__folder?>/img/more_info.png" alt="">
                            <p>Описание<br>мероприятия</p>
                        </div>
                    </div>
                    <div class="content_right bg_white">
                        <div class="content_right_item m-h110 space_between pr-10">
                            <?
                            $description_booking_type = '';
                            foreach ($arResult['BOOKING_TYPE_ID'] as $bookingType) {
                                if($bookingType['SELECTED']){ $description_booking_type = $bookingType['DESCRIPTION']; }
                            }
                            ?>
                            <textarea
                                    name=""
                                    id="more_info"
                                    placeholder="<?if(!$description_booking_type) {?>Введите описание мероприятия<?}?>"><?
                                    if($description_booking_type) { echo $description_booking_type; }
                                    ?></textarea>
                        </div>
                    </div>
                </div>
                <div class="divider">
                    <div class="content_left"></div>
                    <div class="content_right"></div>
                </div>

                <!-- Участники меропниятия -->
                <div class="content_item">
                    <div class="content_left second_color">
                        <div class="content_left_item  m-h70 lh_init">
                            <img src="<?=$this->__folder?>/img/partipicant.png" alt="">
                            <p>Участники</p>
                        </div>
                    </div>
                    <div class="content_right bg_white">
                        <div id="upload_negs" class="content_right_item m-h70 pr-10" style="display: block !important;">
                            <div id="preloader_2"></div>
                            <div class="not_negs" >Нет выбранных переговорных...</div>
                            <form id="NedUsers">
                                <div class="data_negs_users">
                                    <?
                                    // Получить все переговорные
                                    $negs = \Websoft\Booking\Entity\NegotiatedTable::getListPure(true,array(),array('ID','NAME'));
                                    global $APPLICATION;
                                    foreach ($negs as $neg) { ?>
                                        <table id="table_NEG_ID_<?=$neg['ID']?>" style="display:none;">
                                            <tr>
                                                <td width="250"><?=$neg['NAME']?></td>
                                                <td><?
                                                    $APPLICATION->IncludeComponent(
                                                        'bitrix:tasks.widget.member.selector', '', array(
                                                            'MAX_WIDTH' => 1200, 'INPUT_PREFIX' => 'NEG_ID_'.$neg['ID'],
                                                            'TYPES' => array('USER', 'USER.EXTRANET', 'USER.MAIL'),
                                                            'ATTRIBUTE_PASS' => array('ID','NAME','LAST_NAME','EMAIL'),
                                                        ), false, array("HIDE_ICONS" => "Y", "ACTIVE_COMPONENT" => "Y")
                                                    );
                                                ?></td>
                                            </tr>
                                        </table>
                                    <? }  ?>
                                </div>
                            </form>
                            <? /*
                            global $APPLICATION;
                            $Members = array('test_1','test_2');
                            foreach ($Members as $member) { ?>
                                <table>
                                    <tr>
                                        <td width="250">Переговорная <?=$member?></td>
                                        <td><?
                                            $APPLICATION->IncludeComponent(
                                                'bitrix:tasks.widget.member.selector', '', array(
                                                    'TEMPLATE_CONTROLLER_ID' => $member,
                                                    'MAX_WIDTH' => 1200,
                                                    'TYPES' => array('USER', 'USER.EXTRANET', 'USER.MAIL'),
                                                    'INPUT_PREFIX' => $member, //$inputPrefix.'[SE_AUDITOR]',
                                                    'ATTRIBUTE_PASS' => array('ID','NAME','LAST_NAME','EMAIL'),
                                                    'DATA' => array(),
                                                ), false, array("HIDE_ICONS" => "Y", "ACTIVE_COMPONENT" => "Y")
                                            );
                                            ?></td>
                                    </tr>
                                </table>
                            <? } */ ?>
                        </div>
                    </div>
                </div>
                <div class="divider">
                    <div class="content_left"></div>
                    <div class="content_right"></div>
                </div>

                <!-- Доп. настройки -->
                <div class="content_item" style="overflow:hidden;">
                    <div class="content_left second_color">
                        <div class="content_left_item lh_init mb-init fx-start">
                            <img src="<?=$this->__folder?>/img/settings.png" alt="">
                            <p>Доп.<br>настройки</p>
                        </div>
                    </div>
                    <div class="content_right ">
                        <div class="content_right_item d-block item_start m-h380 space_between pr-10 pl-65 bxec-popup" id="dop_settings" style="display: none;">

                            <form id="Repeatability">
                                <span class="not_negs">Повторяемость:</span>
                                <select name="EVENT_RRULE_EX[FREQ]" class="calendar-select">
                                    <option value="NONE" >Не повторяется</option>
                                    <option value="DAILY"   data-first="каждый" data-last="день">Каждый день</option>
                                    <option value="WEEKLY"  data-first="каждую" data-last="неделю">Каждую неделю</option>
                                    <option value="MONTHLY" data-first="каждый" data-last="месяц">Каждый месяц</option>
                                    <option value="YEARLY"  data-first="каждый" data-last="год">Каждый год</option>
                                </select>
                                <script>
                                    $('select[name="EVENT_RRULE_EX[FREQ]"]').on('change', function() {
                                        var value = $(this).val();
                                        if(value == 'NONE') {
                                            $('div#INTERVAL_EX').css('display','none');
                                            $('div#COUNT_EX').css('display','none');
                                        }
                                        else {
                                            var data_first = $(this).children(":selected").attr('data-first');
                                            var data_last = $(this).children(":selected").attr('data-last');
                                            $('div#INTERVAL_EX span#FIRST').find('span').html(data_first);
                                            $('div#INTERVAL_EX span#LAST').find('span').html(data_last);
                                            $('div#INTERVAL_EX').css('display','inline-block');
                                            $('div#COUNT_EX').css('display','block');
                                        }

                                        if(value == 'WEEKLY') { $('div#BYDAY').css('display','block'); }
                                        else { $('div#BYDAY').css('display','none'); }
                                    });
                                </script>

                                <div id="INTERVAL_EX" style="display:none;">
                                    <span id="FIRST"><span class="not_negs" style="margin-left: 10px"></span></span>
                                    <select name="EVENT_RRULE[INTERVAL]" class="calendar-select">
                                        <? $days = 35; for ($i=1;$i<=$days;$i++):?>
                                            <option value="<?=$i?>"><?=$i?></option>
                                        <? endfor; ?>
                                    </select>
                                    <span id="LAST"><span class="not_negs" style="margin-left: 10px"></span></span>
                                </div>
                                <div id="BYDAY_EX" style="display:none;">
                                    <label><input name="EVENT_RRULE[BYDAY][]" type="checkbox" value="MO">Пн</label>
                                    <label><input name="EVENT_RRULE[BYDAY][]" type="checkbox" value="TU">Вт</label>
                                    <label><input name="EVENT_RRULE[BYDAY][]" type="checkbox" value="WE">Ср</label>
                                    <label><input name="EVENT_RRULE[BYDAY][]" type="checkbox" value="TH">Чт</label>
                                    <label><input name="EVENT_RRULE[BYDAY][]" type="checkbox" value="FR">Пт</label>
                                    <label><input name="EVENT_RRULE[BYDAY][]" type="checkbox" value="SA">Сб</label>
                                    <label><input name="EVENT_RRULE[BYDAY][]" type="checkbox" value="SU">Вс</label>
                                </div>
                                <div id="COUNT_EX" style="display:none;">

                                    <span class="not_negs">Окончание:</span>
                                    <br>
                                    <br>

                                    <label for="never">
                                        <input name="rrule_endson" id="never" type="radio"  value="never" checked="checked">
                                        <span class="not_negs">Никогда</span>
                                    </label>
                                    <br>
                                    <br>

                                    <label for="endson_count">
                                        <input id="endson_count" name="rrule_endson" type="radio" value="count">
                                        <span class="not_negs">После</span>
                                        <input class="calendar-inp" style="width: 60px" type="number" min="1" max="100" name="EVENT_RRULE[COUNT]"
                                               placeholder="10" value="10"
                                               onchange="controllMaxValue(this);"
                                        >
                                        <span class="not_negs">повторений</span>
                                    </label>
                                    <br>
                                    <br>

                                    <label for="rrule_endson">
                                        <input id="rrule_endson" name="rrule_endson" type="radio" value="until">
                                        <input name="EVENT_RRULE[UNTIL]" type="text" class="calendar-inp calendar-inp-cal" placeholder="дата" onclick="BX.calendar({node: this, field: this, bTime: false});">
                                    </label>

                                </div>
                                <br>
                                <br>
                                <hr>
                                <br>

                                <input name="NOTIFY" type="checkbox">
                                <span class="not_negs">Уведомлять участников</span>
                                <br>
                                <br>

                                <span style="
                                    color: #565656;
                                    font-family: 'Gotham-Medium';
                                    font-size: 12px;">Уведомить за:</span>
                                <br>
                                <? $listTime = array( // Тут минуты
                                    '<span class="not_negs">В момент события</span>' => 0,
                                    '<span class="not_negs">За 5 минут до события</span>' => 5,
                                    '<span class="not_negs">За 10 минут до события</span>' => 10,
                                    '<span class="not_negs">За 30 минут до события</span>' => 30,
                                    '<span class="not_negs">За 1 час до события</span>' => 60,
                                    '<span class="not_negs">За 2 часа до события</span>' => 60,
                                    '<span class="not_negs">За день до события</span>' => (60*24),
                                    '<span class="not_negs">За 2 дня до события</span>' => ((60*24)*2),
                                ); ?>
                                <? foreach ($listTime as $decsription => $time): ?>
                                    <input type="checkbox" name="NOTIFY_TIME[]" value="<?=$time?>"/>
                                    <span class="not_negs"><?=$decsription?></span>
                                    <br>
                                <? endforeach; ?>
                                <br>

                                <hr>


                                <input name="PRIVATE_EVENT" type="checkbox" value="">
                                <span class="not_negs">Частное событие</span>


                                <div id="PARTICIPANTS" style="display:none;">
                                    <br><hr><div class="listGroups"></div>
                                </div>
                            </form>

                            <? /*

                            <div class="break">
                                <div class="break_title">
                                    ПЕРЕРЫВЫ
                                </div>
                                <div class="break_choose">
                                    <table id="listBreaksItem">
                                    </table>

                                    <?

// <div class="break_item">
//                                        <span class="w-77">Кофе-брейк с </span>
//                                        <div class="break_input-wrap">
//                                            <input type="text" maxlength="2" id="">
//                                            <input type="text" maxlength="2" id="">
//                                        </div>
//                                        <span>по</span>
//                                        <div class="break_input-wrap">
//                                            <input type="text" maxlength="2" id="">
//                                            <input type="text" maxlength="2" id="">
//                                        </div>
                                       <a href="#" class="break_input_delete" onclick="removeBreak(this); return false;"><img src="<?=$this->__folder?>/img/close_lines.png" id="delete"*/
//                                                width="8" alt=""></a>
//                                    </div>
//                                    <div class="break_item">
//                                        <span class="w-77">Обед с </span>
//                                        <div class="break_input-wrap">
//                                            <input type="text" maxlength="2" id="">
//                                            <input type="text" maxlength="2" id="">
//                                        </div>
//                                        <span>по</span>
//                                        <div class="break_input-wrap">
//                                            <input type="text" maxlength="2" id="">
//                                            <input type="text" maxlength="2" id="">
//                                        </div>
/*                                        <a href="#" class="break_input_delete" onclick="removeBreak(this); return false;"><img src="<?=$this->__folder?>/img/close_lines.png" id="delete"
//                                                width="8" alt=""></a>
//                                    </div>
//

                            ?>
                                    <div style="padding-top: 4px;">
                                        <a href="javascript:void(0);" id="add" onclick="addBreak('<?=$this->__folder?>');"><img src="<?=$this->__folder?>/img/plus_ic.png" alt=""></a>
                                    </div>
                                </div>
                            </div>

                            <hr>

                                <h2>Пример:</h2>
                                    <b>Дополниетельные настройки:</b><br><br>
                                    Повторяемость: <select name="EVENT_RRULE_EX[FREQ]">
                                        <option value="NONE" >Не повторяется</option>
                                        <option value="DAILY"   data-first="кадый" data-last="день">Каждый день</option>
                                        <option value="WEEKLY"  data-first="кадую" data-last="неделю">Каждую неделю</option>
                                        <option value="MONTHLY" data-first="кадый" data-last="месяц">Каждый месяц</option>
                                        <option value="YEARLY"  data-first="кадый" data-last="год">Каждый год</option>
                                    </select>
                                    <script>
                                        $('select[name="EVENT_RRULE_EX[FREQ]"]').on('change', function() {
                                            var value = $(this).val();
                                            if(value == 'NONE') {
                                                $('div#INTERVAL_EX').css('display','none');
                                                $('div#COUNT_EX').css('display','none');
                                            }
                                            else {
                                                var data_first = $(this).children(":selected").attr('data-first');
                                                var data_last = $(this).children(":selected").attr('data-last');
                                                $('div#INTERVAL_EX span#FIRST').html(data_first);
                                                $('div#INTERVAL_EX span#LAST').html(data_last);
                                                $('div#INTERVAL_EX').css('display','inline-block');
                                                $('div#COUNT_EX').css('display','block');
                                            }

                                            if(value == 'WEEKLY') { $('div#BYDAY').css('display','block'); }
                                            else { $('div#BYDAY').css('display','none'); }
                                        });
                                    </script>
                                    <div id="INTERVAL_EX" style="display:none;">
                                        <span id="FIRST"></span>
                                        <select name="EVENT_RRULE[INTERVAL]">
                                            <? $days = 35; for ($i=1;$i<=$days;$i++):?>
                                                <option value="<?=$i?>"><?=$i?></option>
                                            <? endfor; ?>
                                        </select>
                                        <span id="LAST"></span>
                                    </div>
                                    <div id="BYDAY_EX" style="display:none;">
                                        <label><input name="EVENT_RRULE[BYDAY][]" type="checkbox" value="MO">Пн</label>
                                        <label><input name="EVENT_RRULE[BYDAY][]" type="checkbox" value="TU">Вт</label>
                                        <label><input name="EVENT_RRULE[BYDAY][]" type="checkbox" value="WE">Ср</label>
                                        <label><input name="EVENT_RRULE[BYDAY][]" type="checkbox" value="TH">Чт</label>
                                        <label><input name="EVENT_RRULE[BYDAY][]" type="checkbox" value="FR">Пт</label>
                                        <label><input name="EVENT_RRULE[BYDAY][]" type="checkbox" value="SA">Сб</label>
                                        <label><input name="EVENT_RRULE[BYDAY][]" type="checkbox" value="SU">Вс</label>
                                    </div>
                                    <div id="COUNT_EX" style="display:none;">

                                        Окончание:<br>

                                        <label for="never">
                                            <input name="rrule_endson" id="never" type="radio"  value="never" checked="checked">
                                            Никогда
                                        </label>
                                        <br>

                                        <label for="endson_count">
                                            <input id="endson_count" name="rrule_endson" type="radio" value="count">
                                            После <input type="text" name="EVENT_RRULE[COUNT]" placeholder="10" value="10"> повторений
                                        </label>
                                        <br>

                                        <label for="rrule_endson">
                                            <input id="rrule_endson" name="rrule_endson" type="radio" value="until">
                                            <input name="EVENT_RRULE[UNTIL]" type="text" placeholder="дата" onclick="BX.calendar({node: this, field: this, bTime: false});">
                                        </label>

                                    </div>

                                    <br>
                                    <br>

                                    <label>
                                        <input name="NOTIFY" type="checkbox" value=""> Уведомлять участников
                                    </label>
                                    <br>
                                    <br>

                                    Уведомить за: <br>
                                    <? $listTime = array( // Тут минуты
                                        'В момент события' => 0,
                                        'За 5 минут до события' => 5,
                                        'За 10 минут до события' => 10,
                                        'За 30 минут до события' => 30,
                                        'За 1 час до события' => 60,
                                        'За 2 часа до события' => 60,
                                        'За день до события' => (60*24),
                                        'За 2 дня до события' => ((60*24)*2),
                                    ); ?>
                                    <? foreach ($listTime as $decsription => $time): ?>
                                        <label><input type="checkbox" name="NOTIFY_TIME[]" value="<?=$time?>"/><?=$decsription?></label><br>
                                    <? endforeach; ?>
                                    <br>

                                    <label type="text"><input name="PRIVATE_EVENT" type="checkbox" value="">Частное событие</label>

                                    <div id="PARTICIPANTS" style="display:none;">
                                        <br><hr><div class="listGroups"></div>
                                    </div>
        
                            <hr>


                                <h2>Результат:</h2>

                                <div class="repeat">
                                    <div class="select_item">
                                        <span>Повторяемость:</span>
                                        <div class="custom-select" style="width:200px;">
                                            <select name="EVENT_RRULE[FREQ]">
                                                <option data-parent-name="EVENT_RRULE[FREQ]" value="NONE" >Не повторяется</option>
                                                <option data-parent-name="EVENT_RRULE[FREQ]" value="NONE">Не повторяется</option>
                                                <option data-parent-name="EVENT_RRULE[FREQ]" value="DAILY"   data-first="кадый" data-last="день">Каждый день</option>
                                                <option data-parent-name="EVENT_RRULE[FREQ]" value="WEEKLY"  data-first="кадую" data-last="неделю">Каждую неделю</option>
                                                <option data-parent-name="EVENT_RRULE[FREQ]" value="MONTHLY" data-first="кадый" data-last="месяц">Каждый месяц</option>
                                                <option data-parent-name="EVENT_RRULE[FREQ]" value="YEARLY"  data-first="кадый" data-last="год">Каждый год</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>


                            <hr>



                            <div class="checkbox_repeat">
                                <p class="checkbox_wrap">
                                    <input type="checkbox" id="rep" />
                                    <label for="rep">Повторять мероприятие</label>
                                </p>
                            </div>
                            <div class="repeat">
                                <div class="select_item">
                                    <span>Повтор:</span>
                                    <div class="custom-select" style="width:200px;">
                                        <select>
                                            <option value="0">периодичность</option>
                                            <option value="1">раз в месяц</option>
                                            <option value="2">раз в неделю</option>
                                            <option value="3">раз в год</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="select_item">
                                    <span>Каждый:</span>
                                    <div class="custom-select" style="width:70px;">
                                        <select>
                                            <option value="0">1</option>
                                            <option value="1">2</option>
                                            <option value="2">3</option>
                                            <option value="3">4</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="end_repeat">
                                <div class="end_repat-title">
                                    Окончание повторения
                                </div>
                                <div class="end_date">
                                    Дата
                                    <div class="datepicker end_datepicker">
                                        <p><input type="text" id="end_datepicker" placeholder="Выберите дату"></p>
                                    </div>
                                </div>
                            </div>
                            <div class="checkbox_repeat">
                                <p class="checkbox_wrap">
                                    <input type="checkbox" id="rep2" />
                                    <label for="rep2">Закрытое мероприятие <span>(детальная информация о событии
                                            видна только вам)</span></label>
                                </p>
                            </div>
                            <div class="remind">
                                <div class="remind_title">
                                    Уведомить за
                                </div>
                                <div class="remid_checkboxes">
                                    <p class="checkbox_wrap">
                                        <input type="checkbox" id="rep3" />
                                        <label for="rep3">Сутки</label>
                                    </p>
                                    <p class="checkbox_wrap">
                                        <input type="checkbox" id="rep4" />
                                        <label for="rep4">3 часа</label>
                                    </p>
                                    <p class="checkbox_wrap">
                                        <input type="checkbox" id="rep5" />
                                        <label for="rep5">30 минут</label>
                                    </p>
                                </div>
                            </div>
                            <div class="mail_post">
                                <p class="checkbox_wrap">
                                    <input type="checkbox" id="rep6" checked />
                                    <label><b class="active">ВКЛ</b> | <b>ВЫКЛ</b></label>
                                    <span>уведомления на почту о начале мероприятия</span>
                                </p>

                            </div>

                            <br>
                            <br>
                            <br>

                            */?>

                        </div>

                        <div class="bg_white" style="position:relative;">
                            <button id="close_sett" class="closed"><span>Развернуть</span></button>
                            <div class="content_right_item m-h110 space_between pr-10">
                                <div class="footer_buttons">
                                    <a class="btns blue" href="javascript:void(0);" onclick="ToBook();"> ЗАБРОНИРОВАТЬ</a>
                                    <a class="btns black" href="/booking/">Назад</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="right_side">
                <div class="avaible_title">
                    <p> Доступные переговорные
                        <br>
                        <span style="font-size: 12px; color: #8a8a8a;">кликните для выбора переговорной</span>
                    </p>
                </div>
                <div class="avaible_wrap" style="position: relative;">
                    <div id="preloader"></div>
                    <div id="listNegs">
                        <br>
                    </div>
                </div>
            </div>
            <script> getNegotiated('<?=$this->__folder?>'); </script>
        </div>
    </div>
</main>
<script>
    $('.company_slider').slick({
        infinite: false,
        arrows: true,
        slidesToShow: 5,
        slidesToScroll: 1,
        /*responsive: [{
                breakpoint: 1500,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    infinite: true,
                }
            },
            {
                breakpoint: 1300,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]*/
    });

    $( window ).resize(function() {
        /*$('.company_slider').slick({
            infinite: false,
            arrows: true,
            slidesToShow: 5,
            slidesToScroll: 1,
            responsive: [{
                breakpoint: 1500,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    infinite: true,
                }
            },
                {
                    breakpoint: 1300,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });*/
    });

    $('.time_slider').slick({
        infinite: false,
        arrows: true,
        slidesToShow: 10,
        slidesToScroll: 1,
        responsive: [{
                breakpoint: 1500,
                settings: {
                    slidesToShow: 10,
                    slidesToScroll: 1,
                    infinite: true,
                }
            },
            {
                breakpoint: 1300,
                settings: {
                    slidesToShow: 10,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 10,
                    slidesToScroll: 1
                }
            }
        ]
    });
</script>
<script>
    function strCountPeoples(count) {
        count = parseInt(count);
        str = '';
        if(parseInt(count) == 50) { str = 'более '; }
        str += count + ' чел';
        return str;
    }
    $(function () {

        sliderMin = 1;
        sliderMax = 50;
        sliderStep = sliderMin;
        fistLoad = true;
        $value = $("#value");
        $("#slider_peoples").slider({
            range: 'min',
            value: 2,//<?=$arResult['COUNT']?>,
            min: sliderMin,
            max: sliderMax,
            step: sliderStep,
            stop: function (event, ui) {
                $value.text(strCountPeoples(ui.value));
                //$value2.val(ui.value);
                var valueAsPct = (ui.value - sliderMin) * 100 / (sliderMax - sliderMin);
                var widthAsPct = $value.width() * 100 / $(this).width();
                var placeAsPct = Math.round(100 * (valueAsPct - widthAsPct / 2)) / 100;
                if (placeAsPct < 0) { placeAsPct = 0 }
                if (placeAsPct + widthAsPct > 100) { placeAsPct = 100 - widthAsPct }
                $value.css("left", placeAsPct + '%');
                $('#minpeople').val(ui.value);
                //setFilterParam('COUNT',ui.value);
            },
            slide: function (event, ui) {
                $value.text(strCountPeoples(ui.value));
                var valueAsPct = (ui.value - sliderMin) * 100 / (sliderMax - sliderMin);
                var widthAsPct = $value.width() * 100 / $(this).width();
                var placeAsPct = Math.round(100 * (valueAsPct - widthAsPct / 2)) / 100;
                if (placeAsPct < 0) { placeAsPct = 0; }
                if (placeAsPct + widthAsPct > 100) { placeAsPct = 100 - widthAsPct; }
                $value.css("left", placeAsPct + '%');
                $('#minpeople').val(ui.value);
                //setFilterParam('COUNT',ui.value);
            },
            change: function (event, ui) {
                $value.text(strCountPeoples(ui.value));
                var valueAsPct = (ui.value - sliderMin) * 100 / (sliderMax - sliderMin);
                var widthAsPct = $value.width() * 100 / $(this).width();
                var placeAsPct = Math.round(100 * (valueAsPct - widthAsPct / 2)) / 100;
                if (placeAsPct < 0) { placeAsPct = 0; }
                if (placeAsPct + widthAsPct > 100) { placeAsPct = 100 - widthAsPct; }
                $value.css("left", placeAsPct + '%');
                $('#minpeople').val(ui.value);
                if(fistLoad) { setFilterParam('COUNT',ui.value,false,true); fistLoad = false; }
                else { setFilterParam('COUNT',ui.value); }
            }
        });

        $("#value").html($("#slider_peoples").slider("value") + ' чел');

        $("#minpeople").on("change", function (event, ui) {
            $value = $("#value");
            $("#slider_peoples").slider("value", this.value);
            $("#value").html($("#slider_peoples").slider("value") + ' чел');
        });

        $("#slider_peoples").slider("value", <?=$arResult['COUNT']?>);
    });

    $('#close_sett').on("click", function () {
        $('#dop_settings').slideToggle();
        $(this).toggleClass('closed');
        if ($(this).hasClass('closed')) {
            $(this).html("<span>развернуть</span>")
        } else {
            $(this).html("<span>свернуть</span>")
        }
    });
    $("#datepicker").datepicker({
        dateFormat: 'dd.mm.yy',
        showOn: "button",
        monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь',
            'Октябрь', 'Ноябрь', 'Декабрь'
        ],
        dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
        buttonImage: "<?=$this->__folder?>/img/arr_calend-hove.png",
        buttonImageOnly: true,
        // beforeShow: function (textbox, instance) {
        //     //$('.datepicker').append($('#ui-datepicker-div'));
        // }
    }).on('change', function(ev){
        var currentDate = $( "#datepicker" ).datepicker( "getDate" );
        date = ('0' + currentDate.getDate()).slice(-2) + '.' + ('0' + (currentDate.getMonth() + 1)).slice(-2) + '.' + currentDate.getFullYear();
        setFilterParam('DATE_BEGIN',date);
    });
    $("#end_datepicker").datepicker({
        showOn: "button",
        monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь',
            'Октябрь', 'Ноябрь', 'Декабрь'
        ],
        dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
        buttonImage: "<?=$this->__folder?>/img/arr_down_select.png",
        buttonImageOnly: true,
        // beforeShow: function (textbox, instance) {
        //     $('.datepicker').append($('#ui-datepicker-div'));
        // }
    });
    $(".ui-datepicker-trigger").on('click', function () {
        var src = $(this).attr('src');
        var newsrc = (src == '<?=$this->__folder?>/img/arr_calend-hove.png') ? '<?=$this->__folder?>/img/arr_calend_up.png' : '<?=$this->__folder?>/img/arr_calend-hove.png';
        $(this).attr('src', newsrc);
    });
    $(".end_datepicker .ui-datepicker-trigger").on('click', function () {
        var src = $(this).toggleClass('active');
        var newsrc = (src == '<?=$this->__folder?>/img/arr_down_select.png') ? '<?=$this->__folder?>/img/arr_calend_up.png' : '<?=$this->__folder?>/img/arr_down_select.png';
        $(this).attr('src', newsrc);
    });
</script>
<script>
    $('.phone_ic').click(function () {
        var parent = $(this).parent();
        parent.find('.company-details').slideToggle().toggleClass("active");
    });
    $('.type_order .order_item').click(function () {

        var children = $(this);
        $('.type_order .order_item').removeClass('active');
        children.addClass("active");

        // Получиь сервисы сопровождения связанные с этим видом бронирования
        service_ids = $(this).data('arr-id-services');
        booking_id = $(this).data('id-booking');
        $(".service_img").removeClass('active');
        for(i=0;i<service_ids.length;i++) { $(".service_id_" + service_ids[i]).addClass('active'); }
        setFilterParam('UF_SERVICE_TYPES_ID',service_ids);
        setFilterParam('BOOKING_TYPE_ID',booking_id);

        description = $(this).data('description-type-booking');
        $('#more_info').val(description);
        if(!description) { $('#more_info').attr('placeholder','Введите описание мероприятия'); }
    });
    $('.company_slider-item').click(function () {
        var children = $(this);
        children.addClass("active");
    });
    $('.close_slide').click(function (e) {
        e.stopPropagation();
        var children = $(this).parent();
        children.removeClass("active");
    });
    $('.service_item p').click(function () {
        service_id = $(this).data('service-id');
        if ($(this).hasClass("active")) {
            $(this).removeClass("active");
            removeFilterParam('UF_SERVICE_TYPES_ID',service_id);
        } else {
            $(this).addClass("active");
            setFilterParam('UF_SERVICE_TYPES_ID',service_id,true);
        }
    });
</script>
<script type="text/javascript">
    /*<![CDATA[*/
/*    $.fn.raty.defaults.path = 'img';
    $.fn.raty.defaults.targetKeep = true;
    $.fn.raty.defaults.targetType = 'number';
    $.fn.raty.defaults.hints = ['bad', 'poor', 'regular', 'good', 'gorgeous'];
    $.fn.raty.defaults.noRatedMsg = 'Not rated yet!';
    $.fn.raty.defaults.cancelHint = 'Cancel this rating!';*/
    /*]]>*/
    /*jQuery(function ($) {
        jQuery('.raty-icons').raty({
            'click': function(score,evt){
                productRatio(score,$(this).attr('data-target'));
                $(this).raty('readOnly',true);
            }, 'readOnly': false, 'score': '0', 'target': '.rates'
        });
        jQuery('.rates').hide();
    });*/
</script>
<script>
    jQuery(document).ready(function () {

        /* слайдер цен */
        <?
        $TIME_START = $arResult['DATE']['TIME_START'] ? $arResult['DATE']['TIME_START'] : 0;
        $TIME_END = $arResult['DATE']['TIME_END'] ? $arResult['DATE']['TIME_END'] : 0;
        ?>
        setFilterParam('TIME_START',convertMinsToHrsMins('<?=$TIME_START?>'),false,true);
        setFilterParam('TIME_END',convertMinsToHrsMins('<?=$TIME_END?>'),false,true);

        slider_time_cnt_load = 0;
        jQuery("#slider_time").slider({
            range: true,
            min: 420,
            max: 1260,
            step: 30,
            values: [<?=$TIME_START?>, <?=$TIME_END?>],
            stop: function (event, ui) {
                var hours1 = Math.floor(ui.values[0] / 60);
                var minutes1 = ui.values[0] - (hours1 * 60);

                if (hours1.length == 1) hours1 = '0' + hours1;
                if (minutes1.length == 1) minutes1 = '0' + minutes1;
                if (minutes1 == 0) minutes1 = '00';
                if (hours1 >= 24) {
                    if (hours1 == 24) { hours1 = hours1; minutes1 = minutes1; }
                    else { hours1 = hours1 - 24; minutes1 = minutes1; }
                } else {
                    hours1 = hours1;
                    minutes1 = minutes1;
                }
                if (hours1 == 0) {
                    hours1 = '00';
                    minutes1 = minutes1;
                }

                $('.slider-time').html(hours1 + ':' + minutes1);

                var hours2 = Math.floor(ui.values[1] / 60);
                var minutes2 = ui.values[1] - (hours2 * 60);

                if (hours2.length == 1) hours2 = '0' + hours2;
                if (minutes2.length == 1) minutes2 = '0' + minutes2;
                if (minutes2 == 0) minutes2 = '00';
                if (hours2 >= 24) {
                    if (hours2 == 24) {
                        hours2 = hours2;
                        minutes2 = minutes2;
                    } else {
                        hours2 = hours2 - 24;
                        minutes2 = minutes2;
                    }
                } else {
                    hours2 = hours2;
                    minutes2 = minutes2;
                }

                $('.slider-time2').html(hours2 + ':' + minutes2);
                setLabelPosition();
                setLabelPosition2();

            },
            change: function (event, ui) {

                //region Если левая и правая граница пересекаются, то сдвигаем левую границу на 30 минут назад
                //       относительно правой.
                if(ui.values[0] == ui.values[1]) {
                    if(ui.values[0]==420) { setIntervalTime(ui.values[1],ui.values[1]+30); }
                    else { setIntervalTime(ui.values[1]-30,ui.values[1]); }
                }
                //endregion

                setLabelPosition();
                setLabelPosition2();
                //region дата начала/окончания

                    //region Получть даты начала/окончания
                    var hours1 = Math.floor(ui.values[0] / 60);
                    TIME_START = sliceTime(hours1) + ':' + sliceTime(ui.values[0] - (hours1 * 60));

                    var hours2 = Math.floor(ui.values[1] / 60);
                    TIME_END = sliceTime(hours2) + ':' + sliceTime(ui.values[1] - (hours2 * 60));
                    //endregion

                    //region Получть даты начала/окончания (Из фильтра)
                    filter_L = getFilter();
                    TIME_START_Filter = filter_L['TIME_START'];
                    TIME_END_Filter = filter_L['TIME_END'];
                    //endregion

                    //region Определить что дата начала/окончания изменены
                    reload = false;
                    if(slider_time_cnt_load > 3) { reload = true; }
                    if(TIME_START != TIME_START_Filter) {
                        if(reload) { setFilterParam('TIME_START',TIME_START); }
                        else { setFilterParam('TIME_START',TIME_START,false,true); }
                    }
                    if(TIME_END != TIME_END_Filter) {
                        if(reload) { setFilterParam('TIME_END',TIME_END); }
                        else { setFilterParam('TIME_END',TIME_END,false,true); }
                    }
                    slider_time_cnt_load++;
                    //endregion

                //endregion
            },
            slide: function (event, ui) {
                var hours1 = Math.floor(ui.values[0] / 60);
                var minutes1 = ui.values[0] - (hours1 * 60);

                if (hours1.length == 1) hours1 = '0' + hours1;
                if (minutes1.length == 1) minutes1 = '0' + minutes1;
                if (minutes1 == 0) minutes1 = '00';
                if (hours1 >= 24) {
                    if (hours1 == 24) { hours1 = hours1; minutes1 = minutes1; }
                    else { hours1 = hours1 - 24; minutes1 = minutes1; }
                }
                else { hours1 = hours1; minutes1 = minutes1; }
                if (hours1 == 0) { hours1 = '00'; minutes1 = minutes1; }

                $('.slider-time').html(hours1 + ':' + minutes1);

                var hours2 = Math.floor(ui.values[1] / 60);
                var minutes2 = ui.values[1] - (hours2 * 60);

                if (hours2.length == 1) hours2 = '0' + hours2;
                if (minutes2.length == 1) minutes2 = '0' + minutes2;
                if (minutes2 == 0) minutes2 = '00';
                if (hours2 >= 24) {
                    if (hours2 == 24) { hours2 = hours2; minutes2 = minutes2; }
                    else { hours2 = hours2 - 24; minutes2 = minutes2; }
                }
                else { hours2 = hours2; minutes2 = minutes2; }

                $('.slider-time2').html(hours2 + ':' + minutes2);

                //region Пересечение показателей времени
                diff_time = ui.values[1] - ui.values[0];
                if(diff_time <= 90) { $('#slider_time .slider-time2').css('top','-40px'); }
                else { $('#slider_time .slider-time2').css('top','0'); }
                //endregion

                setLabelPosition();
                setLabelPosition2();
            }
        });

        //region Пересечение показателей времени
        diff_time = <?=$TIME_START?> - <?=$TIME_END?>;
        if(diff_time <= 90) { $('#slider_time .slider-time2').css('top','-40px'); }
        else { $('#slider_time .slider-time2').css('top','0'); }
        //endregion
        //setIntervalTimePlus(90);

        var thumb = $($('#slider_time').children('.ui-slider-handle:first'));
        setLabelPosition2();

        $('#slider_time').bind('slide', function () {
            $('#col-1').val($('#slider_time').slider('value'));
            setLabelPosition2();
        });

        function setLabelPosition2() {
            var label = $('#col-1');
            label.css('top', thumb.position().top + label.outerHeight(true));
            label.css('left', thumb.position().left - (label.width() - thumb.width()) / 1.4);
        }
        var thumb2 = $($('#slider_time').children('.ui-slider-handle:last'));
        setLabelPosition();

        $('#slider_time').bind('slide', function () {
            $('#col-2').val($('#slider_time').slider('value'));
            setLabelPosition();
        });

        function setLabelPosition() {
            var label2 = $('#col-2');
            label2.css('top', thumb2.position().top + label2.outerHeight(true));
            label2.css('left', thumb2.position().left - (label2.width() - thumb2.width()) / 1.4);
        }

    });

    // <!-- CUSTOM SELECT -->
    var x, i, j, selElmnt, a, b, c;
    /*look for any elements with the class "custom-select":*/
    x = document.getElementsByClassName("custom-select");
    for (i = 0; i < x.length; i++) {
        selElmnt = x[i].getElementsByTagName("select")[0];
        /*for each element, create a new DIV that will act as the selected item:*/
        a = document.createElement("DIV");
        a.setAttribute("class", "select-selected");
        a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
        x[i].appendChild(a);
        /*for each element, create a new DIV that will contain the option list:*/
        b = document.createElement("DIV");
        b.setAttribute("class", "select-items select-hide");
        for (j = 1; j < selElmnt.length; j++) {
            /*for each option in the original select element,
            create a new DIV that will act as an option item:*/
            c = document.createElement("DIV");
            c.innerHTML = selElmnt.options[j].innerHTML;
            c.setAttribute("data-value", $(selElmnt.options[j]).attr('value'));
            c.setAttribute("data-first", $(selElmnt.options[j]).attr('data-first') ? $(selElmnt.options[j]).attr('data-first') : '');
            c.setAttribute("data-last", $(selElmnt.options[j]).attr('data-last') ? $(selElmnt.options[j]).attr('data-last') : '');
            c.setAttribute("data-parent-name", $(selElmnt.options[j]).attr('data-parent-name') ? $(selElmnt.options[j]).attr('data-parent-name') : '');
            c.addEventListener("click", function (e) {
                /*when an item is clicked, update the original select box,
                and the selected item:*/
                var y, i, k, s, h;
                s = this.parentNode.parentNode.getElementsByTagName("select")[0];
                h = this.parentNode.previousSibling;
                for (i = 0; i < s.length; i++) {
                    if (s.options[i].innerHTML == this.innerHTML) {
                        s.selectedIndex = i;
                        h.innerHTML = this.innerHTML;
                        y = this.parentNode.getElementsByClassName("same-as-selected");
                        for (k = 0; k < y.length; k++) { y[k].removeAttribute("class"); }
                        this.setAttribute("class", "same-as-selected");

                        parentName = $(this).data('parent-name');

                        console.log('this');
                        console.log(this);

                        console.log('parentName: ' + parentName);
                        if(parentName == 'EVENT_RRULE[FREQ]') {
                            console.log(123);
                        }

                        break;
                    }
                }
                h.click();
            });
            b.appendChild(c);
        }
        x[i].appendChild(b);
        a.addEventListener("click", function (e) {            
            /*when the select box is clicked, close any other select boxes,
            and open/close the current select box:*/
            e.stopPropagation();
            closeAllSelect(this);
            this.nextSibling.classList.toggle("select-hide");
            this.classList.toggle("select-arrow-active");
            this.parentElement.classList.toggle("active");
        });
    }

    function closeAllSelect(elmnt) {
        /*a function that will close all select boxes in the document,
        except the current select box:*/
        var x, y, i, arrNo = [];
        x = document.getElementsByClassName("select-items");
        y = document.getElementsByClassName("select-selected");
        z = document.getElementsByClassName("custom-select");
        for (i = 0; i < y.length; i++) {
            if (elmnt == y[i]) { arrNo.push(i); }
            else { y[i].classList.remove("select-arrow-active"); z[i].classList.remove("active"); }
        }
        for (i = 0; i < x.length; i++) { if (arrNo.indexOf(i)) { x[i].classList.add("select-hide"); } }
    }
    /*if the user clicks anywhere outside the select box,
    then close all select boxes:*/
    document.addEventListener("click", closeAllSelect);
</script>
<script>



    selectNegOne = true;
    function after_reload_filter() {
        <? if(isset($_REQUEST['ID_NEG']) && $_REQUEST['ID_NEG']): ?>
        if(selectNegOne) {
            setTimeout(function () {
                $('.avaible_item[data-neg-id=<?=$_REQUEST['ID_NEG']?>]').trigger( "click" ); //.click();
            },200);
            selectNegOne = false;
        }
        <? endif; ?>
    }


    jQuery(document).ready(function () {
        // Дата
        <? if(empty($arResult['DATE'])):?>
            set_user_date();
        <? else: ?>
            setFilterParam('DATE_BEGIN','<?=$arResult['DATE']['DATE_BEGIN']?>',false,true);
        <? endif; ?>

        if (!UserCompanyID) {
            BX.MyAjax.Get('booking/GetCompaniesHTML', {}, '', function (data) {
                var PopupBX = new BX.MyPopupBX();
                PopupBX.AddBtn('Сохранить',function () {
                    form = $(this.popupWindow.contentContainer).find('form').serializeArray();
                    if(form.length > 0) {
                        UF_COMPANIES_ID = form[0]['value'];
                        BX.MyAjax.Get('booking/SnapUserToCompany',{'UF_COMPANIES_ID':UF_COMPANIES_ID,'USER_ID':UserID});
                        UserCompanyID = UF_COMPANIES_ID;
                        PopupBX.CloseLastPopup();
                    } else { alert('Вы не выбрали компанию'); }
                },true);
                PopupBX.Message('what_company_do_you_belong_to','К какой компании вы принадлежите?',data);
            });
        }

        // Выделенные компании
        <? foreach ($arResult['UF_COMPANIES_ID'] as $company){ ?>
            <? if($company['SELECTED']){ ?>
                setFilterParam('UF_COMPANIES_ID','<?=$company['ID']?>',true);
                //removeFilterParam('UF_COMPANIES_ID','<?=$company['ID']?>');
            <? }; ?>
        <? }; ?>

        <? foreach ($arResult['BOOKING_TYPE_ID'] as $bookingType) { ?>
            <? if($bookingType['SELECTED']) { ?>
                //setFilterParam('UF_SERVICE_TYPES_ID',<?=json_encode($bookingType['UF_SERVICE_TYPES'])?>);
                //setFilterParam('BOOKING_TYPE_ID',<?=$bookingType['ID']?>);
            <? break; } ?>
        <? }; ?>


}); </script>
