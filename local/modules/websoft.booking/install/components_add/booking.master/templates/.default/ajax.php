<?

require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
use \Websoft\Booking\Entity\NegotiatedTable as Negotiated;

$COUNT = $_REQUEST['COUNT'] ? $_REQUEST['COUNT'] : 0; // Кол-во участников
$UF_SERVICE_TYPES_ID = $_REQUEST['UF_SERVICE_TYPES_ID'] ? $_REQUEST['UF_SERVICE_TYPES_ID'] : array(); // Сервисы сопровождения
$UF_COMPANIES_ID = $_REQUEST['UF_COMPANIES_ID'] ? $_REQUEST['UF_COMPANIES_ID'] : array(); // Компании
$DATE_BEGIN = $_REQUEST['DATE_BEGIN'].' '.$_REQUEST['TIME_START'].':00';
$DATE_END = $_REQUEST['DATE_BEGIN'].' '.$_REQUEST['TIME_END'].':00';

//region Генерация фильтра
$arFilter = array();

    //region Компании
    if(!empty($UF_COMPANIES_ID)) { $arFilter['COMPANY_ID'] = $UF_COMPANIES_ID; }
    //endregion

    //region Кол-во участников
    if(!empty($COUNT)) { $arFilter['>=COUNT_PLACE'] = $COUNT; }
    //endregion

    //region Определить минимальные СЕРВИСЫ, которыми должна обладать переговорная
    $UF_SERVICE_TYPES = $_REQUEST['UF_SERVICE_TYPES_ID'] ? $_REQUEST['UF_SERVICE_TYPES_ID'] : array();
    if(!empty($UF_SERVICE_TYPES)) {
        //region Из-за этого запроса падает сервер....
        //$arFilter['LOGIC'] = 'AND';
        //foreach ($UF_SERVICE_TYPES as $SERVICE_TYPE) { $arFilter[] = array( '=UF_SERVICE_TYPES' => $SERVICE_TYPE ); }
        //endregion
        //foreach ($UF_SERVICE_TYPES as $SERVICE_TYPE) { $arFilter['=UF_SERVICE_TYPES'][] = $SERVICE_TYPE; }
    }
    //endregion

    //region Определить переговорные, которые выбрал Организатор
    $UF_PARTICIPANTS = $_REQUEST['UF_PARTICIPANTS'] ? $_REQUEST['UF_PARTICIPANTS'] : array();
    if(!empty($UF_PARTICIPANTS)) { foreach ($UF_PARTICIPANTS as $idNeg => $users) {
        // ... Удалить из выборки те переговорные, которые он выбрал
        $arFilter['!=ID'][] = $idNeg;
        // ... + Если выбрали переговорную(ые), то другие переговорные из этой(этих) компании не нужны (логика клиента)
        $neg_one = Negotiated::getListPure(false, array('ID'=>$idNeg), array('COMPANY_ID'));
        if($neg_one) {if(($key = array_search($neg_one['COMPANY_ID'],$arFilter['COMPANY_ID'])) !== false){
            unset($arFilter['COMPANY_ID'][$key]);
        }}
        if($neg_one && !isset($arFilter['COMPANY_ID'])) { $arFilter['!=COMPANY_ID'][] = $neg_one['COMPANY_ID']; }
    }}
    //endregion

//endregion

//\Websoft\Booking\Handler::dd('$arFilter',$arFilter);

//region Для обхода запроса сложной логики, которая ложит сервер, пишем этот хак
$allNegs = \Websoft\Booking\Entity\NegotiatedTable::getListPure(true,$arFilter,array('ID','UF_SERVICE_TYPES'));
$arFilter = array('ID' => array());
foreach ($allNegs as $neg) {
    if(empty($UF_SERVICE_TYPES)) { $arFilter['ID'][] = $neg['ID']; }
    else { // Переговорная должна содержать все сервисы, иначе выпадает из поиска
        $UF_SERVICE_TYPES_NEG = $neg['UF_SERVICE_TYPES'];
        $diff = array_diff($UF_SERVICE_TYPES,$UF_SERVICE_TYPES_NEG);
        if(empty($diff)) { $arFilter['ID'][] = $neg['ID']; }
    }
}
//endretion

if(empty($arFilter['ID'])) {
    echo 'Нет перегворных подходящих под ваши критерии...';
} else {

    //region Поиск подходящей переговорной
    CBitrixComponent::includeComponentClass("websoft.booking:booking.master");
    $CBookingMasterComponent = 'CBookingMasterComponent';
    $negsCategories = $CBookingMasterComponent::getNegotiationCategories($arFilter,$DATE_BEGIN,$DATE_END);
    //endregion

    // Печать (Тестовая, тут можно забрать массив или распечатать HTML)
    foreach ($negsCategories as $cat => $arrNegs) {
        foreach ($arrNegs as $neg) {
            $resizeImg = CFile::ResizeImageGet($neg['UF_PREVIEW_IMAGE'],array('width'=>135,'height'=>73),BX_RESIZE_IMAGE_EXACT);
            $img = $resizeImg['src'];
            $company = \Websoft\Booking\Entity\CompanyBookingTable::getListPure(
                    false,array('ID' => $neg['COMPANY_ID']),array('NAME'));
            $SERVICES = \Websoft\Booking\Entity\ServiceTypesTable::getListPure(
                    true,array('ID'=>$neg['UF_SERVICE_TYPES']),array('UF_ICON_ACTIVE'));
            ?>
            <div class="avaible_item <?if($cat == 'BUSY'){?>ordered<?}?>"
                 data-neg-id="<?=$neg['ID']?>"
                 data-company-id="<?=$neg['COMPANY_ID']?>">
                <a href="javascript:void(0);" class="avaible_close"></a>
                <div class="avaible_tags">
                    <? $avaible_hidden = array(); ?>
                    <? $avaible_count = 0; ?>
                    <? foreach ($SERVICES as $SERVICE): ?>
                        <? $avaible_count++; ?>
                        <? $src = CFile::GetFileArray($SERVICE['UF_ICON_ACTIVE']); $src = $src['SRC']; ?>
                        <? if($avaible_count <= 5): ?>
                            <img src="<?=$src?>" width="12">
                        <? else: ?>
                            <? $avaible_hidden[] = $src; ?>
                        <? endif; ?>
                    <? endforeach; ?>
                    <? if(!empty($avaible_hidden)): ?>
                        <div style="height: 100%; width: 10px; display: inline-block;"></div>
                        <span class="more_info">
                            <div class="box_info">
                                <? foreach ($avaible_hidden as $src2): ?>
                                    <div style="display: inline-block">
                                        <img src="<?=$src2?>" width="12">
                                    </div>
                                <? endforeach; ?>
                            </div>
                        </span>

                    <? endif; ?>
                </div>
                <div class="avaible_image">
                    <img src="<?=$img?>">
                    <?if($cat == 'BUSY'){?><div class="violet_wrap">Занято</div><?}?>
                </div>
                <div class="avaible_text">
                    <div class="avaible_item-title">Название:</div>
                    <div class="avaible_item-name"><b><?=$neg['NAME']?></b></div>
                    <div class="avaible_item_descr">
                        <p>Компания:</p>
                        <p style="padding-bottom: 5px;"><b><?=$company['NAME']?></b></p>

                        <p>Кол-во мест:</p>
                        <p style="padding-bottom: 5px;"><b><?=$neg['COUNT_PLACE']?></b></p>

                        <p>Адрес:</p>
                        <p style="padding-bottom: 5px;"><b><?=$neg['ADDRESS']?></b></p>
                    </div>
                </div>
            </div>
            <?
        }
    } ?>
    <script>
    $('.avaible_item').click(function (e) {
        if(!$(this).hasClass('ordered')) {
            var children = $(this);
            children.addClass("active");
            neg_id = $(this).data('neg-id');
            setFilterParam('NEG_IDS',neg_id,true,true);
            company_id = $(this).data('company-id');
            $('.avaible_item[data-company-id="' + company_id + '"]').css('display','none');
            $(this).css('display','block');
            upload_negs();
        }
    });
    $('.avaible_close').click(function (e) {
        e.stopPropagation();
        var children = $(this).parent();
        children.removeClass("active");
        neg_id = $(this).parent().data('neg-id');
        removeFilterParam('NEG_IDS',neg_id,true);
        company_id = $(this).parent().data('company-id');
        $('.avaible_item[data-company-id="' + company_id + '"]').css('display','block');
        upload_negs();
    });
</script>

<? } ?>
