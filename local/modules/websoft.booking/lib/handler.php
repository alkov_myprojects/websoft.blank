<?php

/**
 * Created by PhpStorm
 * User: Sergey Pokoev
 * www.pokoev.ru
 * @ Академия 1С-Битрикс - 2015
 * @ academy.1c-bitrix.ru
 *
 * файл event.php
 */

namespace Websoft\Booking;
use Websoft\Booking\Classes\CustomEventsCalendar;

class Handler {

    private static $module_id; // ID Модуля
    private static $asset; // Для регистрации JS + Css + Lang

    /** Инициализация */
    public static function init() {

//        global $BX_MENU_CUSTOM;
//        $BX_MENU_CUSTOM->AddItem('top_links', array(
//            'TEXT' => 'Правила автоматического назначения курсов',
//            'LINK' => '/services/learning/snapcourse.php',
//            'SELECTED' => false,/*(
//                strstr($page,'/services/learning/snapcourse.php') ||
//                strstr($page,'/rulelearning/')
//            ) ? true : false,*/
//            'PERMISSION' => 'X',
//            'ADDITIONAL_LINKS' => array(),
//            'ITEM_TYPE' => 'P',
//            'ITEM_INDEX' => 5,
//            'PARAMS' => array(),
//            'DEPTH_LEVEL' => 1,
//            'IS_PARENT' => false
//        ));

        //TODO: Системные данные
        $dir = dirname(__FILE__);
        for ($i = 1; $i < ($level = 2); $i++) { $dir = dirname($dir); }
        self::$module_id = basename($dir);
        self::$asset = \Bitrix\Main\Page\Asset::getInstance();

        global $APPLICATION;
        $page = $APPLICATION->GetCurPageParam("", array());

        //TODO: Получить пространства имён и классы сущностей
        /*$listEntity = \COption::GetOptionString(self::$module_id, "listEntity");
        $listEntity = json_decode($listEntity);*/
        $db = \Bitrix\Main\Application::getConnection();
        $listEntity = array();
        if(is_dir($dir = dirname(__FILE__).'/entity/')) {
            $TreeFiles = self::TreeDirAndFiles($dir);
            foreach ($TreeFiles as $file) { $listEntity[] = self::get_namespace($file); }
        }

        //TODO: Процесс синхронизации(файлов и сущностей) на время разработки
        $update = false;

        /** Синхронизация файлов */
        $SynchronizeFilesModule = \COption::GetOptionString(self::$module_id, "SynchronizeFilesModule");
        if($SynchronizeFilesModule) {
        //if(false) {
            $directiories_files = \COption::GetOptionString(self::$module_id, "directiories_files");
            $directiories_files = json_decode($directiories_files, true);
            foreach ($directiories_files as $pathDirModuleAbs => $pathiDirPulic) {
                // Получить Хэш-сумму файлов модуля
                $TreeDirAndFiles_Module = self::TreeDirAndFiles($pathDirModuleAbs);
                $ArrHash_Module = array();
                foreach ($TreeDirAndFiles_Module as $path) {
                    $ArrHash_Module[] = md5_file($path);
                }
                $hashSumModule = md5(implode('', $ArrHash_Module));

                // Получить Хэш-сумму файлов в публичной части
                $ArrHashSumPublic = array();
                foreach ($TreeDirAndFiles_Module as $path) {
                    $path = str_replace($pathDirModuleAbs, $pathiDirPulic, $path);
                    $path = $_SERVER['DOCUMENT_ROOT'] . $path;
                    $ArrHashSumPublic[] = md5_file($path);
                }
                $hashSumPublic = md5(implode('', $ArrHashSumPublic));

                // Если не равны, то копировать папку из модуля в публичку:
                if ($hashSumPublic != $hashSumModule) {
                    CopyDirFiles($pathDirModuleAbs, $_SERVER['DOCUMENT_ROOT'] . $pathiDirPulic, true, true);
                    $update = true;
                }
            }
        }

        /** Синхронизация сущностей */
        $SynchronizeEntity = \COption::GetOptionString(self::$module_id, "SynchronizeEntity");
        if($SynchronizeEntity) {
        //if(false) {
            foreach ($listEntity as $entityName) {
                if(class_exists($entityName)) {
                    $entity = $entityName::getEntity();
                    if (!$db->isTableExists($entityName::getTableName())) { $entity->createDbTable(); $update = true; }
                    else { // Переобновление сущностей
                        $TableFieldsCode = array(); // Поля текущей таблицы
                        $TableFields = $db->getTableFields($entityName::getTableName());
                        foreach ($TableFields as $code => $field) { $TableFieldsCode[] = $code; }
                        $EntityFieldsCode = array(); // Поля сущности
                        $EntityFields = $entityName::getMap();
                        foreach ($EntityFields as $EnFields) {
                            if($EnFields->getDataType() != '\Bitrix\Main\User') {
                                $EntityFieldsCode[] = $EnFields->getName();
                            }
                        }
                        $addFields = array_diff($EntityFieldsCode,$TableFieldsCode); // Каких полей не хватает?
                        $removeFields = array_diff($TableFieldsCode,$EntityFieldsCode); // Лишние поля?
                        if(!empty($addFields) || !empty($removeFields)) {
                            $connection = \Bitrix\Main\Application::getConnection();
                            $connection->dropTable($entityName::getTableName());
                            $update = true;
                        }
                    }

                    $oUserTypeEntity = new \CUserTypeEntity();
                    $mapUF = $entityName::getMapUF();
                    if(!empty($mapUF)) {
                        foreach ($mapUF as $uf_field) {
                            if(!empty($uf_field)) {
                                $rsData = \CUserTypeEntity::GetList( array(), array(
                                    'ENTITY_ID' => $uf_field['ENTITY_ID'], 'FIELD_NAME' => $uf_field['FIELD_NAME']
                                ));
                                if(!($arRes = $rsData->Fetch())) { // Свойства не найдено, над добавить
                                    $iUserFieldId = $oUserTypeEntity->Add($uf_field); // int
                                    if(!$iUserFieldId) {
                                        /*echo 'Что то пошло не так =(';
                                        $errors[] = $APPLICATION->getException()->getString();
                                        echo '<pre>';
                                        print_r($errors);
                                        echo '</pre>';*/
                                    }
                                } else { // Обновление
                                    /*echo '<b>$uf_field</b>';
                                    echo '<pre>';
                                    print_r($uf_field);
                                    echo '</pre>';
                                    echo '<hr>';*/
                                }
                            }
                        }
                    }
                }
            }
        }

        //TODO: Установка DB чёт не работает в модуле =), придумал хак!
        foreach ($listEntity as $entityName) {
            if(class_exists($entityName)) {
                $entity = $entityName::getEntity();
                if (!$db->isTableExists($entity->getDBTableName())) { $entity->createDbTable(); $update = true; }
            }
        }

        //TODO: Если произошли какие то изменения(update/add файлы или сущности), то редирект на текущую страницу
        if($update) { LocalRedirect($page); } else {
            //region Custom - Определение сервисов сопровождения, которые ещё не назначены не текущий день.
            \Websoft\Booking\Classes\RuleServiceData::checkAllServiceData();
            //endregion
        }

        // JS + LangForJS + CssForJS
        self::initCJSCore();


    }
    public static function initCJSCore() {

        global $APPLICATION;
        $page = $APPLICATION->GetCurPage();

        // Инициализация класса модальных окон везде
        self::AddCJSCore('MyPopupBX',array('core_viewer','disk'),true,false,true);

        // Инициализация класса для работы с Ajax
        if(strstr($page,'/booking/')) {
            $path_to_ajax_files = '/bitrix/ajax/' . self::$module_id . '/';
            //$path_to_ajax_files = '/ajax/' . self::$module_id . '/';
            $JS = '<script>BX.ready(function(){ BX.MyAjax = new BX.MyAjax("' . $path_to_ajax_files . '"); });</script>';
            self::AddCJSCore('MyAjax', array(), true, false, false, $JS);
        }

        if($page === '/bitrix/admin/websoft.blank_settings.php') {
            // Подключение стилей модальных окон если в настройках модуля
            $APPLICATION->SetAdditionalCSS("/bitrix/js/main/core/css/core_viewer.min.css");

            // Подключение классов JS для работы с модулем
            $JS = '<script>BX.ready(function(){ BX.FilesModule = new BX.FilesModule(); });</script>';
            self::AddCJSCore('FilesModule',array(),true,true,true,$JS);
        }

        // Пример подключения JS + Css + Lang через CJSCore
        //$JS = '<script>BX.ready(function(){ BX.TestJS = new BX.TestJS("Иван","Альков"); BX.TestJS.init(); });</script>';
        //self::AddCJSCore('FilesModule',array(/*'disk',*//*'ajax',*/),true,true,true,$JS);
    }

    /**
     * Добавление пунктов меню в менюшки CRM`ки
     * @param $items - ссылка на менюшки
     */
    public static function addItemsToCrmControlPanelBuild(&$items) {

        //region Левое меню пользователя
        $newLink = array(
            'TEXT' => 'Резервирование переговорных',
            'LINK' => '/booking/',
            'ID' => 'WEBSOFT_BOOKING',
            'NEW_PAGE' => 'N'
        );
        $selfItems = \CUserOptions::GetOption("intranet", "left_menu_self_items_".SITE_ID);

        $add = true;
        foreach ($selfItems as $item) { if($item['ID'] == $newLink['ID']) { $add = false; } }

        if($add) {
            $selfItems[] = $newLink;
            \CUserOptions::SetOption("intranet", "left_menu_self_items_".SITE_ID, $selfItems);
        }
        //endregion

        //region Менюха CRM
        /*echo '<pre>';
        print_r($selfItems);
        echo '</pre>';*/
        //CUserOptions::SetOption("intranet", "left_menu_self_items_".SITE_ID, $selfItems);
        /*$items[16]['NAME'] = 'Test';
        $items[16]['TITLE'] = 'Test 2';*/
        /*$items[] = array(
            'ID' => 'BOOKING',
            'MENU_ID' => 'menu_websoft_booking',
            'NAME' => 'Резервирование перегоорных',
            'TITLE' => 'Резервирование перегоорных',
            'URL' => '/booking/',
            'ICON' => 'booking'
        );*/
        //endregion
    }

    /**
     * Системаный метод, для создания своего расширения - JS + Css + Lang и ещё подключение расширений в Bitrix
     * такие как jquery, popup, date, bootsrap и другие, в скором времени будет подробное описание каждого расширения =)
     *
     * @param string $name - наименование файлов и ID вашего расширения (пример: $name.js,$name.php,$name.css)
     * @param array $rel - список расширений которые нужно подключить, например ('popup','jquery'), по умолчанию подключаются
     *                     'core','jquery','date','window' + ваши =)
     * @param bool $js - флаг, регистрировать js файла?
     * @param bool $css - флаг, регистрировать css файла?
     * @param bool $lang - флаг, регистрировать lang файла?
     * @param string $JS - JS код, который будет вызван после подключения
     */
    private static function AddCJSCore($name='',$rel = array(),$js = false,$css = false,$lang = false,$JS = '') {
        if($js) { $add['js'] = '/bitrix/js/'.self::$module_id.'/'.$name.'.js'; }
        if($css) { $add['css'] = '/bitrix/css/'.self::$module_id.'/'.$name.'.css'; }
        if($lang) { $add['lang'] = '/bitrix/js/'.self::$module_id.'/lang/'.LANGUAGE_ID.'/'.$name.'.php'; }
        $add['rel'] = array('core','jquery','date','window','popup'); // Базовые расширения, которые всегда
                                                       // пригодяться + можно свою подключить =)
                                                       // Ещё есть: disk, ajax, jquery, popup, ls, timer, tootip,
                                                       //          webrtc, json, input, fx, ws_progectssettings_main,
                                                       //          im_common, voximplant, mobile_voximplant, file_dialog
                                                       //          uploader, dd и наверно ещё много чего =)
                                                       //TODO:: Надо будет потом расписать про каждый по пдробней!!!
        // "skip_core" => false | true, -> При подключении расширения не требуется подключение core.js.
        if(!empty($rel)) {
            $add['rel'] = array_merge($add['rel'],$rel);
            $add['rel'] = array_unique($add['rel']);
        }
        \CJSCore::RegisterExt($name,$add);
        \CJSCore::Init($name);
        if($JS) { self::$asset->addString($JS); }
    }
    private static function hashDirectory($directory) {
        if (! is_dir($directory)) { return false; }
        $files = array();
        $dir = dir($directory);
        while (false !== ($file = $dir->read())) {
            if ($file != '.' and $file != '..') {
                $checkDir = $directory . $file;
                if (is_dir($checkDir)) { $files[] = self::hashDirectory($checkDir . '/'); }
                else { $files[] = md5_file($checkDir); }
            }
        }
        $dir->close();
        return md5(implode('', $files));
    }
    private static function TreeDirAndFiles($directory) {
        if (! is_dir($directory)) { return false; }
        $files = array();
        $dir = dir($directory);
        while (false !== ($file = $dir->read())) {
            if ($file != '.' and $file != '..') {
                $checkDir = $directory . $file;
                if (is_dir($checkDir)) { $files = array_merge($files,self::TreeDirAndFiles($checkDir . '/')); }
                else { $files[] = $directory . $file; }
            }
        }
        $dir->close();
        return $files;
    }
    private static function get_namespace($path) {
        $content = file_get_contents($path);
        $tokens = token_get_all($content);
        $namespace = '';
        for ($index = 0; isset($tokens[$index]); $index++) {
            if (!isset($tokens[$index][0])) {
                continue;
            }
            if (T_NAMESPACE === $tokens[$index][0]) {
                $index += 2; // Skip namespace keyword and whitespace
                while (isset($tokens[$index]) && is_array($tokens[$index])) {
                    $namespace .= $tokens[$index++][1];
                }
            }
            if (T_CLASS === $tokens[$index][0]) {
                $index += 2; // Skip class keyword and whitespace
                $fqcns[] = $namespace.'\\'.$tokens[$index][1];
                $namespace .= '\\'.$tokens[$index++][1];
            }
        }
        return '\\'.$namespace;
    }

    /** Публичные методы */
    public static function fullNameUser($USER_ID) {
        $rsUser = \CUser::GetByID($USER_ID);
        $arUser = $rsUser->Fetch();
        return \CUser::FormatName(\CSite::GetNameFormat(false), $arUser, true, false);
    }
    public static function getFieldUser($USER_ID,$CODE) {
        $rsUser = \CUser::GetByID($USER_ID);
        $arUser = $rsUser->Fetch();
        return $arUser[$CODE];
    }
    public static function getPathUser($USER_ID) {
        return \CComponentEngine::MakePathFromTemplate(
            \Bitrix\Main\Config\Option::get('intranet', 'path_user', '', SITE_ID), array('USER_ID' => $USER_ID)
        );
    }
    public static function covertDateToServer($date,$format = 'd.m.Y H:i:s') {
        $strtotime = strtotime($date);
        $time_string = ConvertTimeStamp($strtotime-\CTimeZone::GetOffset(), "FULL");
        return $time_string;
    }
    public static function dd($name,$mass) {
        global $USER;
        if($USER->GetID()==1) {
            echo '<b>'.$name.'</b>';
            echo '<pre>';
            print_r($mass);
            echo '</pre>';
            echo '<hr>';
        }
    }
    public static function changeDateEvent($id,$dateStart,$dateEnd) {
        $errors = array();
        if(\CModule::IncludeModule("calendar")) {
            //... Определить тип события (Обычное/Переодичное)
            //$calendarEvent = \CCalendarEvent::GetById($id, false);
            //if($calendarEvent['RRULE'] == '') {
                self::updateEvent($id,array(
                    'DATE_FROM' => $dateStart,
                    'DATE_TO' => $dateEnd,
                ));
            /*} else {
                echo 'Переодичное событие для него пока нет алгоритма...';
            }*/
        }
        return $errors;
    }
    public static function removeEvent($id) {
        if (\CModule::IncludeModule("calendar")) {

            $event = \Websoft\Booking\Classes\CustomEventsCalendar::GetEventByID($id);
            $LOCATION = explode('_',$event['LOCATION']);
            if(!\CIBlockElement::Delete($LOCATION[2])) {
                echo 'Ошибка удаления места проведения в событии календаря';
            }
            \CCalendar::DeleteEvent($id, false);
        }
    }
    public static function updateEvent($ID,$data,$recursionEditMode = '') { //
        if(\CModule::IncludeModule("calendar")) {
            $arFields = $data;
            $arFields['ID'] = $ID;
            //$arFields['NAME'] = $ID; // string
            $newId = \CCalendar::SaveEvent(array(
                'arFields' => $arFields,
                //'UF' => $arUFFields,
                //'silentErrorMode' => false,
                'recursionEditMode' => $recursionEditMode, //$_REQUEST['rec_edit_mode'], // 'this' - Только это || 'next' || -
                //'currentEventDateFrom' => CCalendar::Date(CCalendar::Timestamp(self::$request['current_date_from']), false)
            ));
        }
    }

}