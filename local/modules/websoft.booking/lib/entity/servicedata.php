<?php

namespace Websoft\Booking\Entity;
use \Websoft\Booking\Classes\EntityBase;

use Bitrix\Main\Entity\IntegerField;   // TODO:: Целочисленное
use Bitrix\Main\Entity\ReferenceField; // TODO:: Свзять поля сущности с другой сущностью/элементом, работает в паре с IntegerField
use Bitrix\Main\Entity\StringField;    // TODO:: Строка
use Bitrix\Main\Entity\DatetimeField;  // TODO:: ДатаВремя
use Bitrix\Main\Entity\BooleanField;   // TODO:: Булеан
use Bitrix\Main\Entity\FloatField;     // TODO:: Число с плавающей точкой
use Bitrix\Main\Entity\EnumField;      // TODO:: Список
use Bitrix\Main\Entity\TextField;      // TODO:: Текстовое поле, Тоже самое что и строка, но можно определить тип

use Bitrix\Main\Entity\UField; // TODO: Его отключили.... =(

use Bitrix\Main\UserTable;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Grid\Panel\Actions;
use Bitrix\Main\Grid\Panel\Types;
use Bitrix\Main\Error;
use Bitrix\Main\Type\Dictionary\ErrorCollection;
use \Bitrix\Main\Localization\Loc;

/**
 * Дополнительные данные к сервису сопровождения + комментарий + рейтинг
 * Class TestEntity1Table
 * @package Websoft\Blank\Entity
 */
class ServiceDataTable extends EntityBase {

    public static function GetStatuses($only_indexes = false) {
        $statuses = array(
            0 => 'в обработке',
            1 => 'готово',
            2 => 'есть проблемы, нужна помощь',
            3 => 'отклонено',
        );
        if($only_indexes) {
            $indexes = array(); foreach ($statuses as $ind => $name_status) { $indexes[] = $ind; }
            return $indexes;
        } else { return $statuses; }
    }

    public static function getMap() {
        return array(
            new IntegerField('ID', array('primary' => true, 'autocomplete' => true)),
            new IntegerField('SERVICE_TYPE_ID'), // ID Сервиса сопровождения
            new IntegerField('RESPONSIBLE_ID'), // ID Ответсвенного за сервис сопровождения
            new StringField('COMMENT'), // Коментарий к сервису сопровождения
            new IntegerField('RATING'), // Кол-во звёзд за сопроводжение сервиса ответсвенным
            new IntegerField('STATUS_ID',array( 'values' => self::GetStatuses(), 'default_value' => 0 )),
        );
    }
    public static function getMapUF() {
        return array(
            self::UF_Integer('UF_SNAP_CHILD_CARD',false,false), // Привязка к дочерней карточке
                                                                                       // бронирования
            self::UF_String('UF_CODE_DATE_BEGIN',false,false),  // по этой строке будем получать
                                                                                       // Сервисы сопровождения, переодических
                                                                                       // мероприятий
        );
    }

    //region Управление страницей создания/редактирования
    public static function getNameOneElement() { return self::getMessage('NameOneElementToAdd'); }
    public static function getFieldsToEdit() {
        \Bitrix\Main\Localization\Loc::loadMessages(__FILE__);
        return self::genHeader( array(
            'NAME'   => self::field(1,'t'),
        ));
    }
    public static function validateFormEdit($element, $errors) {
        /*if (empty($element['NAME'])) {
            $errors->setError(new Error('Не указанно название...'));
        }
        if (empty($element['ASSIGNED_BY_ID'])) {
            $errors->setError(new Error(Loc::getMessage('CRMSTORES_ERROR_EMPTY_ASSIGNED_BY_ID')));
        } else {
            $dbUser = UserTable::getById($element['ASSIGNED_BY_ID']);
            if ($dbUser->getSelectedRowsCount() <= 0) {
                $errors->setError(new Error(Loc::getMessage('CRMSTORES_ERROR_UNKNOWN_ASSIGNED_BY_ID')));
            }
        }*/
        return $errors;
    }
    //endregion

    //region Управление гридом
    public static function getStringName() {
        return  \Bitrix\Main\Localization\Loc::getMessage('ServiceTypesTable');
    }
    public static function getHeaderGrid() {
        \Bitrix\Main\Localization\Loc::loadMessages(__FILE__);
        return self::genHeader( array(
            'ID'     => self::field(0,'i',0,'d','',60,'minimal'),
            'NAME'   => self::field(1,'t',1),
        ));
    }
    public static function filterFieldsGrid() {
        \Bitrix\Main\Localization\Loc::loadMessages(__FILE__);
        return self::genFilter(array(
            'ID'     => self::field(0),
            'NAME'   => self::field(1),
            'ACTIVE' => self::field(1,'c'),
        ));
    }
    public static function uniqueDisplayColumn($row) {
        return array();
    }
    public static function addCustomAction($gridManagerId,$applyButton) {
        return array(
            //region Одно шаговое действие
            array(
                'NAME' => GetMessage('Activate'), 'VALUE' => 'actvate_elements', 'ONCHANGE' => array(
                array( 'ACTION' => Actions::CREATE, 'DATA' => array($applyButton) ),
                array('ACTION' => Actions::CALLBACK, 'DATA' => array(array(
                    'JS' => "BX.CrmUIGridExtension.processActionChange('".$gridManagerId."', 'actvate_elements')"
                )))
            )),
            array(
                'NAME' => GetMessage('DeActivate'), 'VALUE' => 'de_actvate_elements', 'ONCHANGE' => array(
                array( 'ACTION' => Actions::CREATE, 'DATA' => array($applyButton) ),
                array('ACTION' => Actions::CALLBACK, 'DATA' => array(array(
                    'JS' => "BX.CrmUIGridExtension.processActionChange('".$gridManagerId."', 'de_actvate_elements')"
                )))
            )),
            //endregion
        );
    }
    public static function processGridActions($action,$allRows,$request){
        $dataToDebag = array();
        switch ($action) {
            case 'actvate_elements':
                $dataToDebag[] = 'Поймали событие =) "one_action"';
                break;
            case 'de_actvate_elements':
                $dataToDebag[] = 'Поймали событие =) "one_action"';
                break;
        }
    }
    public static function getActions() { return self::generationAtions(true, true, true); }
    //endregion
}
