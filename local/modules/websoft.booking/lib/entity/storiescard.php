<?php

namespace Websoft\Booking\Entity;
use \Websoft\Booking\Classes\EntityBase;

use Bitrix\Main\Entity\IntegerField;   // TODO:: Целочисленное
use Bitrix\Main\Entity\ReferenceField; // TODO:: Свзять поля сущности с другой сущностью/элементом, работает в паре с IntegerField
use Bitrix\Main\Entity\StringField;    // TODO:: Строка
use Bitrix\Main\Entity\DatetimeField;  // TODO:: ДатаВремя
use Bitrix\Main\Entity\BooleanField;   // TODO:: Булеан
use Bitrix\Main\Entity\FloatField;     // TODO:: Число с плавающей точкой
use Bitrix\Main\Entity\EnumField;      // TODO:: Список
use Bitrix\Main\Entity\TextField;      // TODO:: Текстовое поле, Тоже самое что и строка, но можно определить тип

use Bitrix\Main\Entity\UField; // TODO: Его отключили.... =(

use Bitrix\Main\UserTable;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Grid\Panel\Actions;
use Bitrix\Main\Grid\Panel\Types;
use Bitrix\Main\Error;
use Bitrix\Main\Type\Dictionary\ErrorCollection;
use \Bitrix\Main\Localization\Loc;

/**
 * История карточки бронирования
 * Class StoriesCard
 * @package Websoft\Booking\Entity
 */
class StoriesCardTable extends EntityBase {

    public static function getMap() {
        return array(
            new IntegerField('ID', array('primary' => true, 'autocomplete' => true)),

            new IntegerField('ENTITY_ID'), // Карточки
            //new StringField('ENTITY_CLASS'), // Карточка бронирования или дочерняя карточка бронирования?

            new DatetimeField('DATE_TIME'), // Дата изменения
            new IntegerField('CHANGE_BY_ID'), // ID Пользователя, который произвёл изменение
            new StringField('DESCRIPTION'), // Описание изменения
            new StringField('BEFORE'), // Значение до
            new StringField('AFTER'), // Значение после
            //new StringField('TEST')
        );
    }
    public static function getMapUF() {}

    //region Управление страницей создания/редактирования
    public static function getNameOneElement() { return self::getMessage('NameOneElementToAdd'); }
    public static function getFieldsToEdit() {
        \Bitrix\Main\Localization\Loc::loadMessages(__FILE__);
        return self::genHeader( array(
            'DATE_TIME' => self::field(1,'d'),
            'CHANGE_BY_ID' => self::field(1,'t'),
            'DESCRIPTION' => self::field(1,'t'),
            'BEFORE' => self::field(1,'t'),
            'AFTER' => self::field(1,'t'),
        ));
    }
    public static function validateFormEdit($element, $errors) {
        /*if (empty($element['NAME'])) {
            $errors->setError(new Error('Не указанно название...'));
        }
        if (empty($element['ASSIGNED_BY_ID'])) {
            $errors->setError(new Error(Loc::getMessage('CRMSTORES_ERROR_EMPTY_ASSIGNED_BY_ID')));
        } else {
            $dbUser = UserTable::getById($element['ASSIGNED_BY_ID']);
            if ($dbUser->getSelectedRowsCount() <= 0) {
                $errors->setError(new Error(Loc::getMessage('CRMSTORES_ERROR_UNKNOWN_ASSIGNED_BY_ID')));
            }
        }*/
        return $errors;
    }
    //endregion

    //region Управление гридом
    public static function getStringName() {
        return  \Bitrix\Main\Localization\Loc::getMessage('BookingTypeTable');
    }
    public static function getHeaderGrid() {
        \Bitrix\Main\Localization\Loc::loadMessages(__FILE__);
        return self::genHeader( array(
            'ID'     => self::field(0,'i',0,'d','',60,'minimal'),
            'DATE_TIME' => self::field(1,'d'),
            'CHANGE_BY_ID' => self::field(1,'t'),
            'DESCRIPTION' => self::field(1,'t'),
            'BEFORE' => self::field(1,'t'),
            'AFTER' => self::field(1,'t'),
        ));
    }
    public static function filterFieldsGrid() {
        \Bitrix\Main\Localization\Loc::loadMessages(__FILE__);
        return self::genFilter(array(
            'ID'     => self::field(0),
            'NAME'   => self::field(1),
        ));
    }
    public static function uniqueDisplayColumn($row) {
        return array();
    }
    public static function addCustomAction($gridManagerId,$applyButton) {
        return array(
            //region Одно шаговое действие
            array(
                'NAME' => GetMessage('Activate'), 'VALUE' => 'actvate_elements', 'ONCHANGE' => array(
                array( 'ACTION' => Actions::CREATE, 'DATA' => array($applyButton) ),
                array('ACTION' => Actions::CALLBACK, 'DATA' => array(array(
                    'JS' => "BX.CrmUIGridExtension.processActionChange('".$gridManagerId."', 'actvate_elements')"
                )))
            )),
            array(
                'NAME' => GetMessage('DeActivate'), 'VALUE' => 'de_actvate_elements', 'ONCHANGE' => array(
                array( 'ACTION' => Actions::CREATE, 'DATA' => array($applyButton) ),
                array('ACTION' => Actions::CALLBACK, 'DATA' => array(array(
                    'JS' => "BX.CrmUIGridExtension.processActionChange('".$gridManagerId."', 'de_actvate_elements')"
                )))
            )),
            //endregion
        );
    }
    public static function processGridActions($action,$allRows,$request){
        $dataToDebag = array();
        switch ($action) {
            case 'actvate_elements':
                $dataToDebag[] = 'Поймали событие =) "one_action"';
                break;
            case 'de_actvate_elements':
                $dataToDebag[] = 'Поймали событие =) "one_action"';
                break;
        }
    }
    public static function getActions() { return self::generationAtions(true, true, true); }
    //endregion
}
