<?php

namespace Websoft\Booking\Entity;
use Bitrix\Main\ORM\Entity;
use \Websoft\Booking\Classes\EntityBase;

use Bitrix\Main\Entity\IntegerField;   // TODO:: Целочисленное
use Bitrix\Main\Entity\ReferenceField; // TODO:: Свзять поля сущности с другой сущностью/элементом, работает в паре с IntegerField
use Bitrix\Main\Entity\StringField;    // TODO:: Строка
use Bitrix\Main\Entity\DatetimeField;  // TODO:: ДатаВремя
use Bitrix\Main\Entity\BooleanField;   // TODO:: Булеан
use Bitrix\Main\Entity\FloatField;     // TODO:: Число с плавающей точкой
use Bitrix\Main\Entity\EnumField;      // TODO:: Список
use Bitrix\Main\Entity\TextField;      // TODO:: Текстовое поле, Тоже самое что и строка, но можно определить тип

use Bitrix\Main\Entity\UField; // TODO: Его отключили.... =(

use Bitrix\Main\UserTable;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Grid\Panel\Actions;
use Bitrix\Main\Grid\Panel\Types;
use Bitrix\Main\Error;
use Bitrix\Main\Type\Dictionary\ErrorCollection;
use \Bitrix\Main\Localization\Loc;
use Websoft\Booking\Handler;



use \Websoft\Booking\Entity\BookingCardTable as BookingCard;
use \Websoft\Booking\Entity\BookingCardChildTable as BookingCardChild;
use \Websoft\Booking\Entity\NegotiatedTable as Negotiated;
use \Websoft\Booking\Entity\CompanyBookingTable as CompanyBooking;


/**
 * Карточка бронирования
 * Class BookingCardTable
 * @package Websoft\Blank\Entity
 */
class BookingCardTable extends EntityBase {

    /**
        Публичные:
            - Период проведения
            - Вид бронирования
            - Дата создания мероприятия
            - Организатор
            - Комментарий к мероприятию
            - Комментарий к сопровождению мероприятия
     */

    public static function getMap() {
        return array(

            //region Системные
            new IntegerField('ID', array('primary' => true, 'autocomplete' => true)),
            new DatetimeField('DATE_CREATE'), // Дата создания мероприятия
            new BooleanField('ACTIVE_BOOKING',array(
                'values' => array('N', 'Y'),
                'default_value' => 'Y'
            )), // Активность
            //endregion

            new StringField('NAME'),                 // Наименование мероприятия
            new StringField('DESCRIPTION'),          // Описание мероприятия
            new StringField('BOOKING_TYPE_ID'),      // ID Типа бронирования

            //region Время начала/окончания + периодичность
            new DatetimeField('DATE_START'), // Дата начала
            new DatetimeField('DATE_END'),   // Дата окончания
            // Повторять/Не повторять
            // Сколько раз?
            // Какой интервал между повторениями
            // Дата окончания повторений
            //endregion

            //region Последний раз карчточка изменялась "Кем и когда?"
            new IntegerField('CHANGE_BY_ID'),
            //endregion

            //region Организатор
            new IntegerField('EXECUTOR_COMPANY_ID'),  // Компания Организатора
            new IntegerField('EXECUTOR_BY_ID'),       // Организатор
            new ReferenceField('EXECUTOR_BY', UserTable::getEntity(), array('=this.EXECUTOR_BY_ID' => 'ref.ID')),
            //endregion

            //region Комментарии
            new StringField('COMMENT_EVENT'),         // Комментраий к мероприятию
            new StringField('COMMENT_SERVICE_TYPES'), // Комментраий к сопровождению мероприятия
            //endregion

            //region Уведомления участников
            new BooleanField('NOTIFY'),        // Уведомлять участников о начале мероприятия
            //endregion

            new BooleanField('PRIVATE_EVENT'), // Частное событие
        );
    }
    public static function getMapUF() {
        return array( // Тут множественные свойства
            /* self::UF_Integer('UF_SERVICES_ID',true,false), // Сервисы сопропровождения,
                                                              // которые были указаны в мастере
                                                              // бронирования и которые должны
                                                              // быть сопровождены во всех
                                                              // переговорных участвующих в
                                                              // бронировании */
            self::UF_Integer('UF_COMPANIES_ID',true,false), // компании, которые участвуют в
                                                                                   // бронирование. По этому полю
                                                                                   // определяем каких переговорных
                                                                                   // не хватает... Админ их добавит

            self::UF_Integer('UF_MIN_SERVICES_ID',true,false), // Минимальное кол-во сервисов
                                                                                      // необходимо для поиска подходящей
                                                                                      // переговорной.
                                                                                      // Эти сервисы должны быть у каждой
                                                                                      // переговорной участвующей в
                                                                                      // в мероприятии!!!
            self::UF_String('UF_RRULE'),
            self::UF_String('UF_REMIND'),
            self::UF_Integer('UF_SUMM_RATING',false,false), // Общая оценка
            self::UF_Integer('UF_FILES',true,false), // ID Файлов
            self::UF_Integer('UF_RESPONSIBLE_EVENT',false,false)

        );
    }

    //region Управление гридом
    public static function getStringName() {
        return  \Bitrix\Main\Localization\Loc::getMessage('BookingCardTable');
    }
    public static function getHeaderGrid() {
        \Bitrix\Main\Localization\Loc::loadMessages(__FILE__);
        return self::genHeader( array(
            'ID'          => self::field(0,'i',0,'d','',60),
            'NAME'        => self::field(1,'t',1),
            'DESCRIPTION' => self::field(1,'t',1),

            //region Время начала/окончания + периодичность + дата создания
            'DATE_CREATE' => self::field(1,'d'),
            'DATE_START' => self::field(1,'d'),
            'DATE_END' => self::field(1,'d'),
            // Повторять/Не повторять
            // Сколько раз?
            // Какой интервал между повторениями
            // Дата окончания повторений
            //endregion

            //region Организатор
            'EXECUTOR_COMPANY_ID' => self::field(1,'i'),
            //'EXECUTOR_BY_ID'      => self::field(1,'u'),
            'EXECUTOR_BY_ID' => self::field(1,'i',0,0,'EXECUTOR_BY_ID'),
            //endregion

            //region Комментарии
            'COMMENT_EVENT'         => self::field(1,'t'),
            'COMMENT_SERVICE_TYPES' => self::field(1,'t'),
            //endregion

            'PRIVATE_EVENT'      => self::field(1,'c'),
            'NOTIFY'      => self::field(1,'c'),
        ));
    }
    public static function filterFieldsGrid() {
        \Bitrix\Main\Localization\Loc::loadMessages(__FILE__);
        return self::genFilter(array(
            'ID'     => self::field(0),
            'NAME'   => self::field(1),
        ));
    }
    public static function uniqueDisplayColumn($row) {

        $result = array();

        //region Организатор
        $rsUser = \CUser::GetByID($row['EXECUTOR_BY_ID']);
        $arUser = $rsUser->Fetch();
        $USER_PROFILE_URL = \CComponentEngine::MakePathFromTemplate(
            Option::get('intranet', 'path_user', '', SITE_ID), array('USER_ID' => $arUser['ID'])
        );
        $result['EXECUTOR_BY_ID'] = empty($row['EXECUTOR_BY_ID']) ? '' : \CCrmViewHelper::PrepareUserBaloonHtml(array(
            'PREFIX' => "{$row['ID']}_RESPONSIBLE", 'USER_ID' => $row['EXECUTOR_BY_ID'],
            'USER_NAME'=> \CUser::FormatName(\CSite::GetNameFormat(), $arUser),
            'USER_PROFILE_URL' => $USER_PROFILE_URL
        ));
        //endregion

        //region Компания Организатора
        $company = CompanyBookingTable::getListPure(false,array('ID'=>$row['EXECUTOR_COMPANY_ID']),array('NAME'));
        $result['EXECUTOR_COMPANY_ID'] = $company['NAME'];
        //endregion

        return $result;
    }
    public static function addCustomAction($gridManagerId,$applyButton) {
        return array(
            //region Одно шаговое действие
            /*array(
                'NAME' => GetMessage('Activate'), 'VALUE' => 'actvate_elements', 'ONCHANGE' => array(
                array( 'ACTION' => Actions::CREATE, 'DATA' => array($applyButton) ),
                array('ACTION' => Actions::CALLBACK, 'DATA' => array(array(
                    'JS' => "BX.CrmUIGridExtension.processActionChange('".$gridManagerId."', 'actvate_elements')"
                )))
            )),
            array(
                'NAME' => GetMessage('DeActivate'), 'VALUE' => 'de_actvate_elements', 'ONCHANGE' => array(
                array( 'ACTION' => Actions::CREATE, 'DATA' => array($applyButton) ),
                array('ACTION' => Actions::CALLBACK, 'DATA' => array(array(
                    'JS' => "BX.CrmUIGridExtension.processActionChange('".$gridManagerId."', 'de_actvate_elements')"
                )))
            )),*/
            //endregion
        );
    }
    public static function processGridActions($action,$allRows,$request){
        $dataToDebag = array();
        switch ($action) {
            case 'actvate_elements':
                $dataToDebag[] = 'Поймали событие =) "one_action"';
                break;
            case 'de_actvate_elements':
                $dataToDebag[] = 'Поймали событие =) "one_action"';
                break;
        }
    }
    public static function getActions() { return self::generationAtions(true, true, false); }
    //endregion

    //region Управление страницей создания/редактирования
    public static function getNameOneElement() { return self::getMessage('NameOneElementToAdd'); }
    public static function getFieldsToEdit() {
        \Bitrix\Main\Localization\Loc::loadMessages(__FILE__);
        return self::genHeader( array(
            'NAME'   => self::field(1,'t'),
            'DESCRIPTION'   => self::field(1,'t'),
        ));
    }
    public static function validateFormEdit($element, $errors) {
        /*if (empty($element['NAME'])) {
            $errors->setError(new Error('Не указанно название...'));
        }
        if (empty($element['ASSIGNED_BY_ID'])) {
            $errors->setError(new Error(Loc::getMessage('CRMSTORES_ERROR_EMPTY_ASSIGNED_BY_ID')));
        } else {
            $dbUser = UserTable::getById($element['ASSIGNED_BY_ID']);
            if ($dbUser->getSelectedRowsCount() <= 0) {
                $errors->setError(new Error(Loc::getMessage('CRMSTORES_ERROR_UNKNOWN_ASSIGNED_BY_ID')));
            }
        }*/
        return $errors;
    }
    //endregion

    /**
     * @param array $element  - поля карточки бронирования
     * @param array $PARTICIPANTS  - массив вида [ 'ID_Переговорной' => ['USER_ID_1','USER_ID_2'] ]
     * @param array $COMPANIES_ID - массив ID компаний (может быть пустым массивом)
     * @param array $UF_SERVICE_TYPES_ID - сервисы сопровождения, которые нужны д/всех переговорных, по ним
     *                                     назначаем ответсвенных за сервис сопровождения.
     *                                     Пример: Если выбрали переговорную 1, и у неё досутпно Wi-Fi и Телефон,
     *                                             а Организатор выбрал только Wi-Fi, то будет назначен
     *                                             ответственный за Wi-Fi и всё.
     * @return array - результирущий массив ['Состояние'=>true|false,'Данные'=>['errors'=>'массив ошибок']]
     */
    public static function _addCard($element,$PARTICIPANTS,$COMPANIES_ID,$UF_SERVICE_TYPES_ID,$RRule=false,$Remind=false) {
        $result = array('success' => true, 'data' => array('errors'=>array()));

        //region Проверка

        //endregion

        return $result;
    }
    public static function addCard($element,$PARTICIPANTS,$COMPANIES_ID,$UF_SERVICE_TYPES_ID,$RRule=false,$Remind=false) {
        $result = array('success' => true, 'data' => array('errors'=>array()));
        if(!\CModule::IncludeModule("calendar")) {
            $result['success'] = false;
            $result['data']['errors'][] = 'Не подключен модуль "Календарть"';
        }

        //region Проверка занятости участников
        $usersCheck = array();
        foreach ($PARTICIPANTS as $idNeg => $users) { foreach ($users as $user_id) { $usersCheck[] = $user_id; } }
        $busyUsers = self::checkBusyUsers($element['DATE_START'],$element['DATE_END'],$usersCheck);
        
        $busyUsersToError = array();
        foreach ($busyUsers as $user_id) { if(in_array($user_id,$usersCheck)) { $busyUsersToError[] = $user_id; } }
        if(!empty($busyUsersToError)) {
            $result['success'] = false;
            foreach ($busyUsersToError as $user_id) {
                $result['data']['errors'][] = 'Участник "'.\Websoft\Booking\Handler::fullNameUser($user_id).'" знанят' .
                    ' на это время.';
            }
            return $result;
        }
        //endregion

        $element['UF_COMPANIES_ID'] = $COMPANIES_ID;
        $element['UF_MIN_SERVICES_ID'] = $UF_SERVICE_TYPES_ID;
        $element['UF_RRULE'] = json_encode($RRule,true);
        $element['UF_REMIND'] = json_encode($Remind,true);
        $resUpdate = self::add($element); // Основная карточка бронирования

        if (!$resUpdate->isSuccess()) {
            $errors = $resUpdate->getErrors(); $result['success'] = false; $result['data']['errors'] = $errors;
        }
        else { // Добавление дочерних карточек
            $PARENT_ID = $resUpdate->getId();
            $result['data']['id'] = $PARENT_ID;
            foreach ($PARTICIPANTS as $idNeg => $users) {
                $result2 = self::addChildCard($PARENT_ID,$idNeg,$users,$UF_SERVICE_TYPES_ID);
                $result = array_merge($result2,$result);
                /*// Получить ID ответственного за контроль качества
                $neg = NegotiatedTable::getListPure(false,array('ID'=>$idNeg),array('ASSIGNED_BY_ID'));

                // Получить статус по умолчанию
                $TrainingStatus = TrainingStatusesTable::getListPure(false,array('DEFAULT'=>true));

                // Создать событие(я) в календаре
                $EventID = self::SetEvent(
                    $idNeg, $element['EXECUTOR_BY_ID'], $users, $element['DATE_START'], $element['DATE_END'],
                    $element['NAME'], $element['DESCRIPTION'], $RRule, $Remind
                );

                // Создать пары (сервис сопровождения/ответсвенны)
                $UF_SERVICES_DATA_ID = array();
                foreach ($UF_SERVICE_TYPES_ID as $service_type_id) {
                    // Получить ответсенного за сервис сопровождения этой переговорной
                    $responsibleSupportServices = ResponsibleSupportServicesTable::getListPure(false,array(
                        'SNAP_TO_SS' => $service_type_id,
                        'SNAP_TO_NEG' => $idNeg,
                    ),array('ASSIGNED_BY_ID'));

                    // Создать элемент "Дополнительные данные к сервису сопровождения + комментарий + рейтинг"
                    if($responsibleSupportServices['ASSIGNED_BY_ID']) {
                        $resAddServiceData = ServiceDataTable::add(array(
                            'SERVICE_TYPE_ID' => $service_type_id,
                            'RESPONSIBLE_ID' => $responsibleSupportServices['ASSIGNED_BY_ID'],
                        ));
                        $UF_SERVICES_DATA_ID[] = $resAddServiceData->getId();
                    } else {
                        $result['success'] = false;
                        $result['data']['errors'][] = 'У переговорной с ID = '.$idNeg.' на сервис с ID = ' .
                            ''.$service_type_id.' не назначен ответсвенный.';
                        return $result;
                    }
                }

                // Добавление дочерней переговорной
                BookingCardChildTable::add(array(
                    'PARENT_ID' => $PARENT_ID,
                    'NEGOTIATED_ID' => $idNeg,
                    'RESPONSIBLE_ID' => $neg['ASSIGNED_BY_ID'],
                    'TRAINING_STATUS_ID' => $TrainingStatus['ID'],
                    'PARENT_EVENT_ID' => $EventID,
                    'UF_PARTICIPANTS' => $users,
                    'UF_SERVICES_DATA_ID' => $UF_SERVICES_DATA_ID
                ));*/
            }
        }
        //$result['data']['test'] = 'Hello world =)';

        return $result;
    }
    public static function addChildCard($parentId,$negId,$users,$UF_SERVICE_TYPES_ID) {
        $result = array('success' => true, 'data' => array('errors'=>array()));

        // Получить ID ответственного за контроль качества
        $neg = NegotiatedTable::getListPure(false,array('ID'=>$negId),array('ASSIGNED_BY_ID'));

        // Получить статус по умолчанию
        $TrainingStatus = \Websoft\Booking\Entity\TrainingStatusesTable::getListPure(false,array('DEFAULT'=>true));

        $element = \Websoft\Booking\Entity\BookingCardTable::getListPure(false,array('ID'=>$parentId),array('*','UF_*'));

        // Создать событие(я) в календаре
        $RRule = json_decode($element['UF_RRULE'],true);
        $Remind = json_decode($element['UF_REMIND'],true);
        $EventID = self::SetEvent(
            $negId, $element['EXECUTOR_BY_ID'], $users, $element['DATE_START'], $element['DATE_END'],
            $element['NAME'], $element['DESCRIPTION'], $RRule, $Remind
        );

        // Добавление дочерней карточки бронирования
        $resultAddChild = BookingCardChildTable::add(array(
            'PARENT_ID' => $parentId,
            'NEGOTIATED_ID' => $negId,
            'RESPONSIBLE_ID' => $neg['ASSIGNED_BY_ID'],
            'TRAINING_STATUS_ID' => $TrainingStatus['ID'],
            'PARENT_EVENT_ID' => $EventID,
            'UF_PARTICIPANTS' => $users,
            //'UF_SERVICES_DATA_ID' => $UF_SERVICES_DATA_ID
        ));

        //region Генерация тегов для отправки


        //$TAGS = json_encode($TAGS,true);
        //endregion

        global $USER;

        //$EventID
        $event = \Websoft\Booking\Classes\CustomEventsCalendar::GetEventByID($EventID);
        $debug = array();
        //$debug['$event'] = $event;
        //$debug['$element'] = $element;


        //region "Создание бронирования"

        // ... кому отправлять?
        $user_ids_to_send = array();
        $user_ids_to_send[] = $element['EXECUTOR_BY_ID']; // 1. организатору => $element['EXECUTOR_BY_ID']
        foreach ($users as $user_id) { $user_ids_to_send[] = $user_id; } // 2. участникам, указанным при бронировании
        $user_ids_to_send[] = $neg['ASSIGNED_BY_ID']; // 3. ответственному за переговорную
        foreach ($UF_SERVICE_TYPES_ID as $service_id) { // 5. ответственным за выбранные сервисы
            $ResponsibleSupportServices = \Websoft\Booking\Entity\ResponsibleSupportServicesTable::getListPure(
                false,array( 'SNAP_TO_SS' => $service_id, 'SNAP_TO_NEG' => $negId ),array('ASSIGNED_BY_ID'));
            $user_ids_to_send[] = $ResponsibleSupportServices['ASSIGNED_BY_ID'];
        }

        // ... сбор данных для отправки
        $TAGS = array(
            'MAIL_USER' => 'Email получателя сообщения',
            'BOOKING_ID' => $parentId,
            'BOOKING_NAME' => $element['NAME'],
        );

        // ... отправка
        if($event['CUSTOM_DATA']['Type'] == 0) { // Простое
            foreach ($user_ids_to_send as $user_id) {

                //region User_L
                $rsUser = \CUser::GetByID($user_id);
                $arUser = $rsUser->Fetch();
                //endregion

                $TAGS['MAIL_USER'] = $arUser['EMAIL'];

                ControlSendTable::add(array(
                    'BOOKING_ID' => $parentId,
                    'CONTROL_SEND_ASSIGNED_BY_ID' => $USER->GetID(),
                    'CONTROL_SEND_ASSIGNED_BY_2_ID' => $user_id,
                    'DATE_CREATE' => new \Bitrix\Main\Type\DateTime(),
                    'DATE_SEND' => new \Bitrix\Main\Type\DateTime(),
                    'ACTION_MARK' => 'local/modules/websoft.booking/lib/entity/bookingcard.php -> addChildCard -> ' .
                                     '[399]',
                    'CODE_MAIL_TEMPLATE' => 'BOOKING_NEW',
                    'SEND' => false,
                    'TAGS' => json_encode($TAGS,true), //$TAGS
                ));
            }
        }

        //endregion

        //region "Уведомить за"
        if($element['UF_REMIND']) {

            // ... кому отправлять?
            $user_ids_to_send = array();
            $user_ids_to_send[] = $element['EXECUTOR_BY_ID']; // 1. организатору => $element['EXECUTOR_BY_ID']
            $user_ids_to_send[] = $neg['ASSIGNED_BY_ID']; // 3. ответственному за переговорную

            // ... сбор данных для отправки
            $TAGS = array(
                'MAIL_USER' => 'Email получателя сообщения',
                'BOOKING_ID' => $parentId,
                'BOOKING_NAME' => $element['NAME'],
                'DESCRIPTION_BEGIN' => $event['CUSTOM_DATA']['When'].' '.$event['CUSTOM_DATA']['Periodicity'],
            );

            //$DATE_SEND = $element['DATE_START'];

            // ... отправка
            if($event['CUSTOM_DATA']['Type'] == 0) { // Простое
                $REMIND = $element['UF_REMIND'];
                $REMIND = json_decode($REMIND,true);
                foreach ($REMIND as $REMIND_ONE) {
                    foreach ($user_ids_to_send as $user_id) {

                        //region $TAGS['MAIL_USER']
                        $rsUser = \CUser::GetByID($user_id);
                        $arUser = $rsUser->Fetch();
                        $TAGS['MAIL_USER'] = $arUser['EMAIL'];
                        //endregion

                        //region $DATE_SEND
                        $DATE_SEND = $element['DATE_START'];
                        if($REMIND_ONE['count'] > 0) {
                            $DS_TS = $DATE_SEND->getTimestamp();
                            $DS_TS -= $REMIND_ONE['count'] * 60;
                            $DATE_SEND = \Bitrix\Main\Type\DateTime::createFromTimestamp($DS_TS);
                        }
                        //endregion

                        //region ControlSendTable
                        ControlSendTable::add(array(
                            'BOOKING_ID' => $parentId,
                            'CONTROL_SEND_ASSIGNED_BY_ID' => $USER->GetID(),
                            'CONTROL_SEND_ASSIGNED_BY_2_ID' => $user_id,
                            'DATE_CREATE' => new \Bitrix\Main\Type\DateTime(),
                            'DATE_SEND' => $DATE_SEND, //new \Bitrix\Main\Type\DateTime(),
                            'ACTION_MARK' => 'local/modules/websoft.booking/lib/entity/bookingcard.php -> ' .
                                'addChildCard -> [493]',
                            'CODE_MAIL_TEMPLATE' => 'BOOKING_BEGIN',
                            'SEND' => false,
                            'TAGS' => json_encode($TAGS,true), //$TAGS
                        ));
                        //endregion
                    }
                }
            }
        }
        //endregion


        if(!empty($debug)) { \Bitrix\Main\Diag\Debug::writeToFile($debug, "", "addChildCard.txt"); }


        return $result;
    }
    public static function changeCard($ID,$DATA) {
        $result = array('success' => true, 'data' => array());
        $errors = array();
        $UpdateCard = array();

        //region Общие

            //region Наименование мероприятия
            if(isset($DATA['NAME'])) {
                if($DATA['NAME']) { $UpdateCard['NAME'] = $DATA['NAME']; }
                else { $errors[] = 'Наименование мероприятия должно быть заполнено.'; }
            }
            //endregion

            //region Описание мероприятия
            if(isset($DATA['DESCRIPTION'])) { $UpdateCard['DESCRIPTION'] = $DATA['DESCRIPTION']; }
            //endregion

            //region Период проведения
                if(isset($DATA['DATE_START']) && isset($DATA['DATE_END'])) {
                    $UpdateCard['DATE_START'] =  new \Bitrix\Main\Type\DateTime(
                        Handler::covertDateToServer($DATA['DATE_START'])
                    );
                    $UpdateCard['DATE_END'] = new \Bitrix\Main\Type\DateTime(
                        Handler::covertDateToServer($DATA['DATE_END'])
                    );
                }
            //endregion

            //region Перерывы
            if(isset($DATA['BREAKS']) && is_array($DATA['BREAKS']) && !empty($DATA['BREAKS'])) {
                // Добавить/Обновить перерывы
                $idsBreak = array();
                foreach ($DATA['BREAKS'] as $BREAK) {
                    if(!isset($BREAK['ID'])) { // Добавление
                        $BREAK_TO_ADD = $BREAK;
                        $BREAK_TO_ADD['BOOKING_CARD_ID'] = $ID;
                        $newBreak = \Websoft\Booking\Entity\BreakTable::add($BREAK_TO_ADD);
                        $idsBreak[] = $newBreak->getId();
                    } else { // Обновление
                        $BREAK_TO_UPDATE = $BREAK;
                        $ID_BREAK = $BREAK_TO_UPDATE['ID'];
                        $idsBreak[] = $ID_BREAK;
                        unset($BREAK_TO_UPDATE['ID']);
                        \Websoft\Booking\Entity\BreakTable::update($ID_BREAK,$BREAK_TO_UPDATE);
                    }
                }
                // Определить, какие перерывы были уделены и удалить их из базы
                $idsBreak_old = array();
                $breaks_old = \Websoft\Booking\Entity\BreakTable::getListPure(true,array(
                    'BOOKING_CARD_ID' => $ID
                ));
                foreach ($breaks_old as $break_old) { $idsBreak_old[] = $break_old['ID']; }
                $diff_idsBreak = array_diff($idsBreak_old,$idsBreak);
                foreach ($diff_idsBreak as $idBreak) { \Websoft\Booking\Entity\BreakTable::delete($idBreak); }
            } else { // Удалить все перерывы
                $breaks_old = \Websoft\Booking\Entity\BreakTable::getListPure(
                    true,array( 'BOOKING_CARD_ID' => $ID )
                );
                foreach ($breaks_old as $break) { \Websoft\Booking\Entity\BreakTable::delete($break['ID']); }
            }
            //endregion

            //region Комментарий к мероприятию
            if(isset($DATA['COMMENT_EVENT'])) { $UpdateCard['COMMENT_EVENT'] = $DATA['COMMENT_EVENT']; }
            //endregion

            //region Комментарий к сопровождению мероприя
            if(isset($DATA['COMMENT_SERVICE_TYPES'])) { $UpdateCard['COMMENT_SERVICE_TYPES'] = $DATA['COMMENT_SERVICE_TYPES']; }
            //endregion

            //region Последний раз карчточка изменялась "Кем и когда?"
            //endregion

            //region Организатор
            if(isset($DATA['EXECUTOR_BY_ID'])) {

                $UpdateCard['EXECUTOR_BY_ID'] = $DATA['EXECUTOR_BY_ID'];
                //
                // /** Определение пользователя (из какой он компании) */
                // $CompanyUsers = \COption::GetOptionString("websoft.booking", "CompanyUsers");
                // $CompanyUsers = json_decode($CompanyUsers,true);
                // global $USER;
                // $COMPANY_ID_THIS_USER = '';
                // if(is_array($CompanyUsers) && count($CompanyUsers) > 0) { foreach ($CompanyUsers as $item) {
                //     if($USER->GetID() == $item['USER_ID']) { $COMPANY_ID_THIS_USER = $item['UF_COMPANIES_ID']; }
                // }};
                //$UpdateCard['EXECUTOR_BY_ID'] = $DATA['EXECUTOR_BY_ID'];
            }
            //endregion

            //region Ответственный за мероприятие
            if(isset($DATA['UF_RESPONSIBLE_EVENT']) && $DATA['UF_RESPONSIBLE_EVENT']) {
                $UpdateCard['UF_RESPONSIBLE_EVENT'] = $DATA['UF_RESPONSIBLE_EVENT'];
            }
            //endregion

            //region Общая оценка
            if($DATA['FINISH']) {
                /*$count = 0;
                $summ = 0;
                foreach ($DATA['ITEMS'] as $ITEM) {
                    foreach ($ITEM['SERVICES_DATA'] as $SERVICES_DATA) {
                        $summ += $SERVICES_DATA['RATING'];
                        $count++;
                    }
                }
                $UpdateCard['UF_SUMM_RATING'] = intval($summ / $count);*/
            }
            //endregion

            //region Файлы
            // Получить старые файлы в БД
            $UF_FILES = self::getListPure(false,array('ID'=>$ID),array('UF_FILES'))['UF_FILES'];
            if(isset($DATA['OLD_FILES']) && !empty($DATA['OLD_FILES'])) { // Удалить старые файлы
                foreach ($DATA['OLD_FILES'] as $idDelFile) {
                    if(($key = array_search($idDelFile,$UF_FILES)) !== FALSE){ unset($UF_FILES[$key]); }
                }
            }
            if(isset($DATA['NEW_FILES']) && !empty($DATA['NEW_FILES'])) { // Дабавить новые файлы
                foreach ($DATA['NEW_FILES'] as $fid) { $UF_FILES[] = $fid; }
            }
            $UpdateCard['UF_FILES'] = $UF_FILES;
            //endregion

        //endregion
        
        // Обновление:
        if(empty($errors)) {
            global $USER;
            if(!empty($UpdateCard)) {
                // Обновление основной карточки
                self::update($ID,$UpdateCard);
                // Обновление дочерних карточeк
                foreach ($DATA['ITEMS'] as $ITEM) {

                    //region Период проведения (Сдвиг события)
                    if(isset($DATA['DATE_START']) && isset($DATA['DATE_END'])) {
                        $childCard = \Websoft\Booking\Entity\BookingCardChildTable::getListPure(
                            false,array('ID' => $ITEM['ID']),array('PARENT_EVENT_ID')
                        );
                        $PARENT_EVENT_ID = $childCard['PARENT_EVENT_ID'];
                        $errors = \Websoft\Booking\Handler::changeDateEvent($PARENT_EVENT_ID,$DATA['DATE_START'],$DATA['DATE_END']);
                        if(!empty($errors)) {
                            $result['success'] = false;
                            foreach ($errors as $error) { $result['data']['errors'][] = $error; }
                        }

                        $event = \Websoft\Booking\Classes\CustomEventsCalendar::GetEventByID($PARENT_EVENT_ID);

                        \Bitrix\Main\Diag\Debug::writeToFile(array(
                            '$event' => $event
                        ), "", "test_12345.txt");


                        //region Оповещение о переносе события
                        $BookingCard_ID = $ID;

                        $BookingCard = BookingCard::getListPure(
                            false,array('ID'=>$BookingCard_ID),
                            array('ID','EXECUTOR_BY_ID','UF_RESPONSIBLE_EVENT','UF_COMPANIES_ID','NAME')
                        );
                        $BookingCardsChild = BookingCardChild::getListPure(
                            true,array('PARENT_ID' => $BookingCard['ID']), array('ID','NEGOTIATED_ID','UF_PARTICIPANTS')
                        );

                        $users = array();

                        //region 1. организатор
                        $users[] = $BookingCard['EXECUTOR_BY_ID'];
                        //endregion

                        //region 2. ответственный за мероприятие
                        if($BookingCard['UF_RESPONSIBLE_EVENT']) { $users[] = $BookingCard['UF_RESPONSIBLE_EVENT']; }
                        //endregion

                        //region 3. администраторы компаний
                        if(empty($BookingCard['UF_COMPANIES_ID'])) {
                            foreach ($BookingCardsChild as $child_card) {
                                $neg = Negotiated::getListPure(false,array('ID'=>$child_card['NEGOTIATED_ID']),array('COMPANY_ID'));
                                $company = CompanyBooking::getListPure(false,array('ID'=>$neg['COMPANY_ID']),array('ASSIGNED_BY_ID'));
                                $users[] = $company['ASSIGNED_BY_ID'];
                            }
                        } else {
                            foreach ($BookingCard['UF_COMPANIES_ID'] as $company_id) {
                                $company = CompanyBooking::getListPure(false,array('ID'=>$company_id),array('ASSIGNED_BY_ID'));
                                $users[] = $company['ASSIGNED_BY_ID'];
                            }
                        }
                        //endregion

                        //region 4. ответсвенные за переговорные
                        foreach ($BookingCardsChild as $child_card) {
                            $neg = Negotiated::getListPure(false,array('ID'=>$child_card['NEGOTIATED_ID']),array('ASSIGNED_BY_ID'));
                            $users[] = $neg['ASSIGNED_BY_ID'];
                        }
                        //endregion

                        //region 5. участники
                        foreach ($BookingCardsChild as $bookingCardChild) {
                            foreach ($bookingCardChild['UF_PARTICIPANTS'] as $part_user_id) { $users[] = $part_user_id; }
                        }
                        //endregion

                        //region 6. ответственным за сервисы
                        foreach ($BookingCardsChild as $bookingCardChild) {
                            $SearchServices = \Websoft\Booking\Entity\ServiceDataTable::getListPure(
                                true, array('UF_SNAP_CHILD_CARD' => $bookingCardChild['ID']), array('RESPONSIBLE_ID'));
                            foreach ($SearchServices as $searchService) { $users[] = $searchService['RESPONSIBLE_ID']; }
                        }
                        //endregion

                        $users = array_unique($users);

                        // ... сбор данных для отправки
                        $TAGS = array(
                            'MAIL_USER' => 'Email получателя сообщения',
                            'BOOKING_ID' => $BookingCard_ID,
                            'BOOKING_NAME' => $BookingCard['NAME'],
                        );

                        global $USER;
                        foreach ($users as $user_id) {

                            //region $TAGS['MAIL_USER']
                            $arUser = \CUser::GetByID($user_id)->Fetch();
                            $TAGS['MAIL_USER'] = $arUser['EMAIL'];
                            //endregion

                            \Websoft\Booking\Entity\ControlSendTable::add(array(
                                'BOOKING_ID' => $BookingCard_ID,
                                'CONTROL_SEND_ASSIGNED_BY_ID' => $USER->GetID(),
                                'CONTROL_SEND_ASSIGNED_BY_2_ID' => $user_id,
                                'DATE_CREATE' => new \Bitrix\Main\Type\DateTime(),
                                'DATE_SEND' => new \Bitrix\Main\Type\DateTime(),
                                'ACTION_MARK' => 'local/modules/websoft.booking/lib/entity/bookingcard.php => [750]',
                                'CODE_MAIL_TEMPLATE' => 'BOOKING_TRANSFER',
                                'SEND' => false,
                                'TAGS' => json_encode($TAGS,true), //$TAGS
                            ));
                        }
                        //endregion

                    }
                    //endregion

                    $dataToUpdateChildCard = array();

                    //region Статус подготовки
                    /*if(isset($ITEM['TRAINING_STATUS']['SELECT'])) {
                        $dataToUpdateChildCard['TRAINING_STATUS_ID'] = $ITEM['TRAINING_STATUS']['SELECT'];
                    }*/
                    //endregion

                    //region Общее описание подготовки
                    if(isset($ITEM['STATUS_DESCRIPTION'])) {

                        $ServiceNegotiatedData = ServiceNegotiatedDataTable::getListPure(false,array(
                            'SNAP_CARD_ID' => $ITEM['ID'], 'DATE_BEGIN' => $DATA['DATE_EVENT']
                        ),array('ID','STATUS_DESCRIPTION'));
                        ServiceNegotiatedDataTable::update($ServiceNegotiatedData['ID'],array(
                            'STATUS_DESCRIPTION' => $ITEM['STATUS_DESCRIPTION']
                        ));
                        if($ServiceNegotiatedData['STATUS_DESCRIPTION'] != $ITEM['STATUS_DESCRIPTION']) {
                            $history = array();
                            $history[] = \Websoft\Booking\Entity\ServiceNegotiatedDataTable::generateRowHistory(
                                $USER->GetID(),$ITEM['NAME'].' -> Общее описание подготовки',
                                $ServiceNegotiatedData['STATUS_DESCRIPTION'], $ITEM['STATUS_DESCRIPTION'],$ID
                            );
                            self::AddRowsHistory($history);
                        }
                    }
                    //endregion

                    //region Организация и сопровождение мероприятия
                    if(isset($ITEM['RATING_1'])) {
                        $ServiceNegotiatedData = ServiceNegotiatedDataTable::getListPure(false,array(
                            'SNAP_CARD_ID' => $ITEM['ID'], 'DATE_BEGIN' => $DATA['DATE_EVENT']
                        ),array('ID','RATING_1'));
                        ServiceNegotiatedDataTable::update($ServiceNegotiatedData['ID'],array(
                            'RATING_1' => $ITEM['RATING_1']
                        ));
                        if($ServiceNegotiatedData['RATING_1'] != $ITEM['RATING_1']) {
                            $history = array();
                            $history[] = \Websoft\Booking\Entity\ServiceNegotiatedDataTable::generateRowHistory(
                                $USER->GetID(),$ITEM['NAME'].' -> Организация и сопровождение мероприятия',
                                $ServiceNegotiatedData['RATING_1'], $ITEM['RATING_1'],$ID
                            );
                            self::AddRowsHistory($history);
                        }
                    }
                    //endregion

                    //region Подготовка оборудования
                    if(isset($ITEM['RATING_2'])) {
                        $ServiceNegotiatedData = ServiceNegotiatedDataTable::getListPure(false,array(
                            'SNAP_CARD_ID' => $ITEM['ID'], 'DATE_BEGIN' => $DATA['DATE_EVENT']
                        ),array('ID','RATING_2'));
                        ServiceNegotiatedDataTable::update($ServiceNegotiatedData['ID'],array(
                            'RATING_2' => $ITEM['RATING_2']
                        ));

                        /*if($ServiceNegotiatedData['RATING_2'] != $ITEM['RATING_2']) {
                            $history = array();
                            $history[] = \Websoft\Booking\Entity\ServiceNegotiatedDataTable::generateRowHistory(
                                $USER->GetID(),$ITEM['NAME'].' -> Подготовка оборудования',
                                $ServiceNegotiatedData['RATING_2'], $ITEM['RATING_2'],$ID
                            );
                            self::AddRowsHistory($history);
                        }*/
                    }
                    //endregion

                    //region Питание
                    if(isset($ITEM['RATING_3'])) {
                        $ServiceNegotiatedData = ServiceNegotiatedDataTable::getListPure(false,array(
                            'SNAP_CARD_ID' => $ITEM['ID'], 'DATE_BEGIN' => $DATA['DATE_EVENT']
                        ),array('ID','RATING_3'));
                        ServiceNegotiatedDataTable::update($ServiceNegotiatedData['ID'],array(
                            'RATING_3' => $ITEM['RATING_3']
                        ));

                        if($ServiceNegotiatedData['RATING_3'] != $ITEM['RATING_3']) {
                            $history = array();
                            $history[] = \Websoft\Booking\Entity\ServiceNegotiatedDataTable::generateRowHistory(
                                $USER->GetID(),$ITEM['NAME'].' -> Подготовка оборудования',
                                $ServiceNegotiatedData['RATING_3'], $ITEM['RATING_3'],$ID
                            );
                            self::AddRowsHistory($history);
                        }
                    }
                    //endregion

                    //region Сервисы сопровождения
                    $allGood = true;
                    foreach ($ITEM['SERVICES_DATA'] as $SERVICE_DATA) {

                        if($SERVICE_DATA['STATUS']['ID'] != 1) { $allGood = false; }

                        $dataToUpdateServiceData = array();

                        //region Получить старые значения Комментария и Рейтига
                        $SERVICE_DATA_OLD = \Websoft\Booking\Entity\ServiceDataTable::getListPure(
                            false,array('ID' => $SERVICE_DATA['ID']),array('COMMENT','RATING','STATUS_ID')
                        );
                        //endregion

                        $SERVICE_DATA_HISTORY = array();

                        //region Комментарий
                        /*if(isset($SERVICE_DATA['COMMENT'])) {
                            $dataToUpdateServiceData['COMMENT'] = $SERVICE_DATA['COMMENT'];
                            if($SERVICE_DATA_OLD['COMMENT'] != $SERVICE_DATA['COMMENT']) {
                                $SERVICE_DATA_HISTORY[] = self::generateRowHistory(
                                    $USER->GetID(),$ITEM['NAME'].' -> '.$SERVICE_DATA['NAME'],
                                    $SERVICE_DATA_OLD['COMMENT'], $SERVICE_DATA['COMMENT'],$ID
                                );
                            }
                        }*/
                        //endregion

                        //region Статус готовности
                        if(isset($SERVICE_DATA['STATUS'])) {
                            $dataToUpdateServiceData['STATUS_ID'] = $SERVICE_DATA['STATUS']['ID'];
                            if($SERVICE_DATA_OLD['STATUS_ID'] != $SERVICE_DATA['STATUS']['ID']) {
                                $Statuses = ServiceDataTable::GetStatuses();
                                $SERVICE_DATA_HISTORY[] = self::generateRowHistory(
                                    $USER->GetID(),$ITEM['NAME'].' -> Статус готовности',
                                    $Statuses[$SERVICE_DATA_OLD['STATUS_ID']],
                                    $Statuses[$SERVICE_DATA['STATUS']['ID']],
                                    $ID
                                );
                            }
                        }
                        //endregion

                        //region Рейтинг
                        if(isset($SERVICE_DATA['RATING'])) {
                            $dataToUpdateServiceData['RATING'] = $SERVICE_DATA['RATING'];
                            if(intval($SERVICE_DATA_OLD['RATING']) != intval($SERVICE_DATA['RATING'])) {
                                $SERVICE_DATA_HISTORY[] = \Websoft\Booking\Entity\ServiceDataTable::generateRowHistory(
                                    $USER->GetID(),$ITEM['NAME'].' -> Оценка',
                                    $SERVICE_DATA_OLD['RATING'], $SERVICE_DATA['RATING'],$ID
                                );
                            }
                        }
                        //endregion

                        if(!empty($SERVICE_DATA_HISTORY)) { self::AddRowsHistory($SERVICE_DATA_HISTORY); }
                        if(!empty($dataToUpdateServiceData)) {
                            \Websoft\Booking\Entity\ServiceDataTable::update(
                                $SERVICE_DATA['ID'],$dataToUpdateServiceData
                            );
                        }
                    }
                    //endregion

                    //region Определить общий статус подготовки
                    $ServiceNegotiatedData = ServiceNegotiatedDataTable::getListPure(false,array(
                        'SNAP_CARD_ID' => $ITEM['ID'], 'DATE_BEGIN' => $DATA['DATE_EVENT']
                    ));
                    $newMainStatus = 1;
                    if(!$allGood) { $newMainStatus = 2; }
                    ServiceNegotiatedDataTable::update(
                        $ServiceNegotiatedData['ID'],array('STATUS_MAIN_ID' => $newMainStatus));
                    //endregion

                    if(!empty($dataToUpdateChildCard)) {
                        \Websoft\Booking\Entity\BookingCardChildTable::update($ITEM['ID'],$dataToUpdateChildCard);
                    }

                }
            }
        } else { $result['success'] = false; $result['data']['errors'] = $errors; }

        // Тестовые данные
        $result['data']['test'] = array(
            '$UpdateCard' => $UpdateCard,
            '$errors' => $errors,
            '$DATA' => $DATA
        );

        return $result;
    }

    //region События
    // ... Перед обновлением
    public static function onBeforeUpdate ($event) {

        global $USER;
        $user_id = $USER->GetID();

        $parameters = $event->getParameters();
        $fields = $parameters['fields'];

        // Получить данные из БД
        $keys = array();
        foreach ($fields as $key => $val) { $keys[] = $key; }
        $ID = $parameters['id']['ID'];
        $fields_old = self::getListPure(false,array('ID'=>$ID),$keys);

        $cl = new \ReflectionClass(get_called_class());
        $pathLangFile = $cl->getFileName();
        \Bitrix\Main\Localization\Loc::loadLanguageFile($pathLangFile,'ru');

        // Расходждение массивов
        //...Д/поля Файлы

        $debug = array();

        $fields_files = $fields['UF_FILES'];
        //$debug['$fields_files'] = $fields_files;

        $fields_old_files = $fields_old['UF_FILES'];
        //$debug['$fields_old_files'] = $fields_old_files;

        unset($fields['UF_FILES']);
        unset($fields_old['UF_FILES']);

        $array_diff_files = array_diff($fields_old_files,$fields_files);
        if(empty($array_diff_files)) { $array_diff_files = array_diff($fields_files,$fields_old_files); }

        if(!empty($array_diff_files)) {
            $history = array();

            $before = '';
            foreach ($fields_old_files as $fid) {
                $file = \CFile::GetFileArray($fid); $before .= $file['ORIGINAL_NAME'].'<br>';
            }

            $after = '';
            foreach ($fields_files as $fid) {
                $file = \CFile::GetFileArray($fid); $after .= $file['ORIGINAL_NAME'].'<br>';
            }

            $history[] = self::generateRowHistory(
                $user_id,Loc::getMessage('UF_FILES'),$before,$after,$ID,get_called_class()
            );
            //$DebugData['$history'] = $history;
            self::AddRowsHistory($history);
        }

        // ... Полей типа ID пользователя
        $data_TYPE_ID_USER = array();
        $typeID_USER = array('UF_RESPONSIBLE_EVENT');
        foreach ($fields as $key => $value) { if(in_array($key,$typeID_USER)) { if($value != $fields_old[$key]) {
            $data_TYPE_ID_USER[$key] = array( 'NewValue' => $value, 'OldValue' => $fields_old[$key] );
        } } }
        foreach ($typeID_USER as $keyToDestroy) { unset($fields[$keyToDestroy]); unset($fields_old[$keyToDestroy]); }
        if(!empty($data_TYPE_ID_USER)) {
            $history = array();
            foreach ($data_TYPE_ID_USER as $key3 => $dataTypeID_User_One) {
                $history[] = self::generateRowHistory(
                    $user_id,Loc::getMessage($key3),
                    Handler::fullNameUser($dataTypeID_User_One['OldValue']),
                    Handler::fullNameUser($dataTypeID_User_One['NewValue']),
                    $ID,get_called_class()
                );
            }
            self::AddRowsHistory($history);
        }

        // Остальные
        //$debug['$fields'] = $fields;
        //$debug['$fields_old'] = $fields_old;

        //$array_diff = array_diff($fields,$fields_old);
        $dateChange = false;
        $array_diff = array();
        foreach ($fields as $key => $val) {
            $old_val = $fields_old[$key];
            if($key != 'DATE_START' && $key != 'DATE_END') {
                if($val != $old_val) { $array_diff[$key] = $val; }
            } else {
                $old_val_convert = $fields_old[$key]->getTimestamp();
                $val_convert = $val->getTimestamp();
                if($val_convert != $old_val_convert) {
                    $fields_old[$key] = $fields_old[$key]->toString();
                    $array_diff[$key] = $val->toString();
                    $dateChange = true;
                }
            }
        }

        //region Уведомить о переносе мероприятия
        if($dateChange) {
            //$debug[] = 'Hello world =)';
        }
        //endregion

        //$debug['$array_diff'] = $array_diff;

        if(!empty($array_diff)) {
            $history = array();
            foreach ($array_diff as $key2 => $value) {
                $history[] = self::generateRowHistory(
                    $user_id,Loc::getMessage($key2),$fields_old[$key2],$value,$ID,get_called_class()
                );
            }
            self::AddRowsHistory($history);
        }

        if(!empty($debug)) { \Bitrix\Main\Diag\Debug::writeToFile($debug, "", "log_111.txt"); }
    }
    // ... После обновления

    //endregion

    //region Системные методы
    private static function convertDateToTimeServer($date) {
        $dateUser = strtotime($date);
        $time_string = ConvertTimeStamp($dateUser-\CTimeZone::GetOffset(), "FULL");
        return $time_string;
    }
    private static function changeDate($cntDays,$date) {
        $date = new \DateTime($date);
        $date->modify($cntDays.' day');
        return $date->format('d.m.Y H:i:s');
    }
    private static function checkBusyUsers($dateFrom,$dateTo,$asUsers) {
        if(\CModule::IncludeModule("calendar")) {

            // Преобразовать входящие даты, ко времени сервера
            //$dateFrom = self::convertDateToTimeServer($dateFrom);
            //$dateTo = self::convertDateToTimeServer($dateTo);

            //echo '$dateFrom: '.$dateFrom.'________';
            //echo '$dateTo: '.$dateTo.'________';

            // 1) +-1 day
            $dateFrom_after = self::changeDate('-1',$dateFrom);
            $dateTo_after = self::changeDate('+1',$dateTo);

            //echo '$dateFrom_after: '.$dateFrom_after.'________';
            //echo '$dateTo_after: '.$dateTo_after.'________';

            // 2) Получить события за этот интервал
            //$params = array('arFilter' => array('FROM_LIMIT' => $dateFrom_after, 'TO_LIMIT' => $dateTo_after));
            //$params = array('arFilter' => array('FROM_LIMIT' => '29.09.2018', 'TO_LIMIT' => '29.09.2018'));
            //$calendarEvents = \CCalendarEvent::GetList($params);

            $originalDate1 = date("d.m.Y", strtotime($dateFrom_after)); //"27.09.2018 00:12:28";
            $originalDate2 = date("d.m.Y", strtotime($dateTo_after));
            $from = new \DateTime($originalDate1);
            $to   = new \DateTime($originalDate2);
            $period = new \DatePeriod($from, new \DateInterval('P1D'), $to);
            $arrayOfDates = array_map(
                function($item){ return $item->format('Y-m-d'); },
                iterator_to_array($period)
            );
            $arrayOfDates[] = date("Y-m-d", strtotime($dateTo_after));
            $calendarEvents = array();
            foreach ($arrayOfDates as $ofDate) {
                /*$arEvents = \CCalendar::GetNearestEventsList(array( 'fromLimit' => $ofDate, 'toLimit' => $ofDate ));
                foreach ($arEvents as $event) {
                    $calendarEvents[] = $event;
                }*/
                /*$params = array('arFilter' => array('FROM_LIMIT' => $ofDate, 'TO_LIMIT' => $ofDate));
                $calendarEvents = \CCalendarEvent::GetList($params);*/

                //echo '$ofDate: '.$ofDate.'______';
                $ofDateFrom = $ofDate.' 00:00:00';
                $ofDateTo = $ofDate.' 23:59:59';
                global $DB;
                $strSql = "SELECT ID from b_calendar_event where DELETED='N' AND DATE_FROM >= '{$ofDateFrom}' AND DATE_TO <= '{$ofDateTo}'";
                $res = $DB->Query($strSql, false, "FILE: ".__FILE__."<br> LINE: ".__LINE__);
                while ($el = $res->Fetch()) {
                    /*echo '<pre>';
                    print_r($el);
                    echo '</pre>';*/
                    $calendarEvents[] = \CCalendarEvent::GetById($el['ID'],false);
                }
            }

            // 3) Получить те события, в которых есть искомые пользователи + ковертировать время события на время сервера
            $calendarEvents_AsUsers = array();
            foreach ($calendarEvents as $calendarEvent) {
                if (isset($calendarEvent['~ATTENDEES'])) {
                    $users = array();
                    foreach ($calendarEvent['~ATTENDEES'] as $user_attends) { $users[] = $user_attends['USER_ID']; }
                    $add = false;
                    foreach ($asUsers as $user_id) { if(in_array($user_id,$users)) { $add = true; break; }}
                    if($add) {
                        //$DATE_FROM = ConvertTimeStamp(strtotime($calendarEvent['DATE_FROM'])-\CTimeZone::GetOffset(),"FULL");
                        //$DATE_TO = ConvertTimeStamp(strtotime($calendarEvent['DATE_TO'])-\CTimeZone::GetOffset(),"FULL");
                        $DATE_FROM = $calendarEvent['DATE_FROM'];
                        $DATE_TO = $calendarEvent['DATE_TO'];
                        $calendarEvents_AsUsers[] = array(
                            'NAME'=>$calendarEvent['NAME'],
                            'DATE_FROM'=>$DATE_FROM,
                            'DATE_TO'=>$DATE_TO,
                            //'$calendarEvent' => $calendarEvent,
                            'users'=>$users
                        );
                    }
                }
            }
            
            /*echo '<b>$calendarEvents_AsUsers</b>';
            echo '<pre>';
            print_r($calendarEvents_AsUsers);
            echo '</pre>';
            echo '<hr>';*/

            //die();

            // 4) Определить занятость пользователя

            //echo '$dateFrom: '.$dateFrom.'___';
            //echo '$dateTo: '.$dateTo.'___';

            $arUserBusy = array();
            foreach ($calendarEvents_AsUsers as $calendarEvents_AsUser) {

                /*echo '<pre>';
                print_r($calendarEvents_AsUsers['NAME']);
                echo '</pre>';*/

                if( strtotime($calendarEvents_AsUser['DATE_FROM'])<=strtotime($dateTo) &&
                    strtotime($calendarEvents_AsUser['DATE_TO'])>=strtotime($dateFrom) ) {
                    foreach ($calendarEvents_AsUser['users'] as $user_id) {
                        if(!in_array($user_id,$arUserBusy)) { $arUserBusy[] = $user_id; }
                    }
                }
            }
            $result = array();
            foreach ($asUsers as $user_id) { if(in_array($user_id,$arUserBusy)) { $result[] = $user_id; } }
            return $result;
        }
    }
    private static function SetEvent(
        $negId, $owner_id, $users, $dateBegin, $dateEnd, $name, $description, $rrule = false, $remind = false
    ) {
        if(\CModule::IncludeModule("calendar")) {

            // 0) Результирующий массив с ID-шниками созданных событий
            //$result = array();

            // 1) Входящие данные
            //$negId = 1; // ID переговорной (наш модуль)
            //$owner_id = 1; // ID создателя
            //$users = array(1, 254, 11); // Список пользователей
            //$dateBegin = '25.09.2018 12:00:00'; // Дата начала
            //$dateEnd = '25.09.2018 14:00:00'; // Дата окончания
            //$name = 'Наименование мероприятия'; // Наименование мероприятия
            //$description = 'Описание мероприятия'; // Описание мероприятия

            // 2) Конвертирование времени (считаем что входящие дата начала/окочание берётся из настроек пользовате и преобразуем
            // их во время сервера)
            $dateBegin = self::convertDateToTimeServer($dateBegin);
            $dateEnd = self::convertDateToTimeServer($dateEnd);

            // 2) Создать бронь переговорной в модуле "Сервисы -> "
            $neg = NegotiatedTable::getListPure(false, array('ID' => $negId), array('SNAP_TO_MEETING'));
            $meeting_id = $neg['SNAP_TO_MEETING'];

            // 3) Добавить родительское событие
            $objDateTime = new \Bitrix\Main\Type\DateTime();
            $tz = $objDateTime->getTimeZone()->getName();

            $arEventFields = array(
                'CAL_TYPE' => 'user',
                'OWNER_ID' => $owner_id,
                'DT_FROM' => $dateBegin,
                //'TIMESTAMP_X' => $dateBegin,
                'DT_TO' => $dateEnd,
                'TZ_FROM' => $tz,
                'TZ_TO' => $tz,
                'NAME' => $name,
                'DESCRIPTION' => \CCalendar::ParseHTMLToBB($description),
                'IS_MEETING' => true,
                'MEETING_HOST' => $owner_id,
                'MEETING' => array( 'HOST_NAME' => \CCalendar::GetUserName($owner_id) ),
                'ATTENDEES' => $users,
                'LOCATION' => 'ECMR_'.$meeting_id, // Воркает
                /*'LOCATION' => array(
                    'OLD' => '',
                    'NEW' => 'ECMR_'.$meeting_id.'_'.$bargaining_id,
                )*/

                /*'RRULE' => array(
                    'FREQ' => "DAILY", // NONE - Не повторять
                                       // DAILY - Каждый день
                                       // MONTHLY - Каждый месяц
                                       // WEEKLY - Каждую неделю
                                       // YEARLY - Каждый год
                    'INTERVAL' => '2'  // Интервал
                    'COUNT' => 2,      // Кол-во повторений
                    'UNTIL' => ''      // Дата остановки (только дата)
                )*/
                // $_REQUEST['rrule_endson'] // never||count||until

                //region Напоминать
                /*'REMIND' => Array(
                    Array( 'type' => 'min', 'count' => '0'),    // В момент события
                    Array( 'type' => 'min', 'count' => '10'),   // За 10 минут
                    Array( 'type' => 'min', 'count' => '15'),   // За 15 минут
                    Array( 'type' => 'min', 'count' => '30'),   // За 30 минут
                    Array( 'type' => 'min', 'count' => '60'),   // За час
                    Array( 'type' => 'min', 'count' => '120'),  // За 2 часа
                    Array( 'type' => 'min', 'count' => '120'),  // За 2 часа
                    Array( 'type' => 'min', 'count' => '1440'), // За сутки
                    Array( 'type' => 'min', 'count' => '2880'), // За сутки
                ),*/
                //endregion
            );

            if($rrule) { $arEventFields['RRULE'] = $rrule; }
            if($remind) { $arEventFields['REMIND'] = $remind; }

            $EventID = \CCalendar::SaveEvent(array(
                'arFields'=>$arEventFields,
                'userId'=>$owner_id,
                'autoDetectSection'=>true,
                'autoCreateSection'=>true
            ));



            return $EventID;
        }
    }
    //endregion
}