<?php

namespace Websoft\Booking\Entity;
use \Websoft\Booking\Classes\EntityBase;

use Bitrix\Main\Entity\IntegerField;   // TODO:: Целочисленное
use Bitrix\Main\Entity\ReferenceField; // TODO:: Свзять поля сущности с другой сущностью/элементом, работает в паре с IntegerField
use Bitrix\Main\Entity\StringField;    // TODO:: Строка
use Bitrix\Main\Entity\DatetimeField;  // TODO:: ДатаВремя
use Bitrix\Main\Entity\BooleanField;   // TODO:: Булеан
use Bitrix\Main\Entity\FloatField;     // TODO:: Число с плавающей точкой
use Bitrix\Main\Entity\EnumField;      // TODO:: Список
use Bitrix\Main\Entity\TextField;      // TODO:: Текстовое поле, Тоже самое что и строка, но можно определить тип

use Bitrix\Main\Entity\UField; // TODO: Его отключили.... =(

use Bitrix\Main\UserTable;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Grid\Panel\Actions;
use Bitrix\Main\Grid\Panel\Types;
use Bitrix\Main\Error;
use Bitrix\Main\Type\Dictionary\ErrorCollection;
use \Bitrix\Main\Localization\Loc;

/**
 * Тестовая сущность 1
 * Class TestEntity1Table
 * @package Websoft\Blank\Entity
 */
class CityTable extends EntityBase {

    public static function getMap() {
        return array(
            new IntegerField('ID', array('primary' => true, 'autocomplete' => true)),
            new StringField('NAME'),
            new BooleanField('ACTIVE'),
        );
    }
    public static function getMapUF() {
        return array(
            //self::UF_String('UF_TestString',false,false,'TEST_STRING'),
            //self::UF_File('UF_PREVIEW_IMAGE',false,false,'PREVIEW_IMAGE'),
        );
    }

    //region Управление страницей создания/редактирования
    public static function getNameOneElement() { return self::getMessage('NameOneElementToAdd'); }
    public static function getFieldsToEdit() {
        \Bitrix\Main\Localization\Loc::loadMessages(__FILE__);
        return self::genHeader( array(
            //'ID'          => self::field(0,'i',0,'d','',60,'minimal'),
            'NAME'   => self::field(1,'t'),
            'ACTIVE' => self::field(1,'c'),
        ));
    }
    public static function validateFormEdit($element, $errors) {
        if (empty($element['NAME'])) {
            $errors->setError(new Error('Не указанно название...'));
        }
        /*
        if (empty($element['ASSIGNED_BY_ID'])) {
            $errors->setError(new Error(Loc::getMessage('CRMSTORES_ERROR_EMPTY_ASSIGNED_BY_ID')));
        } else {
            $dbUser = UserTable::getById($element['ASSIGNED_BY_ID']);
            if ($dbUser->getSelectedRowsCount() <= 0) {
                $errors->setError(new Error(Loc::getMessage('CRMSTORES_ERROR_UNKNOWN_ASSIGNED_BY_ID')));
            }
        }*/
        return $errors;
    }
    //endregion

    //region Управление гридом
    public static function getStringName() {
        return  \Bitrix\Main\Localization\Loc::getMessage('CityTable');
    }
    public static function getHeaderGrid() {
        \Bitrix\Main\Localization\Loc::loadMessages(__FILE__);
        return self::genHeader( array(
            'ID'          => self::field(0,'i',0,'d','',60,'minimal'),
            'NAME'        => self::field(1,'t',1),
            'ACTIVE' => self::field(1,'c',1),
        ));
    }
    public static function filterFieldsGrid() {
        \Bitrix\Main\Localization\Loc::loadMessages(__FILE__);
        return self::genFilter(array(
            'ID'        => self::field(0),
            'NAME'      => self::field(1),
            'ACTIVE' => self::field(1,'c',1),
        ));
    }
    public static function uniqueDisplayColumn($row) {
        return array(
            //'NAME' => '<a href="/" target="_self">' . $row['NAME'] . '</a>',
            /*'ASSIGNED_BY' => empty($row['ASSIGNED_BY']) ? '' : \CCrmViewHelper::PrepareUserBaloonHtml(
                array(
                    'PREFIX' => "{$row['ID']}_RESPONSIBLE",
                    'USER_ID' => $row['ASSIGNED_BY_ID'],
                    'USER_NAME'=> \CUser::FormatName(\CSite::GetNameFormat(), $row['ASSIGNED_BY']),
                    'USER_PROFILE_URL' => Option::get('intranet', 'path_user', '', SITE_ID) . '/'
                )
            )*/
        );
    }
    public static function addCustomAction($gridManagerId,$applyButton) {
        return array(
            /*//region Одно шаговое действие
            array(
                'NAME' => GetMessage('TestEntity1Table_CustomAction1'),
                'VALUE' => 'one_action',
                'ONCHANGE' => array(
                    array( 'ACTION' => Actions::CREATE, 'DATA' => array($applyButton) ),
                    array(
                        'ACTION' => Actions::CALLBACK, 'DATA' => array(
                            array('JS' => "BX.CrmUIGridExtension.processActionChange('".$gridManagerId."', 'one_action')")
                        )
                    )
                )
            ),
            //endregion

            //region действие "Выбор ответсвенного" из коробки
            array(
                'NAME' => GetMessage('TestEntity1Table_CustomAction2'),
                'VALUE' => 'assign_to',
                'ONCHANGE' => array(
                    array(
                        'ACTION' => Actions::CREATE,
                        'DATA' => array(
                            array(
                                'TYPE' => Types::TEXT,
                                'ID' => 'action_assigned_by_search',
                                'NAME' => 'ACTION_ASSIGNED_BY_SEARCH'
                            ),
                            array(
                                'TYPE' => Types::HIDDEN,
                                'ID' => 'action_assigned_by_id',
                                'NAME' => 'ACTION_ASSIGNED_BY_ID'
                            ),
                            $applyButton
                        )
                    ),
                    array(
                        'ACTION' => Actions::CALLBACK,
                        'DATA' => array(
                            array(
                                'JS' => "BX.CrmUIGridExtension.prepareAction('{$gridManagerId}', 'assign_to',  { searchInputId: 'action_assigned_by_search_control', dataInputId: 'action_assigned_by_id_control', componentName: '{$gridManagerId}_ACTION_ASSIGNED_BY' })"
                            )
                        )
                    ),
                    array(
                        'ACTION' => Actions::CALLBACK,
                        'DATA' => array(
                            array('JS' => "BX.CrmUIGridExtension.processActionChange('{$gridManagerId}', 'assign_to')")
                        )
                    )
                )
            ),
            //endregion

            //region Двух шаговое действие
            array(
                'NAME' => GetMessage('TestEntity1Table_CustomAction3'),
                'VALUE' => 'multiply_action',
                'ONCHANGE' => array(
                    array(
                        'ACTION' => Actions::CREATE,
                        'DATA' => array(
                            array(
                                'TYPE' => Types::DROPDOWN,
                                'ID' => 'id_1', // Д/JS-ки
                                'NAME' => 'ACTION_ID_1', // Д/Обработчика
                                'ITEMS' => array(
                                    array('NAME' => 'Значение 1', 'VALUE' => 'KEY_1_1'),
                                    array('NAME' => 'Значение 2', 'VALUE' => 'KEY_1_2'),
                                )
                            ),
                            $applyButton
                        )
                    ),
                    array(
                        'ACTION' => Actions::CALLBACK,
                        'DATA' => array(array('JS' => "BX.CrmUIGridExtension.processActionChange('{$gridManagerId}', 'multiply_action')"))
                    )
                )
            )
            //endregion*/
        );
    }
    public static function processGridActions($action,$allRows,$request){
        $dataToDebag = array();
        switch ($action) {
            case 'one_action':
                $dataToDebag[] = 'Поймали событие =) "one_action"';
                break;
        }
    }
    public static function getActions() {
        return self::generationAtions(
            true,
            true,
            true
        );
    }
    //endregion

    //region Проверка полноценности данных (предупреждения)
    public static function ListWarning() {
        //self::getList();
        return array();
    }
    //endregion
}
