<?php

namespace Websoft\Booking\Entity;
use \Websoft\Booking\Classes\EntityBase;

use Bitrix\Main\Entity\IntegerField;   // TODO:: Целочисленное
use Bitrix\Main\Entity\ReferenceField; // TODO:: Свзять поля сущности с другой сущностью/элементом, работает в паре с IntegerField
use Bitrix\Main\Entity\StringField;    // TODO:: Строка
use Bitrix\Main\Entity\DatetimeField;  // TODO:: ДатаВремя
use Bitrix\Main\Entity\BooleanField;   // TODO:: Булеан
use Bitrix\Main\Entity\FloatField;     // TODO:: Число с плавающей точкой
use Bitrix\Main\Entity\EnumField;      // TODO:: Список
use Bitrix\Main\Entity\TextField;      // TODO:: Текстовое поле, Тоже самое что и строка, но можно определить тип

use Bitrix\Main\Entity\UField; // TODO: Его отключили.... =(

use Bitrix\Main\UserTable;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Grid\Panel\Actions;
use Bitrix\Main\Grid\Panel\Types;
use Bitrix\Main\Error;
use Bitrix\Main\Type\Dictionary\ErrorCollection;
use \Bitrix\Main\Localization\Loc;

/**
 * Компании
 * Class СompanyBooking
 * @package Websoft\Booking\Entity
 */
class CompanyBookingTable extends EntityBase {

    public static function getMap() {
        return array(
            new IntegerField('ID', array('primary' => true, 'autocomplete' => true)),
            new StringField('NAME'),
            new BooleanField('ACTIVE'),
            new IntegerField('ASSIGNED_BY_ID'), // Администратор компании
            new ReferenceField( //Описание связи поля ASSIGNED_BY_ID с другой сущностью -> А можно свзять с Инфоблоком?
                'ASSIGNED_BY', UserTable::getEntity(), array('=this.ASSIGNED_BY_ID' => 'ref.ID')
            )
        );
    }
    public static function getMapUF() {
        return array(
            self::UF_File('UF_LOGO',false,false),
            //self::UF_Integer('UF_SNAP_NEGOTIATION',true,false),
        );
    }

    //region Управление страницей создания/редактирования
    public static function getNameOneElement() { return self::getMessage('NameOneElementToAdd'); }
    public static function getFieldsToEdit($ID = '',$DataToRequest = '') {
        \Bitrix\Main\Localization\Loc::loadMessages(__FILE__);
        /*ob_start();
            $selected = array();
            if($ID) {
                $element = self::getList(array(
                    'filter' =>array('ID'=>$ID),'select' => array('UF_SNAP_NEGOTIATION'))
                )->fetchAll();
                if(!empty($element)) {
                    $element = $element[0];
                    $selected = $element['UF_SNAP_NEGOTIATION'];
                }
            } elseif($DataToRequest) { $selected = $DataToRequest['UF_SNAP_NEGOTIATION']; }
            $res = \Websoft\Booking\Entity\NegotiatedTable::getList(array('select' => array('ID','NAME')))->fetchAll();
            $countView = 10;
            if(count($res) < $countView) { $countView = count($res); }
            */?><!--
            <select name="UF_SNAP_NEGOTIATION[]" multiple size="<?/*=$countView*/?>">
                <?/* foreach ($res as $neg): */?>
                    <option <?/*=(in_array($neg['ID'],$selected)) ? 'selected' : ''*/?> value="<?/*=$neg['ID']*/?>" ><?/*=$neg['NAME']*/?></option>
                <?/* endforeach; */?>
            </select>
        --><?/* $UF_SNAP_TO_NEGOTIATION_HTML = ob_get_clean();*/
        return self::genHeader( array(
            'NAME'    => self::field(1,'t'),
            'ACTIVE'  => self::field(1,'c'),
            'UF_LOGO' => self::field(1,'f'),
            'ASSIGNED_BY' => self::field(1,'u'),
            //'UF_SNAP_TO_NEGOTIATION' => self::fieldCustom($UF_SNAP_TO_NEGOTIATION_HTML,1),
            //'UF_SNAP_TO_NEGOTIATION' => self::field(1),
        ));
    }
    public static function validateFormEdit($element, $errors) {
        if (empty($element['NAME'])) { $errors->setError(new Error('Не указанно название.')); }
        if (empty($element['UF_LOGO'])) { $errors->setError(new Error('Не установлен логотип компании.')); }
        if (empty($element['ASSIGNED_BY_ID'])) { $errors->setError(new Error('Не назначен администратор компании')); }
        else {
            $dbUser = UserTable::getById($element['ASSIGNED_BY_ID']);
            if ($dbUser->getSelectedRowsCount() <= 0){$errors->setError(new Error('Администратор компании не найден'));}
        }
        return $errors;
    }
    //endregion

    //region Управление гридом
    public static function getStringName() { return  \Bitrix\Main\Localization\Loc::getMessage('СompanyTable'); }
    public static function getHeaderGrid() {
        \Bitrix\Main\Localization\Loc::loadMessages(__FILE__);
        return self::genHeader( array(
            'ID'      => self::field(0,'i',0,'d','',60,'minimal'),
            'NAME'    => self::field(1,'t',1),
            'ACTIVE'  => self::field(1,'c',1),
            'UF_LOGO' => self::field(1,'f'),
            'ASSIGNED_BY' => self::field(1,'i',0,0,'ASSIGNED_BY_ID'),
            //'UF_SNAP_NEGOTIATION' => self::field(1,'i'),
        ));
    }
    public static function filterFieldsGrid() {
        \Bitrix\Main\Localization\Loc::loadMessages(__FILE__);
        return self::genFilter(array(
            'ID'     => self::field(0),
            'NAME'   => self::field(1),
            'ACTIVE' => self::field(1,'c'),
            'ASSIGNED_BY_ID' => array(
                'type' => 'custom_entity',
                'params' => array( 'multiple' => 'Y' ),
                'selector' => array(
                    'TYPE' => 'user', 'DATA' => array(
                        'ID' => 'ASSIGNED_BY',
                        'FIELD_ID' => 'ASSIGNED_BY_ID'
                    )
                ),'default' => true,
            ),
        ));
    }
    public static function uniqueDisplayColumn($row) {
        ob_start();
        $listID = $row['UF_SNAP_NEGOTIATION'];
        if(is_array($listID)) {
            $res = \Websoft\Booking\Entity\NegotiatedTable::getList(array(
                'select' => array('ID','NAME','ACTIVE'), 'filter' => array('ID'=>$listID)
            ))->fetchAll();
            foreach ($res as $neg) {
                $messDeactivate = "";
                if(!$neg['ACTIVE']) {$messDeactivate .= " <span style='color: #ff8a00;'>(Деактивирована)</span>";}
                echo "[{$neg['ID']}] {$neg['NAME']}{$messDeactivate}<br>";
                if(($key = array_search($neg['ID'],$listID)) !== FALSE){ unset($listID[$key]); }
            }
            if(!empty($listID)) {
                foreach ($listID as $nedDelete) {
                    echo "Переговорная с ID = {$nedDelete} была <span style='color: red;'>удалена</span><br>";
                }
            }
            
        } else{ echo "<span style='color: #8f9491;'>Нет привязанных переговорных...</span>"; }
        //$res = \Websoft\Booking\Entity\NegotiatedTable::getList(array('select' => array('ID','NAME')))->fetchAll();
        /*$countView = 10;
        if(count($res) < $countView) { $countView = count($res); }*/
        ?>
        <? $UF_SNAP_NEGOTIATION_HTML = ob_get_clean();
        return array(
            //'UF_SNAP_NEGOTIATION' => $UF_SNAP_NEGOTIATION_HTML
            'ASSIGNED_BY' => empty($row['ASSIGNED_BY']) ? '' : \CCrmViewHelper::PrepareUserBaloonHtml(array(
                'PREFIX' => "{$row['ID']}_RESPONSIBLE", 'USER_ID' => $row['ASSIGNED_BY_ID'],
                'USER_NAME'=> \CUser::FormatName(\CSite::GetNameFormat(), $row['ASSIGNED_BY']),
                'USER_PROFILE_URL' => Option::get('intranet', 'path_user', '', SITE_ID) . '/'
            ))
        );
    }
    public static function addCustomAction($gridManagerId,$applyButton) {
        return array(
            //region Одно шаговое действие
            array(
                'NAME' => GetMessage('Activate'), 'VALUE' => 'actvate_elements', 'ONCHANGE' => array(
                array( 'ACTION' => Actions::CREATE, 'DATA' => array($applyButton) ),
                array('ACTION' => Actions::CALLBACK, 'DATA' => array(array(
                    'JS' => "BX.CrmUIGridExtension.processActionChange('".$gridManagerId."', 'actvate_elements')"
                )))
            )),
            array(
                'NAME' => GetMessage('DeActivate'), 'VALUE' => 'de_actvate_elements', 'ONCHANGE' => array(
                array( 'ACTION' => Actions::CREATE, 'DATA' => array($applyButton) ),
                array('ACTION' => Actions::CALLBACK, 'DATA' => array(array(
                    'JS' => "BX.CrmUIGridExtension.processActionChange('".$gridManagerId."', 'de_actvate_elements')"
                )))
            )),
            //endregion
        );
    }
    public static function processGridActions($action,$allRows,$request){
        //$dataToDebag = array();
        $rows = $request->get('rows');
        if(!empty($allRows)) { $rows = $allRows; }
        switch ($action) {
            case 'actvate_elements':
                foreach ($rows as $id) { self::update($id,array('ACTIVE'=>true)); }
                break;
            case 'de_actvate_elements':
                foreach ($rows as $id) { self::update($id,array('ACTIVE'=>false)); }
                break;
        }
        /*\Bitrix\Main\Diag\Debug::writeToFile(array(
            '$dataToDebag' => $dataToDebag,
            '$request->get(\'rows\')' => $request->get('rows'),
            '$allRows' => $allRows,
        ), "", "log.txt");*/
    }
    public static function getActions() { return self::generationAtions(true, true, true); }
    //endregion

    //region Проверка полноценности данных (предупреждения)
    public static function ListWarning() {
        // Получить активные компании
        $res = self::getListPure(true,array('ACTIVE' => true),array('ID','NAME'));
        // Прекреплены к переговорным
        $errors = array();
        foreach ($res as $el) {
            $res2 = NegotiatedTable::getListPure(false,array('COMPANY_ID'=>$el['ID']));
            if(!$res2) {
                $errors[] = '<b>'.$el['NAME'].'</b> <u>активна</u>, но ни одна переговорная <u>не привязана к ней</u>' .
                    ', по этой причине эта компания будет <b>НЕ</b> доступна в публичной части.';
            }
        }
        // Прикреплены к НЕ активным переговорным
        foreach ($res as $el) {
            $res2 = NegotiatedTable::getListPure(false,array('COMPANY_ID'=>$el['ID'],'ACTIVE'=>true));
            if(!$res2) {
                $errors[] = '<b>'.$el['NAME'].'</b> <u>активна</u>, но нет <u>ни одной активной переговорной привязаной к ней</u>' .
                    ', по этой причине эта компания будет <b>НЕ</b> доступна в публичной части.';
            }
        }
        return $errors;
    }
    //endregion
}
