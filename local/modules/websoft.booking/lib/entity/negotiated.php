<?php

namespace Websoft\Booking\Entity;
use \Websoft\Booking\Classes\EntityBase;

use Bitrix\Main\Entity\IntegerField;   // TODO:: Целочисленное
use Bitrix\Main\Entity\ReferenceField; // TODO:: Свзять поля сущности с другой сущностью/элементом, работает в паре с IntegerField
use Bitrix\Main\Entity\StringField;    // TODO:: Строка
use Bitrix\Main\Entity\DatetimeField;  // TODO:: ДатаВремя
use Bitrix\Main\Entity\BooleanField;   // TODO:: Булеан
use Bitrix\Main\Entity\FloatField;     // TODO:: Число с плавающей точкой
use Bitrix\Main\Entity\EnumField;      // TODO:: Список
use Bitrix\Main\Entity\TextField;      // TODO:: Текстовое поле, Тоже самое что и строка, но можно определить тип

use Bitrix\Main\Entity\UField; // TODO: Его отключили.... =(

use Bitrix\Main\UserTable;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Grid\Panel\Actions;
use Bitrix\Main\Grid\Panel\Types;
use Bitrix\Main\Error;
use Bitrix\Main\Type\Dictionary\ErrorCollection;
use \Bitrix\Main\Localization\Loc;

use \Bitrix\Main\Diag\Debug as Deb;

use Websoft\Booking\Entity\CompanyBookingTable as CompanyBooking;
use Websoft\Booking\Handler;

/**
 * Переговорная
 * Class NegotiatedTable
 * @package Websoft\Booking\Entity
 */
class NegotiatedTable extends EntityBase {

    //region Maps
    public static function getMap() {

        $listCity = CityTable::getList(array(
            'select'=>array('ID','NAME'),'filter'=>array()
        ))->fetchAll();
        $EnumCity = array();
        foreach ($listCity  as $city) { $EnumCity[$city['NAME']] = $city['ID']; }
        if(empty($listCity)) { $EnumCity = array(''=>''); }

        $listCompany = CompanyBookingTable::getList(array('select'=>array('ID','NAME'),'filter'=>array()))->fetchAll();
        $EnumCompany  = array('Без компании'=>'000000');
        foreach ($listCompany  as $company) { $EnumCompany[$company['NAME']] = strval($company['ID']); }
        //if(empty($listCompany)) { $EnumCompany = array('Без компании'=>'000000'); }

        /*echo '<b>$EnumCompany</b>';
        echo '<pre>';
        print_r($EnumCompany);
        echo '</pre>';
        echo '<hr>';*/
        
        return array(
            new IntegerField('ID', array('primary' => true, 'autocomplete' => true)),
            new BooleanField('ACTIVE'),
            new StringField('NAME'),
            new StringField('DESCRIPTION'),
            new EnumField('COMPANY_ID',array('values' => $EnumCompany)),
            new StringField('ADDRESS'),
            new IntegerField('COUNT_PLACE'),
            new StringField('FLOOR'), // Этаж д/"Сервисы -> Переговорные"
            new StringField('PHONE'), // Телефон д/"Сервисы -> Переговорные"
            new IntegerField('SNAP_TO_MEETING'), // Привязка к месту проведения (д/интеграции в календарь и модуль
                                                 // "Сервисы -> Переговорные"). P.S.: Системаное поле.

            new IntegerField('ASSIGNED_BY_ID'),       // Администратор переговорной
            new ReferenceField('ASSIGNED_BY', UserTable::getEntity(), array('=this.ASSIGNED_BY_ID' => 'ref.ID')),
        );
    }
    public static function getMapUF() {
        return array(
            self::UF_File('UF_PREVIEW_IMAGE',false,false),
            self::UF_Integer('UF_SERVICE_TYPES',true,false), // Сервисы сопровождения данной переговорной
        );
    }
    //endregion

    //region Управление страницей создания/редактирования
    public static function getNameOneElement() { return self::getMessage('NameOneElementToAdd'); }
    public static function getFieldsToEdit($ID = '',$DataToRequest = '') {
        \Bitrix\Main\Localization\Loc::loadMessages(__FILE__);

        ob_start();
        $UF_CODE = 'UF_SERVICE_TYPES';
        $entity = "\Websoft\Booking\Entity\ServiceTypesTable";
        $selected = array();
        if($ID) {
            $element = self::getList(array('filter'=>array('ID'=>$ID),'select'=>array($UF_CODE)))->fetchAll();
            if(!empty($element)) { $element = $element[0]; $selected = $element[$UF_CODE]; }
        } elseif($DataToRequest) { $selected = $DataToRequest[$UF_CODE]; }
        $res = $entity::getList(array(
            'select' => array('ID','NAME'),'filter'=>array('ACTIVE'=>true)
        ))->fetchAll();
        $countView = 10;
        if(count($res) < $countView) { $countView = count($res); } ?>
        <select name="UF_SERVICE_TYPES[]" multiple size="<?=$countView?>">
            <option <?if(empty($selected)):?>selected<?endif;?> value="" > ------ Не связывать ------</option>
            <? foreach ($res as $el): ?>
                <option <?=(in_array($el['ID'],$selected)) ? 'selected' : ''?> value="<?=$el['ID']?>" ><?=$el['NAME']?></option>
            <? endforeach; ?>
        </select>
        <? $UF_SERVICE_TYPES_HTML = ob_get_clean();

        $result = self::genHeader( array(
            'ACTIVE' => self::field(1,'c'),
            'NAME'   => self::field(1,'t'),
            'DESCRIPTION'   => self::field(1,'t'),
            'UF_PREVIEW_IMAGE' => self::field(1,'f'),
            'COMPANY_ID' => self::field(1,'l'),
            'ADDRESS' => self::field(1,'s'),
            'COUNT_PLACE' => self::field(1,'i'),
            'FLOOR' => self::field(1,'s'),
            'PHONE' => self::field(1,'s'),
            'UF_SERVICE_TYPES' => self::fieldCustom($UF_SERVICE_TYPES_HTML,1),
            /*'ADMIN_BY_ID' => array(
                'type' => 'custom_entity',
                'params' => array( 'multiple' => 'Y' ),
                'selector' => array(
                    'TYPE' => 'user', 'DATA' => array(
                        'ID' => 'ADMIN_BY',
                        'FIELD_ID' => 'ADMIN_BY_ID'
                    )
                ),'default' => true,
            ),*/
            'ASSIGNED_BY' => self::field(1,'u'),

        ));
        return $result;
    }
    public static function validateFormEdit($element, $errors) {
        if (empty($element['NAME'])) { $errors->setError(new Error('Не указанно название')); }
        if (empty($element['UF_PREVIEW_IMAGE'])) { $errors->setError(new Error('Нет превью переговорной')); }
        if (empty($element['ADDRESS'])) { $errors->setError(new Error('Не указан адрес')); }
        if (empty($element['COMPANY_ID'])) { $errors->setError(new Error('Не указана компания')); }
        return $errors;
    }
    //endregion

    //region Управление гридом
    public static function getStringName() { return  \Bitrix\Main\Localization\Loc::getMessage('NegotiatedTable'); }
    public static function getHeaderGrid() {
        \Bitrix\Main\Localization\Loc::loadMessages(__FILE__);
        return self::genHeader( array(
            'ID'     => self::field(0,'i',0,'d','',60,'minimal'),
            'NAME'   => self::field(1,'t',1),
            'DESCRIPTION'   => self::field(1,'t',1),
            'ACTIVE' => self::field(1,'c',1),
            'UF_PREVIEW_IMAGE' => self::field(1,'f'),
            'COMPANY_ID' => self::field(1,'l',0),
            'ADDRESS' => self::field(1,'t',1),
            'COUNT_PLACE' => self::field(1,'i',1),
            'FLOOR' => self::field(1,'t',1),
            'PHONE' => self::field(1,'t',1),
            'UF_SERVICE_TYPES' => self::field(1,'i'),
            'ASSIGNED_BY' => self::field(1,'i',0,0,'ASSIGNED_BY_ID'),
        ));
    }
    public static function filterFieldsGrid() {
        \Bitrix\Main\Localization\Loc::loadMessages(__FILE__);
        return self::genFilter(array(
            'ID'     => self::field(0),
            'NAME'   => self::field(1),
            'ACTIVE' => self::field(1,'c'),
            'COMPANY_ID' => self::field(1,'l'),
            'COUNT_PLACE' => self::field(1,'i'),
        ));
    }
    public static function uniqueDisplayColumn($row) {

        $result = array();

        //region сервисы сопровождения
        ob_start();
        $listID = $row['UF_SERVICE_TYPES'];
        if(is_array($listID) && !empty($listID)) {
            $res = \Websoft\Booking\Entity\ServiceTypesTable::getList(array(
                'select' => array('ID','NAME','ACTIVE'), 'filter' => array('ID'=>$listID)
            ))->fetchAll();
            foreach ($res as $item) {
                $messDeactivate = "";
                if(!$item['ACTIVE']) {$messDeactivate .= " <span style='color: #ff8a00;'>(Деактивирован)</span>";}
                echo "[ID:{$item['ID']}] {$item['NAME']}{$messDeactivate}<br>";
                if(($key = array_search($item['ID'],$listID)) !== FALSE){ unset($listID[$key]); }
            }
            if(!empty($listID)) { foreach ($listID as $nedDelete) {
                echo "Сервис сопровождения с ID = {$nedDelete} был <span style='color: red;'>удалён</span><br>";
            }}
        } else { echo "<span style='color: #8f9491;'>Нет сервисов...</span>"; }
        $UF_SERVICE_TYPES_HTML = ob_get_clean();
        $result['UF_SERVICE_TYPES'] = $UF_SERVICE_TYPES_HTML;
        //endregion

        //region Организатор
        $rsUser = \CUser::GetByID($row['ASSIGNED_BY']['ID']);
        $arUser = $rsUser->Fetch();
        $USER_PROFILE_URL = \CComponentEngine::MakePathFromTemplate(
            Option::get('intranet', 'path_user', '', SITE_ID), array('USER_ID' => $arUser['ID'])
        );
        $result['ASSIGNED_BY'] = empty($row['ASSIGNED_BY_ID']) ? '' : \CCrmViewHelper::PrepareUserBaloonHtml(array(
            'PREFIX' => "{$row['ID']}_RESPONSIBLE", 'USER_ID' => $row['ASSIGNED_BY_ID'],
            'USER_NAME'=> \CUser::FormatName(\CSite::GetNameFormat(), $arUser),
            'USER_PROFILE_URL' => $USER_PROFILE_URL
        ));
        //endregion

        return $result;
    }
    public static function addCustomAction($gridManagerId,$applyButton) {
        return array(
            //region Одно шаговое действие
            array(
                'NAME' => GetMessage('Activate'), 'VALUE' => 'actvate_elements', 'ONCHANGE' => array(
                array( 'ACTION' => Actions::CREATE, 'DATA' => array($applyButton) ),
                array('ACTION' => Actions::CALLBACK, 'DATA' => array(array(
                    'JS' => "BX.CrmUIGridExtension.processActionChange('".$gridManagerId."', 'actvate_elements')"
                )))
            )),
            array(
                'NAME' => GetMessage('DeActivate'), 'VALUE' => 'de_actvate_elements', 'ONCHANGE' => array(
                array( 'ACTION' => Actions::CREATE, 'DATA' => array($applyButton) ),
                array('ACTION' => Actions::CALLBACK, 'DATA' => array(array(
                    'JS' => "BX.CrmUIGridExtension.processActionChange('".$gridManagerId."', 'de_actvate_elements')"
                )))
            )),
            //endregion
        );
    }
    public static function processGridActions($action,$allRows,$request){
        $rows = $request->get('rows');
        if(!empty($allRows)) { $rows = $allRows; }
        switch ($action) {
            case 'actvate_elements':
                foreach ($rows as $id) { self::update($id,array('ACTIVE'=>true)); }
                break;
            case 'de_actvate_elements':
                foreach ($rows as $id) { self::update($id,array('ACTIVE'=>false)); }
                break;
        }
    }
    public static function getActions() { return self::generationAtions(true, true, true); }
    //endregion

    //region Проверка полноценности данных (предупреждения)
    public static function ListWarning() {
        $errors = array();
        // Определить переговорные где не активен город, такие преговорные не участвуют в поиске
        $res = self::getList(array('select'=>array('ID','NAME','COMPANY_ID')))->fetchAll();
        foreach ($res as $el) {
            if($el['COMPANY_ID']) {
                // Определить активность компании
                $company = CompanyBookingTable::getList(
                    array('select'=>array('ID','NAME','ACTIVE'),'filter'=>array('ID'=>$el['COMPANY_ID'])
                    ))->fetchAll();
                if(!empty($company)) {
                    $company = $company[0];
                    if(!$company['ACTIVE']) {
                        $errors[] = '<b>'.$el['NAME'].'</b> привязана к не активной компании компании. Такая переговорная ' .
                            'не будет доступна в публичной части.';
                    }
                } else {
                    $errors[] = '<b>'.$el['NAME'].'</b> привязана к удалённой компании. Такая переговорная не будет ' .
                        'доступна в публичной части.';
                }
            } else {
                $errors[] = '<b>'.$el['NAME'].'</b> НЕ привязана к компании. Такая переговорная не будет ' .
                    'доступна в публичной части.';
            }
        }

        return $errors;
    }
    //endregion

    //region Публчные методы, для проекта "Модуль бронирования"
    public static function GetCorrectElements() {
        $result = array();
        $res = self::getListPure(true,array('ACTIVE'=>true),array('ID','COMPANY_ID','UF_PREVIEW_IMAGE'));
        foreach ($res as $el) {
            // Проверка активности компании
            $company = CompanyBooking::getListPure(false,array('ID'=>$el['COMPANY_ID'],'ACTIVE'=>true));
            if($company) { $result[] = $el; }
        }
        return $result;
    }
    //endregion

    //region События
    public static function onAfterAdd($event) {
        $DebugData = array();
        if(\CModule::IncludeModule('meeting') && \CModule::IncludeModule('calendar')) {
            $parameters = $event->getParameters();
            $settings = \CCalendar::GetSettings();
            $arFields = array(
                "NAME" => $parameters['fields']['NAME'],
                "DESCRIPTION" => $parameters['fields']['DESCRIPTION'],
                "ACTIVE" => $parameters['fields']['ACTIVE'],
                "IBLOCK_ID" => $settings['rm_iblock_id'],
                "IBLOCK_SECTION_ID" => 0,
                "UF_FLOOR" => $parameters['fields']['FLOOR'],
                "UF_PLACE" => $parameters['fields']['COUNT_PLACE'],
                "UF_PHONE" => $parameters['fields']['PHONE'],
                "URL" => ''
            );
            $iblockSectionObject = new \CIBlockSection;
            $idMeeting = $iblockSectionObject->Add($arFields);
            $ID = $parameters['id'];
            self::update($ID,array('SNAP_TO_MEETING'=>$idMeeting));
        }
        if(!empty($DebugData)) { Deb::writeToFile($DebugData, "", "log.txt"); }
    }
    public static function onAfterUpdate($event) {
        $DebugData = array();
        if(\CModule::IncludeModule('meeting') && \CModule::IncludeModule('calendar')) {
            $parameters = $event->getParameters();
            $ID_EL_ENTITY = $parameters['id'];
            $elEntity = self::getListPure(false,array('ID' => $ID_EL_ENTITY),array('*'));

            // Обновление связанного места проведения (сервисы + календарь)
            $arFields = array(
                "NAME"        => $elEntity['NAME'],
                "DESCRIPTION" => $elEntity['DESCRIPTION'],
                "ACTIVE"      => $elEntity['ACTIVE'] ? 'Y': 'N',
                "UF_FLOOR"    => $elEntity['FLOOR'],
                "UF_PLACE"    => $elEntity['COUNT_PLACE'],
                "UF_PHONE"    => $elEntity['PHONE'],
            );
            $iblockSectionObject = new \CIBlockSection;
            $iblockSectionObject->Update($elEntity['SNAP_TO_MEETING'],$arFields);

            // Если переговорная деактивировалась, то нужно анулировать связанные карточки бронирования
            // ... если была активна и деактивировалась:
            // ... если была деактивирована и активировалась:
        }
        if(!empty($DebugData)) { Deb::writeToFile($DebugData, "", "log.txt"); }
    }
    public static function onBeforeDelete($event) {
        $DebugData = array();
        if(\CModule::IncludeModule('meeting') && \CModule::IncludeModule('calendar')) {
            $parameters = $event->getParameters();
            $ID_EL_ENTITY = $parameters['id'];
            $elEntity = self::getListPure(false,array('ID' => $ID_EL_ENTITY),array('*','UF_*'));
            \CIBlockSection::Delete($elEntity['SNAP_TO_MEETING']);
        }
        if(!empty($DebugData)) { Deb::writeToFile($DebugData, "", "log.txt"); }
    }
    //endregion

    public static $section_ids = array();

    public static function GetEventsByParentIdAndDate($ID,$date) {
        \CModule::IncludeModule('calendar');
        $event = \CCalendarEvent::GetById($ID);
        if(!$event) { return; }
        if($event['DELETED'] != 'N') { return; }


        $date = \date("Y-m-d", strtotime($date));
        $fromLimit = $date.' 00:00:00';
        $toLimit = $date.' 23:59:59';
        $arFilter = array( 'ID' => $ID );
        $events = \CCalendarEvent::GetList(array( 'arFilter' =>  $arFilter, 'parseRecursion' => true ));

        $rrule = \CCalendarEvent::ParseRRULE($event['RRULE']);
        if(!empty($rrule) && !isset($rrule['COUNT'])) {
            $PARENT_ID = $ID;
            if(empty(self::$section_ids)) {
                $arSections = \CCalendar::GetSectionList(array());
                $section_ids = array();
                foreach ($arSections as $section) { $section_ids[] = $section['ID']; }
                self::$section_ids = $section_ids;
            }
            $date_2 = \date("d.m.Y", strtotime($date));
            $arEvents = \CCalendar::GetEventList(array(
                'section' => self::$section_ids, 'fromLimit' => $date_2, 'toLimit' => $date_2
            ), $arAttendees);
            $arEvents_new = array(); foreach ($arEvents as $event_2) {
                if($event_2['PARENT_ID'] == $event_2['ID'] && $event_2['PARENT_ID'] == $PARENT_ID) {
                    $arEvents_new[] = $event_2;
                }
            }
            foreach ( $arEvents_new as $new_event) { $events[] = $new_event; }
        }

        $event_new = array();
        foreach ($events as $event) {
            $fromTs = \CCalendar::Timestamp($event['DATE_FROM']);
            $toTs = \CCalendar::Timestamp($event['DATE_TO']);
            $skipTime = $event['DT_SKIP_TIME'] == "Y";
            if ($skipTime) { $toTs += \CCalendar::DAY_LENGTH; }
            if (!$skipTime) { $fromTs -= $event['~USER_OFFSET_FROM']; $toTs -= $event['~USER_OFFSET_TO']; }
            $fromTs = date("d.m.Y H:i:s", $fromTs);
            $toTs = date("d.m.Y H:i:s", $toTs);
            if( strtotime($fromTs) <= strtotime($toLimit) &&
                strtotime($fromLimit) <= strtotime($toTs) ) {
                $event['DATE_FROM'] = $fromTs;
                $event['DATE_TO'] = $toTs;
                $event_new[] = $event;
            }
        }

        //$events = \CCalendarEvent::GetList(array( 'arFilter' =>  array(),'parseRecursion' => true));
        
        /*echo '<b>$events</b>';
        echo '<pre>';
        print_r($events);
        echo '</pre>';
        echo '<hr>';*/
        
        
        /*echo '<b>$event_new</b>';
        echo '<pre>';
        print_r($event_new);
        echo '</pre>';
        echo '<hr>';*/

        //$event_new = self::GetEventsByParentIdAndDate2($ID,$date);
        
        /*echo '<b>$test</b>';
        echo '<pre>';
        print_r($test);
        echo '</pre>';
        echo '<hr>';*/
        
        return $event_new; //$event_new;
        //return self::GetEventsByParentIdAndDate2($ID,$date); //$event_new;
    }

    private static function GetEventsByParentIdAndDate2($ID,$date) {
        \CModule::IncludeModule('calendar');
        $date = \date("Y-m-d", strtotime($date));
        $fromLimit = $date.' 00:00:00';
        $toLimit = $date.' 23:59:59';
        $events = \CCalendarEvent::GetList(
            array(
                'arFilter' => array( "ID" => $ID, "FROM_LIMIT" => $fromLimit, "TO_LIMIT" => $toLimit ),
                'parseRecursion' => true,
                'fetchAttendees' => true,
                'checkPermissions' => true,
                'skipDeclined' => true
            )
        );
        $events_res = array();
        foreach ($events as $event) {
            $fromTs = \CCalendar::Timestamp($event['DATE_FROM']);
            $toTs = \CCalendar::Timestamp($event['DATE_TO']);
            $skipTime = $event['DT_SKIP_TIME'] == "Y";
            if ($skipTime) { $toTs += \CCalendar::DAY_LENGTH; }
            if (!$skipTime) { $fromTs -= $event['~USER_OFFSET_FROM']; $toTs -= $event['~USER_OFFSET_TO']; }
            $fromTs = date("d.m.Y H:i:s", $fromTs);
            $toTs = date("d.m.Y H:i:s", $toTs);

            if( strtotime($fromTs) <= strtotime($toLimit) &&
                strtotime($fromLimit) <= strtotime($toTs) ) {
                $event['DATE_FROM'] = $fromTs;
                $event['DATE_TO'] = $toTs;
                $events_res = $event;
            }
        }
        return $events_res;
    }
    public static function GetInterpreter($id,$date,$daterange,$ExcludeChildCardsFromSearch = array()) {

        //region Входящие данные
        $date = \date("Y-m-d", strtotime($date));
        //endregion

        //region Получить группы событи
        $childCards = BookingCardChildTable::getListPure(
            true,array('NEGOTIATED_ID'=>$id,'!ID'=>$ExcludeChildCardsFromSearch),array('PARENT_EVENT_ID','PARENT_ID')
        );

        //\Websoft\Booking\Handler::dd('$childCards',$childCards);

        $childCards_new = array();
        foreach ($childCards as $childCards_one) {
            $activeParent = \Websoft\Booking\Entity\BookingCardTable::getListPure(
                false,array('ID'=>$childCards_one['PARENT_ID'],'ACTIVE_BOOKING' => 'Y')
            );
            if($activeParent) {
                $childCards_one['ID_MAIN_BOOKING_CARD'] = $activeParent['ID'];
                $childCards_new[] = $childCards_one;
            }
        }

        $childCards = $childCards_new;

        $dataTest = array();
        foreach ($childCards as $childCard) {

            //Handler::dd('$childCard',$childCard);

            $PARENT_EVENT_ID = $childCard['PARENT_EVENT_ID'];
            $child_events = self::GetEventsByParentIdAndDate($PARENT_EVENT_ID,$date);
            foreach ($child_events as $el) {
                $dataTest[$el['PARENT_ID']] = array(
                    'ID_MAIN_BOOKING_CARD' => $childCard['ID_MAIN_BOOKING_CARD'],
                    'ID' => $el['ID'],
                    'PARENT_ID' => $el['PARENT_ID'],
                    'OWNERS_ID' => $el['~ATTENDEES'],
                    'DATE_FROM' => $el['DATE_FROM'],
                    'DATE_TO' => $el['DATE_TO'],
                    'DT_SKIP_TIME' => $el['DT_SKIP_TIME'],
                    'USER_OFFSET_TO' => $el['~USER_OFFSET_TO'],
                    'USER_OFFSET_FROM' => $el['~USER_OFFSET_FROM'],
                    'RRULE_DESCRIPTION' => $el['~RRULE_DESCRIPTION'],
                );
            }
        }
        //endregion
        
        //region Получить даты событий
        foreach ($dataTest as $idParentEven => &$data) {

            $fromTs = \CCalendar::Timestamp($data['DATE_FROM']);
            $toTs = \CCalendar::Timestamp($data['DATE_TO']);
            $skipTime = $data['DT_SKIP_TIME'] == "Y";
            if ($skipTime) { $toTs += \CCalendar::DAY_LENGTH; }
            if (!$skipTime) { $fromTs -= $data['~USER_OFFSET_FROM']; $toTs -= $data['~USER_OFFSET_TO']; }
            $data['DATE_FROM'] = date("d.m.Y H:i:s",$fromTs);
            $data['DATE_TO'] = date("d.m.Y H:i:s",$toTs);
            $data['TIME_START'] = date("H:i",$fromTs);
            $data['TIME_END'] = date("H:i",$toTs);

            //... Кол-во секций по 30 минут (возвращает целое число, т.е. остаток не считаем)
            $datetime1 = new \DateTime($data['DATE_FROM']);
            $datetime2 = new \DateTime($data['DATE_TO']);
            $interval = $datetime1->diff($datetime2);
            $diffMinutes = ($interval->format('%h') * 60 + $interval->format('%i'));
            $data['COUNT_TIME_SECTION'] = (int)($diffMinutes/30);
            $data['RRULE_DESCRIPTION'] = $data['~RRULE_DESCRIPTION']; // Описание повторения
            $bookingCardChild = BookingCardChildTable::getListPure( // Дочерняя карточка
                false, array('PARENT_EVENT_ID'=>$data['PARENT_ID']),array('PARENT_ID','UF_SERVICES_DATA_ID')
            );
            $bookingCard = BookingCardTable::getListPure( // Родительская карточка
                false, array('ID'=>$bookingCardChild['PARENT_ID']),array('BOOKING_TYPE_ID','EXECUTOR_BY_ID','ID')
            );
            // Тип бронирования (иконка + наименование)
            $bookingType = BookingTypeTable::getListPure(
                false,array('ID'=>$bookingCard['BOOKING_TYPE_ID']),array('NAME','UF_ICON')
            );
            $data['BOOKING_TYPE'] = array(
                'ID' => $bookingCard['BOOKING_TYPE_ID'],
                'NAME' => $bookingType['NAME'],
                'SRC_ICON' => \CFile::GetPath($bookingType["UF_ICON"])
            );

            // Сервисы сопровождения + их ответсвенные
            $services = array();
            foreach ($bookingCardChild['UF_SERVICES_DATA_ID'] as $SERVICES_DATA_ID) {
                $serviceData = ServiceDataTable::getListPure(false,array('ID'=>$SERVICES_DATA_ID),array(
                    'SERVICE_TYPE_ID','RESPONSIBLE_ID'
                ));
                $service = ServiceTypesTable::getListPure(
                    false,array('ID'=>$serviceData['SERVICE_TYPE_ID']),array('NAME','UF_ICON')
                );
                $services[] = array(
                    'NAME' => $service['NAME'],
                    'ASSIGNED_BY' => array(
                        'ID' => $serviceData['RESPONSIBLE_ID'],
                        'FIO' => Handler::fullNameUser($serviceData['RESPONSIBLE_ID']),
                        'PERSONAL_PHONE' => Handler::getFieldUser($serviceData['RESPONSIBLE_ID'],'WORK_PHONE'),
                        'LINK_TO_PROFILE' => Handler::getPathUser($serviceData['RESPONSIBLE_ID']),
                    ),
                    'SRC_ICON' => \CFile::GetPath($service["UF_ICON"])
                );
            }

            // Перерывы (название с T1 до T2)
            $breaks = BreakTable::getListPure(true,array('BOOKING_CARD_ID'=>$bookingCard['ID']),array('NAME','START','END'));

            // Информация
            $EXECUTOR = \CUser::GetByID($bookingCard['EXECUTOR_BY_ID'])->Fetch();
            $data['INFORMATION'] = array(
                'BOOKING_CARD_ID' => $bookingCard['ID'],
                'EXECUTOR' => array(
                    'FIO' => Handler::fullNameUser($EXECUTOR['ID']),
                    'PERSONAL_PHONE' => Handler::getFieldUser($EXECUTOR['ID'],'WORK_PHONE'),
                    'LINK' => Handler::getPathUser($EXECUTOR['ID']),
                ), // Организатор
                'COUNT_OWNERS' => count($data['OWNERS_ID']), // Кол-во участников
                'BREAKS' => $breaks, // Перерывы
                'SERVICES' => $services, // Сервисы сопровождения + их ответсвенные
            );
        }
        //endregion

        //region Получить все ячейки
        $rows = array();
        $arEventsAdd = array();
        foreach($daterange as $date) {
            $row = array('TYPE'=>'EMPTY','DATA'=>array(
                'ID_NEG' => $id,
                'DATE' => $date->format("d.m.Y H:i:s")
            ));
            // Проерить входждение текущего времени в события
            $event_id_L = '';
            $event_L = '';
            $add = true;
            $date_str = $date->format("d.m.Y H:i:s");
            foreach ($dataTest as $event_id => $event_data) {
                if( strtotime($event_data['DATE_FROM']) <= strtotime($date_str) &&
                    strtotime($date_str) < strtotime($event_data['DATE_TO'])) {
                        $add = false; $event_L = $event_data;$event_id_L = $event_id;
                }
            }
            // Добавить пустую строку
            if($add) { $rows[] = $row; } else {
                if(!in_array($event_id_L,$arEventsAdd)) {
                    $row['TYPE'] = 'EVENT';
                    $link = '/booking/card/'.$event_L['INFORMATION']['BOOKING_CARD_ID'].'/';
                    $event_L['LINK_EDIT'] = $link.'edit/?neg_id='.$id.'&DATE='.$date->format("d.m.Y");
                    $event_L['LINK_VIEW'] = $link.'?neg_id='.$id.'&DATE='.$date->format("d.m.Y");
                    //$event_L['NEG_ID'] = $id;
                    //$event_L['TEST_123'] = "1111111";
                    $row['DATA'] = $event_L;
                    $rows[] = $row;
                    $arEventsAdd[] = $event_id_L;
                }
            }
        }
        //endregion

        return $rows;
    }
    public static function GetInterpreterTest($id,$date,$daterange) {


        //region Входящие данные
        echo '<h2>$id: '.$id.'</h2>';

        $date = \date("Y-m-d", strtotime($date));
        echo '<b>$date:</b> '.$date.'<br>';
        //endregion

        $arEvents = array();
        //region Извлечение событий из календаря
        $childCards = BookingCardChildTable::getListPure(true,array('NEGOTIATED_ID'=>$id),array('PARENT_EVENT_ID'));
        foreach ($childCards as $childCard) {
            // Получить ParentEventID
            $ParentEventID = $childCard['PARENT_EVENT_ID'];
            echo '$ParentEventID: '.$ParentEventID.'<br>';
            // Получить все дочерние события связанные с входящей датой $date
            $events = self::GetEventsByParentIdAndDate2($ParentEventID,$date);
            /*echo '<b>$events</b>';
            echo '<pre>';
            print_r($events);
            echo '</pre>';
            echo '<hr>';*/


            //echo '<hr>';
        }
        //endregion

        echo '<hr>';
        
        
        return array();
    }
}
