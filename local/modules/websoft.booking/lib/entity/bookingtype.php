<?php

namespace Websoft\Booking\Entity;
use \Websoft\Booking\Classes\EntityBase;

use Bitrix\Main\Entity\IntegerField;   // TODO:: Целочисленное
use Bitrix\Main\Entity\ReferenceField; // TODO:: Свзять поля сущности с другой сущностью/элементом, работает в паре с IntegerField
use Bitrix\Main\Entity\StringField;    // TODO:: Строка
use Bitrix\Main\Entity\DatetimeField;  // TODO:: ДатаВремя
use Bitrix\Main\Entity\BooleanField;   // TODO:: Булеан
use Bitrix\Main\Entity\FloatField;     // TODO:: Число с плавающей точкой
use Bitrix\Main\Entity\EnumField;      // TODO:: Список
use Bitrix\Main\Entity\TextField;      // TODO:: Текстовое поле, Тоже самое что и строка, но можно определить тип

use Bitrix\Main\Entity\UField; // TODO: Его отключили.... =(

use Bitrix\Main\UserTable;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Grid\Panel\Actions;
use Bitrix\Main\Grid\Panel\Types;
use Bitrix\Main\Error;
use Bitrix\Main\Type\Dictionary\ErrorCollection;
use \Bitrix\Main\Localization\Loc;

/**
 * Тестовая сущность 1
 * Class TestEntity1Table
 * @package Websoft\Blank\Entity
 */
class BookingTypeTable extends EntityBase {

    public static function getMap() {
        // ServiceTypesTable
        $listsServiceTypes = ServiceTypesTable::getList(array('select'=>array('ID','NAME')))->fetchAll();
        $EnumsServiceTypes = array();
        foreach ($listsServiceTypes  as $serviceType) { $EnumsServiceTypes[$serviceType['NAME']] = $serviceType['ID']; }
        return array(
            new IntegerField('ID', array('primary' => true, 'autocomplete' => true)), // ID`шник
            new StringField('NAME'), // Наименование
            new BooleanField('ACTIVE'), // Активность
            new StringField('DESCRIPTION'), // Описание вида бронирования (д/ответсвенных)
        );
    }
    public static function getMapUF() {
        return array(
            // Связанные сервисы сопровождения
            self::UF_Integer('UF_SERVICE_TYPES',true,false),
            self::UF_File('UF_ICON',false,false),
        );
    }

    //region Управление страницей создания/редактирования
    public static function getNameOneElement() { return self::getMessage('NameOneElementToAdd'); }
    public static function getFieldsToEdit($ID = '',$DataToRequest = '') {
        \Bitrix\Main\Localization\Loc::loadMessages(__FILE__);

        ob_start();
        $UF_CODE = 'UF_SERVICE_TYPES';
        $entity = "\Websoft\Booking\Entity\ServiceTypesTable";
        $selected = array();
        if($ID) {
            $element = self::getList(array('filter'=>array('ID'=>$ID),'select'=>array($UF_CODE)))->fetchAll();
            if(!empty($element)) { $element = $element[0]; $selected = $element[$UF_CODE]; }
        } elseif($DataToRequest) { $selected = $DataToRequest[$UF_CODE]; }
        $res = $entity::getList(array(
                'select' => array('ID','NAME'),'filter'=>array('ACTIVE'=>true)
        ))->fetchAll();
        $countView = 10;
        if(count($res) < $countView) { $countView = count($res); } ?>
        <select name="UF_SERVICE_TYPES[]" multiple size="<?=$countView?>">
            <option <?if(empty($selected)):?>selected<?endif;?> value="" > ------ Не связывать ------</option>
            <? foreach ($res as $el): ?>
                <option <?=(in_array($el['ID'],$selected)) ? 'selected' : ''?> value="<?=$el['ID']?>" ><?=$el['NAME']?></option>
            <? endforeach; ?>
        </select>
        <? $UF_SERVICE_TYPES_HTML = ob_get_clean();
        return self::genHeader( array(
            'NAME'   => self::field(1,'t'),
            'ACTIVE' => self::field(1,'c'),
            'DESCRIPTION' => self::field(1,'ta',1),
            'UF_SERVICE_TYPES' => self::fieldCustom($UF_SERVICE_TYPES_HTML,1),
            'UF_ICON' => self::field(1,'f'),
        ));
    }
    public static function validateFormEdit($element, $errors) {
        if (empty($element['NAME'])) {
            $errors->setError(new Error('Не указанно название...'));
        }
        /*if (empty($element['DESCRIPTION'])) {
            $errors->setError(new Error('Нет описания...'));
        }*/
        if (empty($element['UF_ICON'])) { $errors->setError(new Error('Нет иконки вида бронирования')); }

        /*
        if (empty($element['ASSIGNED_BY_ID'])) {
            $errors->setError(new Error(Loc::getMessage('CRMSTORES_ERROR_EMPTY_ASSIGNED_BY_ID')));
        } else {
            $dbUser = UserTable::getById($element['ASSIGNED_BY_ID']);
            if ($dbUser->getSelectedRowsCount() <= 0) {
                $errors->setError(new Error(Loc::getMessage('CRMSTORES_ERROR_UNKNOWN_ASSIGNED_BY_ID')));
            }
        }*/
        return $errors;
    }
    //endregion

    //region Управление гридом
    public static function getStringName() {
        return  \Bitrix\Main\Localization\Loc::getMessage('BookingTypeTable');
    }
    public static function getHeaderGrid() {
        \Bitrix\Main\Localization\Loc::loadMessages(__FILE__);
        return self::genHeader( array(
            'ID'     => self::field(0,'i',0,'d','',60,'minimal'),
            'NAME'   => self::field(1,'t',1),
            'ACTIVE' => self::field(1,'c',1),
            'DESCRIPTION' => self::field(1,'ta',1),
            'UF_SERVICE_TYPES' => self::field(1,'i'),
            'UF_ICON' => self::field(1,'f'),
        ));
    }
    public static function filterFieldsGrid() {
        \Bitrix\Main\Localization\Loc::loadMessages(__FILE__);
        return self::genFilter(array(
            'ID'     => self::field(0),
            'NAME'   => self::field(1),
            'ACTIVE' => self::field(1,'c'),
            //'UF_SERVICE_TYPES' => self::field(1,'i'),
        ));
    }
    public static function uniqueDisplayColumn($row) {
        ob_start();
        $listID = $row['UF_SERVICE_TYPES'];
        if(is_array($listID) && !empty($listID)) {
            $res = \Websoft\Booking\Entity\ServiceTypesTable::getList(array(
                'select' => array('ID','NAME','ACTIVE'), 'filter' => array('ID'=>$listID)
            ))->fetchAll();
            foreach ($res as $item) {
                $messDeactivate = "";
                if(!$item['ACTIVE']) {$messDeactivate .= " <span style='color: #ff8a00;'>(Деактивирован)</span>";}
                echo "[ID:{$item['ID']}] {$item['NAME']}{$messDeactivate}<br>";
                if(($key = array_search($item['ID'],$listID)) !== FALSE){ unset($listID[$key]); }
            }
            if(!empty($listID)) { foreach ($listID as $nedDelete) {
                echo "Сервис сопровождения с ID = {$nedDelete} был <span style='color: red;'>удалён</span><br>";
            }}
        } else { echo "<span style='color: #8f9491;'>Нет привязок...</span>"; }

        //$res = \Websoft\Booking\Entity\NegotiatedTable::getList(array('select' => array('ID','NAME')))->fetchAll();
        /*$countView = 10;
        if(count($res) < $countView) { $countView = count($res); }*/
        ?>
        <? $UF_SERVICE_TYPES_HTML = ob_get_clean();
        return array(
            'UF_SERVICE_TYPES' => $UF_SERVICE_TYPES_HTML
        );
    }
    public static function uniqueEditForm($fields) {
        return array(
            'UF_SERVICE_TYPES' => 'Hello world'
        );
    }
    public static function addCustomAction($gridManagerId,$applyButton) {
        return array(
            //region Одно шаговое действие
            array(
                'NAME' => GetMessage('Activate'), 'VALUE' => 'actvate_elements', 'ONCHANGE' => array(
                array( 'ACTION' => Actions::CREATE, 'DATA' => array($applyButton) ),
                array('ACTION' => Actions::CALLBACK, 'DATA' => array(array(
                    'JS' => "BX.CrmUIGridExtension.processActionChange('".$gridManagerId."', 'actvate_elements')"
                )))
            )),
            array(
                'NAME' => GetMessage('DeActivate'), 'VALUE' => 'de_actvate_elements', 'ONCHANGE' => array(
                array( 'ACTION' => Actions::CREATE, 'DATA' => array($applyButton) ),
                array('ACTION' => Actions::CALLBACK, 'DATA' => array(array(
                    'JS' => "BX.CrmUIGridExtension.processActionChange('".$gridManagerId."', 'de_actvate_elements')"
                )))
            )),
            //endregion
        );
    }
    public static function processGridActions($action,$allRows,$request){
        $dataToDebag = array();
        switch ($action) {
            case 'actvate_elements':
                $dataToDebag[] = 'Поймали событие =) "one_action"';
                break;
            case 'de_actvate_elements':
                $dataToDebag[] = 'Поймали событие =) "one_action"';
                break;
        }
    }
    public static function getActions() { return self::generationAtions(true, true, true); }
    //endregion
}
