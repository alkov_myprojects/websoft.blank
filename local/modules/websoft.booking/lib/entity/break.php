<?php

namespace Websoft\Booking\Entity;
use \Websoft\Booking\Classes\EntityBase;

use Bitrix\Main\Entity\IntegerField;   // TODO:: Целочисленное
use Bitrix\Main\Entity\ReferenceField; // TODO:: Свзять поля сущности с другой сущностью/элементом, работает в паре с IntegerField
use Bitrix\Main\Entity\StringField;    // TODO:: Строка
use Bitrix\Main\Entity\DatetimeField;  // TODO:: ДатаВремя
use Bitrix\Main\Entity\BooleanField;   // TODO:: Булеан
use Bitrix\Main\Entity\FloatField;     // TODO:: Число с плавающей точкой
use Bitrix\Main\Entity\EnumField;      // TODO:: Список
use Bitrix\Main\Entity\TextField;      // TODO:: Текстовое поле, Тоже самое что и строка, но можно определить тип

use Bitrix\Main\Entity\UField; // TODO: Его отключили.... =(

use Bitrix\Main\UserTable;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Grid\Panel\Actions;
use Bitrix\Main\Grid\Panel\Types;
use Bitrix\Main\Error;
use Bitrix\Main\Type\Dictionary\ErrorCollection;
use \Bitrix\Main\Localization\Loc;

/**
 * Перерыв
 * Class BreakTable
 * @package Websoft\Booking\Entity
 */
class BreakTable extends EntityBase {

    public static function getMap() {
        return array(
            new IntegerField('ID', array('primary' => true, 'autocomplete' => true)),
            new IntegerField('BOOKING_CARD_ID'),
            new StringField('NAME'),
            new StringField('START'),
            new StringField('END'),
        );
    }
    public static function getMapUF() {}

    //region Управление страницей создания/редактирования
    public static function getNameOneElement() { return self::getMessage('NameOneElementToAdd'); }
    public static function getFieldsToEdit() {
        \Bitrix\Main\Localization\Loc::loadMessages(__FILE__);
        return self::genHeader( array(
            'NAME'   => self::field(1,'t'),
            'START'   => self::field(1,'t'),
            'END'   => self::field(1,'t'),
            'BOOKING_CARD_ID'   => self::field(1,'i'),
        ));
    }
    public static function validateFormEdit($element, $errors) {
        /*if (empty($element['NAME'])) {
            $errors->setError(new Error('Не указанно название...'));
        }
        if (empty($element['ASSIGNED_BY_ID'])) {
            $errors->setError(new Error(Loc::getMessage('CRMSTORES_ERROR_EMPTY_ASSIGNED_BY_ID')));
        } else {
            $dbUser = UserTable::getById($element['ASSIGNED_BY_ID']);
            if ($dbUser->getSelectedRowsCount() <= 0) {
                $errors->setError(new Error(Loc::getMessage('CRMSTORES_ERROR_UNKNOWN_ASSIGNED_BY_ID')));
            }
        }*/
        return $errors;
    }
    //endregion

    //region Управление гридом
    public static function getStringName() {
        return  \Bitrix\Main\Localization\Loc::getMessage('BreakTable');
    }
    public static function getHeaderGrid() {
        \Bitrix\Main\Localization\Loc::loadMessages(__FILE__);
        return self::genHeader( array(
            'ID'     => self::field(0,'i',0,'d','',60,'minimal'),
            'NAME'   => self::field(1,'t',1),
            'START'  => self::field(1,'t'),
            'END'    => self::field(1,'t'),
            'BOOKING_CARD_ID'   => self::field(1,'i'),
        ));
    }
    public static function filterFieldsGrid() {
        \Bitrix\Main\Localization\Loc::loadMessages(__FILE__);
        return self::genFilter(array(
            'ID'     => self::field(0),
            'NAME'   => self::field(1),
            'ACTIVE' => self::field(1,'c'),
        ));
    }
    public static function uniqueDisplayColumn($row) {
        return array();
    }
    public static function addCustomAction($gridManagerId,$applyButton) {
        return array(
            //region Одно шаговое действие
            array(
                'NAME' => GetMessage('Activate'), 'VALUE' => 'actvate_elements', 'ONCHANGE' => array(
                array( 'ACTION' => Actions::CREATE, 'DATA' => array($applyButton) ),
                array('ACTION' => Actions::CALLBACK, 'DATA' => array(array(
                    'JS' => "BX.CrmUIGridExtension.processActionChange('".$gridManagerId."', 'actvate_elements')"
                )))
            )),
            array(
                'NAME' => GetMessage('DeActivate'), 'VALUE' => 'de_actvate_elements', 'ONCHANGE' => array(
                array( 'ACTION' => Actions::CREATE, 'DATA' => array($applyButton) ),
                array('ACTION' => Actions::CALLBACK, 'DATA' => array(array(
                    'JS' => "BX.CrmUIGridExtension.processActionChange('".$gridManagerId."', 'de_actvate_elements')"
                )))
            )),
            //endregion
        );
    }
    public static function processGridActions($action,$allRows,$request){
        $dataToDebag = array();
        switch ($action) {
            case 'actvate_elements':
                $dataToDebag[] = 'Поймали событие =) "one_action"';
                break;
            case 'de_actvate_elements':
                $dataToDebag[] = 'Поймали событие =) "one_action"';
                break;
        }
    }
    public static function getActions() { return self::generationAtions(true, true, true); }
    //endregion

    //region События
    public static function addMyStory($ID) {

    }
    public static function onBeforeUpdate ($event) {
        global $USER;
        $user_id = $USER->GetID();
        $parameters = $event->getParameters();
        $fields = $parameters['fields'];

        // Получить данные из БД
        $keys = array();
        foreach ($fields as $key => $val) { $keys[] = $key; }
        $ID = $parameters['id']['ID'];
        $fields_old = self::getListPure(false,array('ID'=>$ID),$keys);

        // Расходждение массивов
        $array_diff = array_diff($fields,$fields_old);
        if(!empty($array_diff)) {
            $break = self::getListPure(false,array('ID' => $ID),array('BOOKING_CARD_ID'));
            $cl = new \ReflectionClass(get_called_class());
            $pathLangFile = $cl->getFileName();
            \Bitrix\Main\Localization\Loc::loadLanguageFile($pathLangFile,'ru');
            $history = array();
            $history[] = self::generateRowHistory(
                $user_id,
                'Перерыв (изменение)',
                '"'.$fields['NAME'].'" с '.$fields_old['START'].' до '.$fields_old['END'],
                '"'.$fields['NAME'].'" c '.$fields['START'].' до '.$fields['END'],
                $break['BOOKING_CARD_ID'], get_called_class()
            );
            self::AddRowsHistory($history);
        }
    }
    public static function OnBeforeDelete($event) {
        global $USER;
        $user_id = $USER->GetID();
        $parameters = $event->getParameters();
        $ID = $parameters['id']['ID'];
        $break = self::getListPure(false,array('ID' => $ID),array('*'));
        $cl = new \ReflectionClass(get_called_class());
        $pathLangFile = $cl->getFileName();
        \Bitrix\Main\Localization\Loc::loadLanguageFile($pathLangFile,'ru');
        $history = array();
        $history[] = self::generateRowHistory(
            $user_id,'Перерыв',
            '"'.$break['NAME'].'" c '.$break['START'].' до '.$break['END'],
            'Удалён', $break['BOOKING_CARD_ID'], get_called_class()
        );
        self::AddRowsHistory($history);
    }
    public static function OnAfterAdd($event) {
        global $USER;
        $user_id = $USER->GetID();
        $parameters = $event->getParameters();
        $ID = $parameters['id'];
        $break = self::getListPure(false,array('ID' => $ID),array('*'));
        $cl = new \ReflectionClass(get_called_class());
        $pathLangFile = $cl->getFileName();
        \Bitrix\Main\Localization\Loc::loadLanguageFile($pathLangFile,'ru');
        $history = array();
        $history[] = self::generateRowHistory(
            $user_id,'Перерыв',
            'Добавлен',
            '"'.$break['NAME'].'" c '.$break['START'].' до '.$break['END'], $break['BOOKING_CARD_ID'], get_called_class()
        );
        self::AddRowsHistory($history);
    }
    //endregion
}
