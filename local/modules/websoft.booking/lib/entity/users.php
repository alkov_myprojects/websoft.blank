<?php

namespace Websoft\Booking\Entity;
use \Websoft\Booking\Classes\EntityBase;

use Bitrix\Main\Entity\IntegerField;   // TODO:: Целочисленное
use Bitrix\Main\Entity\ReferenceField; // TODO:: Свзять поля сущности с другой сущностью/элементом, работает в паре с IntegerField
use Bitrix\Main\Entity\StringField;    // TODO:: Строка
use Bitrix\Main\Entity\DatetimeField;  // TODO:: ДатаВремя
use Bitrix\Main\Entity\BooleanField;   // TODO:: Булеан
use Bitrix\Main\Entity\FloatField;     // TODO:: Число с плавающей точкой
use Bitrix\Main\Entity\EnumField;      // TODO:: Список
use Bitrix\Main\Entity\TextField;      // TODO:: Текстовое поле, Тоже самое что и строка, но можно определить тип

use Bitrix\Main\Entity\UField; // TODO: Его отключили.... =(

use Bitrix\Main\UserTable;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Grid\Panel\Actions;
use Bitrix\Main\Grid\Panel\Types;
use Bitrix\Main\Error;
use Bitrix\Main\Type\Dictionary\ErrorCollection;
use \Bitrix\Main\Localization\Loc;

/**
 * Пользователи которые имеют особый статус (бан,ответсвенный за подготовку,Организатор)
 * Class StoriesCard
 * @package Websoft\Booking\Entity
 */

class UsersTable extends EntityBase {

    public static function getMap() {
        return array(
            new IntegerField('ID', array('primary' => true, 'autocomplete' => true)),
            new IntegerField('ASSIGNED_BY_ID'), // Отвественный
            new ReferenceField( //Описание связи поля ASSIGNED_BY_ID с другой сущностью -> А можно свзять с Инфоблоком?
                'ASSIGNED_BY', UserTable::getEntity(), array('=this.ASSIGNED_BY_ID' => 'ref.ID')
            ),
            new EnumField('GROUP',array(
                'values' => array(
                    'Ответсвенный за подготовку' => 'GROUP_PREPARATION',
                    'Ответственный за сопровождение' => 'GROUP_SUPPORT',
                    'Ответственный за оценку качества' => 'GROUP_QUALITY',
                    'Бан' => 'BAN',
                    'Администратор модуля' => 'BOOKING_ADMIN'
                )
            )),
        );
    }
    public static function getMapUF() {}

    //region Управление страницей создания/редактирования
    public static function getNameOneElement() { return self::getMessage('NameOneElementToAdd'); }
    public static function getFieldsToEdit() {
        \Bitrix\Main\Localization\Loc::loadMessages(__FILE__);
        return self::genHeader( array(
            //'NAME'        => self::field(1,'t'),
            'ASSIGNED_BY' => self::field(1,'u'),
            'GROUP'       => self::field(1,'l'),
        ));
    }
    public static function validateFormEdit($element, $errors) {
        /*if (empty($element['NAME'])) {
            $errors->setError(new Error('Не указанно название...'));
        }*/
        if (empty($element['ASSIGNED_BY_ID'])) {
            $errors->setError(new Error('Не указан ответственный.'));
        } else {
            $dbUser = UserTable::getById($element['ASSIGNED_BY_ID']);
            if ($dbUser->getSelectedRowsCount() <= 0) {
                $errors->setError(new Error('Указанный ответственный сотрудник не существует.'));
            }
        }
        return $errors;
    }
    //endregion

    //region Управление гридом
    public static function getStringName() { return  \Bitrix\Main\Localization\Loc::getMessage('UsersTable'); }
    public static function getHeaderGrid() {
        \Bitrix\Main\Localization\Loc::loadMessages(__FILE__);
        return self::genHeader( array(
            'ID'     => self::field(0,'i',0,'d','',60,'minimal'),
            'ASSIGNED_BY' => self::field(1,'u'),
            'GROUP'       => self::field(1,'l'),
        ));
    }
    public static function filterFieldsGrid() {
        \Bitrix\Main\Localization\Loc::loadMessages(__FILE__);
        return self::genFilter(array(
            'ID'     => self::field(0),
            'ASSIGNED_BY_ID' => array(
                'type' => 'custom_entity',
                'params' => array( 'multiple' => 'Y' ),
                'selector' => array(
                    'TYPE' => 'user', 'DATA' => array(
                        'ID' => 'ASSIGNED_BY',
                        'FIELD_ID' => 'ASSIGNED_BY_ID'
                    )
                ),'default' => true,
            ),
            'GROUP'       => self::field(1,'l'),
        ));
    }
    public static function uniqueDisplayColumn($row) {
        return array(
            //'NAME' => '<a href="/" target="_self">' . $row['NAME'] . '</a>',
            'ASSIGNED_BY' => empty($row['ASSIGNED_BY']) ? '' : \CCrmViewHelper::PrepareUserBaloonHtml(
                array(
                    'PREFIX' => "{$row['ID']}_RESPONSIBLE",
                    'USER_ID' => $row['ASSIGNED_BY_ID'],
                    'USER_NAME'=> \CUser::FormatName(\CSite::GetNameFormat(), $row['ASSIGNED_BY']),
                    'USER_PROFILE_URL' => Option::get('intranet', 'path_user', '', SITE_ID) . '/'
                )
            )
        );
    }
    public static function addCustomAction($gridManagerId,$applyButton) {
        return array(
            //region Одно шаговое действие
            /*array(
                'NAME' => GetMessage('Activate'), 'VALUE' => 'actvate_elements', 'ONCHANGE' => array(
                array( 'ACTION' => Actions::CREATE, 'DATA' => array($applyButton) ),
                array('ACTION' => Actions::CALLBACK, 'DATA' => array(array(
                    'JS' => "BX.CrmUIGridExtension.processActionChange('".$gridManagerId."', 'actvate_elements')"
                )))
            )),
            array(
                'NAME' => GetMessage('DeActivate'), 'VALUE' => 'de_actvate_elements', 'ONCHANGE' => array(
                array( 'ACTION' => Actions::CREATE, 'DATA' => array($applyButton) ),
                array('ACTION' => Actions::CALLBACK, 'DATA' => array(array(
                    'JS' => "BX.CrmUIGridExtension.processActionChange('".$gridManagerId."', 'de_actvate_elements')"
                )))
            )),*/
            //endregion
        );
    }
    public static function processGridActions($action,$allRows,$request){
        $dataToDebag = array();
        switch ($action) {
            case 'actvate_elements':
                $dataToDebag[] = 'Поймали событие =) "one_action"';
                break;
            case 'de_actvate_elements':
                $dataToDebag[] = 'Поймали событие =) "de_actvate_elements"';
                break;
        }
    }
    public static function getActions() { return self::generationAtions(true, true, true); }
    //endregion
}

