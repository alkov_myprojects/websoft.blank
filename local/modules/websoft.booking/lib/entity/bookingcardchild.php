<?php

namespace Websoft\Booking\Entity;
use \Websoft\Booking\Classes\EntityBase;

use Bitrix\Main\Entity\IntegerField;   // TODO:: Целочисленное
use Bitrix\Main\Entity\ReferenceField; // TODO:: Свзять поля сущности с другой сущностью/элементом, работает в паре с IntegerField
use Bitrix\Main\Entity\StringField;    // TODO:: Строка
use Bitrix\Main\Entity\DatetimeField;  // TODO:: ДатаВремя
use Bitrix\Main\Entity\BooleanField;   // TODO:: Булеан
use Bitrix\Main\Entity\FloatField;     // TODO:: Число с плавающей точкой
use Bitrix\Main\Entity\EnumField;      // TODO:: Список
use Bitrix\Main\Entity\TextField;      // TODO:: Текстовое поле, Тоже самое что и строка, но можно определить тип

use Bitrix\Main\Entity\UField; // TODO: Его отключили.... =(

use Bitrix\Main\UserTable;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Grid\Panel\Actions;
use Bitrix\Main\Grid\Panel\Types;
use Bitrix\Main\Error;
use Bitrix\Main\Type\Dictionary\ErrorCollection;
use \Bitrix\Main\Localization\Loc;

/**
 * Карточка бронирования
 * Class BookingCardTable
 * @package Websoft\Blank\Entity
 */
class BookingCardChildTable extends EntityBase {

    public static function getMap() {
        return array(
            //region Системные поля
            new IntegerField('ID', array('primary' => true, 'autocomplete' => true)),
            //... привязка к событию календаря
            //endregion
            new IntegerField('PARENT_ID'),          // Родительская карточка бронирования
            new IntegerField('NEGOTIATED_ID'),      // ID переговорной
            new IntegerField('RESPONSIBLE_ID'),     // ID Ответсвенного за контроль качества
            new IntegerField('TRAINING_STATUS_ID'), // Статус подготовки
            new StringField('STATUS_DESCRIPTION'),  // Общее описание подготовки
            new FloatField('SUM_RATING'),           // Общая оценка
            new IntegerField('PARENT_EVENT_ID'),    // ID главного события (через него можно получить остальные)
        );
    }
    public static function getMapUF() {
        return array(
            self::UF_String('UF_BREAKS',true),            // Перерывы
            self::UF_Integer('UF_PARTICIPANTS',true),     // Участники UF_PARTICIPANTS
            self::UF_Integer('UF_SERVICES_DATA_ID',true), // Сервисы сопровождения (комментарий,
                                                                         // рейтинг, состояние(в подготовке,
                                                                         //подготоволенно,ожидает оцеки) )
        );
    }

    //region Управление страницей создания/редактирования
    public static function getNameOneElement() { return self::getMessage('NameOneElementToAdd'); }
    public static function getFieldsToEdit() {
        \Bitrix\Main\Localization\Loc::loadMessages(__FILE__);
        return self::genHeader( array(
            'ID' => self::field(0,'i',0,'d','',60),
        ));
    }
    public static function validateFormEdit($element, $errors) {
        /*if (empty($element['NAME'])) {
            $errors->setError(new Error('Не указанно название...'));
        }
        if (empty($element['ASSIGNED_BY_ID'])) {
            $errors->setError(new Error(Loc::getMessage('CRMSTORES_ERROR_EMPTY_ASSIGNED_BY_ID')));
        } else {
            $dbUser = UserTable::getById($element['ASSIGNED_BY_ID']);
            if ($dbUser->getSelectedRowsCount() <= 0) {
                $errors->setError(new Error(Loc::getMessage('CRMSTORES_ERROR_UNKNOWN_ASSIGNED_BY_ID')));
            }
        }*/
        return $errors;
    }
    //endregion

    //region Управление гридом
    public static function getStringName() {
        return  \Bitrix\Main\Localization\Loc::getMessage('BookingCardChild');
    }
    public static function getHeaderGrid() {
        \Bitrix\Main\Localization\Loc::loadMessages(__FILE__);
        return self::genHeader( array(
            'ID'     => self::field(0,'i',0,'d','',60,'minimal'),
            'PARENT_ID' => self::field(1,'i'),
            'NEGOTIATED_ID' => self::field(1,'i'),
            'RESPONSIBLE_ID' => self::field(1,'i'),
            'TRAINING_STATUS_ID' => self::field(1,'i'),
            'STATUS_DESCRIPTION' => self::field(1,'t'),
            'SUM_RATING' => self::field(1,'n'),
            'PARENT_EVENT_ID' => self::field(1,'i'),
        ));
    }
    public static function filterFieldsGrid() {
        \Bitrix\Main\Localization\Loc::loadMessages(__FILE__);
        return self::genFilter(array(
            'ID'     => self::field(0),
            'NAME'   => self::field(1),
        ));
    }
    public static function uniqueDisplayColumn($row) {
        return array();
    }
    public static function addCustomAction($gridManagerId,$applyButton) {
        return array(
            //region Одно шаговое действие
            /*array(
                'NAME' => GetMessage('Activate'), 'VALUE' => 'actvate_elements', 'ONCHANGE' => array(
                array( 'ACTION' => Actions::CREATE, 'DATA' => array($applyButton) ),
                array('ACTION' => Actions::CALLBACK, 'DATA' => array(array(
                    'JS' => "BX.CrmUIGridExtension.processActionChange('".$gridManagerId."', 'actvate_elements')"
                )))
            )),
            array(
                'NAME' => GetMessage('DeActivate'), 'VALUE' => 'de_actvate_elements', 'ONCHANGE' => array(
                array( 'ACTION' => Actions::CREATE, 'DATA' => array($applyButton) ),
                array('ACTION' => Actions::CALLBACK, 'DATA' => array(array(
                    'JS' => "BX.CrmUIGridExtension.processActionChange('".$gridManagerId."', 'de_actvate_elements')"
                )))
            )),*/
            //endregion
        );
    }
    public static function processGridActions($action,$allRows,$request){
        $dataToDebag = array();
        switch ($action) {
            case 'actvate_elements':
                $dataToDebag[] = 'Поймали событие =) "one_action"';
                break;
            case 'de_actvate_elements':
                $dataToDebag[] = 'Поймали событие =) "one_action"';
                break;
        }
    }
    public static function getActions() { return self::generationAtions(true, true, false); }
    //endregion

    //region События
    public static function onBeforeUpdate ($event) {

        global $USER;
        $user_id = $USER->GetID();

        $parameters = $event->getParameters();
        $fields = $parameters['fields'];

        // Получить данные из БД
        $keys = array();
        foreach ($fields as $key => $val) { $keys[] = $key; }
        $ID = $parameters['id']['ID'];
        $fields_old = self::getListPure(false,array('ID'=>$ID),$keys);

        // Расходждение массивов
        $array_diff = array_diff($fields,$fields_old);
        if(!empty($array_diff)) {
            $cl = new \ReflectionClass(get_called_class());
            $pathLangFile = $cl->getFileName();
            \Bitrix\Main\Localization\Loc::loadLanguageFile($pathLangFile,'ru');
            $history = array();
            foreach ($array_diff as $key => $value) {
                $element = self::getListPure(false,array('ID' => $ID),array('PARENT_ID','NEGOTIATED_ID'));
                $neg = \Websoft\Booking\Entity\NegotiatedTable::getListPure(
                    false,array('ID' => $element['NEGOTIATED_ID']),array('NAME')
                );
                $new_value = $value;
                $old_value = $fields_old[$key];
                if($key == 'TRAINING_STATUS_ID') {
                    $old_value = \Websoft\Booking\Entity\TrainingStatusesTable::getListPure(
                        false,array('ID' => $old_value),array('NAME'));
                    $old_value = $old_value['NAME'];

                    $new_value = \Websoft\Booking\Entity\TrainingStatusesTable::getListPure(
                        false,array('ID' => $new_value),array('NAME'));
                    $new_value = $new_value['NAME'];
                }
                $history[] = self::generateRowHistory(
                    $user_id,$neg['NAME'].' -> '.Loc::getMessage($key),$old_value,$new_value,
                    $element['PARENT_ID'],get_called_class()
                );
            }
            self::AddRowsHistory($history);
        }

    }
    //endregion
}
