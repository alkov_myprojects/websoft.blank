<?php
namespace Websoft\Booking\Classes;
class RuleServiceData {
    public static function checkAllServiceData() {

        // BookingCardTable
        // BookingCardChildTable
        // ResponsibleSupportServicesTable
        // ServiceDataTable
        // NegotiatedTable
        $allTableIsset = true;
        $db = \Bitrix\Main\Application::getConnection();
        $listTableNames = array(
            \Websoft\Booking\Entity\BookingCardTable::getTableName(),
            \Websoft\Booking\Entity\BookingCardChildTable::getTableName(),
            \Websoft\Booking\Entity\ResponsibleSupportServicesTable::getTableName(),
            \Websoft\Booking\Entity\ServiceDataTable::getTableName(),
            \Websoft\Booking\Entity\NegotiatedTable::getTableName(),
        );
        foreach ($listTableNames as $tableName) {
            if ($db->isTableExists($tableName)) {
                $allTableIsset = false;
                break;
            }
        }

        if($allTableIsset) {
            global $DB;
            $date = date($DB->DateFormatToPHP(\CSite::GetDateFormat("SHORT")), time());

            //region Получить расписание на текущий день
            \CBitrixComponent::includeComponentClass("websoft.booking:booking.master");
            $CBookingCardComponent = 'CBookingMasterComponent';
            $negsCategories = $CBookingCardComponent::getNegotiationCategories(array(),$date.' 00:00:00',$date.' 23:59:59');

            \CBitrixComponent::includeComponentClass("websoft.booking:booking.main");
            $CBookingMainComponent = 'CBookingMainComponent';
            $dateIntervals = $CBookingMainComponent::dateIntervals();
            $date_time_start = date_format(date_create($date), 'Y-m-d').' '.$dateIntervals['Date_time_start'];
            $date_time_end = date_format(date_create($date), 'Y-m-d').' '.$dateIntervals['Date_time_end'];

            $begin = new \DateTime( $date_time_start );
            $end = new \DateTime( $date_time_end );
            $interval = new \DateInterval('PT'.$dateIntervals['DateInterval_Min'].'M');
            $daterange = new \DatePeriod($begin, $interval ,$end);
            //endregion

            //region Определить бронирования на сегодня
            $ITEMS = array();
            foreach ($negsCategories['BUSY'] as $negs) {
                foreach ($negs as $neg) {
                    $Interpreter = \Websoft\Booking\Entity\NegotiatedTable::GetInterpreter($neg['ID'],$date,$daterange);
                    $ITEMS[] = $Interpreter;
                }
            }
            $eventIDS_For_Ignore = array();
            $eventIDS_And_BookingCardID = array();
            foreach ($ITEMS as $ITEMS_BOX) {
                foreach ($ITEMS_BOX as $ITEM) {
                    if($ITEM['TYPE'] == 'EVENT') {
                        $PARENT_ID = $ITEM['DATA']['PARENT_ID'];
                        if(!in_array($PARENT_ID,$eventIDS_For_Ignore)) {
                            $eventIDS_For_Ignore[] = $PARENT_ID;
                            $eventIDS_And_BookingCardID[] = array(
                                'BoockingCardID' => $ITEM['DATA']['ID_MAIN_BOOKING_CARD'],
                                'EventID' => $PARENT_ID,
                                'NegID' => $ITEM['NEG_ID'],
                            );
                        }
                    }
                }
            }
            //endregion

            //region Анализ бронирований на сегодня
            if(!empty($eventIDS_And_BookingCardID)) {
                foreach ($eventIDS_And_BookingCardID as $item) {

                    //region получить минимальные сервисы сопровождения, которые должны быть у каждой переговорной:
                    $BookingCard = \Websoft\Booking\Entity\BookingCardTable::getListPure(
                        false,array('ID'=>$item['BoockingCardID']),array('UF_MIN_SERVICES_ID'));
                    //endregion

                    //region На текущую дату сгенерированы все сервисы? Если нет, то сгенерировать
                    if(!empty($BookingCard['UF_MIN_SERVICES_ID'])) {
                        // ... Получить дочение карточки бронирования
                        $BookingCardChilds = \Websoft\Booking\Entity\BookingCardChildTable::getListPure(
                            true,array('PARENT_ID' => $item['BoockingCardID']),array('ID','NEGOTIATED_ID'));
                        foreach ($BookingCardChilds as $CardChild) {
                            // ...  Получить сервисы сопровождения для этой дочерней карточки
                            $ServiceData = \Websoft\Booking\Entity\ServiceDataTable::getListPure(
                                true,array('UF_SNAP_CHILD_CARD' => $CardChild['ID'],'UF_CODE_DATE_BEGIN' => $date
                            ),array('ID','UF_*'));
                            // Создать сервисы
                            if(empty($ServiceData)) {
                                foreach ($BookingCard['UF_MIN_SERVICES_ID'] as $idService) {
                                    // Получить ответсенного за сервис сопровождения этой переговорной
                                    $responsibleSupportServices = \Websoft\Booking\Entity\ResponsibleSupportServicesTable::getListPure(
                                        false,array(
                                        'SNAP_TO_SS' => $idService,
                                        'SNAP_TO_NEG' => $CardChild['NEGOTIATED_ID'],
                                    ),array('ASSIGNED_BY_ID'));
                                    if($responsibleSupportServices['ASSIGNED_BY_ID']) {
                                        \Websoft\Booking\Entity\ServiceDataTable::add(array(
                                            'SERVICE_TYPE_ID' => $idService,
                                            'RESPONSIBLE_ID' => $responsibleSupportServices['ASSIGNED_BY_ID'],
                                            'UF_SNAP_CHILD_CARD' => $CardChild['ID'],
                                            'UF_CODE_DATE_BEGIN' => $date,
                                        ));
                                    }
                                }
                            }
                        }
                    }
                    //endregion
                }
            }
            //endregion
        }
    }

    public static function generateServiceDateByDay($CardChildID,$date_str) {
        $ChildCard = \Websoft\Booking\Entity\BookingCardChildTable::getListPure(
            false,array('ID' => $CardChildID),array('ID','NEGOTIATED_ID','PARENT_ID'));

        $Card = \Websoft\Booking\Entity\BookingCardTable::getListPure(
            false,array( 'ID' => $ChildCard['PARENT_ID']), array('UF_MIN_SERVICES_ID') );

        $UF_SERVICES_DATA_ID = array();
        foreach ($Card['UF_MIN_SERVICES_ID'] as $idService) {
            // Получить ответсенного за сервис сопровождения этой переговорной
            $responsibleSupportServices = \Websoft\Booking\Entity\ResponsibleSupportServicesTable::getListPure(
                false,array('SNAP_TO_SS' => $idService, 'SNAP_TO_NEG' => $ChildCard['NEGOTIATED_ID'],
            ),array('ASSIGNED_BY_ID'));
            if($responsibleSupportServices['ASSIGNED_BY_ID']) {
                $result = \Websoft\Booking\Entity\ServiceDataTable::add(array(
                    'SERVICE_TYPE_ID' => $idService,
                    'RESPONSIBLE_ID' => $responsibleSupportServices['ASSIGNED_BY_ID'],
                    'UF_SNAP_CHILD_CARD' => $ChildCard['ID'],
                    'UF_CODE_DATE_BEGIN' => $date_str,
                ));
                $UF_SERVICES_DATA_ID[] = $result->getId();
            }
        }
        return $UF_SERVICES_DATA_ID;
    }

}