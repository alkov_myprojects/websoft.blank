<?php

namespace Websoft\Booking\Classes;
abstract class CustomEventsCalendar {

    //region Тип события
    private static $Types = array(
        0 => 'Простое',
        1 => 'Переодичное N раз',
        2 => 'Переодичное до конкретной даты',
    );
    public static  function GetTypes() { return self::$Types; }
    //endregion

    public function GetEventByID($ID) {

        $date_str = $_REQUEST['DATE'];
        $event = \CCalendarEvent::GetById($ID,false);
        $TIME_START = date("H:i:s",\CCalendar::Timestamp($event['DATE_FROM']));
        $TIME_END = date("H:i:s",\CCalendar::Timestamp($event['DATE_TO']));
        $result = array(
            'Type' => '',        // self::GetTypes(); - один из индексов возвращаемого массива
            'When' => '',        // время начала события
            'Periodicity' => '', // описание повторения
        );

        //region Определить какое это событие:
            $rrule = \CCalendarEvent::ParseRRULE($event['RRULE']);

            //region Простое
            if(empty($rrule)) {
                $result['Type'] = 0;
                $result_L = self::GenerateCustomDescriptionEvent($event,$event['DATE_FROM'],$event['DATE_TO']);
                foreach ($result_L as $key => $val) { $result[$key] = $val; }
            }
            //endregion

            //region Переодичное -> N раз
            elseif(isset($rrule['COUNT'])) {
                $result['Type'] = 1;
                $result_L = self::GenerateCustomDescriptionEvent(
                    $event,$date_str.' '.$TIME_START,$date_str.' '.$TIME_END
                );
                foreach ($result_L as $key => $val) { $result[$key] = $val; }
            }
            //endregion

            //region Переодичное -> До конкретной даты
            else {
                $result['Type'] = 2;
                $result_L = self::GenerateCustomDescriptionEvent(
                    $event,$date_str.' '.$TIME_START,$date_str.' '.$TIME_END
                );
                foreach ($result_L as $key => $val) { $result[$key] = $val; }
            }
            //endregion

        //endregion

        $event['CUSTOM_DATA'] = $result;
        return $event;
    }
    private static function GenerateCustomDescriptionEvent($event,$DATE_FROM,$DATE_TO) {
        $result = array();
        $fromTs = \CCalendar::Timestamp($DATE_FROM);
        $toTs = \CCalendar::Timestamp($DATE_TO);
        $skipTime = $event['DT_SKIP_TIME'] == "Y";
        if ($skipTime) { $toTs += \CCalendar::DAY_LENGTH; }
        if (!$skipTime) { $fromTs -= $event['~USER_OFFSET_FROM']; $toTs -= $event['~USER_OFFSET_TO']; }
        $fromTs = date("d.m.Y H:i:s", $fromTs);
        $toTs = date("d.m.Y H:i:s", $toTs);
        $result['When'] = \CCalendar::GetFromToHtml($fromTs, $toTs, $event['DT_SKIP_TIME'] == 'Y', $event['DT_LENGTH']);
        $Periodicity = \CCalendarEvent::GetRRULEDescription($event, true);
        $Periodicity = str_replace('<br>',' ',$Periodicity);
        $result['Periodicity'] = $Periodicity;
        return $result;
    }
}