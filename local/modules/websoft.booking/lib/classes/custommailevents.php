<?php
namespace Websoft\Booking\Classes;
class CustomMailEvents {
    /**
     * @var array структура д/CEventType
     */
    private $eventType = array(
        "EVENT_NAME"    => "",   //ID почтового события.
        "LID"           => "ru", // ID языка.
        "SORT"          => 100,  // Порядок сортировки.
        "NAME"          => "",   // Название.
        "DESCRIPTION"   => "",   // "#ID# - ID баннера\r".            // Текст с макросами типа #поле# - описание.
                                 // "#CONTRACT_ID# - ID контракта\r".
                                 // "#TYPE_SID# - ID типа\r"
    );

    /**
     * Добавление типа события
     * @param $EVENT_ID          - ID почтового события.
     * @param $EVENT_NAME        - ID языка.
     * @param array $DESCRIPTION - Описание, массив с макросами => array('CODE_1'=>'описание 1','CODE_2'=>'описание 2')
     * @param string $LID        - ID языка, по умолчанию ru
     * @param int $SORT          - Порядок сортировки, по умолчанию 100
     */
    public function setEventType($EVENT_ID, $EVENT_NAME, $DESCRIPTION = array(),$LID = 'ru',$SORT = 100) {
        $this->eventType['EVENT_NAME'] = $EVENT_ID;
        $this->eventType['NAME'] = $EVENT_NAME;
        if(empty($DESCRIPTION)) { $this->eventType['DESCRIPTION'] = ""; } else {
            foreach ($DESCRIPTION as $code_desc => $val_desc) {
                $this->eventType['DESCRIPTION'] .= "#{$code_desc}# - {$val_desc}\r";
            }
        }
        $this->eventType['LID'] = $LID;
        $this->eventType['SORT'] = $SORT;
    }

    private $EventTemplates = array();

    /**
     * @param string $BODY_TYPE - Тип сообщения (text|html)
     * @param string $SUBJECT - Тема письма, пример "Изменен статус баннера #ID#"
     * @param string $MESSAGE - Тело сообщения, текст|html, зависит то $BODY_TYPE
     * @param string $EMAIL_TO - Кому, по умолчанию администратор сайта
     * @param string $EMAIL_FROM - От кого, по умолчанию администратор сайта
     * @param string $BCC - Скрытая копия, по умолчанию "#DEFAULT_EMAIL_FROM#"
     */
    public function addEventTemplate($BODY_TYPE,$SUBJECT,$MESSAGE,$EMAIL_TO = "",$EMAIL_FROM = "",$BCC = "") {

        if(!$EMAIL_FROM) { $EMAIL_FROM = "#DEFAULT_EMAIL_FROM#"; }
        if(!$EMAIL_TO) { $EMAIL_TO = "#DEFAULT_EMAIL_FROM#"; }
        //if(!$BCC) { $BCC = "#DEFAULT_EMAIL_FROM#"; }

        $model = array();
        $model["ACTIVE"]      = "Y";
        $model["EVENT_NAME"]  = $this->eventType['EVENT_NAME'];
        $model["LID"]         = array(SITE_ID);
        $model["EMAIL_FROM"]  = $EMAIL_FROM; // От кого
        $model["EMAIL_TO"]    = $EMAIL_TO; // Кому
        $model["BCC"]         = $BCC; // Скрытая копия.
        $model["SUBJECT"]     = $SUBJECT; // Тема письма
        $model["BODY_TYPE"]   = $BODY_TYPE; // Тип сообщения (text|html)
        $model["MESSAGE"]     = $MESSAGE;
        $this->EventTemplates[] = $model;
    }

    private $errors = array();
    public function add() {

        $result = true;

        //region добавление почтового типа
        $obEventType = new \CEventType;
        $find = \CEventType::GetList(array("EVENT_NAME" => $this->eventType['EVENT_NAME']))->Fetch();
        if($find) {
            \CEventType::Update(array("ID" => $find['ID']), array(
                'NAME' => $this->eventType['NAME'],
                'DESCRIPTION' => $this->eventType['DESCRIPTION'],
                'SORT' => $this->eventType['SORT'],
            ));
        } else{if(!$obEventType->Add($this->eventType)){ $result = false; $this->errors[] = $obEventType->LAST_ERROR; }}
        //endregion

        //region Добавление почтовых шаблонов для данного типа
        foreach ($this->EventTemplates as $eventTemplate) {
            $obTemplate = new \CEventMessage;
            $OldTemplateMess = \CEventMessage::GetList( $by="site_id", $order="desc", array(
                'EVENT_NAME'=> $eventTemplate['EVENT_NAME']
            ))->GetNext();
            if($OldTemplateMess) {
                $result = $obTemplate->Update($OldTemplateMess['ID'],$eventTemplate);
                if(!$result) { $this->errors[] = $obTemplate->LAST_ERROR; }
            } else {
                $result = $obTemplate->Add($eventTemplate);
                if(!$result) { $this->errors[] = $obTemplate->LAST_ERROR; }
            }
        }
        //endregion

        return $result;
    }
}